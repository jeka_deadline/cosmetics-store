<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'core' => [
            'class' => 'frontend\modules\core\Module',
        ],
        'user' => [
            'class' => 'frontend\modules\user\Module',
        ],
        'news' => [
            'class' => 'frontend\modules\news\Module',
        ],
        'product' => [
            'class' => 'frontend\modules\product\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\user\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['user/security/login'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/many-glass/views',
                    '@app/modules' => '@app/themes/many-glass/modules',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'core/index/error',
        ],
        'request' => [
            'class' => 'frontend\modules\core\components\FrontRequest',
            'baseUrl' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'core/index/index',
                //'<page_uri:[\w-]+>' => 'core/index/page',
                //'news-category/<uri:[\w-]+>' => '/news/news-category/category',
                //'<module>/<controller>/<action>' => '<module>/<controller>/<action>',
            ],
        ],
        'getUserIdGroup' =>[
            'class' => 'frontend\components\GetUserIdGroup',
        ],

    ],
    'as beforeRequest' =>[
        'class' => 'frontend\components\GetRregionGroup',
    ],
    'params' => $params,
];
