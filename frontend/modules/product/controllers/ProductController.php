<?php
namespace frontend\modules\product\controllers;

use frontend\modules\core\components\FrontController; 

class ProductController extends FrontController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionItem($uri)
    {
        return $this->render('item');
    }

}