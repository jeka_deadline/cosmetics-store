<?php
namespace frontend\modules\product\controllers;

use frontend\modules\core\components\FrontController; 

class ShopcartController extends FrontController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

}