<?php
namespace frontend\modules\product\controllers;

use frontend\modules\core\components\FrontController; 

class CategoryController extends FrontController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCategory($uri)
    {
        return $this->render('category');
    }

}