<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="main-slider">
    <div id="bxslider">
        <?php foreach ($banners as $banner) : ?>
          
            <?= Html::img($banner->getImage(), ['alt' => $banner->alt]); ?>

        <?php endforeach; ?>
    </div>
    <div class="controls container">
        <span class="next"></span>
        <span class="prev"></span>
        <span class="slider-pagination"></span>
    </div>
</div>