<?php
namespace frontend\modules\adv\widgets\BannersWidget;

use yii\base\Widget;
use frontend\modules\adv\models\Bannerplace;

class BannersWidget extends Widget
{

    public $code;
    public $template = 'default';

    public function run()
    {
        if ($this->code) {
            $bannerplace = Bannerplace::find()->where(['=', 'active', '1'])->andWhere(['=', 'code', $this->code])->one();
            if ($bannerplace) {
                $banners = [];
                foreach ($bannerplace->banners as $item) {
                    if ($item->active) {
                        $banners[ $item->id ] = $item;
                    }
                }

                usort($banners, function($a, $b) {
                    if ($a->display_order === $b->display_order) {
                        return 0;
                    }

                    return ($a->display_order < $b->display_order) ? -1 : 1;
                });

                if ($banners) {
                    return $this->render($this->template, ['banners' => $banners]);
                }
            }
        }
    }

}