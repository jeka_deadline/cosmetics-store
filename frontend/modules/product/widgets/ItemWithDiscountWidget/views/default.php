<?php
use yii\helpers\Html;
?>
<h2>Товары со скидкой</h2>
    <div class="slider-container">
        <?php foreach ($items as $item) : ?>

            <div class="item-wrapper">
                <p class="name"><?= $item->title; ?></p>
                <p class="code"><?= $item->articul; ?></p>
                <?php if ($item->preview) : ?>
                    <?= Html::img($item->getPreview()); ?>
                <?php endif; ?>
                <p class="price">
                    <span class="old"><?= $item->price . ' грн.'; ?></span>
                    <span class="new"><?= $item->getPriceWithDiscount() . ' грн.'; ?></span>
                </p>
                <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
            </div>

        <?php endforeach; ?>

    </div>