<?php
namespace frontend\modules\product\widgets\ItemWithDiscountWidget;

use yii;
use yii\base\Widget;
use common\models\price_book\PriceBook;
use common\models\product\Item;

class ItemWithDiscountWidget extends Widget
{
    public $limit     = 10;
    public $template  = 'default';

    public function run()
    {
        $query = Item::find()
                        ->where(['<>', 'discount', 0])
                        ->andWhere(['=', 'active', '1']);
                        
        if ($this->limit) {
            $query->limit($this->limit);
        }
        $items = $query->all();

        $session = Yii::$app->session;
        if($session->has('region_group_id')){
            $group_id = $session->get('region_group_id');
        }
        if($session->has('userid_group_id')){
            $group_id = $session->get('userid_group_id');
        }

        if(!is_null($group_id)){
            foreach ($items as $key=>&$value){
                $item_id =$value['id'];
                $price_book = PriceBook::find()
                    ->where(['group_id'=>$group_id])
                    ->andWhere(['product_id'=>$item_id])
                    ->indexBy('id')
                    ->one();
                if($price_book){
                    $value['price'] = $price_book['price'];
                }
            }
        }
        return $this->render($this->template, ['items' => $items]);
    }
}