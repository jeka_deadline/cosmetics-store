<?php
namespace frontend\modules\product;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'frontend\modules\product\controllers';

    public function init()
    {
        parent::init();
    }

    public function getUrlRules()
    {
        return [
            'product/category'                => '/product/category/index',
            'product/category/index'          => '/product/category/index',
            'product/category/<uri:[\w-]+>'   => '/product/category/category',
            'product'                         => '/product/product/index',
            'product/product'                 => '/product/product/index',
            'product/product/index'           => '/product/product/index',
            'product/product/<uri:[\w-]+>'    => '/product/product/item',
        ];
    }

}