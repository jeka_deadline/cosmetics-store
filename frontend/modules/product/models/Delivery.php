<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_delivery".
 *
 * @property integer $id
 * @property string $title
 * @property integer $display_order
 * @property integer $active
 *
 * @property Item[] $items
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['delivery_id' => 'id']);
    }
}
