<?php
namespace frontend\modules\core\components;

use Yii;
use yii\web\Controller;
use common\models\core\PagesMeta;

class FrontController extends Controller
{

    public $page;
    public $meta;

    public function init()
    {

        parent::init();      
        if (isset(Yii::$app->params[ 'meta' ])) {
            $this->page = Yii::$app->params[ 'page' ];
            $this->meta = Yii::$app->params[ 'meta' ];
        }

    }

    public function setMetaTitle($meta_title = '')
    {
        if ($meta_title) {
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_title]);
        } else {
            if ($this->meta) {
                $this->view->title = $this->meta->meta_title;
            }
        }
    }

    public function setMetaDescription($meta_description = '')
    {
        if ($meta_description) {
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_description]);
        } else {
            if ($this->meta) {
                $this->view->registerMetaTag(['name' => 'description', 'content' => $this->meta->meta_description]);
            }
        }
    }

    public function setMetaKeywords($meta_keywords = '')
    {
        if ($meta_keywords) {
            $this->view->registerMetaTag(['name' => 'keywords', 'content' => $meta_keywords]);
        } else {
            if ($this->meta) {
                $this->view->registerMetaTag(['name' => 'keywords', 'content' => $this->meta->meta_keywords]);
            }
        }
    }

}