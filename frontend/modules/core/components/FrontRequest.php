<?php
namespace frontend\modules\core\components;

use Yii;
use yii\web\Request;
use yii\helpers\Url;
use common\models\core\PagesMeta;
use common\models\core\Page;

class FrontRequest extends Request
{
    
    private $_url;

    /*public function getUrl()
    {
        if ($this->_url === null) {
            $this->_url = $this->resolveRequestUri();
        }

        return $this->_url;
    }*/
    
    protected function resolveRequestUri()
    {
        if ($this->_url === null) {
            $this->_url = parent::resolveRequestUri();
        }

        $request_uri = parse_url($this->_url);
        $request_uri = $request_uri['path'];

        if ($request_uri != '/' && substr($request_uri, 0, 1) == '/')
        {
            // удаляем ведущий слэш
            $request_uri = substr($request_uri, 1);
        }

        if ($request_uri != '/' && substr($request_uri, -1, 1) == '/')
        {
            // удаляем слэш в конце
            $request_uri = substr($request_uri, 0, -1);
        }

        $pageMeta = PagesMeta::find()
                                  ->where(['=', 'request_path', $request_uri])
                                  ->andWhere(['=', 'active', '1'])
                                  ->one();

        if ($pageMeta) {
            $this->_url = $pageMeta->target_path;
            $page = Page::findOne($pageMeta->page_id);
            if ($page) {
                Yii::$app->params[ 'page' ] = $page;
                Yii::$app->params[ 'meta' ] = $pageMeta;
                /*if ($page->module === 'core') {
                    return $pageMeta->target_path;
                }*/
                
            }
            $method = 'getUrlRules';
            $module = Yii::$app->getModule($page->module);
            if (method_exists($module, $method)) {
                $rules = $module->$method();
                Yii::$app->getUrlManager()->addRules($rules, false);
            }
            //throw new \Exception($pageMeta->target_path);
            
        } else {
            //return parent::resolvePathInfo();
        }
        //throw new \Exception($this->_url);
        
        return $this->_url;
    }

}