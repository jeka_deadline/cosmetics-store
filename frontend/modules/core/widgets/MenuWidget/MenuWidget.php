<?php
namespace frontend\modules\core\widgets\MenuWidget;

use yii\base\Widget;
use common\models\core\TypeMenu;

class MenuWidget extends Widget
{

    public $typeMenu;
    public $template = 'top';

    public function run()
    {
        $menus = [];
        $menuPages = TypeMenu::find()->where(['=', 'code', $this->typeMenu])->andWhere(['=', 'active', '1'])->one();
        $pages = $menuPages->linksPages;
        if(!empty($pages)){
            usort($pages, function($a, $b) {
                if ($a[ 'display_order' ] === $b[ 'display_order' ]) {
                    return 0;
                } else {
                    return ($a[ 'display_order' ] < $b[ 'display_order' ]) ? -1 : 1;
                }
            });
            foreach ($pages as $page) {
                if ($page->active) {
                    $pos = strpos($page->uri, '/');
                    $menus[ $page->id ] = [
                        'url'   => ($pos === 0) ? $page->uri : '/' . $page->uri,
                        'title' => $page->header,
                    ];
                }
            }
        }



        return $this->render($this->template, ['menu' => $menus]);
    }

}