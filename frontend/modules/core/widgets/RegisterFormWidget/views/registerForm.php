<?php
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="registration popup">
    <h4>Регистрация <span class="close"></span></h4>
    <p class="registration-through-socials">

        <span class="reg-soc">Регистрация с помощью соцсетей</span>

        <?php $authAuthChoice = AuthChoice::begin([
            'baseAuthUrl' => ['/user/security/auth'],
            'autoRender' => FALSE,
        ]); ?>

        <?php foreach ($authAuthChoice->getClients() as $client): ?>
            <?php
            $classForLink = '';
            $text         = '';
            switch ($client->getName()) {
                case 'facebook':
                    $classForLink = 'social-links facebook auth-link';
                    $text = 'Использовать аккаунт Facebook';
                    break;
                case 'vkontakte':
                    $classForLink = 'social-links vk auth-link';
                    $text = 'Использовать аккаунт Вконтакте';
                    break;
                case 'google':
                    $classForLink = 'social-links google auth-link';
                    $text = 'Использовать аккаунт Google';
                    break;
            }
            ?>

            <?= Html::a($text,array_shift($authAuthChoice->getBaseAuthUrl()).'?authclient=' . $client->getName(), ['class' => $classForLink]) ;?>
        <?php endforeach; ?>
        <?php AuthChoice::end(); ?>
    </p>
    <div class="registration-through-login">
        <span class="bold">Обычная регистрация</span>
        <?php $form = yii\widgets\ActiveForm::begin(['id'=>'registerForm',
                                                    'enableAjaxValidation'=>true,
                                                    
                                                    'validationUrl'=>Url::toRoute('index/create-validation'),]);
        ?>
            <fieldset>
                <?= $form->field($registerForm, 'username') ?>
            </fieldset>
            <fieldset>
                <?= $form->field($registerForm, 'email') ?>
            </fieldset>
            <fieldset>
                <?= $form->field($registerForm, 'password')->passwordInput() ?>
            </fieldset>
            <fieldset>
                <?= $form->field($registerForm, 'password_repeat')->passwordInput() ?>
            </fieldset>


        <?= Html::submitButton('Зарегистрироваться', ['class' => 'submit', 'id'=>"register"]) ?>
        <?= $form->errorSummary($registerForm)?>
        <?php yii\widgets\ActiveForm::end(); ?>



       <!-- <form action="#">
            <fieldset>
                <label for="login-registration">Имя:</label>
                <input id="login-registration" type="text">
            </fieldset>
            <fieldset>
                <label for="email-registration">Email:</label>
                <input id="email-registration" type="text">
            </fieldset>
            <fieldset>
                <label for="password-registration">Пароль:</label>
                <input id="password-registration" type="password">
            </fieldset>
            <fieldset>
                <label for="password-confirmation-registration">Подтвердить пароль:</label>
                <input id="password-confirmation-registration" type="password">
            </fieldset>
            <button type="submit">Зарегистрироваться!!!!</button>
        </form>-->
    </div>
</div>