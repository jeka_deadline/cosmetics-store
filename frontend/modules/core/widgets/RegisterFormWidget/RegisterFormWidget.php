<?php
namespace frontend\modules\core\widgets\RegisterFormWidget;

use frontend\modules\core\models\SignupForm;
use frontend\modules\user\models\CreateUserForm;
use yii\base\Widget;


class RegisterFormWidget extends Widget
{

    public $template = 'registerForm';

    public function run()
    {
        $registerForm = new CreateUserForm;
        return $this->render($this->template, ['registerForm' => $registerForm]);
    }

}