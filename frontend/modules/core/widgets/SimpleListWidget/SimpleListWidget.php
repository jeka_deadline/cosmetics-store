<?php
namespace frontend\modules\core\widgets\SimpleListWidget;

use yii\base\Widget;

class SimpleListWidget extends Widget
{

    public $items     = [];
    public $template  = 'default';

    public function run()
    {
        return $this->render($this->template, ['items' => $items]);
    }

}