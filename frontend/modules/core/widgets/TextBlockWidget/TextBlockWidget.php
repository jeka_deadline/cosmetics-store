<?php
namespace frontend\modules\core\widgets\TextBlockWidget;

use yii\base\Widget;
use common\models\core\TextBlock;

class TextBlockWidget extends Widget
{

    public $uri;

    public function run()
    {
        $data = TextBlock::find()
                              ->where(['=', 'active', '1'])
                              ->andWhere(['=', 'uri', $this->uri])
                              ->one();

        if ($data) {
            echo $data->content;
        }
    }

}