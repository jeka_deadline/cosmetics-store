<?php
namespace frontend\modules\core\widgets\LoginFormWidget;

use frontend\modules\user\models\LoginForm;
use yii\base\Widget;
use common\models\core\TypeMenu;

class LoginFormWidget extends Widget
{

    public $template = 'loginForm';

    public function run()
    {
       $loginForm = new LoginForm;
       $loginForm->rememberMe =true;
       return $this->render($this->template, ['loginForm' => $loginForm]);
    }

}