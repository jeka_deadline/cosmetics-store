<?php
use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
?>

<div class="login popup">
    <h4>Вход <span class="close"></span></h4>
    <p class="enter-through-socials">
        Вход с помощью соцсетей

        <?php $authAuthChoice = AuthChoice::begin([
            'baseAuthUrl' => ['/user/security/auth'],
            'autoRender' => FALSE,
        ]); ?>

        <?php foreach ($authAuthChoice->getClients() as $client): ?>
            <?php
            $classForLink = '';
            $text         = '';
            switch ($client->getName()) {
                case 'facebook':
                    $classForLink = 'social-links facebook auth-link';
                    $text = 'Использовать аккаунт Facebook';
                    break;
                case 'vkontakte':
                    $classForLink = 'social-links vk auth-link';
                    $text = 'Использовать аккаунт Вконтакте';
                    break;
                case 'google':
                    $classForLink = 'social-links google auth-link';
                    $text = 'Использовать аккаунт Google';
                    break;
            }
            ?>

            <?= Html::a($text,array_shift($authAuthChoice->getBaseAuthUrl()).'?authclient=' . $client->getName(), ['class' => $classForLink]) ;?>
        <?php endforeach; ?>
        <?php AuthChoice::end(); ?>

    </p>
    <div class="enter-through-login">
        Вход через логин и пароль
        <?= Html::beginForm([], '', ['id'=>'loginForm']) ?>
            <fieldset>
                <?= Html::activeLabel($loginForm, 'username')?>
                <?= Html::activeInput('text', $loginForm, 'username', ['id' => 'login-login']) ?>
            </fieldset>
            <fieldset>
                <?= Html::activeLabel($loginForm, 'password')?>
                <?= Html::activeInput('password', $loginForm, 'password', ['id' => 'password-login']) ?>
            </fieldset>
            <fieldset class="chekbox">
                <?= Html::activeCheckbox($loginForm, 'rememberMe', ['id' => 'remember-me-login'])?>
            </fieldset>
            <div class="error" style="display: none"></div>
             <?= Html::a('Напомнить пароль', ['#'], ['class' => 'remind-me-password']) ?>
             <?= Html::submitButton('Войти', ['class' => 'submit', 'id'=>"login"]) ?>
        <?= Html::endForm() ?>
       <!-- <form action="#">
            <fieldset>
                <label for="login-login">Лолгин</label>
                <input id="login-login" type="text">
            </fieldset>
            <fieldset>
                <label for="password-login">Пароль</label>
                <input id="password-login" type="password">
            </fieldset>
            <fieldset>
                <input type="checkbox" checked id="remeber-me-login">
                <label for="remeber-me-login">Запонить меня</label>
            </fieldset>
            <a href="#" class="remind-me-password">Напомнить пароль</a>
            <button type="submit">Войти!!!</button>
        </form>-->
    </div>
</div>