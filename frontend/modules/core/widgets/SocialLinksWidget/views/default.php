<h4>Мы в соцсетях</h4>
<ul class="soc">
    <?php foreach ($socialLinks as $link) : ?>
        <li><a class="<?= $link->class; ?>" href="<?= $link->url; ?>"><?= $link->title; ?></a></li>
    <?php endforeach; ?>
</ul>