<?php
namespace frontend\modules\core\widgets\SocialLinksWidget;

use yii\base\Widget;
use frontend\modules\core\models\SocialLinks;

class SocialLinksWidget extends Widget 
{

    public $template = 'default';

    public function run()
    {
        $socialLinks = SocialLinks::find()
                                      ->where(['=', 'active', '1'])
                                      ->orderBy(['display_order' => SORT_ASC])
                                      ->all();

        return $this->render($this->template, ['socialLinks' => $socialLinks]);
    }

}