<?php
namespace frontend\modules\core\controllers;


use frontend\modules\user\models\CreateUserForm;
use Yii;
//use common\models\LoginForm;
use frontend\modules\core\models\PasswordResetRequestForm;
use frontend\modules\core\models\ResetPasswordForm;
use frontend\modules\core\models\SignupForm;
use frontend\modules\user\models\LoginForm;
use frontend\modules\core\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\modules\core\components\FrontController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\core\Page;
use yii\web\Response;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

class IndexController extends FrontController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        // установка meta-данных для страницы
        $this->setMetaTitle();
        $this->setMetaDescription();
        $this->setMetaKeywords();
        return $this->render('index');
    }

    public function actionPage()
    {
        // установка meta-данных для страницы
        $this->setMetaTitle();
        $this->setMetaDescription();
        $this->setMetaKeywords();
        return $this->render('page', ['content' => $this->page->content]);
    }

    public function about()
    {

    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        };
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $name = Yii::$app->user->identity->username;
            Yii::$app->response->format='json';


            //получаем группу пользователя по ид
            $user_id = Yii::$app->user->id;
            $group_id= Yii::$app->getUserIdGroup->getId($user_id);
            $session = Yii::$app->session;
            $session->open();
            $session->set('userid_group_id', $group_id);

            $curentUrl = Yii::$app->controller->route;
            $json_data = ['login'=>'true', 'name'=> $name, 'url' =>$curentUrl];
            //return $this->redirect($curentUrl,302);
            //return Yii::$app->getResponse()->redirect($curentBaseUrl);
            return json_encode($json_data);
        } else {
            Yii::$app->response->format='json';
            $json_data = ['login'=>'Неверный логин или пароль'];
            return json_encode($json_data);
        };
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        Yii::$app->response->format='json';
        $json_data = ['logout'=>'true'];
        return json_encode($json_data);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        
        $model = new CreateUserForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->create()) {
                if (Yii::$app->getUser()->login($user)) {
                    //ToDo получить группу пользователя

                    $data = ['registration'=> 'true', 'name'=>$user->username];
                    $response->data =$data;
                    return $response;
                }
                else{
                    $data = ['registration'=> 'false'];
                    $response->data =$data;
                    return $response;
                }
            }
        }
        return $this->goHome();

    }

    public function actionCreateValidation(){
        $model = new CreateUserForm();
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            $model->validate();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);

        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    

}
