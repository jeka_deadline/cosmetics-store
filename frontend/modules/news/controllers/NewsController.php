<?php
namespace frontend\modules\news\controllers;

use yii\web\Controller;

class NewsController extends Controller
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionItem($uri)
    {
        return $this->render('item');
    }

}