<?php
namespace frontend\modules\news\controllers;

use frontend\modules\core\components\FrontController; 
use common\models\news\Category;

class CategoryController extends FrontController
{

    public function actionIndex()
    {
        $categories = Category::find()
                                    ->where(['=', 'active', '1'])
                                    ->orderBy(['display_order' => SORT_ASC])
                                    ->all();
        $this->setMetaTitle();
        $this->setMetaDescription();
        $this->setMetaKeywords();

        return $this->render('index', ['categories' => $categories]);
    }

    public function actionCategory($uri)
    {
        return $this->render('category');
    }

}