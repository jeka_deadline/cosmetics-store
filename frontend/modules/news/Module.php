<?php
namespace frontend\modules\news;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'frontend\modules\news\controllers';

    public function init()
    {
        parent::init();
    }

    public function getUrlRules()
    {
        return [
            'news/category'               => '/news/category/index',
            'news/category/index'         => '/news/category/index',
            'news/category/<uri:[\w-]+>'  => '/news/category/category',
            'news'                        => '/news/news/index',
            'news/news'                   => '/news/news/index',
            'news/news/index'             => '/news/news/index',
            'news/news/<uri:[\w-]+>'      => '/news/news/item',
        ];
    }

}