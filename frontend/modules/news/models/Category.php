<?php

namespace common\models\news;

use Yii;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $display_order
 * @property integer $active
 *
 * @property LinksNewsCategory[] $linksNewsCategories
 */
class Category extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'news_category';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string'],
            [['display_order', 'active'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getLinksNewsCategories()
    {
        return $this->hasMany(LinksNewsCategory::className(), ['category_id' => 'id']);
    }
}
