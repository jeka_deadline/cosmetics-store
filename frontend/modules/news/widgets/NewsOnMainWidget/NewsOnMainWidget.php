<?php
namespace frontend\modules\news\widgets\NewsOnMainWidget;

use yii\base\Widget;
use frontend\modules\news\models\News;

class NewsOnMainWidget extends Widget
{

    public $limit = 3;
    public $order = '';
    public $offset;
    public $template = 'default';

    public function run()
    {
        $query = News::find()->where(['=', 'active', '1'])->andWhere(['<=', 'date', time()]);
        if ($this->limit) {
            $query->limit($this->limit);
        }
        if ($this->offset) {
            $query->offset($this->offset);
        }
        if (!empty($this->order)) {
            $query->orderBy($this->order);
        } else {
            $query->orderBy(['date' => SORT_DESC]);
        }
        $news = $query->all();
        return $this->render($this->template, ['news' => $news]);
    }

}