<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\DateConverter;
?>

<div class="col-md-6 news">
    <h2 class="news-header">Новости</h2>

    <?php foreach ($news as $itemNews) : ?>

        <div class="single-news">

            <?php if ($itemNews->image) : ?>
                <?= Html::img($itemNews->getImage()); ?>
            <?php endif; ?>

            <h3><?= $itemNews->title; ?></h3>
            <div class="date"><?= DateConverter::timestampToDate($itemNews->date, 'd.m.Y'); ?></div>
            <p class="text"><?= $itemNews->small_description; ?></p>
            <a href="#" class="read-more">Читать далее</a>
        </div>

    <?php endforeach; ?>
</div>