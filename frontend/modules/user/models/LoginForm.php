<?php
namespace frontend\modules\user\models;

use Yii;
use yii\base\Model;
use common\models\user\User;
use common\models\user\AccessAdmin;
use yii\helpers\ArrayHelper;

class LoginForm extends Model
{

	public $username;
	public $password;
	public $rememberMe;
	private $user = FALSE;

	public function rules()
	{
		return [
			[['username', 'password'], 'required'],
			[['password'], 'validatePassword'],
			[['rememberMe'], 'boolean'],
			//[['username'], 'filter', 'trim'],
		];
	}

	public function attributeLabels()
	{
		return [
			'username' => 'Логин',
			'password' => 'Пароль',
			'rememberMe' => 'Запонить меня',
				];
	}

		public function validatePassword()
		{
				$user = $this->getUser();
				if ($user) {
						if (!Yii::$app->getSecurity()->validatePassword($this->password, $user->password_hash)) {
								$this->addError('username', 'Access danied');
						}
				} else {
						$this->addError('username', 'Access danied');
				}
		}

	public function login()
	{
			if ($this->validate()) {
					$user = User::findByUsername($this->username);
					Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
					return TRUE;
			}
			return FALSE;
	}

	private function getUser()
	{
		if ($this->user === FALSE) {
			$this->user = User::findByUsername($this->username);
		}
		return $this->user;
	}

}