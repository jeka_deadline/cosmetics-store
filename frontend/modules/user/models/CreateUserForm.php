<?php
namespace frontend\modules\user\models;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;



class CreateUserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $confirmEmail = FALSE;
    public $roleId = 2; //user

    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required', 'message' =>'Имя не может быть пустым'],
            ['username', 'unique', 'targetClass' => '\common\models\user\User', 'message' => 'Данное имя уже занято'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' =>'Email не может быть пустым'],
            ['email', 'email', 'message' =>'Не корректный Email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\user\User', 'message' => 'Данный email уже занят'],

            ['password', 'string', 'min' => 6, 'tooShort' =>'Пароль не может быть меньше 6 символов'],
            ['password', 'required', 'message' => 'Пароль не может быть пустым'],


            ['password_repeat', 'required', 'message' =>'Пароль не может быть пустым' ],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'operator' => '===', 'message' => 'Пароль не совпадает'],




        //    ['username', 'match', 'pattern' => '/[A-Za-z0-9_-]{4,50}/'],
       //     ['username', 'unique', 'targetClass' => User::className()],
         //   ['email', 'unique', 'targetClass' => User::className()],
        ];

    }
    public function attributeLabels()
    {
        return [
            'username' => 'Имя',
            'email' =>'Email',
            'password' => 'Пароль',
            'password_repeat' =>'Подтвердить пароль:',
            
        ];
    }

   

    public function getIp(){
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }


    public function create()
    {
        if ($this->validate()) {
            $user                   = Yii::createObject(User::className());
            
            $user->email            = $this->email;
            $user->username         = $this->username;
            $user->role_id          = $this->roleId;
            $user->password_hash    = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $user->register_ip      = $this->getIp();
            $user->auth_key         = time() . '_' . Yii::$app->getSecurity()->generateRandomString(21);
            $user->confirm_email_at = ($this->confirmEmail) ? time() : NULL;
            $res = $user->save(); //нет атрибутов create_at и update_at
            return $user;
        }
        return FALSE;
    }

}