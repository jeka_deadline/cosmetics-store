<?php
namespace frontend\modules\user\models;

use common\models\user\Social;
use Yii;
use yii\helpers\ArrayHelper;
use frontend\modules\user\models\CreateUserForm;

class User extends \common\models\user\User
{

	public function unblock()
	{
      return (bool)$this->updateAttributes(['blocked_at' => NULL]);
	}

	public function block()
	{
      return (bool)$this->updateAttributes(['blocked_at' => time()]);
	}

    public function generateUsername($username = '')
    {
        if (!empty($username)) {
            return $username . rand(0, 9) . rand(0, 9);
        } else {
            return 'user' . time();
        }
    }

    public static function createSocial($client){
        $attributes=$client->getUserAttributes();
        $email=ArrayHelper::getValue($attributes, 'email');
        $id=ArrayHelper::getValue($attributes, 'id');

        $user=new User();
        $userData = explode('@', $email);
        $username = mb_strtolower($userData[0]);
        $tmpName  = $username;
        while($user->findByUsername($tmpName)) {
            $tmpName = $user->generateUsername($username);
        }
        $user->username             = $tmpName;
        $user->confirm_email_at = time();
        $user->email            = $email;
        $user->login_with_social  = 1;
        $user->role_id            = 2;
        $user->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString(20));
        $user->register_ip      = CreateUserForm::getIp();
        $user->auth_key         = time() . '_' . Yii::$app->getSecurity()->generateRandomString(21);
        if ($user->validate() && $user->save()) {
            $social             = new Social();
            $social->provider   = $client->getId();
            $social->client_id  = (string)$id;
            $social->created_at = time();
            $user->link('socials', $social);
            $profile=new Profile();
            $profile->surname=ArrayHelper::getValue($attributes, 'last_name');
            $profile->name=ArrayHelper::getValue($attributes, 'first_name');
            $profile->date_birth=strtotime(ArrayHelper::getValue($attributes, 'bdate'));
            $sex=[0=>'n',1=>'w',2=>'m',3=>'n'];
            $profile->sex=$sex[ArrayHelper::getValue($attributes, 'sex')];
            $user->link('profile', $profile);
        }
        else
            return false;
        return $user;
    }
}