<?php
namespace frontend\modules\user\controllers;

use common\models\user\Social;
use frontend\modules\user\models\Profile;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\modules\user\models\LoginForm;
use frontend\modules\user\models\User;

class SecurityController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'index','auth'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
                'successUrl'=>Url::to(['/']),
//                'cancelUrl'=>Url::to(['/test2']),
            ],
        ];
    }

    public function successCallback($client)
    {
        if (Yii::$app->user->isGuest) {
            $attributes=$client->getUserAttributes();
            $email=ArrayHelper::getValue($attributes, 'email');

            if (!empty($email))
                $user=User::findByEmail($email);
            else
               return false;

            if (!$user) {
                $user=User::createSocial($client);
                if (!$user) return false;
            }

            if (Yii::$app->getUser()->login($user)) {
                return true;
            }
            else{
                return false;
            }
        }
        return true;
    }

    // Вход в админ панель
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->request->isAjax) {
                echo 'ok';
                exit;
            }
            return $this->goBack();
        }
        return (Yii::$app->request->isAjax) ? $this->renderPartial('login', ['model' => $model]) : $this->render('login', ['model' => $model]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}