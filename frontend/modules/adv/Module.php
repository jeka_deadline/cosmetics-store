<?php
namespace frontend\modules\adv;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'frontend/modules/adv/controllers';

    public function init()
    {
        parent::init();
    }

}