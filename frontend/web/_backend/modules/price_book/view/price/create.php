<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\price_book\PriceBook */

$this->title = 'Create Price Book';
$this->params['breadcrumbs'][] = ['label' => 'Price Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-book-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
