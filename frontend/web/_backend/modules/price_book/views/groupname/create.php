<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\price_book\models\GroupName */

$this->title = 'Create Group Name';
$this->params['breadcrumbs'][] = ['label' => 'Group Names', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-name-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
