<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\price_book\models\RegionGroupName */

$this->title = 'Update Region Group Name: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Region Group Names', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="region-group-name-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
