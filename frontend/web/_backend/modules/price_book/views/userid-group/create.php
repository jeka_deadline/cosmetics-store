<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\price_book\models\UseridGroup */

$this->title = 'Create Userid Group';
$this->params['breadcrumbs'][] = ['label' => 'Userid Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userid-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
