<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price_book\models\UseridGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Userid Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userid-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Userid Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'group_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
