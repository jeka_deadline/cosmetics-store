<?php
namespace frontend\components;

use common\models\price_book\RegionGroup;
use yii;
use yii\base\Behavior;

class GetRregionGroup extends Behavior
{
    public function events()
    {
        return [
            \yii\web\Application::EVENT_BEFORE_REQUEST => 'getRegionGroup',
        ];
    }

    public function getRegionGroup()
    {
        //проверяем назначена ли пользователю группа
        $session = Yii::$app->session;
        $session->open();
        if(empty($session['region_group_id']) ){

            //проверяем действительно ли запрос к какому либо модулю
            $url =Yii::$app->request->getUrl();
            $request_module = explode('/', $url)[0];
            $modules = include (\Yii::getAlias('@frontend/config/main.php'));
            if (array_key_exists($request_module, $modules['modules'])) {

                $ip = Yii::$app->request->getUserIP();
                $ip = ($ip == '127.0.0.1') ? '31.129.115.107' : $ip;

                //внешний сервис 10000 запросов в час
                //@return регион
                $url = "http://www.freegeoip.net/json/" . $ip;
                $is_bot = preg_match("~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i", $_SERVER['HTTP_USER_AGENT']);
                $response = !$is_bot ? json_decode(file_get_contents($url)) : [];
                $region = $response->region_name;

                //ищем в базе id региона
                $region = RegionGroup::find()
                    ->where(['region' => $region])
                    ->one();
                if ($region) {
                    $region_id = $region['group_id'];
                } else {
                    $region_id = null;
                }

                $session->set('region_group_id', $region_id);
            }
        }

    }
}