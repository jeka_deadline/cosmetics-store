<?php
use frontend\modules\core\widgets\SocialLinksWidget\SocialLinksWidget;
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <h4>Информация</h4>
                <ul>
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Доставка</a></li>
                    <li><a href="#">Оплата</a></li>
                    <li><a href="#">Обмен и возврат</a></li>
                    <li><a href="#">Гарантии</a></li>
                    <li><a href="#">Оптовым поупателям</a></li>
                </ul>
            </div>
            <div class="col-xs-6">
                <h4>Категории товаров</h4>
                <ul>
                    <li><a href="#">Для собак</a></li>
                    <li><a href="#">Для кошек</a></li>
                    <li><a href="#">Аквариумистика</a></li>
                    <li><a href="#">Спорт и охота</a></li>
                    <li><a href="#">Заводчикам</a></li>
                    <li><a href="#">Для выставок</a></li>
                    <li><a href="#">Для путешествий</a></li>
                    <li><a href="#">Другие животные</a></li>
                </ul>
            </div>
            <div class="col-xs-6">
                <h4>Прочие ссылки</h4>
                <ul>
                    <li><a href="#">Наш другой сайт</a></li>
                    <li><a href="#">Наши партнёры</a></li>
                    <li><a href="#">Спосоры</a></li>
                    <li><a href="#">Ещё один наш сайт</a></li>
                    <li><a href="#">Сайт зоомагазина</a></li>
                    <li><a href="#">Интернет-магазин</a></li>
                    <li><a href="#">Наши филиалы</a></li>
                    <li><a href="#">Другие компании</a></li>
                </ul>
            </div>
            <div class="col-xs-6">
                <?= SocialLinksWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="footer-bottom-line">
        <div class="container">
            <p>&copy; 2013 СуперZoo. Интернет-магазин товаров для животных. Все права защищены.</p>
        </div>
    </div>
</footer>