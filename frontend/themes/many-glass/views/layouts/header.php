<?php

use frontend\modules\core\widgets\LoginFormWidget\LoginFormWidget;
use frontend\modules\core\widgets\RegisterFormWidget\RegisterFormWidget;
use frontend\modules\core\widgets\MenuWidget\MenuWidget;
use yii\helpers\Html;
?>
<?php
$user ='';
$data='';
if(\Yii::$app->user->isGuest){
    $user ='Вход';
    $data='login';
}
else{
    $data ='logout';
    $user = 'Выход('. \Yii::$app->user->identity->username . ')';
}
?>

<header>
    <div class="top-line">
        <div class="container">
            <div class="left-side">

                <?= MenuWidget::widget(['typeMenu' => 'top']); ?>
                
            </div>
            <div class="right-side">
                <ul class="social">
                    <li><a href="#"><span></span></a></li>
                    <li><a href="#"><span></span></a></li>
                </ul>
                <ul class="autorization">
                    <li class="login-link">
                        <a href="#" data="<?= $data ?>" id="autorization-link"><?= $user ?></a>

                        <?= LoginFormWidget::widget(); ?>
                        
                        <div class="success popup">
                            <h4>Успешно! <span class="close"></span></h4>
                            <p class="message">На указаный вами адрес было отправленно письмо с вашим паролем.
                                Проверте вашу почту!</p>
                            <button>Спасибо</button>
                        </div>
                        <div class="popup password-reminder">
                            <h4>Востановление пароля <span class="close"></span></h4>
                            <p class="message">Напоминание забытого пароля</p>
                            <form action="#">
                                <fieldset>
                                <label for="email-password-reminder">Email:</label>
                                <input id="email-password-reminder" type="text">
                                </fieldset>
                                <fieldset>
                                    <label for="captcha-password-reminder">Введите код</label>
                                    <input id="captcha-password-reminder" type="text">
                                </fieldset>
                                <div class="captcha">
                                    <img src="<?= $bundle->baseUrl . '/img/icons/captca.png'; ?>" alt="">
                                </div>
                                <button type="submit">Подтвердить</button>
                            </form>
                        </div>
                    </li>
                    <li class="login-link">
                        <a href="#" id="registration-link">Регистрация</a>
                   
                        <?= RegisterFormWidget::widget(); ?>
                        
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="head">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 logo">
                    <img src="<?= $bundle->baseUrl . '/img/icons/logo.png'; ?>" class="img-responsive" alt="">
                </div>
                <div class="col-xs-10 top-search">
                    <form action="#">
                        <input type="search" placeholder="Поиск по сайту">
                        <button type="submit"><span class="seach-icon"></span></button>
                    </form>
                    <div class="tel">
                        <span class="tel-big-red">тел.(0462)123-456</span>
                        <span class="back-call">
                            <a href="#" id="call-me-form-open">Заказать обратный звонок</a>
                        </span>
                    </div>
                    <div class="call popup">
                        <h4>Заказать обратный звонок <span class="close"></span></h4>
                        <form action="#">
                            <fieldset>
                                <label for="phone-number-call">Ваш номер телефона:</label>
                                <input id="phone-number-call" type="text" placeholder="+380">
                            </fieldset>
                            <fieldset>
                                <label for="name-call"><span class="bold">Ваше имя:</span></label>
                                <input id="name-call" type="text">
                            </fieldset>
                            <fieldset class="time">
                                <label for="call-me-at-h">Перезвонить в:</label>
                                <input id="call-me-at-h" type="number" placeholder="15">
                                <label for="call-me-at-m">:</label>
                                <input id="call-me-at-m" type="number" placeholder="30">
                                <input type="checkbox" checked id="remember-me-login">
                                <label for="remember-me-login">Не важно</label>
                            </fieldset>
                            <button type="submit">Оставить заявку</button>
                        </form>
                    </div>
                </div>
                <div class="col-xs-8 basket">
                    <div class="basket-wrapper">
                        <span class="basket-label">Корзина</span>
                        <span class="order">Вы заказали <span class="count">0</span> товаров</span>
                        <a href="#" class="look-basket">Посмотреть</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="main-nav">
    <div class="container">
        <ul class="main-menu for-mobile">
            <li>
                <a href="#" class="menu-button">Меню</a>
                <div class="menu-mobile">
                    <h4>Меню <span class="close"></span></h4>
                    <ul>
                        <li><a href="#">Ортопедические средства</a></li>
                        <li><a href="#">Обувь</a></li>
                        <li><a href="#">Мастэктомия</a></li>
                        <li><a href="#">Технические средства</a></li>
                        <li><a href="#">Протезирование</a></li>
                    </ul>
                </div>
            </li>
            <li><a href="#" class="filter-link">Настроить фильтр</a></li>
        </ul>
        <ul class="main-menu for-desktop">
            <li>
                <a href="#">Дизайнерскике</a>
                <div class="drop-down-menu">
                    <div class="left-side">
                        <h4>Товары</h4>
                        <ul>
                            <li><a href="#">Аквариумы</a></li>
                            <li><a href="#">Оборудование для аквариумов</a></li>
                        </ul>
                        <h4>Корм</h4>
                        <ul>
                            <li><a href="#">Корм для рыб</a></li>
                        </ul>
                    </div>
                    <div class="right-side">
                        <h4>Уход и аксессуары</h4>
                        <ul>
                            <li><a href="#">Аксессуары для аквариума</a></li>
                            <li><a href="#">Аквариумная химия</a></li>
                            <li><a href="#">Грунт и наполнение аквариума</a></li>
                            <li><a href="#">Уход за аквариумом</a></li>
                        </ul>
                    </div>
                </div>
            </li>
            <li><a href="#">Спортивные</a></li>
            <li><a href="#">Для водителей</a></li>
            <li><a href="#">Копьютерные</a></li>
            <li><a href="#">Оправы Kids</a></li>
            <li><a href="#">Очки с диоптриями</a></li>
        </ul>
    </div>
</nav>