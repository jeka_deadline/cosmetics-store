<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\ManyGlassAsset;
use common\widgets\Alert;

$bundle = ManyGlassAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=750">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $this->render('header', ['bundle' => $bundle]); ?>

    <?= $content; ?>
    
<?= $this->render('footer'); ?>

<div class="filter">
    <h4>Фильтры <span class="close"></span></h4>
    <p class="current-filtered-price">
        Цена: от <span class="from"></span> до <span class="till"></span> грн.
    </p>
    <div class="slider-range"></div>
    <h6>Форма лица</h6>
    <div class="face-shape">
        <div class="select-item">
            <a href="#">Острое</a>
        </div>
        <div class="select-item">
            <a href="#">Овальное </a>
        </div>
        <div class="select-item">
            <a href="#">Круглое</a>
        </div>
        <div class="select-item">
            <a href="#">Квадратное</a>
        </div>
    </div>
    <h6>Форма оправы</h6>
    <div class="frame-shape">
        <div class="select-item"><a href="#">Авиаторы</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Квадратные</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Тонкие</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Круглые</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Вайфарер</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Клуб-мастер</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Кошачий глаз</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Овальные</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Бабочки</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Римы</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Полу-римы</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Сплошные</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Щит</a><input type="checkbox"></div>
        <div class="select-item"><a href="#">Гугл</a><input type="checkbox"></div>
        <div class="empty"></div>
    </div>
    <h6>Цвета оправы</h6>
    <div class="shape-color">
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
    </div>
    <h6>Цвета линз</h6>
    <div class="glass-color">
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
        <div class="select-item"></div>
    </div>
    <h6>Материал оправы</h6>
    <div class="frame-material">
        <fieldset>
            <input type="checkbox" id="metal">
            <label for="metal">Металлическая оправа</label>
        </fieldset>
        <fieldset>
            <input type="checkbox" id="plastic">
            <label for="plastic">Пластиковая оправа</label>
        </fieldset>
        <fieldset>
            <input type="checkbox" id="wood">
            <label for="wood">Деревянная оправа</label>
        </fieldset>
    </div>
    <div class="filter-control">
        <button class="filter-apply">Применить</button>
        <button class="filter-reject">Отмена</button>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
