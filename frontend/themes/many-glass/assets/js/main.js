jQuery(function($) {
  var filter, passwordReminder, passwordReminderSuccess;
  $('#bxslider').bxSlider({
    controls: true,
    nextSelector: '.controls .next',
    prevSelector: '.controls .prev',
    nextText: '<img src="./img/icons/slider-button-right.png" alt="">',
    prevText: '<img src="./img/icons/slider-button-left.png" alt="">'
  });
  $('.slider-container').eq(0).bxSlider({
    controls: true,
    pager: false,
    maxSlides: 3,
    minSlides: 3,
    slideWidth: 225,
    slideHeight: 400,
    infiniteLoop: false,
    responsive: false,
    nextText: '<span class="next"></span>',
    prevText: '<span class="prev"></span>',
    hideControlOnEnd: true,
    slideMargin: 15
  });
  $('.slider-container').eq(1).bxSlider({
    controls: true,
    pager: false,
    maxSlides: 3,
    minSlides: 3,
    slideWidth: 225,
    slideHeight: 400,
    infiniteLoop: false,
    responsive: false,
    nextText: '<span class="next"></span>',
    prevText: '<span class="prev"></span>',
    hideControlOnEnd: true,
    slideMargin: 15
  });
  $('.main-menu.for-mobile a.menu-button').on('click', function(e) {
    var background, menu;
    e.preventDefault();
    menu = $(this).parent().find('.menu-mobile').clone().css('display', 'block');
    background = $('<div>').css({
      'background': 'rgba(0,0,0, 0.8)',
      'width': '100%',
      'height': '100%',
      'position': 'fixed',
      'z-index': '10000',
      'top': '0'
    }).attr('id', 'popup-background').appendTo('body').append($(menu));
    return $('.menu-mobile .close, #popup-background').on('click', function(e) {
      if (e.target === $('.menu-mobile .close').get(1) || e.target === $('#popup-background').get(0)) {
        return $('#popup-background').remove();
      }
    });
  });
  filter = $('.filter').remove().css('display', 'block');
  $('.main-menu.for-mobile a.filter-link').on('click', function(e) {
    var background;
    e.preventDefault();
    background = $('<div>').css({
      'background': 'rgba(0,0,0, 0.8)',
      'width': '100%',
      'height': $(document).outerHeight() + "px",
      'position': 'absolute',
      'z-index': '10000',
      'top': '0'
    }).attr('id', 'popup-background').appendTo('body').append($(filter));
    $('.filter .close, #popup-background').on('click', function(e) {
      if (e.target === $('.filter .close').get(0) || e.target === $('#popup-background').get(0)) {
        return $('#popup-background').remove();
      }
    });
    $(".slider-range").slider({
      range: true,
      values: [0, 1215],
      min: 0,
      max: 5000,
      slide: function(e, ui) {
        $(".current-filtered-price .from").text(ui.values[0]);
        return $(".current-filtered-price .till").text(ui.values[1]);
      }
    });
    $(".current-filtered-price .from").text($(".slider-range").slider("values", 0));
    $(".current-filtered-price .till").text($(".slider-range").slider("values", 1));
    $('.filter .shape-color, .filter .glass-color').bind('mousedown', function(e) {
      return e.metaKey = true;
    }).selectable();
    return $('.filter a').on('click', function(e) {
      return e.preventDefault();
    });
  });


  $('#registration-link').on('click', function(e) {
    e.preventDefault();
    return $(this).parent().find('.registration.popup').show();
  });

  $(document).on('click', function(e) {
    if ($('.registration.popup').is(':visible') && $('.registration.popup').not(':animated')) {
      if (e.target === $('.registration.popup').get(0) || $('.registration.popup').find(e.target).length !== 0 && e.target !== $('.registration.popup .close').get(0) || e.target === $('#registration-link').get(0)) {

      } else {
        return $('.registration.popup').hide();
      }
    } else {

    }
  });
  passwordReminder = $('.popup.password-reminder').remove().css('display', 'block');
  passwordReminderSuccess = $('.success.popup').remove().css('display', 'block');
  $(document).on('click', function(e) {
    if ($('.login.popup').is(':visible') && $('.login.popup').not(':animated')) {
      if (e.target === $('.login.popup').get(0) || $('.login.popup').find(e.target).length !== 0 && e.target !== $('.login.popup .close').get(0) || e.target === $('#autorization-link').get(0)) {

      } else {
        return $('.login.popup').hide();
      }
    } else {

    }
  });



  $('#autorization-link').on('click', function(e) {
    e.preventDefault();
    if($(this).attr('data') == 'login'){ //показываем форму входа
      return $(this).parent().find('.login.popup').show();
    }
    else{
      //выход
      var csrfToken = $('meta[name="csrf-token"]').attr("content");
      $.ajax({
        type: 'POST',
        url: 'core/index/logout',
        dataType: 'json',
        data: {logout: 'logout', _csrf : csrfToken},
        success: function(data){
          $('#autorization-link').attr('data', 'login');
          $('#autorization-link').text('Вход');
          $('#registration-link').show();
        },
      });
    }
  });


  //обрабатываем форму входа
  $('#login').on('click', function(e){
    e.preventDefault();
    $.ajax({
        type: 'POST', 
        url: 'core/index/login',
        data: $('#loginForm').serialize(),
        success: function(data){
           var res = JSON.parse (data);
           if(res.login == 'true'){
             $('.login.popup').hide();
             $('#autorization-link').attr('data', 'logout');
             $('#autorization-link').text('Выход('+res.name+')');
             $('#registration-link').hide();
             window.location.reload(true);
           }else{
             $('.error').text(res.login).show();
           }
        },
        error: function(xhr, str){}
      
    });
    return false;
  });

  $('#login-login').on('change', function(){
    $('.error').hide();
  });
  $('#password-login').on('change', function(){
    $('.error').hide();
  });

  //форма регистрации

  $('#register').on('click', function(e){
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: 'core/index/signup',
      data: $('#registerForm').serialize(),
      success: function(data){
        if(data.registration){
          $('.registration.popup').hide();
          $('#autorization-link').attr('data', 'logout');
          $('#autorization-link').text('Выход('+data.name+')');
          $('#registration-link').hide();
          $('#registerForm').trigger('reset');
        }
      },
      error: function(xhr, str){}
    });
    return false;
  });



  $('a.remind-me-password').on('click', function(e) {
    var background;
    e.preventDefault();
    background = $('<div>').css({
      'background': 'rgba(0,0,0, 0.8)',
      'width': '100%',
      'height': '100%',
      'position': 'fixed',
      'z-index': '10000',
      'top': '0'
    }).attr('id', 'popup-background').appendTo('body').append($(passwordReminder));
    return $('.password-reminder .close, #popup-background').on('click', function(e) {
      if (e.target === $('.password-reminder .close').get(0) || e.target === $('#popup-background').get(0)) {
        $('#captcha-password-reminder').val('');
        $('#captcha-password-reminder').removeClass('invalid-value');
        $('#popup-background').remove();
      }
      return $('.password-reminder form').on('submit', function(e) {
        e.preventDefault();
        if ($('#captcha-password-reminder').val() === 'tof') {
          $('#captcha-password-reminder').removeClass('invalid-value');
          $('#captcha-password-reminder').val('');
          $('.password-reminder').remove();
          $(passwordReminderSuccess).appendTo('#popup-background');
          return $('.success.popup .close, .success.popup button ').on('click', function(e) {
            $('#captcha-password-reminder').removeClass('invalid-value');
            $('#captcha-password-reminder').val('');
            return $('#popup-background').remove();
          });
        } else {
          return $('#captcha-password-reminder').addClass('invalid-value');
        }
      });
    });
  });
  $('#call-me-form-open').on('click', function(e) {
    e.preventDefault();
    return $(this).parent().parent().parent().find('.call.popup').show();
  });
  $(document).on('click', function(e) {
    if ($('.call.popup').is(':visible') && $('.call.popup').not(':animated')) {
      if (e.target === $('.call.popup').get(0) || $('.call.popup').find(e.target).length !== 0 && e.target !== $('.call.popup .close').get(0) || e.target === $('#call-me-form-open').get(0)) {

      } else {
        return $('.call.popup').hide();
      }
    } else {

    }
  });
  return $('.arrow-down').on('click', function() {
    return $('html, body').animate({
      scrollTop: $('.popular').offset().top
    });
  });
}).bind(jQuery);
