<?php
use frontend\modules\news\widgets\NewsOnMainWidget\NewsOnMainWidget;
use frontend\modules\adv\widgets\BannersWidget\BannersWidget;
use frontend\modules\product\widgets\ItemWithDiscountWidget\ItemWithDiscountWidget;
?>

<?= ''//BannersWidget::widget(['code' => 'bannerplace1']); ?>

<div class="content">
    <div class="container">
        <div class="row">
            <span class="arrow-down"></span>
            <div class="col-md-18 goods">
            
                <?= ItemWithDiscountWidget::widget(); ?>

                <h2 class="popular">Популярные товары</h2>
                <div class="slider-container">
                    <div class="item-wrapper">
                        <p class="name">Иммобилайзер скобка
                            запястья</p>
                        <p class="code">MQ501</p>
                             <img src="./img/glasses1.png" alt="">
                        <p class="price">
                            <span class="new">65 грн.</span>
                        </p>
                        <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
                    </div>
                    <div class="item-wrapper">
                        <p class="name">Иммобилайзер скобка
                            кисти</p>
                        <p class="code">MQ501</p>
                             <img src="assets/img/glasses2.png" alt="">
                        <p class="price">
                            <span class="new">65 грн.</span>
                        </p>
                        <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
                    </div>
                    <div class="item-wrapper">
                        <p class="name">Иммобилайзер скобка
                            локтя</p>
                        <p class="code">MQ501</p>
                              <img src="./img/glasses3.png" alt="">
                        <p class="price">
                            <span class="new">65 грн.</span>
                        </p>
                        <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
                    </div>
                    <div class="item-wrapper">
                        <p class="name">Иммобилайзер скобка
                            запястья</p>
                        <p class="code">MQ501</p>
                            <img src="./img/glasses1.png" alt="">
                        <p class="price">
                            <span class="new">65 грн.</span>
                        </p>
                        <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
                    </div>
                    <div class="item-wrapper">
                        <p class="name">Иммобилайзер скобка
                            кисти</p>
                        <p class="code">MQ501</p>
                            <img src="./img/glasses2.png" alt="">
                        <p class="price">
                            <span class="new">65 грн.</span>
                        </p>
                        <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
                    </div>
                    <div class="item-wrapper">
                        <p class="name">Иммобилайзер скобка
                            локтя</p>
                        <p class="code">MQ501</p>
                            <img src="./img/glasses3.png" alt="">
                        <p class="price">
                            <span class="new">65 грн.</span>
                        </p>
                        <div class="button-wrapper"><button><a href="#">В корзину</a></button></div>
                    </div>
                </div>
            </div>

            <?= NewsOnMainWidget::widget(); ?>
            
        </div>
    </div>
    <div class="pagination">
        <a href="#" class="start disabled"></a>
        <a href="#" class="prev disabled">Предыдущая</a>
        <a href="#" class="page active">1</a>
        <a href="#" class="page">2</a>
        <a href="#" class="page">3</a>
        <a href="#" class="page">4</a>
        <a href="#" class="page">5</a>
        <a href="#" class="next">Следующая</a>
        <a href="#" class="end"></a>
    </div>
</div>