<?php

namespace common\models\photo;

use Yii;

/**
 * This is the model class for table "photo_image".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $image
 * @property string $title
 * @property string $alt
 * @property string $description
 * @property integer $display_order
 *
 * @property PhotoAlbum $album
 */
class Image extends \yii\db\ActiveRecord
{

    protected $filePath = 'files/photo';

    public static function tableName()
    {
        return 'photo_image';
    }

    public function rules()
    {
        return [
            [['album_id', 'title', 'image'], 'required'],
            [['album_id', 'display_order'], 'integer'],
            [['description'], 'string'],
            [['title', 'alt'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhotoAlbum::className(), 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Album ID',
            'image'    => 'Image',
            'title' => 'Title',
            'description' => 'Description',
            'display_order' => 'Display Order',
        ];
    }


    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'album_id']);
    }

    public function getImagePath()
    {
        return '/' . $this->filePath . '/' . $this->image;
    }

    /*public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!is_dir('@web' . $this->filePath)) {
                mkdir('@web' . $this->filePath, 0777, TRUE);
            }
            return TRUE;
        }
        return FALSE;
    }*/

}
