<?php

namespace common\models\photo;

use Yii;
use backend\modules\photo\widgets\ImagesWidget\ImagesWidget;

/**
 * This is the model class for table "photo_album".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $display_order
 * @property integer $active
 *
 * @property PhotoImage[] $photoImages
 */
class Album extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'photo_album';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['album_id' => 'id']);
    }

    public function getFormElements()
    {
        return [
            'name'          => ['type' => 'text'],
            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
            'photos'        => ['type' => 'widget', 'nameWidget' => ImagesWidget::className(), 'attributes' => ['images' => $this->images]],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'description', 'display_order', 'active'];
    }
}
