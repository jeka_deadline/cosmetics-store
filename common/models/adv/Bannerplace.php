<?php

namespace common\models\adv;

use Yii;

/**
 * This is the model class for table "advertising_bannerplaces".
 *
 * @property integer $id
 * @property string $code
 * @property string $description
 * @property integer $display_order
 * @property integer $active
 *
 * @property Banner[] $banners
 */
class Bannerplace extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'advertising_bannerplaces';
    }

    public function rules()
    {
        return [
            [['code', 'description'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getBanners()
    {
        return $this->hasMany(Banner::className(), ['bannerplace_id' => 'id']);
    }
}
