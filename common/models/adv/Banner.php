<?php

namespace common\models\adv;

use Yii;

/**
 * This is the model class for table "advertising_banners".
 *
 * @property integer $id
 * @property integer $bannerplace_id
 * @property string $image
 * @property string $alt
 * @property string $url
 * @property integer $new_window
 * @property integer $display_order
 * @property integer $active
 *
 * @property Bannerplace $bannerplace
 */
class Banner extends \yii\db\ActiveRecord
{

    public $filePath = 'files/banners';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertising_banners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bannerplace_id'], 'required'],
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on' => ['insert']],
            [['bannerplace_id', 'new_window', 'display_order', 'active'], 'integer'],
            [['alt', 'url'], 'string', 'max' => 255],
            [['bannerplace_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bannerplace::className(), 'targetAttribute' => ['bannerplace_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bannerplace_id' => 'Bannerplace ID',
            'image' => 'Image',
            'alt' => 'Alt',
            'url' => 'Url',
            'new_window' => 'New Window',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerplace()
    {
        return $this->hasOne(Bannerplace::className(), ['id' => 'bannerplace_id']);
    }

    // получение полного url для изображения
    public function getImage()
    {
        return '/' . $this->filePath . '/' . $this->image;
    }
}
