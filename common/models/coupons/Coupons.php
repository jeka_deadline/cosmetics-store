<?php

namespace common\models\coupons;

use Yii;

/**
 * This is the model class for table "coupons".
 *
 * @property integer $id
 * @property string $code
 * @property integer $count
 * @property string $type
 * @property double $value
 */
class Coupons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'count', 'type', 'value'], 'required'],
            [['count'], 'integer'],
            [['type'], 'string'],
            [['value'], 'number'],
            [['code'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'count' => 'Count',
            'type' => 'Type',
            'value' => 'Value',
        ];
    }
}
