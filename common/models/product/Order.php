<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_order".
 *
 * @property integer $id
 * @property integer $order_number
 * @property string $to
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property integer $type_pay_id
 * @property integer $type_delivery_id
 * @property integer $sum
 *
 * @property ProductLinksOrderItem[] $productLinksOrderItems
 * @property ProductOrderTypeDelivery $typeDelivery
 * @property ProductOrderTypePay $typePay
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_number', 'to', 'email', 'phone', 'address', 'type_pay_id', 'type_delivery_id'], 'required'],
            [['order_number', 'type_pay_id', 'type_delivery_id', 'sum'], 'integer'],
            [['address'], 'string'],
            [['to', 'email', 'phone'], 'string', 'max' => 255],
            [['type_delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderTypeDelivery::className(), 'targetAttribute' => ['type_delivery_id' => 'id']],
            [['type_pay_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderTypePay::className(), 'targetAttribute' => ['type_pay_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_number' => 'Order Number',
            'to' => 'To',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'type_pay_id' => 'Type Pay ID',
            'type_delivery_id' => 'Type Delivery ID',
            'sum' => 'Sum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinksOrderItems()
    {
        return $this->hasMany(LinksOrderItem::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeDelivery()
    {
        return $this->hasOne(OrderTypeDelivery::className(), ['id' => 'type_delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypePay()
    {
        return $this->hasOne(OrderTypePay::className(), ['id' => 'type_pay_id']);
    }
}
