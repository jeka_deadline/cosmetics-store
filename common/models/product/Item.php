<?php

namespace common\models\product;

use Yii;
use common\models\photo\Album;
use common\models\core\Page;

/**
 * This is the model class for table "product_item".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $delivery_id
 * @property integer $page_id
 * @property integer $brand_id
 * @property string $articul
 * @property string $title
 * @property string $uri
 * @property string $small_description
 * @property string $description
 * @property double $price
 * @property integer $discount
 * @property integer $count
 * @property string $preview
 * @property integer $active
 * @property integer $photo_album_id
 * @property integer $create_at
 * @property integer $update_at
 * @property integer $master_id
 * @property Brand $brand
 * @property Category $category
 * @property Delivery $delivery
 * @property PhotoAlbum $Album
 * @property Page $page
 */
class Item extends \yii\db\ActiveRecord
{

    public $filePath = 'files/item';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'delivery_id', 'articul', 'title', 'small_description', 'description', 'price', 'page_id', 'uri'], 'required'],
            [['category_id', 'delivery_id', 'brand_id', 'discount', 'count', 'active', 'photo_album_id', 'create_at', 'update_at', 'page_id', 'master_id'], 'integer'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['articul'], 'string', 'max' => 20],
            [['title', 'small_description', 'uri'], 'string', 'max' => 255],
            /*[['uri'], 'unique', 'targetClass'=>Item::className(), 'message'=>'адресс должен быть уникальным'],
            [['articul'], 'unique', 'targetClass'=>Item::className(), 'message'=>'артикул должен быть уникальным'],*/
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['delivery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Delivery::className(), 'targetAttribute' => ['delivery_id' => 'id']],
            [['photo_album_id'], 'exist', 'skipOnError' => true, 'targetClass' => Album::className(), 'targetAttribute' => ['photo_album_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['preview'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'delivery_id' => 'Delivery ID',
            'parent_id' => 'Parent ID',
            'uri' => 'Uri',
            'brand_id' => 'Brand ID',
            'articul' => 'Articul',
            'title' => 'Title',
            'small_description' => 'Small Description',
            'description' => 'Description',
            'price' => 'Price',
            'discount' => 'Discount',
            'count' => 'Count',
            'preview' => 'Preview',
            'active' => 'Active',
            'photo_album_id' => 'Photo Album ID',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'master_id' => 'Master ID'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'photo_album_id']);
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    public function getProductLinksOrderItems()
    {
        return $this->hasMany(LinksOrderItem::className(), ['item_id' => 'id']);
    }

    public function getDiscounts()
    {
        //$discounts = [ 0 => 'Выберите скидку'];
        for ($i = 1; $i <= 99; $i++) {
            $discounts[ $i ] = $i . '%';
        }
        return $discounts;
    }

    // получение полного url для изображения
    public function getPreview()
    {
        return '/' . $this->filePath . '/' . $this->preview;
    }

    // получение цены с учетом скидки
    public function getPriceWithDiscount()
    {
        return $this->price - ($this->price * ($this->discount / 100));
    }

    // проверка существует ли скидка для данного товара
    public function isIssetDiscount()
    {
        return (bool)$this->discount;
    }
}
