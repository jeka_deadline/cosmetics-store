<?php

namespace common\models\product;

use Yii;
use common\models\core\Page;

/**
 * This is the model class for table "product_category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $page_id
 * @property string $preview
 * @property string $title
 * @property string $uri
 * @property string $description
 * @property integer $display_order
 * @property integer $active
 *
 * @property Item[] $items
 * @property Page $page
 */
class Category extends \yii\db\ActiveRecord
{

    public $filePath = 'files/category';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'parent_id', 'display_order', 'active'], 'integer'],
            [['title', 'page_id', 'uri'], 'required'],
            [['title', 'uri'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['preview'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'uri' => 'Uri',
            'page_id' => 'Page ID',
            'preview' => 'Preview',
            'title' => 'Title',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['category_id' => 'id']);
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    // получение полного url для изображения
    public function getPreview()
    {
        return '/' . $this->filePath . '/' . $this->preview;
    }
}
