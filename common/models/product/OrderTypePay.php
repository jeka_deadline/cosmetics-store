<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_order_type_pay".
 *
 * @property integer $id
 * @property string $title
 * @property integer $display_order
 * @property integer $active
 *
 * @property ProductOrder[] $productOrders
 */
class OrderTypePay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_order_type_pay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['type_pay_id' => 'id']);
    }
}
