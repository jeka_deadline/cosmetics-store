<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "user_roles".
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property integer $display_order
 * @property integer $created_at
 * @property integer $update_at
 *
 * @property UserRolesAccessAdmin[] $userRolesAccessAdmins
 * @property UserUsers[] $userUsers
 */
class Role extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_roles';
    }

    public function rules()
    {
        return [
            [['name', 'created_at', 'update_at'], 'required'],
            [['active', 'display_order', 'created_at', 'update_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'active' => 'Active',
            'display_order' => 'Display Order',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }

    public function getUserRolesAccessAdmins()
    {
        return $this->hasMany(RolesAccessAdmin::className(), ['role_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }
}
