<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "user_profiles".
 *
 * @property integer $id
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property integer $date_birth
 * @property string $sex
 * @property integer $user_id
 *
 * @property UserUsers $user
 */
class Profile extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_profiles';
    }

    public function rules()
    {
        return [
            [['date_birth', 'user_id'], 'integer'],
            [['sex'], 'string'],
            [['user_id'], 'required'],
            [['surname', 'name', 'patronymic'], 'string', 'max' => 30],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserUsers::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'surname' => 'Surname',
            'name' => 'Name',
            'patronymic' => 'Patronymic',
            'date_birth' => 'Date Birth',
            'sex' => 'Sex',
            'user_id' => 'User ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
