<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "user_socials".
 *
 * @property integer $id
 * @property string $provider
 * @property string $client_id
 * @property integer $created_at
 * @property integer $user_id
 *
 * @property UserUsers $user
 */
class Social extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_socials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider', 'client_id', 'created_at', 'user_id'], 'required'],
            [['created_at', 'user_id'], 'integer'],
            [['provider', 'client_id'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserUsers::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provider' => 'Provider',
            'client_id' => 'Client ID',
            'created_at' => 'Created At',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserUsers::className(), ['id' => 'user_id']);
    }
}
