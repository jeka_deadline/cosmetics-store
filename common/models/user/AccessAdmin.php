<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "user_roles_access_admin".
 *
 * @property integer $id
 * @property integer $role_id
 *
 * @property UserRoles $role
 */
class AccessAdmin extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_roles_access_admin';
    }

    public function rules()
    {
        return [
            [['role_id'], 'required'],
            [['role_id'], 'integer'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserRoles::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
        ];
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }
}
