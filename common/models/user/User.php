<?php

namespace common\models\user;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user_users".
 *
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property string $password_hash
 * @property integer $reset_password_token
 * @property string $auth_key
 * @property integer $blocked_at
 * @property integer $confirm_email_at
 * @property string $register_ip
 * @property integer $created_at
 * @property integer $update_at
 * @property integer $role_id
 * @property integer $login_with_social
 *
 * @property UserProfiles[] $userProfiles
 * @property UserSocials[] $userSocials
 * @property UserRoles $role
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    public static function tableName()
    {
        return 'user_users';
    }

    public function rules()
    {
        return [
            [['username', 'password_hash', 'auth_key', 'register_ip', 'role_id'], 'required'],
            [['reset_password_token', 'blocked_at', 'confirm_email_at', 'created_at', 'update_at', 'role_id', 'login_with_social'], 'integer'],
            [['email', 'password_hash'], 'string', 'max' => 100],
            [['username'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 32],
            [['register_ip'], 'string', 'max' => 15],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'reset_password_token' => 'Reset Password Token',
            'auth_key' => 'Auth Key',
            'blocked_at' => 'Blocked At',
            'confirm_email_at' => 'Confirm Email At',
            'register_ip' => 'Register Ip',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
            'role_id' => 'Role ID',
            'login_with_social' => 'Login With Social',
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'update_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
    public function getProfile()
    {
        return $this->hasMany(Profile::className(), ['user_id' => 'id']);
    }

    public function getSocials()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findByUserNameOrEmail($username)
    {
        $user = self::find()
                        ->where(['=', 'username', $username])
                        ->orWhere(['=', 'email', $username])
                        ->one();

        return ($user) ? $user : NULL;
    }

    public static function findByUsername($username)
    {
        $user = self::find()
                        ->where(['=', 'username', $username])
                        ->one();

        return ($user) ? $user : NULL;
    }

    public static function findByEmail($email)
    {
        $user = self::find()
                        ->where(['=', 'email', $email])
                        ->one();

        return ($user) ? $user : NULL;
    }

    public function isBlocked()
    {
        return ($this->blocked_at) ? TRUE : FALSE;
    }

    public function isConfirmEmail()
    {
        return ($this->confirm_email_at) ? TRUE : FALSE;
    }
}
