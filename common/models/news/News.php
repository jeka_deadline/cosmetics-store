<?php

namespace common\models\news;

use Yii;
use common\models\user\User;
use common\models\core\Page;

/**
 * This is the model class for table "news_news".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $title
 * @property string $uri
 * @property string $small_description
 * @property string $description
 * @property string $image
 * @property integer $date
 * @property integer $user_id
 * @property integer $display_order
 * @property integer $active
 *
 * @property LinksNewsCategory[] $linksNewsCategories
 * @property Page $page
 * @property User $user
 */
class News extends \yii\db\ActiveRecord
{

    public $filePath = 'files/news';

    public static function tableName()
    {
        return 'news_news';
    }

    public function rules()
    {
        return [
            [['page_id', 'title', 'uri', 'date', 'user_id'], 'required'],
            [['page_id', 'user_id', 'display_order', 'active'], 'integer'],
            [['description'], 'string'],
            [['title', 'uri', 'small_description'], 'string', 'max' => 255],
            [['uri'], 'unique'],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'title' => 'Title',
            'uri' => 'Uri',
            'small_description' => 'Small Description',
            'description' => 'Description',
            'image' => 'Image',
            'date' => 'Date',
            'user_id' => 'User ID',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    public function getLinksNewsCategories()
    {
        return $this->hasMany(LinksNewsCategory::className(), ['news_id' => 'id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    // получение полного url для изображения
    public function getImage()
    {
        return '/' . $this->filePath . '/' . $this->image;
    }

}
