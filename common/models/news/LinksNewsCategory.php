<?php

namespace common\models\news;

use Yii;

/**
 * This is the model class for table "links_news_category".
 *
 * @property integer $id
 * @property integer $news_id
 * @property integer $category_id
 *
 * @property Category $category
 * @property News $news
 */
class LinksNewsCategory extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'links_news_category';
    }

    public function rules()
    {
        return [
            [['news_id', 'category_id'], 'required'],
            [['news_id', 'category_id'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'category_id' => 'Category ID',
        ];
    }


    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
