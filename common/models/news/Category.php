<?php

namespace common\models\news;

use Yii;
use common\models\core\Page;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $title
 * @property string $uri
 * @property string $description
 * @property integer $display_order
 * @property integer $active
 *
 * @property LinksNewsCategory[] $linksNewsCategories
 * @property Page $page
 */
class Category extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'news_category';
    }

    public function rules()
    {
        return [
            [['page_id', 'title', 'uri'], 'required'],
            [['page_id', 'display_order', 'active'], 'integer'],
            [['description'], 'string'],
            [['title', 'uri'], 'string', 'max' => 255],
            [['uri'], 'unique'],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'title' => 'Title',
            'uri' => 'Uri',
            'description' => 'Description',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getLinksNewsCategories()
    {
        return $this->hasMany(LinksNewsCategory::className(), ['category_id' => 'id']);
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}
