<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_templates".
 *
 * @property integer $id
 * @property integer $title
 * @property string $path
 * @property integer $display_order
 * @property integer $active
 *
 * @property CorePages[] $corePages
 */
class Templates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'display_order', 'active'], 'integer'],
            [['path'], 'required'],
            [['path'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'path' => 'Path',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCorePages()
    {
        return $this->hasMany(CorePages::className(), ['template_id' => 'id']);
    }
}
