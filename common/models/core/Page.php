<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_pages".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $uri
 * @property string $route
 * @property string $module
 * @property string $header
 * @property string $menu_class
 * @property string $content
 * @property integer $template_id
 * @property integer $display_order
 * @property integer $active
 *
 * @property Menu[] $coreMenus
 * @property Templates $template
 * @property PagesMeta[] $corePagesMetas
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'template_id', 'display_order', 'active'], 'integer'],
            [['uri', 'route', 'header', 'template_id'], 'required'],
            [['content'], 'string'],
            [['uri'], 'string', 'max' => 100],
            [['route', 'module', 'header', 'menu_class'], 'string', 'max' => 255],
            [['uri'], 'unique'],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => Templates::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'uri' => 'Uri',
            'route' => 'Route',
            'module' => 'Module',
            'header' => 'Header',
            'menu_class' => 'Menu Class',
            'content' => 'Content',
            'template_id' => 'Template ID',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getMenuPages()
    {
        return $this->hasMany(MenuPages::className(), ['page_id' => 'id']);
    }

    public function getTemplate()
    {
        return $this->hasOne(Templates::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageMeta()
    {
        return $this->hasOne(PagesMeta::className(), ['page_id' => 'id']);
    }
}
