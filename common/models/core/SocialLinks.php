<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_social_links".
 *
 * @property integer $id
 * @property string $title
 * @property string $class
 * @property string $image
 * @property string $url
 * @property integer $display_order
 * @property integer $active
 */
class SocialLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_social_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg'],
            [['class', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'class' => 'Class',
            'url' => 'Url',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public function getImage()
    {
        return '/' . $this->filePath . '/' . $this->image;
    }
}
