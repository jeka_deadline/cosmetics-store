<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "core_pages_meta".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property string $model
 * @property integer $page_id
 * @property string $request_path
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property CorePages $page
 */
class PagesMeta extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'core_pages_meta';
    }

    public function rules()
    {
        return [
            [['owner_id', 'model', 'request_path'], 'required'],
            [['owner_id', 'page_id'], 'integer'],
            [['meta_description', 'meta_keywords'], 'string'],
            [['model', 'request_path', 'meta_title'], 'string', 'max' => 255],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'model' => 'Model',
            'page_id' => 'Page ID',
            'request_path' => 'Request Path',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}
