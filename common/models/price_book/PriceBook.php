<?php

namespace common\models\price_book;

use backend\modules\price_book\models\GroupName;
use yii;

use common\models\product\Item;


/**
 * This is the model class for table "price_book".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $price
 * @property integer $group_id
 *
 * @property GroupName $group
 * @property Item $product
 */
class PriceBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'price', 'group_id'], 'required'],
            [['product_id', 'group_id'], 'integer'],
            [['price'], 'number'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GroupName::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_id', 'group_id'], 'unique', 'targetAttribute' => ['product_id', 'group_id'], 'message' => 'Данный продукт уже включен в эту группу'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
            'group_id' => 'Group ID',
        ];
    }
}