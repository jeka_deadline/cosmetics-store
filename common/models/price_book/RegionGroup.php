<?php

namespace common\models\price_book;

use Yii;
use backend\modules\price_book\models\GroupName;

/**
 * This is the model class for table "region_group".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $region
 *
 * @property GroupName $group
 */
class RegionGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'region'], 'required'],
            [['group_id'], 'integer'],
            [['region'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GroupName::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'region' => 'Region',
        ];
    }

   

}



