<?php

namespace common\models\price_book;

use backend\modules\price_book\models\GroupType;
use yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "group_name".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $active
 *
  */
class GroupName extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'active'], 'required'],
            [['type', 'active'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique'],
            [['type'], 'exist', 'skipOnError' => true, 'targetClass' => GroupType::className(), 'targetAttribute' => ['type' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'active' => 'Active',
        ];
    }

    
}
