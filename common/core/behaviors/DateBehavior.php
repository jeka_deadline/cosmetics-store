<?php
namespace common\core\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class DateBehavior extends Behavior
{

    public $attribute;
    public $convertType = 'toTimestamp';

    public events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    public function beforeSave()
    {
        $model = $this->owner;
    }

}