<?php
namespace common\core\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class UploadFileBehavior extends Behavior
{

    public $attribute;
    public $uploadPath;

    private $_file;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteFile',
        ];
    }

    public function beforeValidate()
    {
        $model = $this->owner;
        if (($file = $model->getAttribute($this->attribute)) instanceof UploadedFile) {
            $this->_file = $file;
        } else {
            $this->_file = UploadedFile::getInstance($model, $this->attribute);
        }
        if ($this->_file instanceof UploadedFile) {
            $this->_file->name = $this->translitString($this->_file->name);
            $model->setAttribute($this->attribute, $this->_file);
        }
    }

    public function beforeSave()
    {
        $model = $this->owner;
        if ($file = UploadedFile::getInstance($model, $this->attribute)) {
            if (!is_dir($this->uploadPath)) {
                mkdir($this->uploadPath, 0777, TRUE);
            }
            if ($model->isAttributeChanged($this->attribute)) {
                $this->deleteFile();
            }
            $fileName = $this->translitString($file->name);
            if ($file->saveAs($this->uploadPath . '/' . $fileName)) {
                chmod($this->uploadPath . '/' . $fileName, 0777);
                $model->{$this->attribute} = $fileName;
            }
        } else {
            if (!$model->isNewRecord) {
                $model->{$this->attribute} = $model->getOldAttribute($this->attribute);
            }
        }
    }

    public function deleteFile()
    {
        $model = $this->owner;
        $file = $this->uploadPath . '/' . $model->getOldAttribute($this->attribute);
        if ($file && is_file($file) && is_writable($file)) {
            unlink($file);
        }
    }

    public function translitString($string)
    {
        $tr = array(
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G",
            "Д" => "D", "Е" => "E", "Ё" => "E", "Ж" => "J", "З" => "Z", "И" => "I",
            "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N",
            "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
            "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "TS", "Ч" => "CH",
            "Ш" => "SH", "Щ" => "SCH", "Ъ" => "", "Ы" => "YI", "Ь" => "",
            "Э" => "E", "Ю" => "YU", "Я" => "YA", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l",
            "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
            "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y",
            "ы" => "yi", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
            " " => '_', "'" => "", "\"" => "" 
        );
        return strtr($string, $tr);
    }

}