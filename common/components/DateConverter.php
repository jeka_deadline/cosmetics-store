<?php
namespace common\components;

class DateConverter
{

    public static function dateToTimestamp($date)
    {
        if (is_int($date)) {
            return $date;
        }
        return strtotime($date);
    }

    public static function timestampToDate($timestamp, $format = 'd-m-Y H:i:s')
    {
        if (is_int($timestamp)) {
            return date($format, $timestamp);
        }
        return $timestamp;
    }

}