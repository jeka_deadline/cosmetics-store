<?php
namespace common\components;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CheckboxListWidget extends Widget
{

    public $nameModelCheckbox;
    public $pkItem = 'id';
    public $nameFieldCheckbox;
    public $fullListItems;
    public $linkModelItems;
    public $htmlOptions = [];

    public function run()
    {
        $i = 0;
        $listCheckedItems = ArrayHelper::map($this->linkModelItems, $this->pkItem, $this->nameFieldCheckbox);
        foreach ($this->fullListItems as $item) {
            $checked = (isset($listCheckedItems[ $item->{$this->pkItem} ])) ? TRUE : FALSE;
            echo Html::checkbox($this->nameModelCheckbox . '[' . $i . '][' . $this->nameFieldCheckbox . ']', $checked, $this->htmlOptions);
        }
    }

}