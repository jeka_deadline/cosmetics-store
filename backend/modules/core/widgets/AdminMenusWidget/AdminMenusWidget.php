<?php
namespace backend\modules\core\widgets\AdminMenusWidget;

use yii\base\Widget;

class AdminMenusWidget extends Widget
{

    public function run()
    {
        $modules  = include(\Yii::getAlias('@backend/config/modules.php'));
        $menus    = $displayOrderModules = [];
        foreach ($modules as $nameModule => $module) {
            $module = \Yii::$app->getModule($nameModule);
            if (isset($module->menuOrder)) {
                $displayOrderModules[ $module->menuOrder ] = $nameModule;
            }
        }

        foreach ($displayOrderModules as $nameModule) {
            $method = 'getMenuItems';
            $module = \Yii::$app->getModule($nameModule);
            if (method_exists($module, $method)) {
                $items = $module->$method();
                foreach ($items as $item) {
                    if (isset($item[ 'items' ])) {
                        
                        $listItems = [
                            'label' => $item[ 'label' ],
                            'items' => [],
                        ];

                        foreach ($item[ 'items' ] as $elemItem) {
                            
                            $listItems[ 'items' ][] = [
                               'label' => $elemItem[ 'label' ],
                               'url'   => [$elemItem[ 'url' ]],
                            ];

                        }

                    } else {
                        $listItems = [
                            'label' => $item[ 'label' ],
                            'url'   => [$item[ 'url' ]],
                        ];
                    }
                    $menus[] = $listItems;
                }
            }
        }
        return $this->render('menu', ['menu' => $menus]);
    }

}