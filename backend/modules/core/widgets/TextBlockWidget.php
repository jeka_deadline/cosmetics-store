<?php
namespace backend\modules\core\widgets;

use yii\base\Widget;
use common\models\core\TextBlock;

class TextBlockWidget extends Widget
{

    public $uri = '';

    public function run()
    {
        if (!empty($this->uri)) {
            $textBlock = TextBlock::find()
                            ->where(['=', 'uri', $this->uri])
                            ->andWhere(['=', 'active', '1'])
                            ->one();

            if ($textBlock) {
                return $textBlock->content;
            }
        }
    }

}