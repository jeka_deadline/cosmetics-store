<?php

/* @var $this yii\web\View */
use backend\modules\core\widgets\TextBlockWidget;
use yii\helpers\Html;

$this->title = 'My Yii Application';
$this->registerCss('.btn-group {
  margin-top: 5px;
}

div.col-lg-2 {
    margin-bottom: 10px;
}

div.col-lg-2 img {
    max-height: 100px;
}
');

$this->registerJs(
'jQuery(document).on("click", "span.glyphicon-trash", function(){
      jQuery(this).closest("div.col-lg-2").remove();
})'
);
?>
<div class="site-index">

    <form action="">
          <div class="form-group">
              <div class="panel panel-default">
                  <div class="panel-body">
                      <div class="row">
                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                              <img src="http://www.nasa.gov/sites/default/files/styles/image_card_4x3_ratio/public/thumbnails/image/idcs1426.jpg?itok=Gc_-Q58L" class="img-responsive" alt="">
                              <div class="btn-group" role="group" aria-label="">
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
                              </div>
                              <div class="data-image">
                                  
                              </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                              <img src="http://i.telegraph.co.uk/multimedia/archive/03589/Wellcome_Image_Awa_3589699k.jpg" class="img-responsive" alt="">
                              <div class="btn-group" role="group" aria-label="">
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
                              </div>
                              <div class="data-image">
                                    
                              </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                              <img src="http://cdn.arstechnica.net/wp-content/uploads/2016/02/5718897981_10faa45ac3_b-640x624.jpg" class="img-responsive" alt="">
                              <div class="btn-group" role="group" aria-label="">
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
                              </div>
                              <div class="data-image">
                                    
                              </div>
                                  
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                              <img src="http://i.dailymail.co.uk/i/pix/2016/04/13/00/331D901800000578-3536787-image-a-11_1460503122350.jpg" class="img-responsive" alt="">
                              <div class="btn-group" role="group" aria-label="">
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
                              </div>
                              <div class="data-image">
                                    
                              </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
                              <img src="http://www.nasa.gov/sites/default/files/styles/image_card_4x3_ratio/public/thumbnails/image/idcs1426.jpg?itok=Gc_-Q58L" class="img-responsive" alt="">
                              <div class="btn-group" role="group" aria-label="">
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                                  <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></button>
                              </div>
                              <div class="data-image">
                                    
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div> 
    </form>
    <!-- <div class="jumbotron">
        <h1>Congratulations!</h1>
    
        <p class="lead">You have successfully created your Yii-powered application.</p>
    
        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>
    
    <div class="body-content">
    
        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>
    
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
    
                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>
    
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
    
                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>
    
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
    
                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>
    
    </div> -->
</div>
