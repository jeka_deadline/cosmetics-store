<?php

use yii\helpers\Html;


$this->title = $title;

?>
<div class="crud-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('crud-form', [
        'model'             => $model,
        'formElements'      => $formElements,
        'activeFormConfig'  => $activeFormConfig,
    ]) ?>

</div>