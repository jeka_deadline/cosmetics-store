<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\assets\DelAsset;


$this->title = $title;
DelAsset::register($this);

?>
<div class="crud-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        if(Yii::$app->session->hasFlash('load')):?>
        <div class="alert alert-danger">
            <?php echo Yii::$app->session->getFlash('load'); ?>
        </div>
    <?php endif; ?>

    <p>
        <?= Html::a("Добавить", ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php //Pjax::begin(); ?>

    <?= GridView::widget([

        'dataProvider'  => $dataProvider,
        'filterModel'   => $searchModel,
        'columns'       => $columns,

    ]); ?>

    <?php //Pjax::end(); ?>

</div>

<div class="hide modalConfirmDelete" id ="modalConfirmDelete">
    <div id="modalCont" class="modalCont">
        <div id="confirm"></div>
        <button class="btn btn-danger" id="yes">Да</button>
        <button class="btn btn-success" id="cancel">Нет</button>
    </div>
</div>



<a href="<?= Url::toRoute(['item/export-xml'])?>" class="btn btn-success" id="product">Экспорт продуктов в XML</a>

<?= Html::beginForm(['item/import-xml'], 'post', ['enctype' => 'multipart/form-data', 'id'=>'import']) ?>

    <div class=" btn btn-success file-upload">
        <label>
            <?= Html::fileInput('importFile','', ['id'=>'importFlie']) ?>
            <span>Импорт продуктов из XML</span>
        </label>
    </div>
<?= Html::endForm() ?>
<?php
    $this->registerJs("
        $('#importFlie').on('change', function(event){
             $('#import').submit();
        }); 
        setTimeout(function() { $('.alert').hide('slow'); }, 10000);
        ");

?>




