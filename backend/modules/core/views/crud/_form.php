<?php
use yii\helpers\Html;

switch ($paramsElement[ 'type' ]) :

    case 'text':
        echo $form->field($model, $nameElement, $fieldAttributes)->textInput($attributes);
        break;

    case 'textarea':
        echo $form->field($model, $nameElement, $fieldAttributes)->textarea($attributes);
        break;

    case 'checkbox':
        echo $form->field($model, $nameElement, $fieldAttributes)->checkbox($attributes);
        break;

    case 'checkboxlist':
        echo $form->field($model, $nameElement, $fieldAttributes)->checkboxList($paramsElement[ 'items' ], $attributes);
        //print_r($fieldAttributes);
        //exit;
        break;

    case 'dropdownlist':
        echo $form->field($model, $nameElement, $fieldAttributes)->dropDownList($paramsElement[ 'items' ], $attributes);
        break;

    case 'file':
        echo $form->field($model, $nameElement, $fieldAttributes)->fileInput($attributes);
        break; 

    case 'image':
        if (!empty($model->$nameElement)) {
            echo Html::beginTag('div', ['class' => 'row']);
                echo Html::beginTag('div', ['class' => 'col-md-4']);
                    echo Html::img($model->$paramsElement[ 'functionGetImage' ](), ['class' => 'img-responsive']);
                echo Html::endTag('div');
            echo Html::endTag('div');
        }
        echo $form->field($model, $nameElement, $fieldAttributes)->fileInput($attributes);
        break;

    case 'hidden':
        echo $form->field($model, $nameElement, $fieldAttributes)->hiddenInput($attributes);
        break;   

    case 'radio':
        echo $form->field($model, $nameElement, $fieldAttributes)->radio($attributes);
        break;

    case 'radiolist':
        echo $form->field($model, $nameElement, $fieldAttributes)->radioList($attributes);
        break;

    case 'listbox':
        echo $form->field($model, $nameElement, $fieldAttributes)->listBox($attributes);
        break;

    case 'datetime':
        echo $form->field($model, $nameElement, $fieldAttributes)->widget(\trntv\yii\datetime\DateTimeWidget::className(), $attributes);
        break;

    case 'widget':
        echo $form->field($model, $nameElement, $fieldAttributes)->widget($paramsElement[ 'nameWidget' ], $attributes);
        break;  

endswitch;
?>