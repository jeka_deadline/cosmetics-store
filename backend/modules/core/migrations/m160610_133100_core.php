<?php

use yii\db\Migration;

class m160610_133100_core extends Migration
{
    public function up()
    {

        // таблица контактов
        $this->createTable('{{core_contacts}}', [
            'id'      => $this->primaryKey(),
            'phone'   => $this->string(15)->defaultValue(NULL),
            'email'   => $this->string(50)->defaultValue(NULL),
            'address' => $this->string(255)->defaultValue(NULL),
        ]);

        $this->createTable('{{core_social_links}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(50)->notNull(),
            'image'         => $this->string(255)->defaultValue(NULL),
            'class'         => $this->string(255)->defaultValue(NULL),
            'url'           => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),

         ]);

        // таблица текстовых блоков
        $this->createTable('{{core_text_block}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(100)->notNull(),
            'description'   => $this->string(255)->defaultValue(NULL),
            'uri'           => $this->string(255)->notNull(),
            'content'       => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createIndex('uix_core_text_block_uri', '{{core_text_block}}', 'uri', TRUE);

        // таблица прикреплений
        $this->createTable('{{core_attachments}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(255)->notNull(),
            'description'   => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'file'          => $this->string(255)->notNull(),
        ]);
        
        // таблица шаблонов
        $this->createTable('{{core_templates}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(100)->notNull(),
            'path'          => $this->string(20)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица страниц
        $this->createTable('{{core_pages}}', [
            'id'                => $this->primaryKey(),
            'parent_id'         => $this->integer()->defaultValue(0),
            'uri'               => $this->string(100)->notNull(),
            'route'             => $this->string(255)->notNull(),
            'module'            => $this->string(255)->notNull(),
            'header'            => $this->string(255)->notNull(),
            'menu_class'        => $this->string(255)->defaultValue(NULL),
            'content'           => $this->text()->defaultValue(NULL),
            'template_id'       => $this->integer()->notNull(),
            'display_order'     => $this->integer()->defaultValue(0),
            'active'            => $this->smallInteger(1)->defaultValue(0),
        ]);

        // создание связи таблицы мета-данных страниц с таблицей страниц
        $this->createIndex('uix_core_pages_uri', '{{core_pages}}', 'uri', TRUE);

        // создание связи таблицы страницы с таблицей шаблонов
        $this->createIndex('idx_core_pages_template_id', '{{core_pages}}', 'template_id');
        $this->addForeignKey('fk_core_pages_template_id', '{{core_pages}}', 'template_id', '{{core_templates}}', 'id', 'CASCADE', 'CASCADE');

        // таблица мета-данных страниц
        $this->createTable('{{core_pages_meta}}', [
            'id'                => $this->primaryKey(),
            'owner_id'          => $this->integer()->notNull(),
            'model'             => $this->string(255)->notNull(),
            'page_id'           => $this->integer(11)->defaultValue(NULL),
            'request_path'      => $this->string(255)->notNull(),
            'target_path'       => $this->string(255)->notNull(),
            'active'            => $this->smallInteger(1)->defaultValue(0),
            'meta_title'        => $this->string(255)->defaultValue(NULL),
            'meta_description'  => $this->text()->defaultValue(NULL),
            'meta_keywords'     => $this->text()->defaultValue(NULL),
        ]);

        // создание связи таблицы мета данных страницы с таблицей страниц
        $this->createIndex('idx_core_pages_meta_page_id', '{{core_pages_meta}}', 'page_id');
        $this->addForeignKey('fk_core_pages_meta_page_id', '{{core_pages_meta}}', 'page_id', '{{core_pages}}', 'id', 'CASCADE', 'CASCADE');

         // таблица для типов меню
        $this->createTable('{{core_type_menu}}', [
            'id'            => $this->primaryKey(),
            'code'          => $this->string(255)->notNull(),
            'title'         => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),

        ]);

        // создание уникального индекса для поля code таблицы типов меню
        $this->createIndex('uix_core_type_menu_code', '{{core_type_menu}}', 'code', TRUE);

        // таблица для связивания типов меню с таблицей страниц 
        $this->createTable('{{core_menu_pages}}', [
            'id'            => $this->primaryKey(),
            'page_id'       => $this->integer(11)->notNull(),
            'type_id'       => $this->integer(11)->notNull(),

        ]);

        // создание связи таблицы меню-страницы с таблицей страниц
        $this->createIndex('idx_core_menu_pages_page_id', '{{core_menu_pages}}', 'page_id');
        $this->addForeignKey('fk_core_menu_pages_page_id', '{{core_menu_pages}}', 'page_id', '{{core_pages}}', 'id', 'CASCADE', 'CASCADE');

        // создание связи таблицы меню-страницы с таблицей типов меню
        $this->createIndex('idx_core_menu_pages_type_id', '{{core_menu_pages}}', 'type_id');
        $this->addForeignKey('fk_core_menu_pages_type_id', '{{core_menu_pages}}', 'type_id', '{{core_type_menu}}', 'id', 'CASCADE', 'CASCADE');

        $this->createPages();
    }

    public function down()
    {
        $this->dropForeignKey('fk_core_menu_pages_type_id', '{{core_menu_pages}}');
        $this->dropForeignKey('fk_core_menu_pages_page_id', '{{core_menu_pages}}');
        $this->dropTable('{{core_menu_pages}}');
        $this->dropTable('{{core_type_menu}}');
        $this->dropForeignKey('fk_core_pages_meta_page_id', '{{core_pages_meta}}');
        $this->dropTable('{{core_pages_meta}}');
        $this->dropForeignKey('fk_core_pages_template_id', '{{core_pages}}');
        $this->dropTable('{{core_templates}}');
        $this->dropTable('{{core_pages}}');
        $this->dropTable('{{core_attachments}}');
        $this->dropTable('{{core_text_block}}');
        $this->dropTable('{{core_social_links}}');
        $this->dropTable('{{core_contacts}}');
    }

    private function createPages()
    {
        $this->insert('{{core_templates}}', [
            'title'         => 'Одноколоночный шаблон',
            'path'          => 'main',
            'display_order' => 0,
            'active'        => 1,
        ]);

        $this->insert('{{core_pages}}', [
            'uri'         => '/',
            'route'       => 'core/index/index',
            'header'      => 'Главная',
            'template_id' => $this->db->getLastInsertID(),
            'module'      => 'core',
            'active'      => 1,
        ]);

        $this->insert('{{core_pages_meta}}', [
            'owner_id'      => $this->db->getLastInsertID(),
            'model'         => 'Pages',
            'page_id'       => $this->db->getLastInsertID(),
            'request_path'  => '/',
            'target_path'   => 'core/index/index',
            'active'        => 1,
        ]);
    }

}
