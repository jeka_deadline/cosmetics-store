<?php
namespace backend\modules\core\components;

use Yii;
use backend\modules\core\components\BackendBaseAction;

class CRUDIndex extends BackendBaseAction
{

    public $view = 'crud-index';

    public function run()
    {
        $searchModel  = $this->controller->model;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->controller->viewPath = $this->viewPath;

        return $this->controller->render($this->view, [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
            'title'         => $this->title,
            'columns'       => $this->controller->getColumns(),
        ]);

    }

}