<?php
namespace backend\modules\core\components;

use yii\web\Controller;
use yii\helpers\Html;

class BackendController extends Controller
{

    public $model;
    public $searchModel;

    public function init()
    {
        parent::init();
        $this->model = new $this->searchModel();
    }

    protected function getGridSerialColumn()
    {
        return ['class' => 'yii\grid\SerialColumn'];
    }

    protected function getGridActions()
    {
        return ['class' => 'yii\grid\ActionColumn',
            'buttons'=>[
                'delete' => function ($url, $model) {
                    return Html::a('', $url, [
                        'class' => 'popup-modal glyphicon glyphicon-trash',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal',
                        'data-id' => $model->id,
                        ]);
                },
            ],


        ];
    }

    protected function getGridActive()
    {
        return [
            'attribute' => 'active',
            'filter'    => Html::activeDropDownList($this->model, 'active', ['0' => 'No', '1' => 'Yes'], ['class' => 'form-control', 'prompt' => '']),
            'value'     => function($model) {
                              return ($model->active) ? '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>';
            },
            'format'    => 'html',
        ];
    }

}