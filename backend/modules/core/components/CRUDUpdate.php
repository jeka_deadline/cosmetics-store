<?php
namespace backend\modules\core\components;

use Yii;
use backend\modules\core\components\BackendBaseAction;

class CRUDUpdate extends BackendBaseAction
{

    public $title             = 'Update';
    public $view              = 'crud-update';
    public $activeFormConfig  = ['options' => ['enctype' => 'multipart/form-data']];

    public function run($id)
    {
        $model = $this->findModel($id);

        if (!empty($this->scenarios)) {
            if (is_string($this->scenarios)) {
                $model->setScenario($this->scenarios);
            }
        }

        $this->controller->viewPath = $this->viewPath;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->controller->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->controller->render($this->view, [
                'model'             => $model,
                'formElements'      => $model->getFormElements(),
                'title'             => $this->title,
                'activeFormConfig'  => $this->activeFormConfig,
            ]);
        }
    }

}