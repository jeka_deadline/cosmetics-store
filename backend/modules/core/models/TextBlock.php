<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\TextBlock as BaseTextBlock;
use yii\helpers\ArrayHelper;

class TextBlock extends BaseTextBlock
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'title'         => ['type' => 'text'],
            'uri'           => ['type' => 'text'],
            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            'content'       => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'uri'           => ['type' => 'text'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'uri', 'description', 'content', 'display_order', 'active'];
    }
}
