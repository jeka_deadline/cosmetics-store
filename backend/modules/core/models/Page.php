<?php

namespace backend\modules\core\models;

use Yii;
use common\models\core\Page as BasePage;
use yii\helpers\ArrayHelper;
use common\models\core\PagesMeta;
use common\models\core\MenuPages;
use common\models\core\Templates;
use yii\helpers\Html;

class Page extends BasePage
{

    public $meta_title;
    public $meta_description;
    public $meta_keywords;
    public $menus;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
                [['meta_description', 'meta_keywords'], 'string'],
                [['meta_title'], 'string', 'max' => 255],
                //[['uri'], 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z\d_-]*[a-zA-Z\d]$/'],
            ]
        );
    }

    public function getFormElements()
    {
        $meta = $this->getMeta();
        $this->setMenus();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }
        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => 'Pages',
                        'active'    => TRUE,
                        'elements'  => [
                            'header'        => ['type' => 'text', 'fieldAttributes' => ['template' => '{label}{input}']],
                            'uri'           => ['type' => 'text'],
                            'route'         => ['type' => 'dropdownlist', 'items' => $this->getListRoutes(), 'attributes' => ['prompt' => 'Choose route']],
                            'content'       => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'template_id'   => [
                                                  'type'        => 'dropdownlist',
                                                  'items'       => ArrayHelper::map(Templates::find()->all(), 'id', 'title'),
                                                  'attributes'  => ['prompt' => 'Choose template'],
                                                ],
                            'menus'         => [
                                                  'type' => 'checkboxlist',
                                                  'items' => ArrayHelper::map(TypeMenu::find()->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'title'),
                                                  'attributes' => [
                                                      'item' => function($index, $label, $name, $checked, $value) {
                                                          return $this->getTemplateForCheckboxList($index, $label, $name, $checked, $value);
                                                      }
                                                  ],
                                              ],
                            'menu_class'    => ['type' => 'text'],
                            'display_order' => ['type' => 'text'],
                            'active'        => ['type' => 'checkbox'],
                        ],
                    ],
                    [
                        'title'     => 'Meta',
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }

    /*public function afterValidate()
    {
        print_r($this->getErrors());
        exit;
    }*/

    public function getViewAttributes()
    {
        return ['id', 'uri', 'route', 'header', 'display_order', 'active'];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->module = substr($this->route, 0, strpos($this->route, '/'));
            return TRUE;
        }
        return FALSE;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();
        $menus = (!empty($_POST[ 'Page' ][ 'menus' ])) ? $_POST[ 'Page' ][ 'menus' ] : [];
        $this->saveMenus($menus);
    }

    private function saveMeta()
    {
        $meta = $this->getMeta();

        if (!$meta) {
            $meta = new PagesMeta();
        }
        $meta->owner_id         = $this->id;
        $meta->page_id          = $this->id;
        $meta->model            = 'Pages';
        $meta->request_path     = $this->uri;
        $meta->target_path      = $this->route;
        $meta->active           = $this->active;
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;
        $meta->save(FALSE);
    }

    private function getMeta()
    {
        return PagesMeta::find()
                              ->where(['=', 'owner_id', $this->id])
                              ->andWhere(['=', 'model', 'Pages'])
                              ->one();
    }

    public function setMenus()
    {
        $this->menus = ArrayHelper::getColumn($this->menuPages, 'type_id');
    }

    // метод который генерирует шаблон для списка чекбоксов
    public function getTemplateForCheckboxList($index, $label, $name, $checked, $value)
    {
        return "<div class='checkbox'><label>" . Html::checkbox($name, $checked, ['value' => $value]) . "{$label}</label></div>";
    }

    // метод для сохранения связей между новостью и ее категориями
    private function saveMenus($menus)
    {
        
        // получаем список категорий, к которым относится данная новость
        $listMenus = ArrayHelper::map($this->menuPages, 'id', function($model){ return $model;});
        
        // перебираем все новости, которые были отмечены
        foreach ($menus as $menuId) {

            // если нет отмеченной новости в списке текущих связей,
            // тогда сохраняем связь
            if (!isset($listMenus[ $menuId ])) {
                $link           = new MenuPages();
                $link->page_id  = $this->id;
                $link->type_id  = $menuId;
                $link->save();
            } else {
                unset($listMenus[ $menuId ]);
            }
        }

        // Удаляем новости, которые были unchecked
        foreach ($listMenus as $model) {
            $model->delete();
        }
    }

    private function getListRoutes()
    {
        $modules = include(Yii::getAlias('@backend/config/modules.php'));
        $rules = [];
        $method = 'getFrontendRoutes';
        foreach ($modules as $nameModule => $object) {
            $module = Yii::$app->getModule($nameModule);
            if (method_exists($module, $method)) {
                $listRoutes = $module->$method();
                foreach ($listRoutes[ 'controllers' ] as $nameController => $dataController) {
                    foreach ($dataController[ 'actions' ] as $nameAction => $dataAction) {
                        $rules[ $nameModule . '/' . $nameController . '/' . $nameAction] = $dataAction[ 'name' ];
                    }
                }
            }
        }
        return $rules;
    }
}
