<?php

namespace backend\modules\core\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\core\SocialLinks as BaseSocialLinks;

class SocialLinks extends BaseSocialLinks
{

    public $filePath = 'files/core/social-links';
    
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

   public function getFormElements()
    {
        return [
            'title'         => ['type' => 'text'],
            'url'           => ['type' => 'text'],
            'image'         => ['type' => 'image', 'functionGetImage' => 'getImage'],
            'class'         => ['type' => 'text'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'url', 'class', 'image', 'display_order', 'active'];
    }

    public function behaviors()
    {
        return [
            'image' => [
              'class' => 'common\core\behaviors\UploadFileBehavior',
              'attribute' => 'image',
              'uploadPath' => Yii::getAlias('@frontend') . '/web/' . $this->filePath,
            ]
        ];
    }
}
