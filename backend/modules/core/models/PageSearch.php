<?php

namespace backend\modules\core\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\core\models\Page;

/**
 * PageSearch represents the model behind the search form about `Page`.
 */
class PageSearch extends Page
{

    public function rules()
    {
        return [
            [['id', 'parent_id', 'display_order', 'active', 'template_id'], 'integer'],
            [['uri', 'route', 'header', 'content', 'module', 'menu_class'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'display_order' => $this->display_order,
            'active' => $this->active,
            'template_id' => $this->template_id,
        ]);

        $query->andFilterWhere(['like', 'uri', $this->uri])
            ->andFilterWhere(['like', 'route', $this->route])
            ->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'menu_class', $this->menu_class])
            ->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}