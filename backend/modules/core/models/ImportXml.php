<?php
namespace backend\modules\core\models;

use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\Model;
use yii\web\UploadedFile;
use backend\modules\product\models\Item;

class ImportXml extends Model
{
    /**
     * @var file
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => ['xml'], 'message'=> 'проверьте тип файла'],
        ];
    }

    public function upload()
    {
        $models = [];
        $model = [];
        try{
            $xml = simplexml_load_file($this->file->tempName);
        }catch (ErrorException $ex){
            return false;
        }
       
        foreach($xml->product as $value) {
            foreach($value as $val){
                 try{
                     $property =(string)$val['name'];
                     $type = (string)$val['type'];
                     $value = (string)$val;
                     settype($value, $type);
                     $model[$property]=$value;
                 }
                 catch(ErrorException $ex){
                     return false;
                 }
            }
            $models[] =$model;
            $model=[];
        }
        return $models;
    }
 }