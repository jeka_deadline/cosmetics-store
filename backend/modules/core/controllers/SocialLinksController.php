<?php

namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * SocialLinksController implements the CRUD actions for SocialLinks model.
 */
class SocialLinksController extends BackendController
{

    public $searchModel = 'backend\modules\core\models\SocialLinksSearch';
    public $modelName   = 'backend\modules\core\models\SocialLinks';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Социальные ссылки',
            ],
            'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Добавить социальную ссылку',
                'modelName'   => $this->modelName,
            ],
            'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить социальную ссылку',
                'modelName'   => $this->modelName,
            ],
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр социальных ссылок',
                'modelName'   => $this->modelName,
            ],
            'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'title'],
            [
                'attribute'   => 'image',
                'value'       => function($model) {return Html::a(Html::encode($model->image), $model->getImage(), ['target' => '_blank']);},
                'filter'      => FALSE,
                'format'      => 'raw',
            ],
            ['attribute'  => 'url'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }

}
