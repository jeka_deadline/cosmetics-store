<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\product\models\Item;
use backend\modules\product\models\Category;
use backend\modules\product\models\Delivery;
use backend\modules\product\models\Brand;
use common\models\core\Page;
use common\models\photo\Album;

?>

<div class="container">
    <div class="crud-create">
        <h1 id="description"><?= $title; ?> </h1>
        <div class="crud-form" id="crud-form">
            <?php $form = ActiveForm::begin([
                'id' => 'id_form',
                'enableAjaxValidation'=>true,
            ]); ?>
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#items" role="tab" data-toggle="tab">Items</a></li>
                <li><a href="#meta" role="tab" data-toggle="tab">Meta</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="items">
                    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'title'), ['prompt'=>'Choose category', 'class'=>'form-control', 'id'=>'item-category'])?>
                    <?php if($master){
                            echo ($form->field($model, 'master_id')->dropDownList([],['prompt'=>'Choose masterId', 'class'=>'form-control', 'id'=>'item', 'disabled'=>'disabled']));
                        }
                    ?>
                    <?php
                        if($master){
                            echo($form->field($model, 'page_id')->dropDownList(ArrayHelper::map(Page::find()->where(['=', 'module', 'product'])->all(), 'id', 'header'),['prompt'=>'Choose page', 'class'=>'myHide form-control']));
                        }
                        else{
                            echo($form->field($model, 'page_id')->dropDownList(ArrayHelper::map(Page::find()->where(['=', 'module', 'product'])->all(), 'id', 'header'),['prompt'=>'Choose page', 'class'=>'updateHide form-control']));
                        }
                    ?>
                    <?= $form->field($model, 'articul')->textInput(); ?>
                    <?= $form->field($model, 'title')->textInput(); ?>
                    <?= $form->field($model, 'uri')->textInput(); ?>
                    <?= $form->field($model, 'small_description')->textArea(['rows' => '5']); ?>
                    <?= $form->field($model, 'description')->textArea(['rows'=>'10']); ?>
                    <?= $form->field($model, 'price')->textInput(); ?>
                    <?= $form->field($model, 'count')->textInput(); ?>
                    <?= $form->field($model, 'discount')->dropDownList($model->getDiscounts(), ['prompt' => 'Choose discount']); ?>
                    <?= $form->field($model, 'preview')->fileInput(); ?>
                    <?php
                        if($master) {
                            echo($form->field($model, 'delivery_id')->dropDownList(ArrayHelper::map(Delivery::find()->all(), 'id', 'title'), ['prompt' => 'Choose delivery', 'class' => 'myHide form-control']));
                        }
                    else{
                        echo($form->field($model, 'delivery_id')->dropDownList(ArrayHelper::map(Delivery::find()->all(), 'id', 'title'), ['prompt' => 'Choose delivery', 'class' => 'updateHide form-control']));
                    }
                    ?>
                    <?php
                        if($master){
                            echo($form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(Brand::find()->all(), 'id', 'title'), ['prompt'=>'Choose brand', 'class'=>'myHide form-control']));    
                        }
                        else{
                            echo($form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(Brand::find()->all(), 'id', 'title'), ['prompt'=>'Choose brand', 'class'=>'updateHide form-control']));
                        }
                    ?>
                    <?= $form->field($model, 'photo_album_id')->dropDownList(ArrayHelper::map(Album::find()->all(), 'id', 'name'),['prompt'=>'Choose Album']); ?>
                    <?= $form->field($model, 'active')->checkbox(); ?>
                </div>
                <div class="tab-pane" id="meta">
                    <?= $form->field($model, 'meta_title')->textInput(); ?>
                    <?= $form->field($model, 'meta_description')->textArea(['rows' => '10']); ?>
                    <?= $form->field($model, 'meta_keywords')->textArea(['rows'=>'10']); ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Update', ['class' => 'btn btn-success']); ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div class="hide" id ='updateMaster' data ="<?= $master?>"></div>