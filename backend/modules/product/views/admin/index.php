<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\user\Role;
use yii\helpers\Url;
use common\widgets\Alert;
?>
<div class="page page-dashboard">
    <?= Alert::widget(); ?>

    <?= Html::a('Create user', Url::toRoute(['/user/admin/create-user']), ['class' => 'btn btn-success']); ?>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
    		    ['attribute' => 'email'],
            ['attribute' => 'username'],
            [
                'attribute' => 'role_id',
                'content' => function ($model){return $model->role->name;},
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'role_id',
                    ArrayHelper::map(Role::find()->asArray()->all(), 'id', 'name'),
                    [
                        'class' => 'form-control',
                        'prompt' => ''
                    ]
                ),
            ],
            [
              	'attribute' => 'confirm_email_at',
              	'content' => function($model) {
              		  if ($model->confirm_email_at) {
                        return 'Активировано';
              		  } else {
                        return Html::a(
              				      'Активировать',
              				      Url::toRoute([
                                '/user/admin/confirm-email',
                                'id' => $model->id
              				      ]),
              				      ['class' => 'btn btn-success btn-xs']
                        );
                    }
              	},
                'filter' => FALSE,
            ],
            [
                'attribute' => 'blocked_at',
                'content' => function($model) {
            		    if ($model->blocked_at) {
                        $name = 'Разблокировать';
                        $class = ['class' => 'btn btn-success btn-xs'];
            		    } else {
            		        $name = 'Блокировать';
            		        $class = ['class' => 'btn btn-danger btn-xs'];
            		    }
            		    return Html::a(
            				    $name,
            				    Url::toRoute([
                            '/user/admin/block',
                            'id' => $model->id
            				    ]),
            				    ['class' => $class]
            			   );
                },
                'filter' => FALSE,
            ],
            ['class' => 'yii\grid\ActionColumn',]
    	],
    ]);?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>