<?php

namespace backend\modules\product\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\core\PagesMeta;
use yii\helpers\ArrayHelper;
use common\models\photo\Album;
use common\models\product\Item as BaseItem;
use common\models\core\Page;
use yii\db\ActiveQuery;


class Item extends BaseItem
{

    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['meta_keywords', 'meta_description'], 'string'],
                [['discount'], 'default', 'value' => 0],
                [['brand_id'], 'default', 'value' => NULL],
                [['meta_title'], 'string', 'max' => 255],
                [['uri'], 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z\d_-]*[a-zA-Z\d]$/'],

                [['uri'], 'unique','except' => 'update', 'targetClass'=>Item::className(), 'message'=>'адресс должен быть уникальным'],
                [['articul'], 'unique', 'except' => 'update', 'targetClass'=>Item::className(), 'message'=>'артикул должен быть уникальным'],

                [['articul'], 'unique', 'on' => 'update', 'targetClass' => self::className(), 'message' => 'артикул должен быть уникальным', 'filter' => function (ActiveQuery $query) {
                    $query->andWhere(['<>', 'id', $this->id]);
                }],
                [['uri'], 'unique', 'on' => 'update', 'targetClass' => self::className(), 'message' => 'адресс должен быть уникальным', 'filter' => function (ActiveQuery $query) {
                    $query->andWhere(['<>', 'id', $this->id]);
                }],
            ]
        );
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'meta_title'        => 'Meta title',
                'meta_description'  => 'Meta description',
                'meta_keywords'     => 'Meta keywords',
            ]
        );
    }

    // атрибуты для CRUD-формы
    public function getFormElements()
    {
        $meta = $this->getMeta();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }
        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => 'Items',
                        'active'    => TRUE,
                        'elements'  => [
                            'category_id'         => [
                                                      'type'        => 'dropdownlist',
                                                      'items'       => ArrayHelper::map(Category::find()->all(), 'id', 'title'),
                                                      'attributes'  => ['prompt' => 'Choose category']
                                                  ],
                            'page_id'           => [
                                                      'type' => 'dropdownlist',
                                                      'items' => ArrayHelper::map(Page::find()->where(['=', 'module', 'product'])->all(), 'id', 'header'),
                                                      'attributes' => ['prompt' => 'Choose page'],
                                                  ],
                            'articul'             => ['type' => 'text'],
                            'title'               => ['type' => 'text'],
                            'uri'                 => ['type' => 'text'],
                            'small_description'   => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
                            'description'         => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'price'               => ['type' => 'text'],
                            'count'               => ['type' => 'text'],
                            'discount'            => ['type' => 'dropdownlist', 'items' => $this->getDiscounts(), 'attributes' => ['prompt' => 'Choose discount']],
                            'preview'             => ['type' => 'image', 'functionGetImage' => 'getPreview'],
                            'delivery_id'         => [
                                                      'type'        => 'dropdownlist',
                                                      'items'       => ArrayHelper::map(Delivery::find()->all(), 'id', 'title'),
                                                      'attributes'  => ['prompt' => 'Choose delivery']
                                                  ],
                            'brand_id'         => [
                                                      'type'        => 'dropdownlist',
                                                      'items'       => ArrayHelper::map(Brand::find()->all(), 'id', 'title'),
                                                      'attributes'  => ['prompt' => 'Choose brand']
                                                  ],
                            'photo_album_id'      => [
                                                        'type' => 'dropdownlist',
                                                        'attributes' => ['prompt' => 'Choose Album', ],
                                                        'items' => ArrayHelper::map(Album::find()->all(), 'id', 'name')
                                                    ],
                            'active'              => ['type' => 'checkbox'],
                            'master_id'           => ['type'=>'text'],
                        ],
                    ],
                    [
                        'title'     => 'Meta',
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }
    
    public function behaviors()
    {
        return [
            'image' => [
              'class' => 'common\core\behaviors\UploadFileBehavior',
              'attribute' => 'preview',
              'uploadPath' => Yii::getAlias('@frontend') . '/web/' . $this->filePath,
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_at',
                'updatedAtAttribute' => 'update_at',
                'value' => time(),
            ],
        ];
    }

    // возвращает массив атрибутов, которые будут показаны по view
    public function getViewAttributes()
    {
        return [
            'id',
            [
                'attribute' => 'category_id',
            ],
            'brand_id', 'delivery_id',
            'articul',
            'title',
            'small_description',
            'price',
            'count',
            'discount'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();
    }

    // сохранение мета-тегов для товара
    private function saveMeta()
    {
        $meta = $this->getMeta();
        $page = Page::findOne($this->page_id);
        if (!$meta) {
            $meta           = new PagesMeta();
            $meta->owner_id = $this->id;
            $meta->model    = 'Item';
        }
        $meta->request_path     = $page->uri . '/' . $this->uri;
        $meta->target_path      = 'product/product/' . $this->uri;
        $meta->page_id          = $this->page_id;
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;
        $meta->active           = $this->active;
        $meta->save(FALSE);
    }

    // получение мета-тегов для товара
    private function getMeta()
    {
        return PagesMeta::find()
                              ->where(['=', 'owner_id', $this->id])
                              ->andWhere(['=', 'model', 'Item'])
                              ->one();
    }
    public function addMeta(){
         $meta =$this->getMeta();
         $this->meta_title = $meta->meta_title;
         $this->meta_description = $meta->meta_description;
         $this->meta_keywords = $meta->meta_keywords;
    }
    public function convertToArray(){
        return  ArrayHelper::toArray($this, [
            'backend\modules\product\models\Item' => [
                'id', 'category_id', 'delivery_id','page_id','brand_id','articul','title','uri','small_description',
                'description','price','discount','count','preview', 'active', 'photo_album_id', 'create_at',
                'update_at', 'master_id', 'meta_title', 'meta_description', 'meta_keywords'
            ],
        ]);
    }
  
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $meta =$this->getMeta();
            $meta->delete();
            return true;
        } else {
            return false;
        }
    }

    public static function saveXml(){
        $models = ArrayHelper::index(Item::find()->all(), 'id');
        $meta = ArrayHelper::index(PagesMeta::find()->where(['model' => 'Item'])->all(), 'owner_id');
        foreach ($models as $key=>$val){
            $val->meta_title = $meta[$key]->meta_title;
            $val->meta_description = $meta[$key]->meta_description;
            $val->meta_keywords = $meta[$key]->meta_keywords;
        }
        $res = self::modelsToXml($models, 'magazine', "product");
        //$res =self::testXml();
        return $res;
    }

    private function testXml(){
        $atributeType = [];
        $model = new Item();
        $atributes = self::attributes();
        foreach ($atributes as $key=>$value){
            $atributeType[$value] = $model->getTableSchema()->getColumn($value)->phpType;
        }
        $xmlData = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
        $xmlData .= "\t<magazine>\n";
        for ($i=0; $i<1000; $i++) {
            $xmlData .= "\t\t<product>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"category_id\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"delivery_id\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"page_id\">2</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"brand_id\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"articul\">{$i}abc_r</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"title\">Обновленный</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"uri\">produ{$i}ct1-p1r</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"small_description\">маленькое описание</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"description\">полное описание</column>\n";
                $xmlData .="\t\t\t<column type=\"double\" name=\"price\">10</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"discount\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"count\">21</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"preview\">null</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"active\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"photo_album_id\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"create_at\">1470140361</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"update_at\">1470222892</column>\n";
                $xmlData .="\t\t\t<column type=\"integer\" name=\"master_id\">1</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"meta_title\">title</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"meta_description\">description</column>\n";
                $xmlData .="\t\t\t<column type=\"string\" name=\"meta_keywords\">слова</column>\n";
            $xmlData .= "\t\t</product>\n";
        }
        $xmlData .= "\t</magazine>\n";
        return $xmlData;
    }


    private function modelsToXml($models, $rootElementName, $childElementName)
    {
        $atributeType = [];
        $model = new Item();
        $atributes = self::attributes();
        foreach ($atributes as $key=>$value){
            $atributeType[$value] = $model->getTableSchema()->getColumn($value)->phpType;
        }
        $xmlData = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
        $xmlData .= "\t<" . $rootElementName . ">\n";
        foreach ($models as $model) {
            $xmlData .= "\t\t<" . $childElementName . ">\n";
            foreach($model as $key=>$value){
                $xmlData .= "\t\t\t<column type=\"".$atributeType[$key] ."\" name=\"" . $key . "\">";
                if(!empty($value)){
                    $xmlData .=  $value;
                } else{
                    $xmlData .= "null";
                }
                $xmlData .= "</column>\n";
            }
            $xmlData .= "\t\t\t<column type=\"string\" name=\"meta_title\">". $model->meta_title ."</column>\n";
            $xmlData .= "\t\t\t<column type=\"string\" name=\"meta_description\">". $model->meta_description ."</column>\n";
            $xmlData .= "\t\t\t<column type=\"string\" name=\"meta_keywords\">". $model->meta_keywords ."</column>\n";
            $xmlData .= "\t\t</" . $childElementName . ">\n";
        }
        $xmlData .= "\t</" . $rootElementName . ">\n";
        return $xmlData;
    }
}
