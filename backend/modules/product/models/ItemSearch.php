<?php

namespace backend\modules\product\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\product\models\Item;

/**
 * ItemSearch represents the model behind the search form about `Item`.
 */
class ItemSearch extends Item
{

    public function rules()
    {
        return [
            [['id', 'discount', 'count', 'active', 'photo_album_id', 'create_at', 'update_at', 'category_id', 'brand_id', 'delivery_id'], 'integer'],
            [['articul', 'title', 'small_description', 'description', 'preview'], 'safe'],
            [['price'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'              => $this->id,
            'price'           => $this->price,
            'discount'        => $this->discount,
            'count'           => $this->count,
            'active'          => $this->active,
            'category_id'     => $this->category_id,
            'brand_id'        => $this->brand_id,
            'delivery_id'     => $this->delivery_id,
            'photo_album_id'  => $this->photo_album_id,
            'create_at'       => $this->create_at,
            'update_at'       => $this->update_at,
        ]);

        $query->andFilterWhere(['like', 'articul', $this->articul])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'small_description', $this->small_description])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'preview', $this->preview]);

        return $dataProvider;
    }
}