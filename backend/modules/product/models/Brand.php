<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\Brand as BaseBrand;
use yii\helpers\ArrayHelper;

class Brand extends BaseBrand
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'title'         => ['type' => 'text'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'display_order', 'active'];
    }
}
