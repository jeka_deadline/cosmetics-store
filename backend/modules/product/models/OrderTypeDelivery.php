<?php

namespace backend\modules\product\models;

use Yii;
use common\models\product\OrderTypeDelivery as BaseOrderTypeDelivery;
use yii\helpers\ArrayHelper;

class OrderTypeDelivery extends BaseOrderTypeDelivery
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'title'         => ['type' => 'text'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'display_order', 'active'];
    }
    
}
