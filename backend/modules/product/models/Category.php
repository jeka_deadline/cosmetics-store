<?php

namespace backend\modules\product\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\core\PagesMeta;
use common\models\product\Category as BaseCategory;
use common\models\core\Page;

class Category extends BaseCategory
{

    public $meta_title;
    public $meta_description;
    public $meta_keywords;
    public static $tree;

    public static function tableName()
    {
        return 'product_category';
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order', 'parent_id'], 'default', 'value' => 0],
                [['meta_description', 'meta_keywords'], 'string'],
                [['meta_title'], 'string', 'max' => 255],
                [['uri'], 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z\d_-]*[a-zA-Z\d]$/'],
            ]
        );
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'meta_title'        => 'Meta title',
                'meta_description'  => 'Meta description',
                'meta_keywords'     => 'Meta keywords',
            ]
        );
    }

    public function getFormElements()
    {
        $categ = self::getListCategories(0, 0, $this->id);
        if (!$categ) {
            $categ = [];
        }
        $meta = $this->getMeta();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }
        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => 'Categories',
                        'active'    => TRUE,
                        'elements'  => [
                            'parent_id'     => [
                                                  'type'        => 'dropdownlist',
                                                  'items'       => $categ,
                                                  'attributes'  => ['prompt' => 'Choose subcategory']
                                              ],
                            'page_id'       => [
                                                  'type'        => 'dropdownlist',
                                                  'items'       => ArrayHelper::map(Page::find()->where(['=', 'module', 'product'])->all(), 'id', 'header'),
                                                  'attributes'  => ['prompt' => 'Choose page'],
                                              ],
                            'title'         => ['type' => 'text'],
                            'uri'           => ['type' => 'text'],
                            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'preview'       => ['type' => 'image', 'functionGetImage' => 'getPreview'],
                            'display_order' => ['type' => 'text'],
                            'active'        => ['type' => 'checkbox'],
                        ],
                    ],
                    [
                        'title'     => 'Meta',
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'parent_id', 'title', 'description', 'preview', 'display_order', 'active'];
    }

    public static function getListCategories($pid = 0, $level = 0, $currentId = 0)
    {
        $categories = self::find()->where(['=', 'parent_id', $pid])->all();
        $level      = ($pid == 0) ? 0 : ++$level; 
        foreach ($categories as $category) {
            if ($category->id !== $currentId) {
                self::$tree[ $category->id ] = str_repeat('-', $level * 2) . $category->title;
            }
            self::getListCategories($category->id, $level, $currentId);
        }
        return self::$tree;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();
    }

    private function saveMeta()
    {
        $meta = $this->getMeta();
        $page = Page::findOne($this->page_id);

        if (!$meta) {
            $meta           = new PagesMeta();
            $meta->owner_id = $this->id;
            $meta->model    = 'CategoryProduct';
        }
        $meta->request_path     = $page->uri . '/' . $this->uri;
        $meta->target_path      = 'product/category/' . $this->uri;
        $meta->page_id          = $this->page_id;
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;
        $meta->active           = $this->active;
        $meta->save(FALSE);
    }

    private function getMeta()
    {
        return PagesMeta::find()
                              ->where(['=', 'owner_id', $this->id])
                              ->andWhere(['=', 'model', 'CategoryProduct'])
                              ->one();
    }

    public function behaviors()
    {
        return [
            'image' => [
              'class' => 'common\core\behaviors\UploadFileBehavior',
              'attribute' => 'preview',
              'uploadPath' => Yii::getAlias('@frontend') . '/web/' . $this->filePath,
            ]
        ];
    }

}
