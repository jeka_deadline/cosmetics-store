<?php

use yii\db\Migration;
use common\models\photo\Album;
use common\models\core\Page;

class m160610_104353_products extends Migration
{
    public function safeUp()
    {
        $photoAlbumTablePk  = 'id';
        $pageTablePk        = 'id';

        // таблица категорий
        $this->createTable('{{product_category}}', [
            'id'            => $this->primaryKey(),
            'parent_id'     => $this->integer()->defaultValue(0),
            'page_id'       => $this->integer(11)->notNull(),
            'uri'           => $this->string(255)->notNull(),
            'preview'       => $this->string(255)->defaultValue(NULL),
            'title'         => $this->string(255)->notNull(),
            'description'   => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // создание уникального uri для категории продуктов
        $this->createIndex('ux_product_category_uri', '{{product_category}}', 'uri', TRUE);

        // создание связи таблицы категорий продуктов с таблицей страниц
        $this->createIndex('idx_product_category_page_id', '{{product_category}}', 'page_id');
        $this->addForeignKey('fk_product_category_page_id', '{{product_category}}', 'page_id', Page::tableName(), $pageTablePk, 'CASCADE', 'CASCADE');

        // таблица брендов товара
        $this->createTable('{{product_brand}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(255)->notNull(),
            'display_order'     => $this->integer()->defaultValue(0),
            'active'            => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица сроков поставок
        $this->createTable('{{product_delivery}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(255)->notNull(),
            'display_order'     => $this->integer()->defaultValue(0),
            'active'            => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица товаров
        $this->createTable('{{product_item}}', [
            'id'                => $this->primaryKey(),
            'category_id'       => $this->integer(11)->notNull(),
            'delivery_id'       => $this->integer(11)->notNull(),
            'page_id'          => $this->integer(11)->notNull(),
            'brand_id'          => $this->integer(11)->defaultValue(NULL),
            'articul'           => $this->string(20)->notNull(),
            'title'             => $this->string(255)->notNull(),
            'uri'               => $this->string(255)->notNull(),
            'small_description' => $this->string(255)->notNull(),
            'description'       => $this->text()->notNull(),
            'price'             => $this->float()->notNull(),
            'discount'          => $this->smallInteger(2)->defaultValue(0),
            'count'             => $this->integer()->defaultValue(0),
            'preview'           => $this->string(255)->defaultValue(NULL),
            'active'            => $this->smallInteger(1)->defaultValue(0),
            'photo_album_id'    => $this->integer()->defaultValue(NULL),
            'create_at'         => $this->integer()->notNull(),
            'update_at'         => $this->integer()->notNull(),
            'master_id'         => $this->integer()->defaultValue(NULL),
        ]);

        // создание уникального uri для продукта
        $this->createIndex('ux_product_itemy_uri', '{{product_item}}', 'uri', TRUE);

        // создание связи таблицы продуктов с таблицей страниц
        $this->createIndex('idx_product_item_page_id', '{{product_item}}', 'page_id');
        $this->addForeignKey('fk_product_item_page_id', '{{product_item}}', 'page_id', Page::tableName(), $pageTablePk, 'CASCADE', 'CASCADE');

        // уникальное значение для Артикула
        $this->createIndex('idx_product_item_articul', '{{product_item}}', 'articul', TRUE);

        // создание связи между таблицей товаров и таблицей альбома
        $this->createIndex('idx_product_item_photo_album_id', '{{product_item}}', 'photo_album_id');
        $this->addForeignKey('fk_product_item_photo_album_id', '{{product_item}}', 'photo_album_id', Album::tableName(), $photoAlbumTablePk, 'CASCADE', 'CASCADE');

        // создание связи между таблицей товаров и таблицей категорий товаров
        $this->createIndex('idx_product_item_category_id', '{{product_item}}', 'category_id');
        $this->addForeignKey('fk_product_item_category_id', '{{product_item}}', 'category_id', '{{product_category}}', 'id', 'CASCADE', 'CASCADE');

        // создание связи между таблицей товаров и таблицей сроков поставки
        $this->createIndex('idx_product_item_delivery_id', '{{product_item}}', 'delivery_id');
        $this->addForeignKey('fk_product_item_delivery_id', '{{product_item}}', 'delivery_id', '{{product_delivery}}', 'id', 'CASCADE', 'CASCADE');

        // создание связи между таблицей товаров и таблицей брендов
        $this->createIndex('idx_product_item_brand_id', '{{product_item}}', 'brand_id');
        $this->addForeignKey('fk_product_item_brand_id', '{{product_item}}', 'brand_id', '{{product_brand}}', 'id', 'CASCADE', 'CASCADE');

        //создание связи между полем master_id  и id
        $this->createIndex('idx_product_item_master_id', '{{product_item}}', 'master_id');
        $this->addForeignKey('fk_product_item_master_id', '{{product_item}}', 'master_id', '{{product_item}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_item_brand_id', '{{product_item}}');
        $this->dropForeignKey('fk_product_item_delivery_id', '{{product_item}}');
        $this->dropForeignKey('fk_product_item_category_id', '{{product_item}}');
        $this->dropForeignKey('fk_product_item_photo_album_id', '{{product_item}}');
        $this->dropForeignKey('fk_product_item_page_id', '{{product_item}}');
        $this->dropForeignKey('fk_product_item_master_id', '{{product_item}}');
        $this->dropForeignKey('fk_product_category_page_id', '{{product_category}}');
        $this->dropTable('{{product_item}}');
        $this->dropTable('{{product_brand}}');
        $this->dropTable('{{product_delivery}}');
        $this->dropTable('{{product_category}}');
    }

}
