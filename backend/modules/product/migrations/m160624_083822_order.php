<?php

use yii\db\Migration;
use common\models\product\Item;

class m160624_083822_order extends Migration
{
    public function safeUp()
    {

        $productTablePk = 'id';

        // таблица типов оплаты
        $this->createTable('{{product_order_type_pay}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица типов доставки
        $this->createTable('{{product_order_type_delivery}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица заказов
        $this->createTable('{{product_order}}', [
            'id'                => $this->primaryKey(),
            'order_number'      => $this->integer()->notNull(),
            'to'                => $this->string(255)->notNull(),
            'email'             => $this->string(255)->notNull(),
            'phone'             => $this->string(255)->notNull(),
            'address'           => $this->text()->notNull(),
            'type_pay_id'       => $this->integer()->notNull(),
            'type_delivery_id'  => $this->integer()->notNull(),
            'sum'               => $this->smallInteger(1)->defaultValue(0),
        ]);

        // создание связи между таблицей заказов и таблицей типов оплаты
        $this->createIndex('idx_product_order_type_pay_id', '{{product_order}}', 'type_pay_id');
        $this->addForeignKey('fk_product_order_type_pay_id', '{{product_order}}', 'type_pay_id', '{{product_order_type_pay}}', 'id', 'CASCADE', 'CASCADE');

        // создание связи между таблицей заказов и таблицей типов доставки
        $this->createIndex('idx_product_order_type_delivery_id', '{{product_order}}', 'type_delivery_id');
        $this->addForeignKey('fk_product_order_type_delivery_id', '{{product_order}}', 'type_delivery_id', '{{product_order_type_delivery}}', 'id', 'CASCADE', 'CASCADE');

        // таблица связи между таблицей заказов и таблицей продуктов
        $this->createTable('{{product_links_order_item}}', [
            'id'        => $this->primaryKey(),
            'item_id'   => $this->integer()->notNull(),
            'count'     => $this->integer()->notNull(),
            'order_id'  => $this->integer()->notNull(),
        ]);

        // создание связи между таблицей заказы-продукты и таблицей заказов
        $this->createIndex('idx_product_links_order_item_item_id', '{{product_links_order_item}}', 'item_id');
        $this->addForeignKey('fk_product_links_order_item_item_id', '{{product_links_order_item}}', 'item_id', Item::tableName(), $productTablePk, 'CASCADE', 'CASCADE');

        // создание связи между таблицей заказы-продукты и таблицей продуктов
        $this->createIndex('idx_product_links_order_item_order_id', '{{product_links_order_item}}', 'order_id');
        $this->addForeignKey('fk_product_links_order_item_order_id', '{{product_links_order_item}}', 'order_id', '{{product_order}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_links_order_item_order_id', '{{product_links_order_item}}');
        $this->dropForeignKey('fk_product_links_order_item_item_id', '{{product_links_order_item}}');
        $this->dropTable('{{product_links_order_item}}');
        $this->dropForeignKey('fk_product_order_type_delivery_id', '{{product_order}}');
        $this->dropForeignKey('fk_product_order_type_pay_id', '{{product_order}}');
        $this->dropTable('{{product_order}}');
        $this->dropTable('{{product_order_type_delivery}}');
        $this->dropTable('{{product_order_type_pay}}');
    }

}
