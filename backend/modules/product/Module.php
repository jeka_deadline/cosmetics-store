<?php
namespace backend\modules\product;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\product\controllers';
    public $menuOrder = 3;

    public function init()
    {
        parent::init();
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => 'Products',
                'items' => [
                    [
                        'label' => 'Category',
                        'url'   => '/product/category/index',
                    ],
                    [
                        'label' => 'Brands',
                        'url'   => '/product/brand/index',
                    ],
                    [
                        'label' => 'Delivery',
                        'url'   => '/product/delivery/index',
                    ],
                    [
                        'label' => 'Item',
                        'url'   => '/product/item/index',
                    ],
                ],
            ],
            [
                'label' => 'Order',
                'items' => [
                    [
                        'label' => 'Orders',
                        'url'   => '/product/order/index',
                    ],
                    [
                        'label' => 'Type pay',
                        'url'   => '/product/order-type-pay/index',
                    ],
                    [
                        'label' => 'Type delivery',
                        'url'   => '/product/order-type-delivery/index',
                    ],
                ],
            ]
        ];
    }

    public function getFrontendRoutes()
    {
        return [
            'controllers' => [
                'category' => [
                    'name' => 'Category',
                    'actions' => [
                        'index' => [
                            'name' => 'Вывод списка категорий товаров',
                        ],
                    ]
                ],
                'product' => [
                    'name' => 'Product',
                    'actions' => [
                        'index' => [
                            'name' => 'Вывод списка товаров',
                        ],
                    ]
                ],
                'shopcart' => [
                    'name' => 'Shopcart',
                    'actions' => [
                        'index' => [
                            'name' => 'Вывод страницы корзины',
                        ],
                    ]
                ],

            ]
        ];
    }

}