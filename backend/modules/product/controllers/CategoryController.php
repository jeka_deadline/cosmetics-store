<?php

namespace backend\modules\product\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends BackendController
{

    public $searchModel = 'backend\modules\product\models\CategorySearch';
    public $modelName   = 'backend\modules\product\models\Category';
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Категории товаров',
            ],
            'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Создать категорию товара',
                'modelName'   => $this->modelName,
            ],
            'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить категорию товара',
                'modelName'   => $this->modelName,
            ],
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр категорий товаров',
                'modelName'   => $this->modelName,
            ],
            'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'parent_id'],
            [
                'attribute'   => 'preview',
                'value'       => function($model) {return Html::a(Html::encode($model->preview), $model->getPreview(), ['target' => '_blank']);},
                'filter'      => FALSE,
                'format'      => 'raw',
            ],
            ['attribute'  => 'title'],
            ['attribute'  => 'description'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }

}
