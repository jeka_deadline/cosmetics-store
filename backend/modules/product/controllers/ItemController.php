<?php

namespace backend\modules\product\controllers;

use common\models\core\PagesMeta;
use Yii;
use yii\web\Response;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use common\models\product\Category;
use backend\modules\product\models\Item;
use backend\modules\core\models\ImportXml;
use yii\widgets\ActiveForm;
use yii\base\Exception;
use yii\web\UploadedFile;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends BackendController
{

    public $searchModel = 'backend\modules\product\models\ItemSearch';
    public $modelName   = 'backend\modules\product\models\Item';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    //выбор мастер продукта
    public function actionChoseMasterModel(){
        if(Yii::$app->request->isAjax) {
            $categoryId = Yii::$app->request->post('categoryId');
            $productId = Yii::$app->request->post('productId');
            if(!empty($categoryId)){ //подтягиваем масетр модели по категории
                if ($result = Category::findOne($categoryId)) {
                    $product = Item::find()
                        ->where(['category_id' => $categoryId])
                        ->andWhere(['master_id' => null])
                        ->asArray()
                        ->all();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $product;
                };
            };
            if(!empty($productId)){ //подтягиваем мастер модель
                $model = Item::findOne($productId);
                $model->addMeta();
                $data =$model->convertToArray();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $data;
            };
        };
    }

    //добавление продукта
    public function actionCreate(){
        $model = new Item();
        $title = 'Добавить мастер товар';

        //Ajax валидация
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format='json';
            return ActiveForm::validate($model);
        };
        
        //сохраняем модель
        if(Yii::$app->request->post('Item') != null ){ 
            if($model->load(Yii::$app->request->post()) && $model->validate()){
                $model->save();
            }
            else{
                $errors = serialize($model->errors);
                return $this->render('create', ['model' => $model, 'title' => $title, 'errors'=>$errors]);
            };
        };

        return $this->render('create', ['model' => $model, 'title' => $title]);
    }

    public function actionUpdate($id){
        $model = Item::findOne($id);
        $model->addMeta();
        if(!empty($model)){
            $model->scenario = 'update';
            if($model->master_id!=null){
                $title = 'Обновить товар';
                $master = false;
            }else{
                $title = 'Обновить мастер товар';
                $master =true;
            }

            if (Yii::$app->request->isGet){
                return $this->render('update', ['model'=>$model, 'title'=>$title, 'master'=>$master]);
            }

            if(Yii::$app->request->isPost){
                if($model->load(Yii::$app->request->post())){
                    if(Yii::$app->request->isAjax){
                        Yii::$app->response->format='json';
                        return ActiveForm::validate($model);
                    }
                    else{
                        if($model->validate()) {
                            $model->save();
                            $this->redirect('@web' . '/product/item/index', 302);
                        }else{
                            $errors = $model->errors;
                            return $this->render('update', ['model'=>$model, 'title'=>$title, 'master'=>$master, 'errors'=>$errors]);
                        };
                    };
                }
                else{
                    $errors = $model->errors;
                    return $this->render('update', ['model'=>$model, 'title'=>$title, 'master'=>$master, 'errors'=>$errors]);
                };
            };
        }
        else{
            $this->redirect(['/product/item/index'], 301);
        }
    }

    public function actionConfirmDelete(){
        if(Yii::$app->request->isAjax && Yii::$app->request->post('productId')!=''){
            $id = Yii::$app->request->post('productId');
            $model = Item::findOne($id);
            if(!empty($model)) {
                $master_id = $model->master_id;
                if($master_id!=null) {//дочерний продукт
                    $models[]= $model;
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $model;
                }
                else {//мастер продукт + дочерние
                    $models['master'] = $model;
                    $models[]=Item::find()
                        ->where(['master_id'=> $id])
                        ->all();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return $models;
                }
            }
            else {
                $this->redirect(['/product/item/index'], 301);
            }
        }
    }

    public function actionDelete(){
        if(Yii::$app->request->isAjax && Yii::$app->request->post('productId')!=''){
            $id = Yii::$app->request->post('productId');
            $model = Item::findOne($id);
            if(!empty($model)) {
                $master_id = $model->master_id;
                if($master_id!=null) {//дочерний продукт
                    $model->delete();
                }
                else {//мастер продукт + дочерние
                    $models =Item::find()
                        ->where(['master_id' => $id])
                        ->all();
                    foreach ($models as $value){
                        $value->delete();
                    }
                   $model->delete();
                }
            }
        }
        $this->redirect(['/product/item/index'], 301);
   }

    public function actionExportXml(){
        $xml = Item::saveXml();
        return Yii::$app->response->sendContentAsFile($xml, 'products.xml');
        $this->redirect(['/product/item/index'], 301);
    }

    public function actionImportXml(){
       $model = new ImportXml();
       if (Yii::$app->request->isPost) {
          $model->file = UploadedFile::getInstanceByName('importFile');
          $errors = [];
          $updateCount =0;
          $newCount =0;
          $errorCount = 0;
          if ($model->validate() && $models =$model->upload()) {
              $items = ArrayHelper::index(Item::find()->all(), 'id');
              $product = new Item();
              foreach($models as $key=>$value){
                  $value['master_id'] = ($value['master_id']==0)? null :$value['master_id'];
                  $value['photo_album_id'] = ($value['photo_album_id']==0)? null :$value['photo_album_id'];
                  $value['brand_id'] = ($value['brand_id']==0)? null :$value['brand_id'];
                  $value['meta_title'] = ($value['meta_title']=='')? null :$value['meta_title'];
                  $value['meta_description'] = ($value['meta_description']=='')? null :$value['meta_description'];
                  $value['meta_keywords'] = ($value['meta_keywords']=='')? null :$value['meta_keywords'];
                try{
                   if(array_key_exists($value['id'], $items)){ //обновление
                       $product = $items[$value['id']];
                       $product->attributes=$value;
                       if($product->validate()){
                          $product->save(false);
                          $updateCount++;
                      } else{
                         $errorCount++;
                         $str="При обновлении id= $product->id<br>";
                         foreach ($model->errors as $key=>$value){
                            foreach ($value as $k=>$val){
                               $str .="$key: $val <br>";
                            }
                         }
                         $errors[] = $str;
                      }
                   }
                   else{                                       //добавление нового
                      $product->attributes=$value;
                      $product->id = $value['id'];
                      if($product->validate()){
                         $product->isNewRecord = true;
                         $product->save(false);
                         $newCount++;
                      }
                      else{
                         $errorCount++;
                         $str="При добавлении $product->articul<br>";
                         foreach ($product->errors as $key=>$value){
                            foreach ($value as $k=>$val){
                                $str .="$key: $val <br>";
                            }
                         }
                         $errors[] = $str;
                      }
                   }
                }catch (Exception $ex){
                   $errorCount++;
                   $message = $ex->errorInfo[2];
                   $errors[] = "Ошибка при сохранении: $message";
                }
             }
              $product->attributes =[];
          }
          else{
              $errorCount++;
              $errors[] = "Ошибка загрузки файла"; //ошибка загрузки файла
          }
       }
       $errorStr = '';
       foreach ($errors as $value){
          $errorStr .= "<p>$value</p>";
       }
       Yii::$app->session->setFlash('load', "Загружено $newCount товаров.<br> Обновлено $updateCount товаров. <br> Ошибок {$errorCount}.<br> $errorStr");
      $this->redirect(['/product/item/index'], 301);
    }


    
    
    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Товары',
                'view'        => 'crud-modul-index',
            ],
            /*'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Добавить товар',
                'modelName'   => $this->modelName,
            ],*/
            /*'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить товар',
                'modelName'   => $this->modelName,
            ],*/
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр товарова',
                'modelName'   => $this->modelName,
            ],
           /* 'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],*/
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            [
                'attribute'  => 'category_id',
                'value' => function($model){return $model->category->title;},
                'filter' => Html::activeDropdownList($this->model, 'category_id', ArrayHelper::map(Category::find()->all(), 'id', 'title'), ['class' => 'form-control', 'prompt' => '']),
            ],
            ['attribute'  => 'articul'],
            ['attribute'  => 'title'],
            ['attribute'  => 'price'],
            ['attribute'  => 'count'],
            [
                'attribute'   => 'preview',
                'value'       => function($model) {return Html::a(Html::encode($model->preview), $model->getPreview(), ['target' => '_blank']);},
                'filter'      => FALSE,
                'format'      => 'raw',
            ],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }

}
