<?php

namespace backend\modules\product\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * DeliveryController implements the CRUD actions for Delivery model.
 */
class DeliveryController extends BackendController
{

    public $searchModel = 'backend\modules\product\models\DeliverySearch';
    public $modelName   = 'backend\modules\product\models\Delivery';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Поставки',
            ],
            'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Создать поставку',
                'modelName'   => $this->modelName,
            ],
            'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить поставку',
                'modelName'   => $this->modelName,
            ],
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр списка поставок',
                'modelName'   => $this->modelName,
            ],
            'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'title'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
