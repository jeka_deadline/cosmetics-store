<?php

namespace backend\modules\product\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{

    public $searchModel = 'backend\modules\product\models\OrderTypePaySearch';
    public $modelName   = 'backend\modules\product\models\OrderTypePay';

    public function index()
    {

    }
    
}
