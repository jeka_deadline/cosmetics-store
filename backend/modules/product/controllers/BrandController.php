<?php

namespace backend\modules\product\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * BrandController implements the CRUD actions for Brand model.
 */
class BrandController extends BackendController
{

    public $searchModel = 'backend\modules\product\models\BrandSearch';
    public $modelName   = 'backend\modules\product\models\Brand';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Бренды',
            ],
            'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Создать бренд',
                'modelName'   => $this->modelName,
            ],
            'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить бренд',
                'modelName'   => $this->modelName,
            ],
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр списка брендов',
                'modelName'   => $this->modelName,
            ],
            'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'title'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
