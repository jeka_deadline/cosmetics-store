<?php

namespace backend\modules\coupons\models;

use common\models\coupons\Coupons as BaseCoupons;
use yii;


/**
 * This is the model class for table "coupons".
 *
 * @property integer $id
 * @property string $code
 * @property integer $count
 * @property string $type
 * @property double $value
 */
class Coupons extends BaseCoupons
{
    

}