<?php
namespace backend\modules\coupons;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\coupons\controllers';
    public $defaultRoute = 'index';
    public $menuOrder = 9;

    public function init()
    {
        return parent::init();
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => 'Coupons',
                'url' =>'/coupons/coupons/index',
            ],
        ];
    }



}