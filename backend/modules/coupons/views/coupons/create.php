<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\coupons\models\Coupons */

$this->title = 'Create Coupons';
$this->params['breadcrumbs'][] = ['label' => 'Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'code' =>$code
    ]) ?>

</div>
