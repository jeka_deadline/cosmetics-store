<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\coupons\models\Coupons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupons-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code' )->textInput(['maxlength' => true, 'value' => $code, 'readonly'=>'true']) ?>

    <?= $form->field($model, 'count')->textInput()->hint('Пожалуйста, введите количество купонов') ?>

    <?= $form->field($model, 'type')->dropDownList([ 'fix' => 'Fix', 'percent' => 'Percent', ], ['prompt' => ''])->hint('Пожалуйста, выберите тип') ?>

    <?= $form->field($model, 'value')->textInput()->hint('Пожалуйста, введите значение') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
