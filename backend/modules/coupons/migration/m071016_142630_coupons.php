<?php


use yii\db\Migration;

class m071016_142630_coupons extends Migration{
    public function up()
    {
        // таблица купоны
        $this->createTable('{{coupons}}', [
            'id'            => $this->primaryKey(),
            'code'          => $this->string(255)->unique()->notNull(),
            'count'         => $this->integer(11)->notNull(),
            'type'          => "ENUM('fix', 'percent')",
            'value'         => $this->float()->notNull(),
        ]);

        // создание уникального индекса для поля code
        $this->createIndex('uk_code', '{{coupons}}', ['code'], TRUE);

    }

    public function down()
    {
        $this->dropTable('{{coupons}}');
    }
}