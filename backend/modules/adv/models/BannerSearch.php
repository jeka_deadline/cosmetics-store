<?php

namespace backend\modules\adv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\adv\models\Banner;

/**
 * BannerSearch represents the model behind the search form about `backend\modules\adv\models\Banner`.
 */
class BannerSearch extends Banner
{

    public function rules()
    {
        return [
            [['id', 'bannerplace_id', 'new_window', 'display_order', 'active'], 'integer'],
            [['image', 'alt', 'url'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Banner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bannerplace_id' => $this->bannerplace_id,
            'new_window' => $this->new_window,
            'display_order' => $this->display_order,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'alt', $this->alt])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}