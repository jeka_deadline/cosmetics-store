<?php

namespace backend\modules\adv\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\adv\models\Bannerplace;

/**
 * BannerplaceSearch represents the model behind the search form about `backend\modules\adv\models\Bannerplace`.
 */
class BannerplaceSearch extends Bannerplace
{
    public function rules()
    {
        return [
            [['id', 'display_order', 'active'], 'integer'],
            [['code', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Bannerplace::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'display_order' => $this->display_order,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}