<?php

namespace backend\modules\adv\models;

use Yii;
use common\models\adv\Bannerplace as BaseBannerplace;
use yii\helpers\ArrayHelper;

class Bannerplace extends BaseBannerplace
{

    public function rules()
    {
        return arrayHelper::merge(
            [
                [['display_order'], 'default', 'value' => 0],
                [['code'], 'match', 'pattern' => '/^[a-zA-Z\d][a-zA-Z\d_-]*[a-zA-Z\d]$/']
            ],
            parent::rules()
        );
    }

    public function getFormElements()
    {
        return [
            'code'          => ['type' => 'text'],
            'description'   => ['type' => 'text'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'code', 'description', 'display_order', 'active'];
    }
}
