<?php

namespace backend\modules\adv\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\adv\Banner as BaseBanner;
use common\models\adv\Bannerplace;

class Banner extends BaseBanner
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'bannerplace_id' => [
                                    'type'        => 'dropdownlist',
                                    'items'       => ArrayHelper::map(Bannerplace::find()->all(), 'id', function($model){return $model->code . ' - ' . $model->description;}),
                                    'attributes'  => ['prompt' => 'Choose bannerplace'],
                                ],
            'alt'           => ['type' => 'text'],
            'image'         => ['type' => 'image', 'functionGetImage' => 'getImage'],
            'alt'           => ['type' => 'text'],
            'url'           => ['type' => 'text'],
            'new_window'    => ['type' => 'checkbox'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'bannerplace_id', 'image', 'display_order', 'active'];
    }

    public function behaviors()
    {
        return [
            'image' => [
              'class' => 'common\core\behaviors\UploadFileBehavior',
              'attribute' => 'image',
              'uploadPath' => Yii::getAlias('@frontend') . '/web/' . $this->filePath,
            ]
        ];
    }

    /*public function beforeValidate()
    {
        echo '<pre>';
        print_r($_FILES);
        exit;
    }*/
}
