<?php

namespace backend\modules\adv\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\core\components\BackendController;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends BackendController
{

    public $searchModel = 'backend\modules\adv\models\BannerSearch';
    public $modelName   = 'backend\modules\adv\models\Banner';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Баннеры',
            ],
            'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Добавить баннер',
                'modelName'   => $this->modelName,
                'scenarios'   => 'insert',
            ],
            'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить баннер',
                'modelName'   => $this->modelName,
            ],
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр баннера',
                'modelName'   => $this->modelName,
            ],
            'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'bannerplace_id'],
            [
                'attribute' => 'image',
                'value'     => function($model) {return Html::a(Html::encode($model->image), $model->getImage(), ['target' => '_blank']);},
                'filter'    => FALSE,
                'format'    => 'raw',
            ],
            ['attribute'  => 'alt'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
