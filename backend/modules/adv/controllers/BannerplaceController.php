<?php

namespace backend\modules\adv\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * BannerplaceController implements the CRUD actions for Bannerplace model.
 */
class BannerplaceController extends BackendController
{

    public $searchModel = 'backend\modules\adv\models\BannerplaceSearch';
    public $modelName   = 'backend\modules\adv\models\Bannerplace';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'       => 'backend\modules\core\components\CRUDIndex',
                'title'       => 'Баннероместа',
            ],
            'create' => [
                'class'       => 'backend\modules\core\components\CRUDCreate',
                'title'       => 'Создать баннероместо',
                'modelName'   => $this->modelName,
            ],
            'update' => [
                'class'       => 'backend\modules\core\components\CRUDUpdate',
                'title'       => 'Обновить баннероместо',
                'modelName'   => $this->modelName,
            ],
            'view' => [
                'class'       => 'backend\modules\core\components\CRUDView',
                'title'       => 'Просмотр баннероместа',
                'modelName'   => $this->modelName,
            ],
            'delete' => [
                'class'       => 'backend\modules\core\components\CRUDDelete',
                'modelName'   => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'code'],
            ['attribute'  => 'description'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
}
