<?php

use yii\db\Migration;

class m160614_124839_advertising extends Migration
{
    public function safeUp()
    {
        // таблица баннеромест
        $this->createTable('{{advertising_bannerplaces}}', [
            'id'            => $this->primaryKey(),
            'code'          => $this->string(50)->notNull(),
            'description'   => $this->string(255)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // уникальное значение для кода баннероместа
        $this->createIndex('uix_advertising_bannerplaces_code', '{{advertising_bannerplaces}}', 'code', TRUE);

        // таблица для баннеров
        $this->createTable('{{advertising_banners}}', [
            'id'              => $this->primaryKey(),
            'bannerplace_id'  => $this->integer(11)->notNull(),
            'image'           => $this->string(255)->notNull(),
            'alt'             => $this->string(255)->defaultValue(NULL),
            'url'             => $this->string(255)->defaultValue(NULL),
            'new_window'      => $this->smallInteger(1)->defaultValue(0),
            'display_order'   => $this->integer()->defaultValue(0),
            'active'          => $this->smallInteger(1)->defaultValue(0),
        ]);

        // создание связи между таблицей баннеров и таблицей баннеромест
        $this->createIndex('idx_advertising_banners_bannerplace_id', '{{advertising_banners}}', 'bannerplace_id');
        $this->addForeignKey('fk_advertising_banners_bannerplace_id', '{{advertising_banners}}', 'bannerplace_id', '{{advertising_bannerplaces}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_advertising_banners_bannerplace_id', '{{advertising_banners}}');
        $this->dropTable('{{advertising_banners}}');
        $this->dropTable('{{advertising_bannerplaces}}');
    }

}
