<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\user\Role;
?>

<div class="page page-dashboard">
    <div class="row">

      <div class="col-lg-3">
          <?= $this->render('sidebar-update-user', ['userId' => $user->id]);?>
      </div>
      <div class="col-lg-9">
          <div class="panel panel-default">
              <div class="panel-body">
                  <h3>User managment</h3>
                  <?php $form = ActiveForm::begin(); ?>

                      <?= $form->field($user, 'email')->textInput(); ?>

                      <?= $form->field($user, 'username')->textInput(); ?>

                      <?= $form->field($user, 'password_hash')->passwordInput(['value' => '']); ?>

                      <?= $form->field($user, 'role_id')->dropDownList(ArrayHelper::map(Role::find()->where(['=', 'active', '1'])->all(), 'id', 'name')); ?>

                      <?= Html::submitButton('Update', ['class' => 'btn btn-primary']); ?>

                  <?php ActiveForm::end(); ?>
              </div>
          </div>
      </div>

    </div>
</div>