<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<ul class="list-group">
    <li class="list-group-item"><?= Html::a('Account', Url::toRoute(['/user/admin/update', 'id' => $userId])); ?></li>
    <li class="list-group-item"><?= Html::a('Information', Url::toRoute(['/user/admin/info', 'id' => $userId])); ?></li>
    <li class="list-group-item"><?= Html::a('Profile', Url::toRoute(['/user/admin/profile', 'id' => $userId])); ?></li>
</ul>