<?php
namespace backend\modules\adv;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\adv\controllers';
    public $menuOrder = 5;

    public function init()
    {
        parent::init();
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => 'Adv',
                'items' => [
                    [
                        'label' => 'Bannerplaces',
                        'url'   => '/adv/bannerplace/index',
                    ],
                    [
                        'label' => 'Banners',
                        'url'   => '/adv/banner/index',
                    ],
                ],
            ]
        ];
    }

}