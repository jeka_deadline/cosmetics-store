<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\user\Role;
use yii\helpers\ArrayHelper;
?>
<div class="page page-dashboard">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'username')->textInput(); ?>

        <?= $form->field($model, 'email')->textInput(); ?>

        <?= $form->field($model, 'password_hash')->passwordInput(); ?>

        <?= $form->field($model, 'role_id')->dropDownList(ArrayHelper::map(Role::find()->where(['=', 'active', '1'])->all(), 'id', 'name'), ['prompt' => 'Choose user role']); ?>

        <?= $form->field($model, 'confirm_email_at')->checkbox(); ?>

        <?= Html::submitButton('Create', ['class' => 'btn btn-info']); ?>

    <?php ActiveForm::end(); ?>

</div>