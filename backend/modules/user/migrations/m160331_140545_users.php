<?php

use yii\db\Migration;

class m160331_140545_users extends Migration
{
    public function safeUp()
    {
        // таблица ролей
        $this->createTable('{{user_roles}}', [
      		'id' 			      => $this->primaryKey(),
      		'name' 			    => $this->string(50)->notNull(),
      		'active'        => $this->smallInteger(1)->defaultValue(0),
      		'display_order' => $this->integer()->defaultValue(0),
      		'created_at'    => $this->integer()->notNull(),
      		'update_at'     => $this->integer()->notNull(),
    	]);

    	// создание уникального индекса для поля name
      $this->createIndex('uix_user_roles_name', '{{user_roles}}', 'name', TRUE);

      // таблица для ролей, которым разрешен доступ в админ.панель
      $this->createTable('{{user_roles_access_admin}}',[
          'id'      => $this->primaryKey(),
          'role_id' => $this->integer()->notNull(),
      ]);

      // создание связи таблицы доступов с таблицей ролей
      $this->createIndex('idx_user_roles_access_admin_role_id', '{{user_roles_access_admin}}', 'role_id');
      $this->addForeignKey('fk_user_roles_access_admin_role_id', '{{user_roles_access_admin}}', 'role_id', '{{user_roles}}', 'id', 'CASCADE', 'CASCADE');

      // таблица пользователей
      $this->createTable('{{user_users}}', [
          'id'                    => $this->primaryKey(),
          'email'                 => $this->string(100)->defaultValue(NULL),
          'username'              => $this->string(50)->notNull(),
          'password_hash'         => $this->string(100)->notNull(),
          'reset_password_token'  => $this->integer()->defaultValue(NULL),
          'auth_key'              => $this->string(32)->notNull(),
          'blocked_at'            => $this->integer()->defaultValue(NULL),
          'confirm_email_at'      => $this->integer()->defaultValue(NULL),
          'register_ip'           => $this->string(15)->notNull(),
          'created_at'            => $this->integer()->notNUll(),
          'update_at'             => $this->integer()->notNull(),
          'role_id'			          => $this->integer()->notNull(),
          'login_with_social'     => $this->smallInteger(1)->defaultValue(0),
      ]);

      // создание связи таблицы пользователя с таблицей ролей
      $this->createIndex('idx_user_users_role_id', '{{user_users}}', 'role_id');
      $this->addForeignKey('fk_user_users_role_id', '{{user_users}}', 'role_id', '{{user_roles}}', 'id', 'CASCADE', 'CASCADE');

      // создание уникального индекса для поля email
      $this->createIndex('uix_user_users_email', '{{user_users}}', 'email', TRUE);

      // создание уникального индекса для поля username
      $this->createIndex('uix_user_users_username', '{{user_users}}', 'username', TRUE);

      // таблица профиля пользователя
      $this->createTable('{{user_profiles}}', [
          'id'          => $this->primaryKey(),
          'surname'     => $this->string(30)->defaultValue(NULL),
          'name'        => $this->string(30)->defaultValue(NULL),
          'patronymic'  => $this->string(30)->defaultValue(NULL),
          'date_birth'  => $this->integer()->defaultValue(NULL),
          'sex'         => 'ENUM("w","m", "n") DEFAULT "n"',
          'user_id'     => $this->integer(11)->notNull(),
      ]);

      // создание связи таблицы профиля пользователя с таблицей пользователей
      $this->createIndex('idx_user_profiles_user_id', '{{user_profiles}}', 'user_id');
      $this->addForeignKey('fk_user_profiles_user_id', '{{user_profiles}}', 'user_id', '{{user_users}}', 'id', 'CASCADE', 'CASCADE');

      // таблица соц. аккаунтов пользователя
      $this->createTable('{{user_socials}}', [
          'id'          => $this->primaryKey(),
          'provider'    => $this->string(100)->notNull(),
          'client_id'   => $this->string(100)->notNull(),
          'created_at'  => $this->integer()->notNull(),
          'user_id'     => $this->integer(11)->notNull(),
      ]);

      // создание связи таблицы соц. аккаунтов пользователя с таблицей пользователей
      $this->createIndex('idx_user_socials_user_id', '{{user_socials}}', 'user_id');
      $this->addForeignKey('fk_user_socials_user_id', '{{user_socials}}', 'user_id', '{{user_users}}', 'id', 'CASCADE', 'CASCADE');

      $this->createAdmins();

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_socials_user_id', '{{user_socials}}');
        $this->dropTable('{{user_socials}}');
        $this->dropForeignKey('fk_user_profiles_user_id', '{{user_profiles}}');
        $this->dropTable('{{user_profiles}}');
        $this->dropForeignKey('fk_user_users_role_id', '{{user_users}}');
        $this->dropTable('{{user_users}}');
        $this->dropForeignKey('fk_user_roles_access_admin_role_id', '{{user_roles_access_admin}}');
        $this->dropTable('{{user_roles_access_admin}}');
        $this->dropTable('{{user_roles}}');
    }

    public function createAdmins()
    {
    	$this->batchInsert(
            '{{user_roles}}',
            ['name', 'created_at', 'update_at', 'active', 'display_order'],
    		[
                ['administrator', time(), time(), 1,  10],
                ['user', time(), time(), 1, 20],
            ]
    	);

      $this->insert('{{user_roles_access_admin}}', [
          'role_id' => 1,
      ]);

      $this->insert('{{user_users}}', [
          'email'                 => 'jeka.deadline@gmail.com',
          'username'              => 'admin',
          'password_hash'         => \Yii::$app->getSecurity()->generatePasswordHash('admin'),
          'reset_password_token'  => NULL,
          'auth_key'              => md5(time() . \Yii::$app->getSecurity()->generateRandomString(50)),
          'confirm_email_at'      => time(),
          'register_ip'           => '127.0.0.1',
          'created_at'            => time(),
          'update_at'             => time(),
          'role_id'			          => 1,
      ]);

      $this->insert('{{user_profiles}}', [
          'surname'       => 'Бублик',
          'name'          => 'Евгений',
          'patronymic'    => 'Владимирович',
          'date_birth'    => strtotime('14-05-1992 16:00'),
          'sex'           => 'm',
          'user_id'       => 1,
      ]);
    }

}
