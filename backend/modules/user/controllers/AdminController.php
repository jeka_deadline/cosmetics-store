<?php
namespace backend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\user\models\User;
use backend\modules\user\models\UserSearch;
use backend\modules\user\Module;
use yii\filters\AccessControl;
use backend\modules\user\models\CreateUserForm;
use common\models\user\Profile;

class AdminController extends Controller
{

    public $defaultAction = 'users';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['users', 'confirm-email', 'block', 'delete', 'create-user', 'update', 'info', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionUsers()
    {
        $searchModel  = Yii::createObject(UserSearch::className());
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'   => $searchModel,
            'dataProvider'  => $dataProvider,
        ]);
    }

    public function actionBlock($id = NULL)
    {
        if (!$id) {

        }
        $user = User::findIdentity($id);
        if (!$user) {

        }
        if ($user->isBlocked()) {
            if ($user->unblock()) {
                Yii::$app->session->setFlash('success', 'Пользователь разблокирован');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось разблокировать пользователя');
            }
        } else {
            if (intval($id) === Yii::$app->user->id) {
                Yii::$app->session->setFlash('error', 'Вы не можете заблокировать самого себя');
            } else {
                if ($user->block()) {
                    Yii::$app->session->setFlash('success', 'Пользователь заблокирован');
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось заблокировать пользователя');
                }
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionConfirmEmail($id = NULL)
    {
        if (!$id) {

        }
        $user = User::findIdentity($id);
        if (!$user) {

        }
        if (!$user->isConfirmEmail()) {
            if ($user->confirmEmail()) {
                Yii::$app->session->setFlash('success', '"Электронная почта пользователя подтверждена"');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось подтвердить почту пользователя');
            }
        } else {
            Yii::$app->session->setFlash('info', 'Электронная почта данного пользователя уже подтверждена');
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDelete($id)
    {
        $user = User::findIdentity($id);
        if (!$user) {

        }
        if (intval($id) === Yii::$app->user->id) {
                Yii::$app->session->setFlash('error', 'Вы не можете удалить самого себя');
        } else {
            if ($user->delete()) {
                Yii::$app->session->setFlash('success', 'Пользователь удален');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось удалить пользователя');
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCreateUser()
    {
        $model = Yii::createObject(User::className());
        if ($model->load(Yii::$app->request->post())) {

            $model->confirm_email_at  = ($model->confirm_email_at) ? time() : NULL;
            $model->register_ip       = '127.0.0.1';
            //$model->validate();
            //throw new \Exception(json_encode($model->getErrors()));

            //$model->auth_key          = md5(time() . Yii::$app->getSecurity()->generateRandomString(50));
            if ($model->validate() && $model->save()) {
                Yii::$app->session->setFlash('succes', 'Пользователь успешно создан');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        return $this->render('create-user', ['model' => $model]);
    }

    public function actionUpdate($id = NULL)
    {
        if (!$id) {

        }
        $user = User::findIdentity($id);
        if (empty($user)) {

        }
        return $this->render('update-user', ['user' => $user]);
    }

    public function actionProfile($id = NULL)
    {
        if (!$id) {

        }
        $profile = Profile::find()->where(['=', 'user_id', $id])->one();
        if (empty($profile)) {

        }
        if ($profile->load(Yii::$app->request->post()) && $profile->validate()) {
            $profile->save();
        }

        return $this->render('update-user-profile', ['profile' => $profile]);
    }

    /*public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!Yii::$app->user->can($action->id)) {
                throw new \yii\web\ForbiddenHttpException('Access denied');
            }
            return TRUE;
        }
        return FALSE;
    }*/

}