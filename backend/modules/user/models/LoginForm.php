<?php
namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
//use common\models\user\User;
use common\models\user\AccessAdmin;
use yii\helpers\ArrayHelper;

class LoginForm extends Model
{

	public $username;
	public $password;
	public $rememberMe;
	private $user = FALSE;

	public function rules()
	{
		return [
			[['username', 'password'], 'required'],
			[['password'], 'validatePassword'],
			[['rememberMe'], 'boolean'],
			//[['username'], 'filter', 'trim'],
		];
	}

		public function validatePassword()
		{
				$user = $this->getUser();
				if ($user) {
						$listAccess = ArrayHelper::map(AccessAdmin::find()->all(), 'id', 'id');
						if (!in_array($user->role->id, $listAccess) || !Yii::$app->getSecurity()->validatePassword($this->password, $user->password_hash)) {
								$this->addError('username', 'Access danied');
						}
				} else {
						$this->addError('username', 'Access danied');
				}
		}

	public function login()
	{
			if ($this->validate()) {
					$user = User::findByUserNameOrEmail($this->username);
					Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
					return TRUE;
			}
			return FALSE;
	}

	private function getUser()
	{
		if ($this->user === FALSE) {
			$this->user = User::findByUserNameOrEmail($this->username);
		}
		return $this->user;
	}

}