<?php

namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Role;

class UserSearch extends User
{
    public function rules()
    {
        return [
            [['id', 'blocked_at', 'confirm_email_at', 'created_at', 'update_at', 'role_id'], 'integer'],
            [['email', 'username', 'password_hash', 'auth_key', 'register_ip', 'reset_password_token'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' 				=> $this->id,
            'blocked_at' 		=> $this->blocked_at,
            'confirm_email_at' 	=> $this->confirm_email_at,
            'created_at'	 	=> $this->created_at,
            'update_at' 		=> $this->update_at,
            'role_id' 			=> $this->role_id,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'reset_password_token', $this->reset_password_token])
            ->andFilterWhere(['like', 'register_ip', $this->register_ip]);

        return $dataProvider;
    }

}