<?php
namespace backend\modules\user\models;

use Yii;
use yii\base\Model;
use common\models\user\User;

class CreateUserForm extends Model
{

    public $email;
    public $password;
    public $confirmEmail = FALSE;
    public $username;
    public $roleId;

    public function rules()
    {

        return [
            [['email', 'password', 'username', 'roleId'], 'required'],
            ['email', 'email'],
            ['password', 'string', 'min' => 6,],
            ['roleId', 'integer'],
            ['confirmEmail', 'boolean'],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'operator' => '==='],
            ['username', 'match', 'pattern' => '/[A-Za-z0-9_-]{4,50}/'],
            ['username', 'unique', 'targetClass' => User::className()],
            ['email', 'unique', 'targetClass' => User::className()],
        ];

    }

    public function create()
    {
        if ($this->validate()) {
            $user                   = Yii::createObject(User::className());
            $user->email            = $this->email;
            $user->username         = $this->username;
            $user->role_id          = $this->roleId;
            $user->password_hash    = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $user->register_ip      = '127.0.0.1';
            $user->auth_key         = time() . '_' . Yii::$app->getSecurity()->generateRandomString(21);
            $user->confirm_email_at = ($this->confirmEmail) ? time() : NULL;
            $user->save();
            return TRUE;
        }
        return FALSE;
    }

}