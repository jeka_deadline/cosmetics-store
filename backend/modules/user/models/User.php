<?php
namespace backend\modules\user\models;

class User extends \common\models\user\User
{

	public function unblock()
	{
      return (bool)$this->updateAttributes(['blocked_at' => NULL]);
	}

	public function block()
	{
      return (bool)$this->updateAttributes(['blocked_at' => time()]);
	}

}