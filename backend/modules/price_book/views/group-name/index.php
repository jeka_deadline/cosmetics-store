<?php

use backend\modules\price_book\models\GroupType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price_book\models\GroupNameSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Group Names';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-name-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Group Name', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'label'=>'Type',
                'attribute' => 'typename.name',
                'filter' => Html::activeDropDownList($searchModel, 'type', ArrayHelper::map(GroupType::find()->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => 'Выберите тип группы']),
            ],
            [
                'attribute' => 'active',
                'filter'    => Html::activeDropDownList($searchModel, 'active', ['0' => 'Not Active', '1' => 'Active'], ['class' => 'form-control', 'prompt' => 'All']),
                'value'     => function($searchModel) {
                    return ($searchModel->active) ? '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                },
                'format'    => 'html',
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
