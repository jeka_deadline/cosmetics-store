<?php

use backend\modules\price_book\models\GroupType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\price_book\models\GroupName */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-name-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(ArrayHelper::map(GroupType::find()->all(), 'id', 'name'), ['prompt'=>'Выберите тип группы'])->label('Group type') ?>

    <?= $form->field($model, 'active')->dropDownList(['0' => 'Not Active', '1' => 'Active'],['prompt' => 'Статус'])?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
