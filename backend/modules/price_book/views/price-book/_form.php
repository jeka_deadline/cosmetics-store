<?php


use backend\modules\price_book\models\GroupName;
use backend\modules\product\models\Item;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\price_book\models\PriceBook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $form->field($model, 'product_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Item::find()->all(), 'id', 'articul'),
        'options' => ['placeholder' => 'Выберите продукт'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'product_id')->dropDownList(ArrayHelper::map(Item::find()->all(), 'id', 'articul') ) ?>

    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'group_id')->dropDownList(ArrayHelper::map(GroupName::find()->all(), 'id', 'name'), ['prompt' => 'Выберите группу']);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
