<?php

use backend\modules\price_book\models\GroupName;
use backend\modules\price_book\models\GroupType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price_book\models\PriceBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Price Book';
$this->params['breadcrumbs'][] = $this->title;

$groups = [];
$type = GroupType::find()->all();
foreach ($type as $value){
    $groups[$value->name] = ArrayHelper::map(GroupName::find()->where(['type'=> $value->id])->all(), 'id', 'name');
}



?>
<div class="price-book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php
    if(Yii::$app->session->hasFlash('load')):?>
        <div class="alert alert-danger">
            <?php echo Yii::$app->session->getFlash('load'); ?>
        </div>
    <?php endif; ?>
    <p>
        <?= Html::a('Create Price Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'product_id',
            [
                'attribute' => 'product.title',
            ],
            [
                'attribute' => 'product.articul',
            ],
            'price',
            [
                'attribute' => 'group.name',
                'filter' => Html::activeDropDownList($searchModel, 'group_id', $groups, ['class' => 'form-control', 'prompt' => 'Выберите группу']),
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<a href="<?= Url::toRoute(['price-book/export-xml'])?>" class="btn btn-success" id="price_book">Экспорт price book в XML</a>

<?= Html::beginForm(['price-book/import-xml'], 'post', ['enctype' => 'multipart/form-data', 'id'=>'import']) ?>

<div class=" btn btn-success file-upload">
    <label>
        <?= Html::fileInput('importFile','', ['id'=>'importFlie']) ?>
        <span>Импорт price book из XML</span>
    </label>
</div>
<?= Html::endForm() ?>

<?php
$this->registerJs("
        $('#importFlie').on('change', function(event){
             $('#import').submit();
        }); 
        setTimeout(function() { $('.alert').hide('slow'); }, 10000);
        ");

?>
