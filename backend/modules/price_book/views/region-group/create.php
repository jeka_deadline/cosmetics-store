<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\price_book\RegionGroup */

$this->title = 'Create Region Group';
$this->params['breadcrumbs'][] = ['label' => 'Region Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <?= Html::input('text', 'ip',  '', ['id'=>'ip', 'class' => 'ip chekip', 'placeholder' => '127.0.0.1', 'pattern'=>"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"]) ?>
    <span class="btn btn-success " id="check_ip">проверить IP</span>
    <div id="region"></div>
    <div class="clearfix"></div>
</div>
<div class="region-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
