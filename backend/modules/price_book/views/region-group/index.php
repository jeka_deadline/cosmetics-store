<?php

use backend\modules\price_book\models\GroupName;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price_book\models\RegionGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Region Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Region Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'group.name',
                 'filter' => Html::activeDropDownList($searchModel, 'group_id', ArrayHelper::map(GroupName::find()->where(['type' => 1])->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' => 'Выберите группу']),
            ],
            'region',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
