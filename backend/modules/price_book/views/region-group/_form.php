<?php

use backend\modules\price_book\models\GroupName;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\price_book\RegionGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="region-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(GroupName::find()->where(['type' => 1])->all(), 'id', 'name'), ['prompt' => 'Выберите группу'])->label('Group name') ?>


    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
