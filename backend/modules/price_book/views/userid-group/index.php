<?php

use backend\modules\price_book\models\GroupName;
use backend\modules\user\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\price_book\models\UseridGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Userid Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userid-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Userid Group', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'label' =>'User ID',
                'attribute' => 'user.id',
               // 'filter' =>Html::activeInput('text', $searchModel, 'user_id'),

            ],
            [
                'label' => 'User Name',
                'attribute' => 'user.username',
                'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::find()->all(), 'id', 'username'), ['class' => 'form-control', 'prompt' => 'Выберите пользователя']),
            ],
            [
                'label' =>'Group Name',
                'attribute' => 'group.name',
                'filter' => Html::activeDropDownList($searchModel, 'group_id', ArrayHelper::map(GroupName::find()->where(['type' => 2])->all(), 'id', 'name'), ['class' => 'form-control', 'prompt' =>'Выберите группу']),
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
