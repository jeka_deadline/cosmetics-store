<?php

use backend\modules\price_book\models\GroupName;
use backend\modules\user\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\price_book\models\UseridGroup */
/* @var $form yii\widgets\ActiveForm */

$a = ArrayHelper::map(User::find()->all(), 'id', 'username');
?>

<div class="userid-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(GroupName::find()->where(['type' => 2])->all(), 'id', 'name'), ['prompt' => 'Выберите группу'])->label('Group name') ?>

    <!--//вставить select2-->
    <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(User::find()->all(),'id','username')) ?>
   
    <?php
    $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
        'options' => ['placeholder' => 'Select user'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
