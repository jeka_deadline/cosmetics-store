<?php

namespace backend\modules\price_book\models;

use Yii;
use common\models\price_book\RegionGroup as BaseGroup;
use yii\helpers\ArrayHelper;


class RegionGroup extends BaseGroup
{


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(GroupName::className(), ['id' => 'group_id']);
    }
}