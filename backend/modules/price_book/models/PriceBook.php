<?php

namespace backend\modules\price_book\models;

use backend\modules\product\models\Item;
use yii;
use common\models\price_book\PriceBook as BasePriceBook;

use yii\helpers\ArrayHelper;


class PriceBook extends BasePriceBook
{
    public $product_articul;
    public $product_title;

    public static function saveXml()
    {
        $sql = 'SELECT pb.id,pb.product_id, pi.articul AS product_articul, pi.title AS product_title,  pb.price, pb.group_id
                FROM price_book pb
                INNER JOIN product_item pi
                ON(pi.id = pb.product_id)';
        $price_book = PriceBook::findBySql($sql)->all();
        $res = self::modelsToXml($price_book, 'price_book', "product");
        return $res;
    }

    private static function modelsToXml($models, $rootElementName, $childElementName)
    {
        $atributeType = [];
        $model = new PriceBook();
        $atributes = self::attributes();
        foreach ($atributes as $key=>$value){
            $atributeType[$value] = $model->getTableSchema()->getColumn($value)->phpType;
        }
        $xmlData = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
        $xmlData .= "\t<" . $rootElementName . ">\n";
        foreach ($models as $model) {
            $xmlData .= "\t\t<" . $childElementName . ">\n";
            foreach($model as $key=>$value){
                $xmlData .= "\t\t\t<column type=\"".$atributeType[$key] ."\" name=\"" . $key . "\">";
                if(!empty($value)){
                    $xmlData .=  $value;
                } else{
                    $xmlData .= "null";
                }
                $xmlData .= "</column>\n";
            }
            $xmlData .= "\t\t\t<column type=\"string\" name=\"product_articul\">". $model->product_articul ."</column>\n";
            $xmlData .= "\t\t\t<column type=\"string\" name=\"product_title\">". $model->product_title ."</column>\n";
            $xmlData .= "\t\t</" . $childElementName . ">\n";
        }
        $xmlData .= "\t</" . $rootElementName . ">\n";
        return $xmlData;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(GroupName::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Item::className(), ['id' => 'product_id']);
    }
 }