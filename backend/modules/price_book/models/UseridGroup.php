<?php

namespace backend\modules\price_book\models;

use backend\modules\user\models\User;
use Yii;
use common\models\price_book\UseridGroup as BaseGroup;



class UseridGroup extends BaseGroup
{


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(GroupName::className(), ['id' => 'group_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
}