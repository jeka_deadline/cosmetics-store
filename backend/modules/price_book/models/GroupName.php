<?php

namespace backend\modules\price_book\models;

use common\models\product\Item;
use common\models\price_book\GroupName as BaseGroupName;
use yii;

/**
 * This is the model class for table "group_name".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $active
 *
 * @property GroupType $type0
 * @property PriceBook[] $priceBooks
 * @property Item[] $products
 * @property RegionGroup[] $regionGroups
 * @property UseridGroup[] $useridGroups
 */
class GroupName extends BaseGroupName
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypename()
    {
        return $this->hasOne(GroupType::className(), ['id' => 'type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceBooks()
    {
        return $this->hasMany(PriceBook::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Item::className(), ['id' => 'product_id'])->viaTable('price_book', ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegionGroups()
    {
        return $this->hasMany(RegionGroup::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUseridGroups()
    {
        return $this->hasMany(UseridGroup::className(), ['group_id' => 'id']);
    }
}
