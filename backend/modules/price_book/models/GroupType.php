<?php

namespace backend\modules\price_book\models;

use Yii;

/**
 * This is the model class for table "group_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property GroupName[] $groupNames
 */
class GroupType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
            [['name'], 'unique', 'message'=>'поле должно быть уникальным']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupNames()
    {
        return $this->hasMany(GroupName::className(), ['type' => 'id']);
    }
}
