<?php

namespace backend\modules\price_book\controllers;

use backend\modules\core\models\ImportXml;
use yii\base\Exception;
use yii;
use backend\modules\price_book\models\PriceBook;
use backend\modules\price_book\models\PriceBookSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PriceBookController implements the CRUD actions for PriceBook model.
 */
class PriceBookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PriceBook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PriceBookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PriceBook model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PriceBook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PriceBook();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PriceBook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PriceBook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PriceBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PriceBook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PriceBook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Export PriceBook model from DB to XML.
     * @return Save price_book.xml file
     */
    public function actionExportXml(){
        $xml = PriceBook::saveXml();
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_XML;
        $response->sendContentAsFile($xml, 'price_book.xml')->send();
    }

    /**
     * Import from XML to PriceBook table.
     * @return Flash message to index page
     * Redirect to index page
     */
    public function actionImportXml(){
        $model = new ImportXml();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstanceByName('importFile');
            $errors = [];
            $updateCount =0;
            $newCount =0;
            $errorCount = 0;
            if ($model->validate() && $models =$model->upload()) {
                $priceBooks = ArrayHelper::index(PriceBook::find()->all(), 'id');
                $priceBook = new PriceBook();
                foreach ($models as $key => $value) {
                    try {
                        if (array_key_exists($value['id'], $priceBooks)) { //обновление
                            $priceBook = $priceBooks[$value['id']];
                            $priceBook->attributes=$value;
                            if($priceBook->validate()){
                                $priceBook->save(false);
                                $updateCount++;
                            } else{
                                $errorCount++;
                                $str="При обновлении id= $priceBook->id<br>";
                                foreach ($priceBook->errors as $key=>$value){
                                    foreach ($value as $k=>$val){
                                        $str .="$key: $val <br>";
                                    }
                                }
                                $errors[] = $str;
                            }
                        } else {                                        //добавление нового
                            $priceBook->attributes=$value;
                            $priceBook->id = $value['id'];
                            if($priceBook->validate()){
                                $priceBook->isNewRecord = true;
                                $priceBook->save(false);
                                $newCount++;
                            }
                            else{
                                $errorCount++;
                                $str="При добавлении записи ID: $priceBook->id<br>";
                                foreach ($priceBook->errors as $key=>$value){
                                    foreach ($value as $k=>$val){
                                        $str .="$key: $val <br>";
                                    }
                                }
                                $errors[] = $str;
                            }
                        }
                    } catch (Exception $ex) {
                        $errorCount++;
                        $message = $ex->errorInfo[2];
                        $errors[] = "Ошибка при сохранении: $message";
                    }
                }
            }
            else{
                $errorCount++;
                $errors[] = "Ошибка загрузки файла"; //ошибка загрузки файла
            }
        }
        $errorStr = '';
        foreach ($errors as $value){
            $errorStr .= "<p>$value</p>";
        }
        Yii::$app->session->setFlash('load', "Загружено $newCount товаров.<br> Обновлено $updateCount товаров. <br> Ошибок {$errorCount}.<br> $errorStr");
        $this->redirect(['/price_book/price-book/index'], 301);
    }

}
