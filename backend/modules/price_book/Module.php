<?php
namespace backend\modules\price_book;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\price_book\controllers';
    public $defaultRoute = 'index';
    public $menuOrder = 8;

    public function init()
    {
        return parent::init();
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => 'Price Book',
                'items' => [
                    [
                        'label' => 'Create Users Group',
                        'url'   => '/price_book/group-name/index',
                    ],
                    [
                        'label' => 'Add Region',
                        'url'   => '/price_book/region-group/index',
                    ],
                    [
                        'label' => 'Add User to Group',
                        'url'   => '/price_book/userid-group/index',
                    ],
                    [
                        'label' => 'Price Book',
                        'url'   => '/price_book/price-book/index',
                    ],
                ],
            ],
            
        ];
    }

    

}