<?php


use yii\db\Migration;

class m160906_115650_price_book extends Migration{
    public function up()
    {
        // таблица тип групп
        $this->createTable('{{group_type}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(50)->notNull()->unique(),
        ]);

        // таблица справочник групп
        $this->createTable('{{group_name}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(50)->notNull()->unique(),
            'type'          => $this->integer(11)->notNull(),
            'active'        => $this->boolean()->defaultValue(1),
        ]);

        // таблица привязка региона к группе
        $this->createTable('{{region_group}}', [
            'id'            => $this->primaryKey(),
            'group_id'      => $this->integer(11)->notNull(),
            'region'        => $this->string(255)->notNull(),
        ]);

        // таблица привязка пользователя к группе
        $this->createTable('{{userid_group}}', [
            'id'            => $this->primaryKey(),
            'user_id'       => $this->integer(11)->notNull(),
            'group_id'      => $this->integer(11)->notNull(),
        ]);

        // таблица прайсбук 
        $this->createTable('{{price_book}}', [
            'id'            => $this->primaryKey(),
            'product_id'    => $this->integer(11)->notNull(),
            'price'         => $this->float()->notNull(),
            'group_id'      => $this->integer(11)->notNull(),
        ]);


        $this->createIndex('uk_group_type', '{{group_type}}', ['name'], TRUE);

        // создание связи между таблицами c типами групп и названиями
        $this->createIndex('idx_group_name_type', '{{group_name}}', 'type');
        $this->createIndex('uk_group_name', '{{group_name}}', ['name'], TRUE);
        $this->addForeignKey('fk_group_name_group_type', '{{group_name}}', 'type', '{{group_type}}', 'id', 'CASCADE', 'CASCADE');

        //создание связи между таблицами с названиями групп и регионами
        $this->createIndex('idx_region_group_group_id', '{{region_group}}', 'group_id');
        $this->addForeignKey('fk_region_group_group_name', '{{region_group}}', 'group_id', '{{group_name}}', 'id', 'CASCADE', 'CASCADE');

        // создание связи между таблицами c названиями групп и группами по ид пользователя
        $this->createIndex('idx_userid_group_group_id', '{{userid_group}}', 'group_id');
        $this->createIndex('idx_userid_group_user_id', '{{userid_group}}', 'user_id');
        $this->addForeignKey('fk_userid_group_group_name','{{userid_group}}','group_id','{{group_name}}','id','CASCADE','CASCADE');
        $this->addForeignKey('fk_userid_group_user_users','{{userid_group}}','user_id','{{user_users}}','id','CASCADE','CASCADE');

        // создание связи между таблицами прайсбук и группами
        $this->createIndex('idx_price_book_product_id', '{{price_book}}', 'product_id');
        $this->createIndex('idx_price_book_group_id', '{{price_book}}', 'group_id');
        $this->createIndex('uk_price_book', '{{price_book}}', ['product_id','group_id'], TRUE);
        $this->addForeignKey('fk_price_book_product_item', '{{price_book}}', 'product_id', '{{product_item}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_price_book_group_name', '{{price_book}}', 'group_id', '{{group_name}}', 'id', 'CASCADE', 'CASCADE');



    }

    public function down()
    {
        $this->dropForeignKey('fk_group_name_group_type', '{{group_name}}');
        $this->dropForeignKey('fk_region_group_group_name', '{{region_group}}');
        $this->dropForeignKey('fk_useid_group_group_name', '{{userid_group}}');
        $this->dropForeignKey('fk_useid_group_user_users', '{{userid_group}}');
        $this->dropForeignKey('fk_price_book_product_item', '{{price_book}}');
        $this->dropForeignKey('fk_price_book_group_name', '{{price_book}}');
        $this->dropTable('{{group_type}}');
        $this->dropTable('{{group_name}}');
        $this->dropTable('{{region_group}}');
        $this->dropTable('{{userid_group}}');
        $this->dropTable('{{price_book}}');
    }
}