<?php

use yii\db\Migration;

class m160610_124342_photo extends Migration
{
    public function up()
    {

        // таблица фотоальбомов
        $this->createTable('{{photo_album}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(50)->notNull(),
            'description'   => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица фотографий
        $this->createTable('{{photo_image}}', [
            'id'            => $this->primaryKey(),
            'album_id'      => $this->integer(11)->notNull(),
            'image'         => $this->string(255)->notNull(),
            'title'         => $this->string(50)->notNull(),
            'alt'           => $this->string(50)->defaultValue(NULL),
            'description'   => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
        ]);

        // создание связи между таблицей фотографий и фотоальбомами
        $this->createIndex('idx_photo_image_album_id', '{{photo_image}}', 'album_id');
        $this->addForeignKey('fk_photo_image_album_id', '{{photo_image}}', 'album_id', '{{photo_album}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk_photo_image_album_id', '{{photo_image}}');
        $this->dropTable('{{photo_image}}');
        $this->dropTable('{{photo_album}}');
    }

}
