<?php

namespace backend\modules\photo\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * AlbumController implements the CRUD actions for Album model.
 */
class AlbumController extends Controller
{

    public $searchModel = 'backend\modules\photo\models\AlbumSearch';
    public $modelName   = 'common\models\photo\Album';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'           => 'backend\modules\core\components\CRUDIndex',
                'searchModel'     => $this->searchModel,
                'title'           => 'Альбомы',
            ],
            'create' => [
                'class'           => 'backend\modules\core\components\CRUDCreate',
                'title'           => 'Создать альбом',
                'modelName'       => $this->modelName,
            ],
            'update' => [
                'class'           => 'backend\modules\core\components\CRUDUpdate',
                'title'           => 'Обновить альбом',
                'modelName'       => $this->modelName,
            ],
            'view' => [
                'class'           => 'backend\modules\core\components\CRUDView',
                'title'           => 'Просмотр альбома',
                'modelName'       => $this->modelName,
            ],
            'delete' => [
                'class'           => 'backend\modules\core\components\CRUDDelete',
                'modelName'       => $this->modelName,
            ],
        ];
    }

    public function getColumns()
    {
        return [

            ['class'      => 'yii\grid\SerialColumn'],
            ['attribute'  => 'name'],
            ['attribute'  => 'description'],
            ['attribute'  => 'display_order'],
            ['attribute'  => 'active'],
            ['class'      => 'yii\grid\ActionColumn'],

        ];
    }
}
