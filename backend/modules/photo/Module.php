<?php
namespace backend\modules\photo;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\photo\controllers';
    public $menuOrder = 4;

    public function init()
    {
        parent::init();
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => 'Albums',
                'url'   => '/photo/album/index',
            ],
        ];
    }

}