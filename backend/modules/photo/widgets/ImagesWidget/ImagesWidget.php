<?php
namespace backend\modules\photo\widgets\ImagesWidget;

use yii\base\Widget;

class ImagesWidget extends Widget
{

    public $model;

    public $attribute;

    public $images = [];

    public $template = 'default';

    public $nameField = 'Album-image';

    public function run()
    {
        /*$modelName = get_class($this->model);
        $modelName = join('', array_slice(explode('\\', $modelName), -1));*/
        return $this->render($this->template, [
            'model'     => $this->model,
            'attribute' => $this->attribute,
            'images'    => $this->images,
            'nameField' => $this->nameField,
        ]);
    }

}