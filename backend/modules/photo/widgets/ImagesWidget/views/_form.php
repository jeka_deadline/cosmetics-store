<?php
use yii\helpers\Html;
?>
<div>

    <div class="col-lg-4">
        <?= Html::img($imagePath, ['class' => 'img-responsive', 'style' => 'display:block', 'alt' => $imageAlt]); ?>
    </div>
    <div class="col-lg-6">
        <?= Html::label($model->getAttributeLabel('title'), NULL); ?>
        <?= Html::textInput($nameField . '[' . $iterator . '][title]', $imageTitle, ['class' => 'form-control']); ?>
        <?= Html::label($model->getAttributeLabel('description'), NULL); ?>
        <?= Html::textarea($nameField . '[' . $iterator . '][description]', $imageDescription, ['class' => 'form-control', 'rows' => 5]); ?>
    </div>
    <div class="col-lg-2">
        <?= Html::tag('span', 'Remove', ['class' => 'btn btn-danger', 'data-remove-action' => '/admin/photo/image/delete-image']); ?>
        <?php if (isset($imageId)) : ?>
            <?= Html::hiddenInput($nameField . '[' . $iterator . '][delete]', $imageId); ?>
        <?php endif; ?>
    </div>

</div>