<?php

namespace backend\modules\news\models;

use Yii;
use common\models\news\Category as BaseCategory;
use yii\helpers\ArrayHelper;
use common\models\core\PagesMeta;
use common\models\core\Page;

class Category extends BaseCategory
{

    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
                [['meta_description', 'meta_keywords'], 'string'],
                [['meta_title'], 'string', 'max' => 255],
                [['uri'], 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z\d_-]*[a-zA-Z\d]$/'],
            ]
        );
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'meta_title'        => 'Meta title',
                'meta_description'  => 'Meta description',
                'meta_keywords'     => 'Meta keywords',
            ]
        );
    }

    public function getFormElements()
    {
        $meta = $this->getMeta();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }
        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => 'Categories',
                        'active'    => TRUE,
                        'elements'  => [
                            'page_id'       => ['type' => 'dropdownlist', 'items' => ArrayHelper::map(Page::find()->where(['=', 'module', 'news'])->all(), 'id', 'header'), 'attributes' => ['prompt' => 'Choose page'],
                            ],
                            'title'         => ['type' => 'text'],
                            'uri'           => ['type' => 'text'],
                            'description'   => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'display_order' => ['type' => 'text'],
                            'active'        => ['type' => 'checkbox'],
                        ],
                    ],
                    [
                        'title'     => 'Meta',
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'description', 'display_order', 'active'];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();
    }

    private function saveMeta()
    {
        $meta = $this->getMeta();
        $page = Page::findOne($this->page_id);
        if (!$page) {
            return FALSE;
        }
        if (!$meta) {
            $meta           = new PagesMeta();
            $meta->owner_id = $this->id;
            $meta->model    = 'CategoryNews';
        }
        $meta->request_path     = $page->uri . '/' . $this->uri;
        $meta->target_path      = 'news/category/' . $this->uri;
        $meta->page_id          = $this->page_id;
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;
        $meta->active           = $this->active;
        $meta->save(FALSE);
    }

    private function getMeta()
    {
        return PagesMeta::find()
                              ->where(['=', 'owner_id', $this->id])
                              ->andWhere(['=', 'model', 'CategoryNews'])
                              ->one();
    }

}
