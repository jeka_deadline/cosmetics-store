<?php

namespace backend\modules\news\models;

use Yii;
use common\models\news\News as BaseNews;
use yii\helpers\ArrayHelper;
use common\models\core\PagesMeta;
use common\models\user\User;
use common\components\DateConverter;
use common\models\news\LinksNewsCategory;
use yii\helpers\Html;
use common\models\core\Page;

class News extends BaseNews
{
    public $meta_title;
    public $meta_description;
    public $meta_keywords;
    public $categories;

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
                [['meta_description', 'meta_keywords'], 'string'],
                [['meta_title'], 'string', 'max' => 255],
                [['uri'], 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z\d_-]*[a-zA-Z\d]$/'],
            ]
        );
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            parent::attributeLabels(),
            [
                'meta_title'        => 'Meta title',
                'meta_description'  => 'Meta description',
                'meta_keywords'     => 'Meta keywords',
            ]
        );
    }

    // атрибуты для CRUD-формы
    public function getFormElements()
    {
        $meta = $this->getMeta();
        $this->setCategories();

        if ($meta) {
            $this->meta_title       = $meta->meta_title;
            $this->meta_description = $meta->meta_description;
            $this->meta_keywords    = $meta->meta_keywords;
        }
        return [
            'main' => [
                'type' => 'tabs',
                'itemTabs' => [
                    [
                        'title'     => 'News',
                        'active'    => TRUE,
                        'elements'  => [
                            'page_id'           => [
                                                      'type' => 'dropdownlist',
                                                      'items' => ArrayHelper::map(Page::find()->where(['=', 'module', 'news'])->all(), 'id', 'header'),
                                                      'attributes' => ['prompt' => 'Choose page'],
                                                  ],
                            'title'             => ['type' => 'text'],
                            'uri'               => ['type' => 'text'],
                            'small_description' => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
                            'description'       => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'date'              => [
                                                      'type' => 'datetime',
                                                      'attributes' => [],
                                                  ],
                            'user_id'           => [
                                                      'type'        => 'dropdownlist',
                                                      'items'       => ArrayHelper::map(User::find()->all(), 'id', 'id'),
                                                      'attributes'  => ['prompt' => 'Choose user']
                                                  ],
                            'image'             => ['type' => 'image', 'functionGetImage' => 'getImage'],
                            'categories'        => [
                                                      'type' => 'checkboxlist',
                                                      'items' => ArrayHelper::map(Category::find()->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'title'),
                                                      'attributes' => [
                                                          'item' => function($index, $label, $name, $checked, $value) {
                                                              return $this->getTemplateForCheckboxList($index, $label, $name, $checked, $value);
                                                          }
                                                      ],
                                                  ],
                            'display_order'     => ['type' => 'text'],
                            'active'            => ['type' => 'checkbox'],
                        ],
                    ],
                    [
                        'title'     => 'Meta',
                        'active'    => FALSE,
                        'elements'  => [
                            'meta_title'        => ['type' => 'text'],
                            'meta_description'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                            'meta_keywords'     => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
                        ],
                    ],
                ],
            ],
        ];
    }
    
    public function behaviors()
    {
        return [
            'image' => [
              'class' => 'common\core\behaviors\UploadFileBehavior',
              'attribute' => 'image',
              'uploadPath' => Yii::getAlias('@frontend') . '/web/' . $this->filePath,
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->date = DateConverter::dateToTimestamp($this->date);
            
            return TRUE;
        }
        return FALSE;
    }

    // возвращает массив атрибутов, которые будут показаны по view
    public function getViewAttributes()
    {
        return [
            'id',
            [
                'attribute' => 'user_id',
            ],
            'small_description',
            'display_order'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveMeta();
        $categories = (!empty($_POST[ 'News' ][ 'categories' ])) ? $_POST[ 'News' ][ 'categories' ] : [];
        $this->saveCategories($categories);
    }

    // метод для сохранения мета-тегов товара
    private function saveMeta()
    {
        $meta = $this->getMeta();
        $page = Page::findOne($this->page_id);
        if (!$page) {
            return FALSE;
        }
        if (!$meta) {
            $meta           = new PagesMeta();
            $meta->owner_id = $this->id;
            $meta->model    = 'News';
        }
        $meta->request_path     = $page->uri . '/' . $this->uri;
        $meta->target_path      = 'news/news/' . $this->uri;
        $meta->page_id          = $this->page_id;
        $meta->meta_title       = $this->meta_title;
        $meta->meta_description = $this->meta_description;
        $meta->meta_keywords    = $this->meta_keywords;
        $meta->active           = $this->active;
        $meta->save(FALSE);
    }

    // получение мета-тегов для товара
    private function getMeta()
    {
        return PagesMeta::find()
                            ->where(['=', 'owner_id', $this->id])
                            ->andWhere(['=', 'model', 'News'])
                            ->one();
    }

    // метод для сохранения связей между новостью и ее категориями
    private function saveCategories($categories)
    {
        // получаем список категорий, к которым относится данная новость
        $listCategories = ArrayHelper::map($this->linksNewsCategories, 'id', function($model){ return $model;});
        
        // перебираем все новости, которые были отмечены
        foreach ($categories as $categoryId) {

            // если нет отмеченной новости в списке текущих связей,
            // тогда сохраняем связь
            if (!isset($listCategories[ $categoryId ])) {
                $link               = new LinksNewsCategory();
                $link->news_id      = $this->id;
                $link->category_id  = $categoryId;
                $link->save();
            } else {
                unset($listCategories[ $categoryId ]);
            }
        }

        // Удаляем новости, которые были unchecked
        foreach ($listCategories as $model) {
            $model->delete();
        }
    }

    public function setCategories()
    {
        $this->categories = ArrayHelper::getColumn($this->linksNewsCategories, 'category_id');
    }

    // метод который генерирует шаблон для списка чекбоксов
    public function getTemplateForCheckboxList($index, $label, $name, $checked, $value)
    {
        return "<div class='checkbox'><label>" . Html::checkbox($name, $checked, ['value' => $value]) . "{$label}</label></div>";
    }

}
