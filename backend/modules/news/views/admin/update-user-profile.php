<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\user\User;
use trntv\yii\datetime\DateTimeWidget;
?>

<div class="page page-dashboard">
    <div class="row">

      <div class="col-lg-3">
          <?= $this->render('sidebar-update-user', ['userId' => $profile->user_id]);?>
      </div>
      <div class="col-lg-9">
          <div class="panel panel-default">
              <div class="panel-body">
                  <h3>User managment</h3>
                  <?php $form = ActiveForm::begin(); ?>

                      <?= $form->field($profile, 'surname')->textInput(); ?>

                      <?= $form->field($profile, 'name')->textInput(); ?>

                      <?= $form->field($profile, 'patronymic')->textInput(); ?>

                      <?= ''/*$form->field($profile, 'date_birth')->widget(DateTimeWidget::className(), [
                              //'phpDatetimeFormat' => 'dd/MM/yyyy HH:mm',
                              'clientOptions' => [
                                  'allowInputToggle' => false,
                                  'sideBySide' => false,
                                  'widgetPositioning' => [
                                     'horizontal' => 'auto',
                                     'vertical' => 'auto'
                                  ]
                              ]
                          ]);*/
                      ?>

                      <?= $form->field($profile, 'sex')->dropDownList(User::getItemsSex()); ?>

                      <?= Html::submitButton('Update', ['class' => 'btn btn-primary']); ?>

                  <?php ActiveForm::end(); ?>
              </div>
          </div>
      </div>

    </div>
</div>