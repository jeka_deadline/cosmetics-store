<?php

use yii\db\Migration;
use common\models\user\User;
use common\models\core\Page;

class m160610_135250_news extends Migration
{
    public function safeUp()
    {
        $userTablePk = 'id';
        $pageTablePk = 'id';

        // таблица категорий новостей
        $this->createTable('{{news_category}}', [
            'id'                => $this->primaryKey(),
            'page_id'           => $this->integer(11)->notNull(),
            'title'             => $this->string(255)->notNull(),
            'uri'               => $this->string(255)->notNull(),
            'description'       => $this->text()->defaultValue(NULL),
            'display_order'     => $this->integer()->defaultValue(0),
            'active'            => $this->smallInteger(1)->defaultValue(0),
        ]);

        // создание уникального uri для категории новости
        $this->createIndex('ux_news_category_uri', '{{news_category}}', 'uri', TRUE);

        // создание связи таблицы категорий с таблицей страниц
        $this->createIndex('idx_news_category_page_id', '{{news_category}}', 'page_id');
        $this->addForeignKey('fk_news_category_page_id', '{{news_category}}', 'page_id', Page::tableName(), $pageTablePk, 'CASCADE', 'CASCADE');

        // таблица новостей
        $this->createTable('{{news_news}}', [
            'id'                => $this->primaryKey(),
            'page_id'           => $this->integer(11)->notNull(),
            'title'             => $this->string(255)->notNull(),
            'uri'               => $this->string(255)->notNull(),
            'small_description' => $this->string(255)->defaultValue(NULL),
            'description'       => $this->text()->defaultValue(NULL),
            'image'             => $this->string(255)->defaultValue(NULL),
            'date'              => $this->integer()->notNull(),
            'user_id'           => $this->integer(11)->notNull(),
            'display_order'     => $this->integer()->defaultValue(0),
            'active'            => $this->smallInteger(1)->defaultValue(0),
        ]);

        // создание уникального uri для новости
        $this->createIndex('ux_news_news_uri', '{{news_news}}', 'uri', TRUE);

        // создание связи таблицы новостей с таблицей пользователей
        $this->createIndex('idx_news_news_user_id', '{{news_news}}', 'user_id');
        $this->addForeignKey('fk_news_news_user_id', '{{news_news}}', 'user_id', User::tableName(), $userTablePk, 'CASCADE', 'CASCADE');

        // создание связи таблицы новостей с таблицей страниц
        $this->createIndex('idx_news_news_page_id', '{{news_news}}', 'page_id');
        $this->addForeignKey('fk_news_news_page_id', '{{news_news}}', 'page_id', Page::tableName(), $pageTablePk, 'CASCADE', 'CASCADE');

        // таблица связи новости-категории
        $this->createTable('{{links_news_category}}', [
            'id'          => $this->primaryKey(),
            'news_id'     => $this->integer(11)->notNull(),
            'category_id' => $this->integer(11)->notNull(),
        ]);

        // создание связи таблицы новости-категории с таблицей новостей
        $this->createIndex('idx_links_news_category_news_id', '{{links_news_category}}', 'news_id');
        $this->addForeignKey('fk_links_news_category_news_id', '{{links_news_category}}', 'news_id', '{{news_news}}', 'id', 'CASCADE', 'CASCADE');

        // создание связи таблицы новости-категории с таблицей категории
        $this->createIndex('idx_links_news_category_category_id', '{{links_news_category}}', 'category_id');
        $this->addForeignKey('fk_links_news_category_category_id', '{{links_news_category}}', 'category_id', '{{news_category}}', 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_links_news_category_category_id', '{{links_news_category}}');
        $this->dropForeignKey('fk_links_news_category_news_id', '{{links_news_category}}');
        $this->dropTable('{{links_news_category}}');
        $this->dropForeignKey('fk_news_news_user_id', '{{news_news}}');
        $this->dropForeignKey('fk_news_news_page_id', '{{news_news}}');
        $this->dropTable('{{news_news}}');
        $this->dropForeignKey('fk_news_category_page_id', '{{news_category}}');
        $this->dropTable('{{news_category}}');
    }

}
