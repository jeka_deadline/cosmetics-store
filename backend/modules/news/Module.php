<?php
namespace backend\modules\news;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\news\controllers';
    public $menuOrder = 2;

    public function init()
    {
        parent::init();
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => 'News',
                'items' => [
                    [
                        'label' => 'Category',
                        'url'   => '/news/category/index',
                    ],
                    [
                        'label' => 'News',
                        'url'   => '/news/news/index',
                    ],
                ],
            ]
        ];
    }

    public function getFrontendRoutes()
    {
        return [
            'controllers' => [
                'category' => [
                    'name' => 'Category',
                    'actions' => [
                        'index' => [
                            'name' => 'Вывод списка категорий новостей',
                        ],
                    ]
                ],
                'news' => [
                    'name' => 'News',
                    'actions' => [
                        'index' => [
                            'name' => 'Вывод списка новостей',
                        ],
                    ]
                ],
            ]
        ];
    }

}