<?php
use yii\helpers\Html;
?>
<div id="list-photos" data-download-image='/admin/photo/image/upload-ajax-image'>

    <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($images as $image) : ?>
              <div>

                  <div class="col-lg-4">
                      <?= Html::img($image->getImagePath(), ['class' => 'img-responsive', 'style' => 'display:block', 'alt' => $image->title]); ?>
                  </div>
                  <div class="col-lg-6">
                      <?= Html::label($model->getAttributeLabel('title'), NULL); ?>
                      <?= Html::textInput($nameField . '[' . $i . '][title]', $image->title, ['class' => 'form-control']); ?>
                      <?= Html::label($model->getAttributeLabel('description'), NULL); ?>
                      <?= Html::textarea($nameField . '[' . $i . '][description]', $image->description, ['class' => 'form-control', 'rows' => 5]); ?>
                  </div>
                  <div class="col-lg-2">
                      <?= Html::tag('span', 'Remove', ['class' => 'btn btn-danger', 'data-remove-action' => '/admin/photo/image/delete-image']); ?>
                      <?= Html::hiddenInput($nameField . '[' . $i . '][delete]', $image->id); ?>
                  </div>

              </div>

        <?php endforeach; ?>

    </div>
    <div class="buttons">
        <?= Html::tag('span', 'Add images', ['class' => 'btn btn-primary', 'id' => 'btn-add-image']); ?>
    </div>

    <input type="file" id="upload-image-button" name="<?= $nameField . '[' . $attribute . ']'; ?>" multiple>

</div>

<?php
    $this->registerCss('
        #upload-image-button {
            display:none;
        }

        #list-photos > .row > div {
            overflow: hidden;
            margin-bottom: 10px;
      }
    ');
    $this->registerJs('
        jQuery("#btn-add-image").click(function() {
            jQuery("#upload-image-button").click();
        });

        jQuery(document).on("change", "#upload-image-button", function (e) {
            if (jQuery(this).val()) {
                jQuery.ajax({
                    url: jQuery("#list-photos").attr("data-download-image"),
                    type: "POST",
                    dataType: "JSON",
                    data: {"images": jQuery(this).val()}
                }).done(function(data){ console.log(data)});
            }
        });
    ');  
?>