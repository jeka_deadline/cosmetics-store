<?php
namespace backend\modules\tags;

class Module extends \yii\base\Module
{

    public $controllerNamespace = 'backend\modules\tags\controllers';

    public function init()
    {
        parent::init();
    }

}