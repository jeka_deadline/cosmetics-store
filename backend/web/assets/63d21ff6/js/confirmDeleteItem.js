$(function() {
    $('.popup-modal').on('click', function(e){
        e.preventDefault();
        var value = $(this).attr('data-id');
        $.ajax({
            type: 'POST',
            url: 'confirm-delete',
            data: {'productId': value},
            dataType: 'json',
            success: function(data){
                var outModal = $('#modalConfirmDelete');
                outModal.removeClass('hide');
                var modal = $('#confirm');
                modal.empty();
                if('master' in data){  //обработка мастер товара и его дочерних
                    $('#yes').attr('data-id', data['master']['id']);
                    var count = data[0].length;
                    var h4 = document.createElement('h4');
                    if(count>1){
                        h4.textContent ='Вы действительно собираетесь удалить мастер товар и '+ count + ' дочерних?'
                    }else
                    if(count==1){
                        h4.textContent = 'Вы действительно собираетесь удалить мастер товар и '+ count + ' дочерний?'
                    }
                    else{
                        h4.textContent ='Вы действительно собираетесь удалить мастер товар?'
                    }
                    var ul = document.createElement('ul');
                    var li = document.createElement('li');
                    var text = 'Артикул: ' + data['master']['articul'] + '. Название: '+data['master']['title'];
                    li.textContent = text;
                    ul.appendChild(li);
                    var innerUl = document.createElement('ul');
                    $.each(data[0], function(key, val){
                        var li = document.createElement('li');
                        var text = 'Артикул: '+ val['articul'] + '. Название: '+ val['title'];
                        console.log(text);
                        li.textContent = 'Артикул: '+ val['articul'] + '. Название: '+ val['title'];
                        innerUl.appendChild(li);
                    });
                    if(innerUl.firstChild){
                        ul.appendChild(innerUl);
                    };
                    modal.append(h4);
                    modal.append(ul);
                }
                else{
                    $('#yes').attr('data-id', data['id']);
                    var h4 = document.createElement('h4');
                    h4.textContent = 'Вы действительно собираетесь удалить товар? '
                    var ul = document.createElement('ul');
                    var li = document.createElement('li');
                    var text = 'Артикул: ' + data['articul'] + '. Название: '+data['title'];
                    li.textContent = text;
                    ul.appendChild(li);
                    modal.append(h4);
                    modal.append(ul);
                }
                var pos = $(window).scrollTop() + 100;
                $('#modalCont').css({'top' : pos+'px'});
            },
            error: function (xhr, str) {}
        });
    });
    
    $('#modalCont').on('click', '#cancel', function(event){
        var outModal = $('#modalConfirmDelete');
        outModal.addClass('hide');
    });

    $('#modalCont').on('click', '#yes', function(event){
        var value = $('#yes').attr('data-id');
        $.ajax({
            type: 'POST',
            url: 'delete',
            data: {'productId': value},
            dataType: 'json',
            success: function(data){
            },
            error: function (xhr, str) {}
        });
    });
 });
