<?php
return [
    'core' => [
        'class' => 'backend\modules\core\Module',
    ],
    'user' => [
        'class' => 'backend\modules\user\Module',
    ],
    'photo' => [
        'class' => 'backend\modules\photo\Module',
    ],
    'product' => [
        'class' => 'backend\modules\product\Module',
    ],
    'adv' => [
        'class' => 'backend\modules\adv\Module',
    ],
    'news' => [
        'class' => 'backend\modules\news\Module',
    ],
    'price_book' =>[
        'class' => 'backend\modules\price_book\Module',
    ],
    'coupons' =>[
        'class' => 'backend\modules\coupons\Module',
    ],
]
?>