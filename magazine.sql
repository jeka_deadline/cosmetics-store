-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 31 2016 г., 10:38
-- Версия сервера: 5.5.48
-- Версия PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `magazine`
--

-- --------------------------------------------------------

--
-- Структура таблицы `advertising_bannerplaces`
--

CREATE TABLE IF NOT EXISTS `advertising_bannerplaces` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `advertising_banners`
--

CREATE TABLE IF NOT EXISTS `advertising_banners` (
  `id` int(11) NOT NULL,
  `bannerplace_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `new_window` smallint(1) DEFAULT '0',
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_attachments`
--

CREATE TABLE IF NOT EXISTS `core_attachments` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `display_order` int(11) DEFAULT '0',
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_contacts`
--

CREATE TABLE IF NOT EXISTS `core_contacts` (
  `id` int(11) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_menu_pages`
--

CREATE TABLE IF NOT EXISTS `core_menu_pages` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_pages`
--

CREATE TABLE IF NOT EXISTS `core_pages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `uri` varchar(100) NOT NULL,
  `route` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `menu_class` varchar(255) DEFAULT NULL,
  `content` text,
  `template_id` int(11) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `core_pages`
--

INSERT INTO `core_pages` (`id`, `parent_id`, `uri`, `route`, `module`, `header`, `menu_class`, `content`, `template_id`, `display_order`, `active`) VALUES
(1, 0, '/', 'core/index/index', 'core', 'Главная', NULL, NULL, 1, 0, 1),
(2, 0, 'product', 'product/product/index', 'product', 'товары', '', '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `core_pages_meta`
--

CREATE TABLE IF NOT EXISTS `core_pages_meta` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `request_path` varchar(255) NOT NULL,
  `target_path` varchar(255) NOT NULL,
  `active` smallint(1) DEFAULT '0',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `meta_keywords` text
) ENGINE=InnoDB AUTO_INCREMENT=1770 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `core_pages_meta`
--

INSERT INTO `core_pages_meta` (`id`, `owner_id`, `model`, `page_id`, `request_path`, `target_path`, `active`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 1, 'Pages', 1, '/', 'core/index/index', 1, NULL, NULL, NULL),
(2, 2, 'Pages', 2, 'product', 'product/product/index', 1, '', '', ''),
(647, 1, 'Item', 2, 'product/product1-p1r', 'product/product/product1-p1r', 1, '', 'description', 'слова'),
(648, 3, 'Item', 2, 'product/product2-p1r', 'product/product/product2-p1r', 0, 'title 2', 'description 2', 'keywords 2'),
(649, 7, 'Item', 2, 'product/test-1', 'product/product/test-1', 1, 'title 6', 'description 6', 'keywords 6'),
(650, 130, 'Item', 2, 'product/product1-p11', 'product/product/product1-p11', 1, NULL, 'description', 'keywords'),
(651, 132, 'Item', 2, 'product/product2-p121', 'product/product/product2-p121', 0, 'title 2', 'description 2', 'keywords 2'),
(652, 136, 'Item', 2, 'product/test-11', 'product/product/test-11', 1, 'title 6', 'description 6', 'keywords 6'),
(653, 138, 'Item', 2, 'product/product1-p112', 'product/product/product1-p112', 1, NULL, 'description', 'keywords'),
(654, 140, 'Item', 2, 'product/product2-p1212', 'product/product/product2-p1212', 0, 'title 2', 'description 2', 'keywords 2'),
(655, 144, 'Item', 2, 'product/test-112', 'product/product/test-112', 1, 'title 6', 'description 6', 'keywords 6'),
(656, 146, 'Item', 2, 'product/product1-p1123', 'product/product/product1-p1123', 1, NULL, 'description', 'keywords'),
(657, 148, 'Item', 2, 'product/product2-p12123', 'product/product/product2-p12123', 0, 'title 2', 'description 2', 'keywords 2'),
(658, 152, 'Item', 2, 'product/test-1123', 'product/product/test-1123', 1, 'title 6', 'description 6', 'keywords 6'),
(659, 154, 'Item', 2, 'product/test-1r', 'product/product/test-1r', 1, 'title 6', 'description 6', 'keywords 6'),
(660, 158, 'Item', 2, 'product/test-112r', 'product/product/test-112r', 1, 'title 6', 'description 6', 'keywords 6'),
(661, 160, 'Item', 2, 'product/product1-p1123r', 'product/product/product1-p1123r', 1, NULL, 'description', 'keywords'),
(662, 162, 'Item', 2, 'product/product2-p12123r', 'product/product/product2-p12123r', 0, 'title 2', 'description 2', 'keywords 2'),
(663, 166, 'Item', 2, 'product/test-1123r', 'product/product/test-1123r', 1, 'title 6', 'description 6', 'keywords 6'),
(664, 169, 'Item', 2, 'product/product1-p11r', 'product/product/product1-p11r', 1, NULL, 'description', 'keywords'),
(665, 171, 'Item', 2, 'product/product2-p121r', 'product/product/product2-p121r', 0, 'title 2', 'description 2', 'keywords 2'),
(666, 175, 'Item', 2, 'product/test-11r', 'product/product/test-11r', 1, 'title 6', 'description 6', 'keywords 6'),
(667, 176, 'Item', 2, 'product/product1-p112r', 'product/product/product1-p112r', 1, NULL, 'description', 'keywords'),
(668, 178, 'Item', 2, 'product/product2-p1212r', 'product/product/product2-p1212r', 0, 'title 2', 'description 2', 'keywords 2'),
(669, 179, 'Item', 2, 'product/product1-p1rq', 'product/product/product1-p1rq', 1, NULL, 'description', 'keywords'),
(670, 181, 'Item', 2, 'product/product2-p1rq', 'product/product/product2-p1rq', 0, 'title 2', 'description 2', 'keywords 2'),
(671, 185, 'Item', 2, 'product/test-1rq', 'product/product/test-1rq', 1, 'title 6', 'description 6', 'keywords 6'),
(672, 187, 'Item', 2, 'product/product1-p11rq', 'product/product/product1-p11rq', 1, NULL, 'description', 'keywords'),
(673, 189, 'Item', 2, 'product/product2-p121rd', 'product/product/product2-p121rd', 0, 'title 2', 'description 2', 'keywords 2'),
(674, 193, 'Item', 2, 'product/test-11rd', 'product/product/test-11rd', 1, 'title 6', 'description 6', 'keywords 6'),
(675, 195, 'Item', 2, 'product/product1-p112dr', 'product/product/product1-p112dr', 1, NULL, 'description', 'keywords'),
(676, 197, 'Item', 2, 'product/product2-p121d2r', 'product/product/product2-p121d2r', 0, 'title 2', 'description 2', 'keywords 2'),
(677, 201, 'Item', 2, 'product/test-112fr', 'product/product/test-112fr', 1, 'title 6', 'description 6', 'keywords 6'),
(678, 203, 'Item', 2, 'product/product1-p11f23r', 'product/product/product1-p11f23r', 1, NULL, 'description', 'keywords'),
(679, 205, 'Item', 2, 'product/product2-p121f23r', 'product/product/product2-p121f23r', 0, 'title 2', 'description 2', 'keywords 2'),
(680, 209, 'Item', 2, 'product/test-1123rf', 'product/product/test-1123rf', 1, 'title 6', 'description 6', 'keywords 6'),
(681, 211, 'Item', 2, 'product/product1-pe1rq', 'product/product/product1-pe1rq', 1, NULL, 'description', 'keywords'),
(682, 213, 'Item', 2, 'product/product2-p1trq', 'product/product/product2-p1trq', 0, 'title 2', 'description 2', 'keywords 2'),
(683, 217, 'Item', 2, 'product/test-1ryq', 'product/product/test-1ryq', 1, 'title 6', 'description 6', 'keywords 6'),
(684, 219, 'Item', 2, 'product/product1-p11yrq', 'product/product/product1-p11yrq', 1, NULL, 'description', 'keywords'),
(685, 221, 'Item', 2, 'product/product2-p121yrd', 'product/product/product2-p121yrd', 0, 'title 2', 'description 2', 'keywords 2'),
(686, 2, 'Item', 2, 'product/product1-p2r', 'product/product/product1-p2r', 1, 'title 1', 'description 1', 'keywords 1'),
(687, 4, 'Item', 2, 'product/product2-p3r', 'product/product/product2-p3r', 1, 'title 3', 'description 3', 'keywords 3'),
(688, 5, 'Item', 2, 'product/product1-p111r', 'product/product/product1-p111r', 1, 'title 4', 'description 4', 'keywords 4'),
(689, 6, 'Item', 2, 'product/product1-p123r', 'product/product/product1-p123r', 0, 'title 5', 'description 5', 'keywords 5'),
(690, 129, 'Item', 2, 'product/test-1-1', 'product/product/test-1-1', 1, NULL, NULL, NULL),
(691, 131, 'Item', 2, 'product/product1-p21', 'product/product/product1-p21', 1, 'title 1', 'description 1', 'keywords 1'),
(692, 133, 'Item', 2, 'product/product2-p31', 'product/product/product2-p31', 1, 'title 3', 'description 3', 'keywords 3'),
(693, 134, 'Item', 2, 'product/product1-p1112', 'product/product/product1-p1112', 1, 'title 4', 'description 4', 'keywords 4'),
(694, 135, 'Item', 2, 'product/product1-p1231', 'product/product/product1-p1231', 0, 'title 5', 'description 5', 'keywords 5'),
(695, 137, 'Item', 2, 'product/test-1-11', 'product/product/test-1-11', 1, NULL, NULL, NULL),
(696, 139, 'Item', 2, 'product/product1-p212', 'product/product/product1-p212', 1, 'title 1', 'description 1', 'keywords 1'),
(697, 141, 'Item', 2, 'product/product2-p312', 'product/product/product2-p312', 1, 'title 3', 'description 3', 'keywords 3'),
(698, 142, 'Item', 2, 'product/product1-p11122', 'product/product/product1-p11122', 1, 'title 4', 'description 4', 'keywords 4'),
(699, 143, 'Item', 2, 'product/product1-p12312', 'product/product/product1-p12312', 0, 'title 5', 'description 5', 'keywords 5'),
(700, 145, 'Item', 2, 'product/test-1-112', 'product/product/test-1-112', 1, NULL, NULL, NULL),
(701, 147, 'Item', 2, 'product/product1-p2123', 'product/product/product1-p2123', 1, 'title 1', 'description 1', 'keywords 1'),
(702, 149, 'Item', 2, 'product/product2-p3123', 'product/product/product2-p3123', 1, 'title 3', 'description 3', 'keywords 3'),
(703, 150, 'Item', 2, 'product/product1-p111223', 'product/product/product1-p111223', 1, 'title 4', 'description 4', 'keywords 4'),
(704, 151, 'Item', 2, 'product/product1-p123123', 'product/product/product1-p123123', 0, 'title 5', 'description 5', 'keywords 5'),
(705, 153, 'Item', 2, 'product/test-1-1123', 'product/product/test-1-1123', 1, NULL, NULL, NULL),
(706, 155, 'Item', 2, 'product/product2-p312r', 'product/product/product2-p312r', 1, 'title 3', 'description 3', 'keywords 3'),
(707, 156, 'Item', 2, 'product/product1-p11122r', 'product/product/product1-p11122r', 1, 'title 4', 'description 4', 'keywords 4'),
(708, 157, 'Item', 2, 'product/product1-p12312r', 'product/product/product1-p12312r', 0, 'title 5', 'description 5', 'keywords 5'),
(709, 159, 'Item', 2, 'product/test-1-112r', 'product/product/test-1-112r', 1, NULL, NULL, NULL),
(710, 161, 'Item', 2, 'product/product1-p2123r', 'product/product/product1-p2123r', 1, 'title 1', 'description 1', 'keywords 1'),
(711, 163, 'Item', 2, 'product/product2-p3123r', 'product/product/product2-p3123r', 1, 'title 3', 'description 3', 'keywords 3'),
(712, 164, 'Item', 2, 'product/product1-p111223r', 'product/product/product1-p111223r', 1, 'title 4', 'description 4', 'keywords 4'),
(713, 165, 'Item', 2, 'product/product1-p123123r', 'product/product/product1-p123123r', 0, 'title 5', 'description 5', 'keywords 5'),
(714, 167, 'Item', 2, 'product/test-1-1123r', 'product/product/test-1-1123r', 1, NULL, NULL, NULL),
(715, 168, 'Item', 2, 'product/test-1-1r', 'product/product/test-1-1r', 1, NULL, NULL, NULL),
(716, 170, 'Item', 2, 'product/product1-p21r', 'product/product/product1-p21r', 1, 'title 1', 'description 1', 'keywords 1'),
(717, 172, 'Item', 2, 'product/product2-p31r', 'product/product/product2-p31r', 1, 'title 3', 'description 3', 'keywords 3'),
(718, 173, 'Item', 2, 'product/product1-p1112r', 'product/product/product1-p1112r', 1, 'title 4', 'description 4', 'keywords 4'),
(719, 174, 'Item', 2, 'product/product1-p1231r', 'product/product/product1-p1231r', 0, 'title 5', 'description 5', 'keywords 5'),
(720, 177, 'Item', 2, 'product/product1-p212r', 'product/product/product1-p212r', 1, 'title 1', 'description 1', 'keywords 1'),
(721, 180, 'Item', 2, 'product/product1-p2rq', 'product/product/product1-p2rq', 1, 'title 1', 'description 1', 'keywords 1'),
(722, 182, 'Item', 2, 'product/product2-p3rq', 'product/product/product2-p3rq', 1, 'title 3', 'description 3', 'keywords 3'),
(723, 183, 'Item', 2, 'product/product1-p111rq', 'product/product/product1-p111rq', 1, 'title 4', 'description 4', 'keywords 4'),
(724, 184, 'Item', 2, 'product/product1-p123rq', 'product/product/product1-p123rq', 0, 'title 5', 'description 5', 'keywords 5'),
(725, 186, 'Item', 2, 'product/test-1-1rq', 'product/product/test-1-1rq', 1, NULL, NULL, NULL),
(726, 188, 'Item', 2, 'product/product1-p21rd', 'product/product/product1-p21rd', 1, 'title 1', 'description 1', 'keywords 1'),
(727, 190, 'Item', 2, 'product/product2-p31rd', 'product/product/product2-p31rd', 1, 'title 3', 'description 3', 'keywords 3'),
(728, 191, 'Item', 2, 'product/product1-p1112rd', 'product/product/product1-p1112rd', 1, 'title 4', 'description 4', 'keywords 4'),
(729, 192, 'Item', 2, 'product/product1-p1231rd', 'product/product/product1-p1231rd', 0, 'title 5', 'description 5', 'keywords 5'),
(730, 194, 'Item', 2, 'product/test-1-11rd', 'product/product/test-1-11rd', 1, NULL, NULL, NULL),
(731, 196, 'Item', 2, 'product/product1-p212dr', 'product/product/product1-p212dr', 1, 'title 1', 'description 1', 'keywords 1'),
(732, 198, 'Item', 2, 'product/product2-p312rd', 'product/product/product2-p312rd', 1, 'title 3', 'description 3', 'keywords 3'),
(733, 199, 'Item', 2, 'product/product1-p11122rd', 'product/product/product1-p11122rd', 1, 'title 4', 'description 4', 'keywords 4'),
(734, 200, 'Item', 2, 'product/product1-p12312rd', 'product/product/product1-p12312rd', 0, 'title 5', 'description 5', 'keywords 5'),
(735, 202, 'Item', 2, 'product/test-1-112fr', 'product/product/test-1-112fr', 1, NULL, NULL, NULL),
(736, 204, 'Item', 2, 'product/product1-p2123fr', 'product/product/product1-p2123fr', 1, 'title 1', 'description 1', 'keywords 1'),
(737, 206, 'Item', 2, 'product/product2-p3123fr', 'product/product/product2-p3123fr', 1, 'title 3', 'description 3', 'keywords 3'),
(738, 207, 'Item', 2, 'product/product1-p111223fr', 'product/product/product1-p111223fr', 1, 'title 4', 'description 4', 'keywords 4'),
(739, 208, 'Item', 2, 'product/product1-p123123rf', 'product/product/product1-p123123rf', 0, 'title 5', 'description 5', 'keywords 5'),
(740, 210, 'Item', 2, 'product/test-1-1123fr', 'product/product/test-1-1123fr', 1, NULL, NULL, NULL),
(741, 212, 'Item', 2, 'product/product1-p2erq', 'product/product/product1-p2erq', 1, 'title 1', 'description 1', 'keywords 1'),
(742, 214, 'Item', 2, 'product/product2y-p3rq', 'product/product/product2y-p3rq', 1, 'title 3', 'description 3', 'keywords 3'),
(743, 215, 'Item', 2, 'product/product1-p1y11rq', 'product/product/product1-p1y11rq', 1, 'title 4', 'description 4', 'keywords 4'),
(744, 216, 'Item', 2, 'product/product1-p12y3rq', 'product/product/product1-p12y3rq', 0, 'title 5', 'description 5', 'keywords 5'),
(745, 218, 'Item', 2, 'product/test-1-1yrq', 'product/product/test-1-1yrq', 1, NULL, NULL, NULL),
(746, 220, 'Item', 2, 'product/product1-p2y1rd', 'product/product/product1-p2y1rd', 1, 'title 1', 'description 1', 'keywords 1'),
(747, 224, 'Item', 2, 'product/product1-p1r1', 'product/product/product1-p1r1', 0, 'update too', 'description', 'keywords'),
(748, 225, 'Item', 2, 'product/tproduct1-p1123', 'product/product/tproduct1-p1123', 1, NULL, 'description', 'keywords'),
(749, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(750, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(751, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(752, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(753, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(754, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(755, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(756, 226, 'Item', 2, 'product/ttproduct1-p1123', 'product/product/ttproduct1-p1123', 1, NULL, 'description', 'keywords'),
(757, 227, 'Item', 2, 'product/produ0ct1-p1r', 'product/product/produ0ct1-p1r', 1, 'title', 'description', 'слова'),
(758, 228, 'Item', 2, 'product/produ1ct1-p1r', 'product/product/produ1ct1-p1r', 1, 'title', 'description', 'слова'),
(759, 229, 'Item', 2, 'product/produ2ct1-p1r', 'product/product/produ2ct1-p1r', 1, 'title', 'description', 'слова'),
(760, 230, 'Item', 2, 'product/produ3ct1-p1r', 'product/product/produ3ct1-p1r', 1, 'title', 'description', 'слова'),
(761, 231, 'Item', 2, 'product/produ4ct1-p1r', 'product/product/produ4ct1-p1r', 1, 'title', 'description', 'слова'),
(762, 232, 'Item', 2, 'product/produ5ct1-p1r', 'product/product/produ5ct1-p1r', 1, 'title', 'description', 'слова'),
(763, 233, 'Item', 2, 'product/produ6ct1-p1r', 'product/product/produ6ct1-p1r', 1, 'title', 'description', 'слова'),
(764, 234, 'Item', 2, 'product/produ7ct1-p1r', 'product/product/produ7ct1-p1r', 1, 'title', 'description', 'слова'),
(765, 235, 'Item', 2, 'product/produ8ct1-p1r', 'product/product/produ8ct1-p1r', 1, 'title', 'description', 'слова'),
(766, 236, 'Item', 2, 'product/produ9ct1-p1r', 'product/product/produ9ct1-p1r', 1, 'title', 'description', 'слова'),
(767, 237, 'Item', 2, 'product/produ10ct1-p1r', 'product/product/produ10ct1-p1r', 1, 'title', 'description', 'слова'),
(768, 238, 'Item', 2, 'product/produ11ct1-p1r', 'product/product/produ11ct1-p1r', 1, 'title', 'description', 'слова'),
(769, 239, 'Item', 2, 'product/produ12ct1-p1r', 'product/product/produ12ct1-p1r', 1, 'title', 'description', 'слова'),
(770, 240, 'Item', 2, 'product/produ13ct1-p1r', 'product/product/produ13ct1-p1r', 1, 'title', 'description', 'слова'),
(771, 241, 'Item', 2, 'product/produ14ct1-p1r', 'product/product/produ14ct1-p1r', 1, 'title', 'description', 'слова'),
(772, 242, 'Item', 2, 'product/produ15ct1-p1r', 'product/product/produ15ct1-p1r', 1, 'title', 'description', 'слова'),
(773, 243, 'Item', 2, 'product/produ16ct1-p1r', 'product/product/produ16ct1-p1r', 1, 'title', 'description', 'слова'),
(774, 244, 'Item', 2, 'product/produ17ct1-p1r', 'product/product/produ17ct1-p1r', 1, 'title', 'description', 'слова'),
(775, 245, 'Item', 2, 'product/produ18ct1-p1r', 'product/product/produ18ct1-p1r', 1, 'title', 'description', 'слова'),
(776, 246, 'Item', 2, 'product/produ19ct1-p1r', 'product/product/produ19ct1-p1r', 1, 'title', 'description', 'слова'),
(777, 247, 'Item', 2, 'product/produ20ct1-p1r', 'product/product/produ20ct1-p1r', 1, 'title', 'description', 'слова'),
(778, 248, 'Item', 2, 'product/produ21ct1-p1r', 'product/product/produ21ct1-p1r', 1, 'title', 'description', 'слова'),
(779, 249, 'Item', 2, 'product/produ22ct1-p1r', 'product/product/produ22ct1-p1r', 1, 'title', 'description', 'слова'),
(780, 250, 'Item', 2, 'product/produ23ct1-p1r', 'product/product/produ23ct1-p1r', 1, 'title', 'description', 'слова'),
(781, 251, 'Item', 2, 'product/produ24ct1-p1r', 'product/product/produ24ct1-p1r', 1, 'title', 'description', 'слова'),
(782, 252, 'Item', 2, 'product/produ25ct1-p1r', 'product/product/produ25ct1-p1r', 1, 'title', 'description', 'слова'),
(783, 253, 'Item', 2, 'product/produ26ct1-p1r', 'product/product/produ26ct1-p1r', 1, 'title', 'description', 'слова'),
(784, 254, 'Item', 2, 'product/produ27ct1-p1r', 'product/product/produ27ct1-p1r', 1, 'title', 'description', 'слова'),
(785, 255, 'Item', 2, 'product/produ28ct1-p1r', 'product/product/produ28ct1-p1r', 1, 'title', 'description', 'слова'),
(786, 256, 'Item', 2, 'product/produ29ct1-p1r', 'product/product/produ29ct1-p1r', 1, 'title', 'description', 'слова'),
(787, 257, 'Item', 2, 'product/produ30ct1-p1r', 'product/product/produ30ct1-p1r', 1, 'title', 'description', 'слова'),
(788, 258, 'Item', 2, 'product/produ31ct1-p1r', 'product/product/produ31ct1-p1r', 1, 'title', 'description', 'слова'),
(789, 259, 'Item', 2, 'product/produ32ct1-p1r', 'product/product/produ32ct1-p1r', 1, 'title', 'description', 'слова'),
(790, 260, 'Item', 2, 'product/produ33ct1-p1r', 'product/product/produ33ct1-p1r', 1, 'title', 'description', 'слова'),
(791, 261, 'Item', 2, 'product/produ34ct1-p1r', 'product/product/produ34ct1-p1r', 1, 'title', 'description', 'слова'),
(792, 262, 'Item', 2, 'product/produ35ct1-p1r', 'product/product/produ35ct1-p1r', 1, 'title', 'description', 'слова'),
(793, 263, 'Item', 2, 'product/produ36ct1-p1r', 'product/product/produ36ct1-p1r', 1, 'title', 'description', 'слова'),
(794, 264, 'Item', 2, 'product/produ37ct1-p1r', 'product/product/produ37ct1-p1r', 1, 'title', 'description', 'слова'),
(795, 265, 'Item', 2, 'product/produ38ct1-p1r', 'product/product/produ38ct1-p1r', 1, 'title', 'description', 'слова'),
(796, 266, 'Item', 2, 'product/produ39ct1-p1r', 'product/product/produ39ct1-p1r', 1, 'title', 'description', 'слова'),
(797, 267, 'Item', 2, 'product/produ40ct1-p1r', 'product/product/produ40ct1-p1r', 1, 'title', 'description', 'слова'),
(798, 268, 'Item', 2, 'product/produ41ct1-p1r', 'product/product/produ41ct1-p1r', 1, 'title', 'description', 'слова'),
(799, 269, 'Item', 2, 'product/produ42ct1-p1r', 'product/product/produ42ct1-p1r', 1, 'title', 'description', 'слова'),
(800, 270, 'Item', 2, 'product/produ43ct1-p1r', 'product/product/produ43ct1-p1r', 1, 'title', 'description', 'слова'),
(801, 271, 'Item', 2, 'product/produ44ct1-p1r', 'product/product/produ44ct1-p1r', 1, 'title', 'description', 'слова'),
(802, 272, 'Item', 2, 'product/produ45ct1-p1r', 'product/product/produ45ct1-p1r', 1, 'title', 'description', 'слова'),
(803, 273, 'Item', 2, 'product/produ46ct1-p1r', 'product/product/produ46ct1-p1r', 1, 'title', 'description', 'слова'),
(804, 274, 'Item', 2, 'product/produ47ct1-p1r', 'product/product/produ47ct1-p1r', 1, 'title', 'description', 'слова'),
(805, 275, 'Item', 2, 'product/produ48ct1-p1r', 'product/product/produ48ct1-p1r', 1, 'title', 'description', 'слова'),
(806, 276, 'Item', 2, 'product/produ49ct1-p1r', 'product/product/produ49ct1-p1r', 1, 'title', 'description', 'слова'),
(807, 277, 'Item', 2, 'product/produ50ct1-p1r', 'product/product/produ50ct1-p1r', 1, 'title', 'description', 'слова'),
(808, 278, 'Item', 2, 'product/produ51ct1-p1r', 'product/product/produ51ct1-p1r', 1, 'title', 'description', 'слова'),
(809, 279, 'Item', 2, 'product/produ52ct1-p1r', 'product/product/produ52ct1-p1r', 1, 'title', 'description', 'слова'),
(810, 280, 'Item', 2, 'product/produ53ct1-p1r', 'product/product/produ53ct1-p1r', 1, 'title', 'description', 'слова'),
(811, 281, 'Item', 2, 'product/produ54ct1-p1r', 'product/product/produ54ct1-p1r', 1, 'title', 'description', 'слова'),
(812, 282, 'Item', 2, 'product/produ55ct1-p1r', 'product/product/produ55ct1-p1r', 1, 'title', 'description', 'слова'),
(813, 283, 'Item', 2, 'product/produ56ct1-p1r', 'product/product/produ56ct1-p1r', 1, 'title', 'description', 'слова'),
(814, 284, 'Item', 2, 'product/produ57ct1-p1r', 'product/product/produ57ct1-p1r', 1, 'title', 'description', 'слова'),
(815, 285, 'Item', 2, 'product/produ58ct1-p1r', 'product/product/produ58ct1-p1r', 1, 'title', 'description', 'слова'),
(816, 286, 'Item', 2, 'product/produ59ct1-p1r', 'product/product/produ59ct1-p1r', 1, 'title', 'description', 'слова'),
(817, 287, 'Item', 2, 'product/produ60ct1-p1r', 'product/product/produ60ct1-p1r', 1, 'title', 'description', 'слова'),
(818, 288, 'Item', 2, 'product/produ61ct1-p1r', 'product/product/produ61ct1-p1r', 1, 'title', 'description', 'слова'),
(819, 289, 'Item', 2, 'product/produ62ct1-p1r', 'product/product/produ62ct1-p1r', 1, 'title', 'description', 'слова'),
(820, 290, 'Item', 2, 'product/produ63ct1-p1r', 'product/product/produ63ct1-p1r', 1, 'title', 'description', 'слова'),
(821, 291, 'Item', 2, 'product/produ64ct1-p1r', 'product/product/produ64ct1-p1r', 1, 'title', 'description', 'слова'),
(822, 292, 'Item', 2, 'product/produ65ct1-p1r', 'product/product/produ65ct1-p1r', 1, 'title', 'description', 'слова'),
(823, 293, 'Item', 2, 'product/produ66ct1-p1r', 'product/product/produ66ct1-p1r', 1, 'title', 'description', 'слова'),
(824, 294, 'Item', 2, 'product/produ67ct1-p1r', 'product/product/produ67ct1-p1r', 1, 'title', 'description', 'слова'),
(825, 295, 'Item', 2, 'product/produ68ct1-p1r', 'product/product/produ68ct1-p1r', 1, 'title', 'description', 'слова'),
(826, 296, 'Item', 2, 'product/produ69ct1-p1r', 'product/product/produ69ct1-p1r', 1, 'title', 'description', 'слова'),
(827, 297, 'Item', 2, 'product/produ70ct1-p1r', 'product/product/produ70ct1-p1r', 1, 'title', 'description', 'слова'),
(828, 298, 'Item', 2, 'product/produ71ct1-p1r', 'product/product/produ71ct1-p1r', 1, 'title', 'description', 'слова'),
(829, 299, 'Item', 2, 'product/produ72ct1-p1r', 'product/product/produ72ct1-p1r', 1, 'title', 'description', 'слова'),
(830, 300, 'Item', 2, 'product/produ73ct1-p1r', 'product/product/produ73ct1-p1r', 1, 'title', 'description', 'слова'),
(831, 301, 'Item', 2, 'product/produ74ct1-p1r', 'product/product/produ74ct1-p1r', 1, 'title', 'description', 'слова'),
(832, 302, 'Item', 2, 'product/produ75ct1-p1r', 'product/product/produ75ct1-p1r', 1, 'title', 'description', 'слова'),
(833, 303, 'Item', 2, 'product/produ76ct1-p1r', 'product/product/produ76ct1-p1r', 1, 'title', 'description', 'слова'),
(834, 304, 'Item', 2, 'product/produ77ct1-p1r', 'product/product/produ77ct1-p1r', 1, 'title', 'description', 'слова'),
(835, 305, 'Item', 2, 'product/produ78ct1-p1r', 'product/product/produ78ct1-p1r', 1, 'title', 'description', 'слова'),
(836, 306, 'Item', 2, 'product/produ79ct1-p1r', 'product/product/produ79ct1-p1r', 1, 'title', 'description', 'слова'),
(837, 307, 'Item', 2, 'product/produ80ct1-p1r', 'product/product/produ80ct1-p1r', 1, 'title', 'description', 'слова'),
(838, 308, 'Item', 2, 'product/produ81ct1-p1r', 'product/product/produ81ct1-p1r', 1, 'title', 'description', 'слова'),
(839, 309, 'Item', 2, 'product/produ82ct1-p1r', 'product/product/produ82ct1-p1r', 1, 'title', 'description', 'слова'),
(840, 310, 'Item', 2, 'product/produ83ct1-p1r', 'product/product/produ83ct1-p1r', 1, 'title', 'description', 'слова'),
(841, 311, 'Item', 2, 'product/produ84ct1-p1r', 'product/product/produ84ct1-p1r', 1, 'title', 'description', 'слова'),
(842, 312, 'Item', 2, 'product/produ85ct1-p1r', 'product/product/produ85ct1-p1r', 1, 'title', 'description', 'слова'),
(843, 313, 'Item', 2, 'product/produ86ct1-p1r', 'product/product/produ86ct1-p1r', 1, 'title', 'description', 'слова'),
(844, 314, 'Item', 2, 'product/produ87ct1-p1r', 'product/product/produ87ct1-p1r', 1, 'title', 'description', 'слова'),
(845, 315, 'Item', 2, 'product/produ88ct1-p1r', 'product/product/produ88ct1-p1r', 1, 'title', 'description', 'слова'),
(846, 316, 'Item', 2, 'product/produ89ct1-p1r', 'product/product/produ89ct1-p1r', 1, 'title', 'description', 'слова'),
(847, 317, 'Item', 2, 'product/produ90ct1-p1r', 'product/product/produ90ct1-p1r', 1, 'title', 'description', 'слова'),
(848, 318, 'Item', 2, 'product/produ91ct1-p1r', 'product/product/produ91ct1-p1r', 1, 'title', 'description', 'слова'),
(849, 319, 'Item', 2, 'product/produ92ct1-p1r', 'product/product/produ92ct1-p1r', 1, 'title', 'description', 'слова'),
(850, 320, 'Item', 2, 'product/produ93ct1-p1r', 'product/product/produ93ct1-p1r', 1, 'title', 'description', 'слова'),
(851, 321, 'Item', 2, 'product/produ94ct1-p1r', 'product/product/produ94ct1-p1r', 1, 'title', 'description', 'слова'),
(852, 322, 'Item', 2, 'product/produ95ct1-p1r', 'product/product/produ95ct1-p1r', 1, 'title', 'description', 'слова'),
(853, 323, 'Item', 2, 'product/produ96ct1-p1r', 'product/product/produ96ct1-p1r', 1, 'title', 'description', 'слова'),
(854, 324, 'Item', 2, 'product/produ97ct1-p1r', 'product/product/produ97ct1-p1r', 1, 'title', 'description', 'слова'),
(855, 325, 'Item', 2, 'product/produ98ct1-p1r', 'product/product/produ98ct1-p1r', 1, 'title', 'description', 'слова'),
(856, 326, 'Item', 2, 'product/produ99ct1-p1r', 'product/product/produ99ct1-p1r', 1, 'title', 'description', 'слова'),
(857, 327, 'Item', 2, 'product/produ100ct1-p1r', 'product/product/produ100ct1-p1r', 1, 'title', 'description', 'слова'),
(858, 328, 'Item', 2, 'product/produ101ct1-p1r', 'product/product/produ101ct1-p1r', 1, 'title', 'description', 'слова'),
(859, 329, 'Item', 2, 'product/produ102ct1-p1r', 'product/product/produ102ct1-p1r', 1, 'title', 'description', 'слова'),
(860, 330, 'Item', 2, 'product/produ103ct1-p1r', 'product/product/produ103ct1-p1r', 1, 'title', 'description', 'слова'),
(861, 331, 'Item', 2, 'product/produ104ct1-p1r', 'product/product/produ104ct1-p1r', 1, 'title', 'description', 'слова'),
(862, 332, 'Item', 2, 'product/produ105ct1-p1r', 'product/product/produ105ct1-p1r', 1, 'title', 'description', 'слова'),
(863, 333, 'Item', 2, 'product/produ106ct1-p1r', 'product/product/produ106ct1-p1r', 1, 'title', 'description', 'слова'),
(864, 334, 'Item', 2, 'product/produ107ct1-p1r', 'product/product/produ107ct1-p1r', 1, 'title', 'description', 'слова'),
(865, 335, 'Item', 2, 'product/produ108ct1-p1r', 'product/product/produ108ct1-p1r', 1, 'title', 'description', 'слова'),
(866, 336, 'Item', 2, 'product/produ109ct1-p1r', 'product/product/produ109ct1-p1r', 1, 'title', 'description', 'слова'),
(867, 337, 'Item', 2, 'product/produ110ct1-p1r', 'product/product/produ110ct1-p1r', 1, 'title', 'description', 'слова'),
(868, 338, 'Item', 2, 'product/produ111ct1-p1r', 'product/product/produ111ct1-p1r', 1, 'title', 'description', 'слова'),
(869, 339, 'Item', 2, 'product/produ112ct1-p1r', 'product/product/produ112ct1-p1r', 1, 'title', 'description', 'слова'),
(870, 340, 'Item', 2, 'product/produ113ct1-p1r', 'product/product/produ113ct1-p1r', 1, 'title', 'description', 'слова'),
(871, 341, 'Item', 2, 'product/produ114ct1-p1r', 'product/product/produ114ct1-p1r', 1, 'title', 'description', 'слова'),
(872, 342, 'Item', 2, 'product/produ115ct1-p1r', 'product/product/produ115ct1-p1r', 1, 'title', 'description', 'слова'),
(873, 343, 'Item', 2, 'product/produ116ct1-p1r', 'product/product/produ116ct1-p1r', 1, 'title', 'description', 'слова'),
(874, 344, 'Item', 2, 'product/produ117ct1-p1r', 'product/product/produ117ct1-p1r', 1, 'title', 'description', 'слова'),
(875, 345, 'Item', 2, 'product/produ118ct1-p1r', 'product/product/produ118ct1-p1r', 1, 'title', 'description', 'слова'),
(876, 346, 'Item', 2, 'product/produ119ct1-p1r', 'product/product/produ119ct1-p1r', 1, 'title', 'description', 'слова'),
(877, 347, 'Item', 2, 'product/produ120ct1-p1r', 'product/product/produ120ct1-p1r', 1, 'title', 'description', 'слова'),
(878, 348, 'Item', 2, 'product/produ121ct1-p1r', 'product/product/produ121ct1-p1r', 1, 'title', 'description', 'слова'),
(879, 349, 'Item', 2, 'product/produ122ct1-p1r', 'product/product/produ122ct1-p1r', 1, 'title', 'description', 'слова'),
(880, 350, 'Item', 2, 'product/produ123ct1-p1r', 'product/product/produ123ct1-p1r', 1, 'title', 'description', 'слова'),
(881, 351, 'Item', 2, 'product/produ124ct1-p1r', 'product/product/produ124ct1-p1r', 1, 'title', 'description', 'слова'),
(882, 352, 'Item', 2, 'product/produ125ct1-p1r', 'product/product/produ125ct1-p1r', 1, 'title', 'description', 'слова'),
(883, 353, 'Item', 2, 'product/produ126ct1-p1r', 'product/product/produ126ct1-p1r', 1, 'title', 'description', 'слова'),
(884, 354, 'Item', 2, 'product/produ127ct1-p1r', 'product/product/produ127ct1-p1r', 1, 'title', 'description', 'слова'),
(885, 355, 'Item', 2, 'product/produ128ct1-p1r', 'product/product/produ128ct1-p1r', 1, 'title', 'description', 'слова'),
(886, 356, 'Item', 2, 'product/produ129ct1-p1r', 'product/product/produ129ct1-p1r', 1, 'title', 'description', 'слова'),
(887, 357, 'Item', 2, 'product/produ130ct1-p1r', 'product/product/produ130ct1-p1r', 1, 'title', 'description', 'слова'),
(888, 358, 'Item', 2, 'product/produ131ct1-p1r', 'product/product/produ131ct1-p1r', 1, 'title', 'description', 'слова'),
(889, 359, 'Item', 2, 'product/produ132ct1-p1r', 'product/product/produ132ct1-p1r', 1, 'title', 'description', 'слова'),
(890, 360, 'Item', 2, 'product/produ133ct1-p1r', 'product/product/produ133ct1-p1r', 1, 'title', 'description', 'слова'),
(891, 361, 'Item', 2, 'product/produ134ct1-p1r', 'product/product/produ134ct1-p1r', 1, 'title', 'description', 'слова'),
(892, 362, 'Item', 2, 'product/produ135ct1-p1r', 'product/product/produ135ct1-p1r', 1, 'title', 'description', 'слова'),
(893, 363, 'Item', 2, 'product/produ136ct1-p1r', 'product/product/produ136ct1-p1r', 1, 'title', 'description', 'слова'),
(894, 364, 'Item', 2, 'product/produ137ct1-p1r', 'product/product/produ137ct1-p1r', 1, 'title', 'description', 'слова'),
(895, 365, 'Item', 2, 'product/produ138ct1-p1r', 'product/product/produ138ct1-p1r', 1, 'title', 'description', 'слова'),
(896, 366, 'Item', 2, 'product/produ139ct1-p1r', 'product/product/produ139ct1-p1r', 1, 'title', 'description', 'слова'),
(897, 367, 'Item', 2, 'product/produ140ct1-p1r', 'product/product/produ140ct1-p1r', 1, 'title', 'description', 'слова'),
(898, 368, 'Item', 2, 'product/produ141ct1-p1r', 'product/product/produ141ct1-p1r', 1, 'title', 'description', 'слова'),
(899, 369, 'Item', 2, 'product/produ142ct1-p1r', 'product/product/produ142ct1-p1r', 1, 'title', 'description', 'слова'),
(900, 370, 'Item', 2, 'product/produ143ct1-p1r', 'product/product/produ143ct1-p1r', 1, 'title', 'description', 'слова'),
(901, 371, 'Item', 2, 'product/produ144ct1-p1r', 'product/product/produ144ct1-p1r', 1, 'title', 'description', 'слова'),
(902, 372, 'Item', 2, 'product/produ145ct1-p1r', 'product/product/produ145ct1-p1r', 1, 'title', 'description', 'слова'),
(903, 373, 'Item', 2, 'product/produ146ct1-p1r', 'product/product/produ146ct1-p1r', 1, 'title', 'description', 'слова'),
(904, 374, 'Item', 2, 'product/produ147ct1-p1r', 'product/product/produ147ct1-p1r', 1, 'title', 'description', 'слова'),
(905, 375, 'Item', 2, 'product/produ148ct1-p1r', 'product/product/produ148ct1-p1r', 1, 'title', 'description', 'слова'),
(906, 376, 'Item', 2, 'product/produ149ct1-p1r', 'product/product/produ149ct1-p1r', 1, 'title', 'description', 'слова'),
(907, 377, 'Item', 2, 'product/produ150ct1-p1r', 'product/product/produ150ct1-p1r', 1, 'title', 'description', 'слова'),
(908, 378, 'Item', 2, 'product/produ151ct1-p1r', 'product/product/produ151ct1-p1r', 1, 'title', 'description', 'слова'),
(909, 379, 'Item', 2, 'product/produ152ct1-p1r', 'product/product/produ152ct1-p1r', 1, 'title', 'description', 'слова'),
(910, 380, 'Item', 2, 'product/produ153ct1-p1r', 'product/product/produ153ct1-p1r', 1, 'title', 'description', 'слова'),
(911, 381, 'Item', 2, 'product/produ154ct1-p1r', 'product/product/produ154ct1-p1r', 1, 'title', 'description', 'слова'),
(912, 382, 'Item', 2, 'product/produ155ct1-p1r', 'product/product/produ155ct1-p1r', 1, 'title', 'description', 'слова'),
(913, 383, 'Item', 2, 'product/produ156ct1-p1r', 'product/product/produ156ct1-p1r', 1, 'title', 'description', 'слова'),
(914, 384, 'Item', 2, 'product/produ157ct1-p1r', 'product/product/produ157ct1-p1r', 1, 'title', 'description', 'слова'),
(915, 385, 'Item', 2, 'product/produ158ct1-p1r', 'product/product/produ158ct1-p1r', 1, 'title', 'description', 'слова'),
(916, 386, 'Item', 2, 'product/produ159ct1-p1r', 'product/product/produ159ct1-p1r', 1, 'title', 'description', 'слова'),
(917, 387, 'Item', 2, 'product/produ160ct1-p1r', 'product/product/produ160ct1-p1r', 1, 'title', 'description', 'слова'),
(918, 388, 'Item', 2, 'product/produ161ct1-p1r', 'product/product/produ161ct1-p1r', 1, 'title', 'description', 'слова'),
(919, 389, 'Item', 2, 'product/produ162ct1-p1r', 'product/product/produ162ct1-p1r', 1, 'title', 'description', 'слова'),
(920, 390, 'Item', 2, 'product/produ163ct1-p1r', 'product/product/produ163ct1-p1r', 1, 'title', 'description', 'слова'),
(921, 391, 'Item', 2, 'product/produ164ct1-p1r', 'product/product/produ164ct1-p1r', 1, 'title', 'description', 'слова'),
(922, 392, 'Item', 2, 'product/produ165ct1-p1r', 'product/product/produ165ct1-p1r', 1, 'title', 'description', 'слова'),
(923, 393, 'Item', 2, 'product/produ166ct1-p1r', 'product/product/produ166ct1-p1r', 1, 'title', 'description', 'слова'),
(924, 394, 'Item', 2, 'product/produ167ct1-p1r', 'product/product/produ167ct1-p1r', 1, 'title', 'description', 'слова'),
(925, 395, 'Item', 2, 'product/produ168ct1-p1r', 'product/product/produ168ct1-p1r', 1, 'title', 'description', 'слова'),
(926, 396, 'Item', 2, 'product/produ169ct1-p1r', 'product/product/produ169ct1-p1r', 1, 'title', 'description', 'слова'),
(927, 397, 'Item', 2, 'product/produ170ct1-p1r', 'product/product/produ170ct1-p1r', 1, 'title', 'description', 'слова'),
(928, 398, 'Item', 2, 'product/produ171ct1-p1r', 'product/product/produ171ct1-p1r', 1, 'title', 'description', 'слова'),
(929, 399, 'Item', 2, 'product/produ172ct1-p1r', 'product/product/produ172ct1-p1r', 1, 'title', 'description', 'слова'),
(930, 400, 'Item', 2, 'product/produ173ct1-p1r', 'product/product/produ173ct1-p1r', 1, 'title', 'description', 'слова'),
(931, 401, 'Item', 2, 'product/produ174ct1-p1r', 'product/product/produ174ct1-p1r', 1, 'title', 'description', 'слова'),
(932, 402, 'Item', 2, 'product/produ175ct1-p1r', 'product/product/produ175ct1-p1r', 1, 'title', 'description', 'слова'),
(933, 403, 'Item', 2, 'product/produ176ct1-p1r', 'product/product/produ176ct1-p1r', 1, 'title', 'description', 'слова'),
(934, 404, 'Item', 2, 'product/produ177ct1-p1r', 'product/product/produ177ct1-p1r', 1, 'title', 'description', 'слова'),
(935, 405, 'Item', 2, 'product/produ178ct1-p1r', 'product/product/produ178ct1-p1r', 1, 'title', 'description', 'слова'),
(936, 406, 'Item', 2, 'product/produ179ct1-p1r', 'product/product/produ179ct1-p1r', 1, 'title', 'description', 'слова'),
(937, 407, 'Item', 2, 'product/produ180ct1-p1r', 'product/product/produ180ct1-p1r', 1, 'title', 'description', 'слова'),
(938, 408, 'Item', 2, 'product/produ181ct1-p1r', 'product/product/produ181ct1-p1r', 1, 'title', 'description', 'слова'),
(939, 409, 'Item', 2, 'product/produ182ct1-p1r', 'product/product/produ182ct1-p1r', 1, 'title', 'description', 'слова'),
(940, 410, 'Item', 2, 'product/produ183ct1-p1r', 'product/product/produ183ct1-p1r', 1, 'title', 'description', 'слова'),
(941, 411, 'Item', 2, 'product/produ184ct1-p1r', 'product/product/produ184ct1-p1r', 1, 'title', 'description', 'слова'),
(942, 412, 'Item', 2, 'product/produ185ct1-p1r', 'product/product/produ185ct1-p1r', 1, 'title', 'description', 'слова'),
(943, 413, 'Item', 2, 'product/produ186ct1-p1r', 'product/product/produ186ct1-p1r', 1, 'title', 'description', 'слова'),
(944, 414, 'Item', 2, 'product/produ187ct1-p1r', 'product/product/produ187ct1-p1r', 1, 'title', 'description', 'слова'),
(945, 415, 'Item', 2, 'product/produ188ct1-p1r', 'product/product/produ188ct1-p1r', 1, 'title', 'description', 'слова'),
(946, 416, 'Item', 2, 'product/produ189ct1-p1r', 'product/product/produ189ct1-p1r', 1, 'title', 'description', 'слова'),
(947, 417, 'Item', 2, 'product/produ190ct1-p1r', 'product/product/produ190ct1-p1r', 1, 'title', 'description', 'слова'),
(948, 418, 'Item', 2, 'product/produ191ct1-p1r', 'product/product/produ191ct1-p1r', 1, 'title', 'description', 'слова'),
(949, 419, 'Item', 2, 'product/produ192ct1-p1r', 'product/product/produ192ct1-p1r', 1, 'title', 'description', 'слова'),
(950, 420, 'Item', 2, 'product/produ193ct1-p1r', 'product/product/produ193ct1-p1r', 1, 'title', 'description', 'слова'),
(951, 421, 'Item', 2, 'product/produ194ct1-p1r', 'product/product/produ194ct1-p1r', 1, 'title', 'description', 'слова'),
(952, 422, 'Item', 2, 'product/produ195ct1-p1r', 'product/product/produ195ct1-p1r', 1, 'title', 'description', 'слова'),
(953, 423, 'Item', 2, 'product/produ196ct1-p1r', 'product/product/produ196ct1-p1r', 1, 'title', 'description', 'слова'),
(954, 424, 'Item', 2, 'product/produ197ct1-p1r', 'product/product/produ197ct1-p1r', 1, 'title', 'description', 'слова'),
(955, 425, 'Item', 2, 'product/produ198ct1-p1r', 'product/product/produ198ct1-p1r', 1, 'title', 'description', 'слова'),
(956, 426, 'Item', 2, 'product/produ199ct1-p1r', 'product/product/produ199ct1-p1r', 1, 'title', 'description', 'слова'),
(957, 427, 'Item', 2, 'product/produ200ct1-p1r', 'product/product/produ200ct1-p1r', 1, 'title', 'description', 'слова'),
(958, 428, 'Item', 2, 'product/produ201ct1-p1r', 'product/product/produ201ct1-p1r', 1, 'title', 'description', 'слова'),
(959, 429, 'Item', 2, 'product/produ202ct1-p1r', 'product/product/produ202ct1-p1r', 1, 'title', 'description', 'слова'),
(960, 430, 'Item', 2, 'product/produ203ct1-p1r', 'product/product/produ203ct1-p1r', 1, 'title', 'description', 'слова'),
(961, 431, 'Item', 2, 'product/produ204ct1-p1r', 'product/product/produ204ct1-p1r', 1, 'title', 'description', 'слова'),
(962, 432, 'Item', 2, 'product/produ205ct1-p1r', 'product/product/produ205ct1-p1r', 1, 'title', 'description', 'слова'),
(963, 433, 'Item', 2, 'product/produ206ct1-p1r', 'product/product/produ206ct1-p1r', 1, 'title', 'description', 'слова'),
(964, 434, 'Item', 2, 'product/produ207ct1-p1r', 'product/product/produ207ct1-p1r', 1, 'title', 'description', 'слова'),
(965, 435, 'Item', 2, 'product/produ208ct1-p1r', 'product/product/produ208ct1-p1r', 1, 'title', 'description', 'слова'),
(966, 436, 'Item', 2, 'product/produ209ct1-p1r', 'product/product/produ209ct1-p1r', 1, 'title', 'description', 'слова'),
(967, 437, 'Item', 2, 'product/produ210ct1-p1r', 'product/product/produ210ct1-p1r', 1, 'title', 'description', 'слова'),
(968, 438, 'Item', 2, 'product/produ211ct1-p1r', 'product/product/produ211ct1-p1r', 1, 'title', 'description', 'слова'),
(969, 439, 'Item', 2, 'product/produ212ct1-p1r', 'product/product/produ212ct1-p1r', 1, 'title', 'description', 'слова'),
(970, 440, 'Item', 2, 'product/produ213ct1-p1r', 'product/product/produ213ct1-p1r', 1, 'title', 'description', 'слова'),
(971, 441, 'Item', 2, 'product/produ214ct1-p1r', 'product/product/produ214ct1-p1r', 1, 'title', 'description', 'слова'),
(972, 442, 'Item', 2, 'product/produ215ct1-p1r', 'product/product/produ215ct1-p1r', 1, 'title', 'description', 'слова'),
(973, 443, 'Item', 2, 'product/produ216ct1-p1r', 'product/product/produ216ct1-p1r', 1, 'title', 'description', 'слова'),
(974, 444, 'Item', 2, 'product/produ217ct1-p1r', 'product/product/produ217ct1-p1r', 1, 'title', 'description', 'слова'),
(975, 445, 'Item', 2, 'product/produ218ct1-p1r', 'product/product/produ218ct1-p1r', 1, 'title', 'description', 'слова'),
(976, 446, 'Item', 2, 'product/produ219ct1-p1r', 'product/product/produ219ct1-p1r', 1, 'title', 'description', 'слова'),
(977, 447, 'Item', 2, 'product/produ220ct1-p1r', 'product/product/produ220ct1-p1r', 1, 'title', 'description', 'слова'),
(978, 448, 'Item', 2, 'product/produ221ct1-p1r', 'product/product/produ221ct1-p1r', 1, 'title', 'description', 'слова'),
(979, 449, 'Item', 2, 'product/produ222ct1-p1r', 'product/product/produ222ct1-p1r', 1, 'title', 'description', 'слова'),
(980, 450, 'Item', 2, 'product/produ223ct1-p1r', 'product/product/produ223ct1-p1r', 1, 'title', 'description', 'слова'),
(981, 451, 'Item', 2, 'product/produ224ct1-p1r', 'product/product/produ224ct1-p1r', 1, 'title', 'description', 'слова'),
(982, 452, 'Item', 2, 'product/produ225ct1-p1r', 'product/product/produ225ct1-p1r', 1, 'title', 'description', 'слова'),
(983, 453, 'Item', 2, 'product/produ226ct1-p1r', 'product/product/produ226ct1-p1r', 1, 'title', 'description', 'слова'),
(984, 454, 'Item', 2, 'product/produ227ct1-p1r', 'product/product/produ227ct1-p1r', 1, 'title', 'description', 'слова'),
(985, 455, 'Item', 2, 'product/produ228ct1-p1r', 'product/product/produ228ct1-p1r', 1, 'title', 'description', 'слова'),
(986, 456, 'Item', 2, 'product/produ229ct1-p1r', 'product/product/produ229ct1-p1r', 1, 'title', 'description', 'слова'),
(987, 457, 'Item', 2, 'product/produ230ct1-p1r', 'product/product/produ230ct1-p1r', 1, 'title', 'description', 'слова'),
(988, 458, 'Item', 2, 'product/produ231ct1-p1r', 'product/product/produ231ct1-p1r', 1, 'title', 'description', 'слова'),
(989, 459, 'Item', 2, 'product/produ232ct1-p1r', 'product/product/produ232ct1-p1r', 1, 'title', 'description', 'слова'),
(990, 460, 'Item', 2, 'product/produ233ct1-p1r', 'product/product/produ233ct1-p1r', 1, 'title', 'description', 'слова'),
(991, 461, 'Item', 2, 'product/produ234ct1-p1r', 'product/product/produ234ct1-p1r', 1, 'title', 'description', 'слова'),
(992, 462, 'Item', 2, 'product/produ235ct1-p1r', 'product/product/produ235ct1-p1r', 1, 'title', 'description', 'слова'),
(993, 463, 'Item', 2, 'product/produ236ct1-p1r', 'product/product/produ236ct1-p1r', 1, 'title', 'description', 'слова'),
(994, 464, 'Item', 2, 'product/produ237ct1-p1r', 'product/product/produ237ct1-p1r', 1, 'title', 'description', 'слова'),
(995, 465, 'Item', 2, 'product/produ238ct1-p1r', 'product/product/produ238ct1-p1r', 1, 'title', 'description', 'слова'),
(996, 466, 'Item', 2, 'product/produ239ct1-p1r', 'product/product/produ239ct1-p1r', 1, 'title', 'description', 'слова'),
(997, 467, 'Item', 2, 'product/produ240ct1-p1r', 'product/product/produ240ct1-p1r', 1, 'title', 'description', 'слова'),
(998, 468, 'Item', 2, 'product/produ241ct1-p1r', 'product/product/produ241ct1-p1r', 1, 'title', 'description', 'слова'),
(999, 469, 'Item', 2, 'product/produ242ct1-p1r', 'product/product/produ242ct1-p1r', 1, 'title', 'description', 'слова'),
(1000, 470, 'Item', 2, 'product/produ243ct1-p1r', 'product/product/produ243ct1-p1r', 1, 'title', 'description', 'слова'),
(1001, 471, 'Item', 2, 'product/produ244ct1-p1r', 'product/product/produ244ct1-p1r', 1, 'title', 'description', 'слова'),
(1002, 472, 'Item', 2, 'product/produ245ct1-p1r', 'product/product/produ245ct1-p1r', 1, 'title', 'description', 'слова'),
(1003, 473, 'Item', 2, 'product/produ246ct1-p1r', 'product/product/produ246ct1-p1r', 1, 'title', 'description', 'слова'),
(1004, 474, 'Item', 2, 'product/produ247ct1-p1r', 'product/product/produ247ct1-p1r', 1, 'title', 'description', 'слова'),
(1005, 475, 'Item', 2, 'product/produ248ct1-p1r', 'product/product/produ248ct1-p1r', 1, 'title', 'description', 'слова'),
(1006, 476, 'Item', 2, 'product/produ249ct1-p1r', 'product/product/produ249ct1-p1r', 1, 'title', 'description', 'слова'),
(1007, 477, 'Item', 2, 'product/produ250ct1-p1r', 'product/product/produ250ct1-p1r', 1, 'title', 'description', 'слова'),
(1008, 478, 'Item', 2, 'product/produ251ct1-p1r', 'product/product/produ251ct1-p1r', 1, 'title', 'description', 'слова'),
(1009, 479, 'Item', 2, 'product/produ252ct1-p1r', 'product/product/produ252ct1-p1r', 1, 'title', 'description', 'слова'),
(1010, 480, 'Item', 2, 'product/produ253ct1-p1r', 'product/product/produ253ct1-p1r', 1, 'title', 'description', 'слова'),
(1011, 481, 'Item', 2, 'product/produ254ct1-p1r', 'product/product/produ254ct1-p1r', 1, 'title', 'description', 'слова'),
(1012, 482, 'Item', 2, 'product/produ255ct1-p1r', 'product/product/produ255ct1-p1r', 1, 'title', 'description', 'слова'),
(1013, 483, 'Item', 2, 'product/produ256ct1-p1r', 'product/product/produ256ct1-p1r', 1, 'title', 'description', 'слова'),
(1014, 484, 'Item', 2, 'product/produ257ct1-p1r', 'product/product/produ257ct1-p1r', 1, 'title', 'description', 'слова'),
(1015, 485, 'Item', 2, 'product/produ258ct1-p1r', 'product/product/produ258ct1-p1r', 1, 'title', 'description', 'слова'),
(1016, 486, 'Item', 2, 'product/produ259ct1-p1r', 'product/product/produ259ct1-p1r', 1, 'title', 'description', 'слова'),
(1017, 487, 'Item', 2, 'product/produ260ct1-p1r', 'product/product/produ260ct1-p1r', 1, 'title', 'description', 'слова'),
(1018, 488, 'Item', 2, 'product/produ261ct1-p1r', 'product/product/produ261ct1-p1r', 1, 'title', 'description', 'слова'),
(1019, 489, 'Item', 2, 'product/produ262ct1-p1r', 'product/product/produ262ct1-p1r', 1, 'title', 'description', 'слова'),
(1020, 490, 'Item', 2, 'product/produ263ct1-p1r', 'product/product/produ263ct1-p1r', 1, 'title', 'description', 'слова'),
(1021, 491, 'Item', 2, 'product/produ264ct1-p1r', 'product/product/produ264ct1-p1r', 1, 'title', 'description', 'слова'),
(1022, 492, 'Item', 2, 'product/produ265ct1-p1r', 'product/product/produ265ct1-p1r', 1, 'title', 'description', 'слова'),
(1023, 493, 'Item', 2, 'product/produ266ct1-p1r', 'product/product/produ266ct1-p1r', 1, 'title', 'description', 'слова'),
(1024, 494, 'Item', 2, 'product/produ267ct1-p1r', 'product/product/produ267ct1-p1r', 1, 'title', 'description', 'слова'),
(1025, 495, 'Item', 2, 'product/produ268ct1-p1r', 'product/product/produ268ct1-p1r', 1, 'title', 'description', 'слова'),
(1026, 496, 'Item', 2, 'product/produ269ct1-p1r', 'product/product/produ269ct1-p1r', 1, 'title', 'description', 'слова'),
(1027, 497, 'Item', 2, 'product/produ270ct1-p1r', 'product/product/produ270ct1-p1r', 1, 'title', 'description', 'слова'),
(1028, 498, 'Item', 2, 'product/produ271ct1-p1r', 'product/product/produ271ct1-p1r', 1, 'title', 'description', 'слова'),
(1029, 499, 'Item', 2, 'product/produ272ct1-p1r', 'product/product/produ272ct1-p1r', 1, 'title', 'description', 'слова'),
(1030, 500, 'Item', 2, 'product/produ273ct1-p1r', 'product/product/produ273ct1-p1r', 1, 'title', 'description', 'слова'),
(1031, 501, 'Item', 2, 'product/produ274ct1-p1r', 'product/product/produ274ct1-p1r', 1, 'title', 'description', 'слова'),
(1032, 502, 'Item', 2, 'product/produ275ct1-p1r', 'product/product/produ275ct1-p1r', 1, 'title', 'description', 'слова'),
(1033, 503, 'Item', 2, 'product/produ276ct1-p1r', 'product/product/produ276ct1-p1r', 1, 'title', 'description', 'слова'),
(1034, 504, 'Item', 2, 'product/produ277ct1-p1r', 'product/product/produ277ct1-p1r', 1, 'title', 'description', 'слова'),
(1035, 505, 'Item', 2, 'product/produ278ct1-p1r', 'product/product/produ278ct1-p1r', 1, 'title', 'description', 'слова'),
(1036, 506, 'Item', 2, 'product/produ279ct1-p1r', 'product/product/produ279ct1-p1r', 1, 'title', 'description', 'слова'),
(1037, 507, 'Item', 2, 'product/produ280ct1-p1r', 'product/product/produ280ct1-p1r', 1, 'title', 'description', 'слова'),
(1038, 508, 'Item', 2, 'product/produ281ct1-p1r', 'product/product/produ281ct1-p1r', 1, 'title', 'description', 'слова'),
(1039, 509, 'Item', 2, 'product/produ282ct1-p1r', 'product/product/produ282ct1-p1r', 1, 'title', 'description', 'слова'),
(1040, 510, 'Item', 2, 'product/produ283ct1-p1r', 'product/product/produ283ct1-p1r', 1, 'title', 'description', 'слова'),
(1041, 511, 'Item', 2, 'product/produ284ct1-p1r', 'product/product/produ284ct1-p1r', 1, 'title', 'description', 'слова'),
(1042, 512, 'Item', 2, 'product/produ285ct1-p1r', 'product/product/produ285ct1-p1r', 1, 'title', 'description', 'слова'),
(1043, 513, 'Item', 2, 'product/produ286ct1-p1r', 'product/product/produ286ct1-p1r', 1, 'title', 'description', 'слова'),
(1044, 514, 'Item', 2, 'product/produ287ct1-p1r', 'product/product/produ287ct1-p1r', 1, 'title', 'description', 'слова'),
(1045, 515, 'Item', 2, 'product/produ288ct1-p1r', 'product/product/produ288ct1-p1r', 1, 'title', 'description', 'слова'),
(1046, 516, 'Item', 2, 'product/produ289ct1-p1r', 'product/product/produ289ct1-p1r', 1, 'title', 'description', 'слова'),
(1047, 517, 'Item', 2, 'product/produ290ct1-p1r', 'product/product/produ290ct1-p1r', 1, 'title', 'description', 'слова'),
(1048, 518, 'Item', 2, 'product/produ291ct1-p1r', 'product/product/produ291ct1-p1r', 1, 'title', 'description', 'слова'),
(1049, 519, 'Item', 2, 'product/produ292ct1-p1r', 'product/product/produ292ct1-p1r', 1, 'title', 'description', 'слова'),
(1050, 520, 'Item', 2, 'product/produ293ct1-p1r', 'product/product/produ293ct1-p1r', 1, 'title', 'description', 'слова'),
(1051, 521, 'Item', 2, 'product/produ294ct1-p1r', 'product/product/produ294ct1-p1r', 1, 'title', 'description', 'слова'),
(1052, 522, 'Item', 2, 'product/produ295ct1-p1r', 'product/product/produ295ct1-p1r', 1, 'title', 'description', 'слова'),
(1053, 523, 'Item', 2, 'product/produ296ct1-p1r', 'product/product/produ296ct1-p1r', 1, 'title', 'description', 'слова'),
(1054, 524, 'Item', 2, 'product/produ297ct1-p1r', 'product/product/produ297ct1-p1r', 1, 'title', 'description', 'слова'),
(1055, 525, 'Item', 2, 'product/produ298ct1-p1r', 'product/product/produ298ct1-p1r', 1, 'title', 'description', 'слова'),
(1056, 526, 'Item', 2, 'product/produ299ct1-p1r', 'product/product/produ299ct1-p1r', 1, 'title', 'description', 'слова'),
(1057, 527, 'Item', 2, 'product/produ300ct1-p1r', 'product/product/produ300ct1-p1r', 1, 'title', 'description', 'слова'),
(1058, 528, 'Item', 2, 'product/produ301ct1-p1r', 'product/product/produ301ct1-p1r', 1, 'title', 'description', 'слова'),
(1059, 529, 'Item', 2, 'product/produ302ct1-p1r', 'product/product/produ302ct1-p1r', 1, 'title', 'description', 'слова'),
(1060, 530, 'Item', 2, 'product/produ303ct1-p1r', 'product/product/produ303ct1-p1r', 1, 'title', 'description', 'слова'),
(1061, 531, 'Item', 2, 'product/produ304ct1-p1r', 'product/product/produ304ct1-p1r', 1, 'title', 'description', 'слова'),
(1062, 532, 'Item', 2, 'product/produ305ct1-p1r', 'product/product/produ305ct1-p1r', 1, 'title', 'description', 'слова'),
(1063, 533, 'Item', 2, 'product/produ306ct1-p1r', 'product/product/produ306ct1-p1r', 1, 'title', 'description', 'слова');
INSERT INTO `core_pages_meta` (`id`, `owner_id`, `model`, `page_id`, `request_path`, `target_path`, `active`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1064, 534, 'Item', 2, 'product/produ307ct1-p1r', 'product/product/produ307ct1-p1r', 1, 'title', 'description', 'слова'),
(1065, 535, 'Item', 2, 'product/produ308ct1-p1r', 'product/product/produ308ct1-p1r', 1, 'title', 'description', 'слова'),
(1066, 536, 'Item', 2, 'product/produ309ct1-p1r', 'product/product/produ309ct1-p1r', 1, 'title', 'description', 'слова'),
(1067, 537, 'Item', 2, 'product/produ310ct1-p1r', 'product/product/produ310ct1-p1r', 1, 'title', 'description', 'слова'),
(1068, 538, 'Item', 2, 'product/produ311ct1-p1r', 'product/product/produ311ct1-p1r', 1, 'title', 'description', 'слова'),
(1069, 539, 'Item', 2, 'product/produ312ct1-p1r', 'product/product/produ312ct1-p1r', 1, 'title', 'description', 'слова'),
(1070, 540, 'Item', 2, 'product/produ313ct1-p1r', 'product/product/produ313ct1-p1r', 1, 'title', 'description', 'слова'),
(1071, 541, 'Item', 2, 'product/produ314ct1-p1r', 'product/product/produ314ct1-p1r', 1, 'title', 'description', 'слова'),
(1072, 542, 'Item', 2, 'product/produ315ct1-p1r', 'product/product/produ315ct1-p1r', 1, 'title', 'description', 'слова'),
(1073, 543, 'Item', 2, 'product/produ316ct1-p1r', 'product/product/produ316ct1-p1r', 1, 'title', 'description', 'слова'),
(1074, 544, 'Item', 2, 'product/produ317ct1-p1r', 'product/product/produ317ct1-p1r', 1, 'title', 'description', 'слова'),
(1075, 545, 'Item', 2, 'product/produ318ct1-p1r', 'product/product/produ318ct1-p1r', 1, 'title', 'description', 'слова'),
(1076, 546, 'Item', 2, 'product/produ319ct1-p1r', 'product/product/produ319ct1-p1r', 1, 'title', 'description', 'слова'),
(1077, 547, 'Item', 2, 'product/produ320ct1-p1r', 'product/product/produ320ct1-p1r', 1, 'title', 'description', 'слова'),
(1078, 548, 'Item', 2, 'product/produ321ct1-p1r', 'product/product/produ321ct1-p1r', 1, 'title', 'description', 'слова'),
(1079, 549, 'Item', 2, 'product/produ322ct1-p1r', 'product/product/produ322ct1-p1r', 1, 'title', 'description', 'слова'),
(1080, 550, 'Item', 2, 'product/produ323ct1-p1r', 'product/product/produ323ct1-p1r', 1, 'title', 'description', 'слова'),
(1081, 551, 'Item', 2, 'product/produ324ct1-p1r', 'product/product/produ324ct1-p1r', 1, 'title', 'description', 'слова'),
(1082, 552, 'Item', 2, 'product/produ325ct1-p1r', 'product/product/produ325ct1-p1r', 1, 'title', 'description', 'слова'),
(1083, 553, 'Item', 2, 'product/produ326ct1-p1r', 'product/product/produ326ct1-p1r', 1, 'title', 'description', 'слова'),
(1084, 554, 'Item', 2, 'product/produ327ct1-p1r', 'product/product/produ327ct1-p1r', 1, 'title', 'description', 'слова'),
(1085, 555, 'Item', 2, 'product/produ328ct1-p1r', 'product/product/produ328ct1-p1r', 1, 'title', 'description', 'слова'),
(1086, 556, 'Item', 2, 'product/produ329ct1-p1r', 'product/product/produ329ct1-p1r', 1, 'title', 'description', 'слова'),
(1087, 557, 'Item', 2, 'product/produ330ct1-p1r', 'product/product/produ330ct1-p1r', 1, 'title', 'description', 'слова'),
(1088, 558, 'Item', 2, 'product/produ331ct1-p1r', 'product/product/produ331ct1-p1r', 1, 'title', 'description', 'слова'),
(1089, 559, 'Item', 2, 'product/produ332ct1-p1r', 'product/product/produ332ct1-p1r', 1, 'title', 'description', 'слова'),
(1090, 560, 'Item', 2, 'product/produ333ct1-p1r', 'product/product/produ333ct1-p1r', 1, 'title', 'description', 'слова'),
(1091, 561, 'Item', 2, 'product/produ334ct1-p1r', 'product/product/produ334ct1-p1r', 1, 'title', 'description', 'слова'),
(1092, 562, 'Item', 2, 'product/produ335ct1-p1r', 'product/product/produ335ct1-p1r', 1, 'title', 'description', 'слова'),
(1093, 563, 'Item', 2, 'product/produ336ct1-p1r', 'product/product/produ336ct1-p1r', 1, 'title', 'description', 'слова'),
(1094, 564, 'Item', 2, 'product/produ337ct1-p1r', 'product/product/produ337ct1-p1r', 1, 'title', 'description', 'слова'),
(1095, 565, 'Item', 2, 'product/produ338ct1-p1r', 'product/product/produ338ct1-p1r', 1, 'title', 'description', 'слова'),
(1096, 566, 'Item', 2, 'product/produ339ct1-p1r', 'product/product/produ339ct1-p1r', 1, 'title', 'description', 'слова'),
(1097, 567, 'Item', 2, 'product/produ340ct1-p1r', 'product/product/produ340ct1-p1r', 1, 'title', 'description', 'слова'),
(1098, 568, 'Item', 2, 'product/produ341ct1-p1r', 'product/product/produ341ct1-p1r', 1, 'title', 'description', 'слова'),
(1099, 569, 'Item', 2, 'product/produ342ct1-p1r', 'product/product/produ342ct1-p1r', 1, 'title', 'description', 'слова'),
(1100, 570, 'Item', 2, 'product/produ343ct1-p1r', 'product/product/produ343ct1-p1r', 1, 'title', 'description', 'слова'),
(1101, 571, 'Item', 2, 'product/produ344ct1-p1r', 'product/product/produ344ct1-p1r', 1, 'title', 'description', 'слова'),
(1102, 572, 'Item', 2, 'product/produ345ct1-p1r', 'product/product/produ345ct1-p1r', 1, 'title', 'description', 'слова'),
(1103, 573, 'Item', 2, 'product/produ346ct1-p1r', 'product/product/produ346ct1-p1r', 1, 'title', 'description', 'слова'),
(1104, 574, 'Item', 2, 'product/produ347ct1-p1r', 'product/product/produ347ct1-p1r', 1, 'title', 'description', 'слова'),
(1105, 575, 'Item', 2, 'product/produ348ct1-p1r', 'product/product/produ348ct1-p1r', 1, 'title', 'description', 'слова'),
(1106, 576, 'Item', 2, 'product/produ349ct1-p1r', 'product/product/produ349ct1-p1r', 1, 'title', 'description', 'слова'),
(1107, 577, 'Item', 2, 'product/produ350ct1-p1r', 'product/product/produ350ct1-p1r', 1, 'title', 'description', 'слова'),
(1108, 578, 'Item', 2, 'product/produ351ct1-p1r', 'product/product/produ351ct1-p1r', 1, 'title', 'description', 'слова'),
(1109, 579, 'Item', 2, 'product/produ352ct1-p1r', 'product/product/produ352ct1-p1r', 1, 'title', 'description', 'слова'),
(1110, 580, 'Item', 2, 'product/produ353ct1-p1r', 'product/product/produ353ct1-p1r', 1, 'title', 'description', 'слова'),
(1111, 581, 'Item', 2, 'product/produ354ct1-p1r', 'product/product/produ354ct1-p1r', 1, 'title', 'description', 'слова'),
(1112, 582, 'Item', 2, 'product/produ355ct1-p1r', 'product/product/produ355ct1-p1r', 1, 'title', 'description', 'слова'),
(1113, 583, 'Item', 2, 'product/produ356ct1-p1r', 'product/product/produ356ct1-p1r', 1, 'title', 'description', 'слова'),
(1114, 584, 'Item', 2, 'product/produ357ct1-p1r', 'product/product/produ357ct1-p1r', 1, 'title', 'description', 'слова'),
(1115, 585, 'Item', 2, 'product/produ358ct1-p1r', 'product/product/produ358ct1-p1r', 1, 'title', 'description', 'слова'),
(1116, 586, 'Item', 2, 'product/produ359ct1-p1r', 'product/product/produ359ct1-p1r', 1, 'title', 'description', 'слова'),
(1117, 587, 'Item', 2, 'product/produ360ct1-p1r', 'product/product/produ360ct1-p1r', 1, 'title', 'description', 'слова'),
(1118, 588, 'Item', 2, 'product/produ361ct1-p1r', 'product/product/produ361ct1-p1r', 1, 'title', 'description', 'слова'),
(1119, 589, 'Item', 2, 'product/produ362ct1-p1r', 'product/product/produ362ct1-p1r', 1, 'title', 'description', 'слова'),
(1120, 590, 'Item', 2, 'product/produ363ct1-p1r', 'product/product/produ363ct1-p1r', 1, 'title', 'description', 'слова'),
(1121, 591, 'Item', 2, 'product/produ364ct1-p1r', 'product/product/produ364ct1-p1r', 1, 'title', 'description', 'слова'),
(1122, 592, 'Item', 2, 'product/produ365ct1-p1r', 'product/product/produ365ct1-p1r', 1, 'title', 'description', 'слова'),
(1123, 593, 'Item', 2, 'product/produ366ct1-p1r', 'product/product/produ366ct1-p1r', 1, 'title', 'description', 'слова'),
(1124, 594, 'Item', 2, 'product/produ367ct1-p1r', 'product/product/produ367ct1-p1r', 1, 'title', 'description', 'слова'),
(1125, 595, 'Item', 2, 'product/produ368ct1-p1r', 'product/product/produ368ct1-p1r', 1, 'title', 'description', 'слова'),
(1126, 596, 'Item', 2, 'product/produ369ct1-p1r', 'product/product/produ369ct1-p1r', 1, 'title', 'description', 'слова'),
(1127, 597, 'Item', 2, 'product/produ370ct1-p1r', 'product/product/produ370ct1-p1r', 1, 'title', 'description', 'слова'),
(1128, 598, 'Item', 2, 'product/produ371ct1-p1r', 'product/product/produ371ct1-p1r', 1, 'title', 'description', 'слова'),
(1129, 599, 'Item', 2, 'product/produ372ct1-p1r', 'product/product/produ372ct1-p1r', 1, 'title', 'description', 'слова'),
(1130, 600, 'Item', 2, 'product/produ373ct1-p1r', 'product/product/produ373ct1-p1r', 1, 'title', 'description', 'слова'),
(1131, 601, 'Item', 2, 'product/produ374ct1-p1r', 'product/product/produ374ct1-p1r', 1, 'title', 'description', 'слова'),
(1132, 602, 'Item', 2, 'product/produ375ct1-p1r', 'product/product/produ375ct1-p1r', 1, 'title', 'description', 'слова'),
(1133, 603, 'Item', 2, 'product/produ376ct1-p1r', 'product/product/produ376ct1-p1r', 1, 'title', 'description', 'слова'),
(1134, 604, 'Item', 2, 'product/produ377ct1-p1r', 'product/product/produ377ct1-p1r', 1, 'title', 'description', 'слова'),
(1135, 605, 'Item', 2, 'product/produ378ct1-p1r', 'product/product/produ378ct1-p1r', 1, 'title', 'description', 'слова'),
(1136, 606, 'Item', 2, 'product/produ379ct1-p1r', 'product/product/produ379ct1-p1r', 1, 'title', 'description', 'слова'),
(1137, 607, 'Item', 2, 'product/produ380ct1-p1r', 'product/product/produ380ct1-p1r', 1, 'title', 'description', 'слова'),
(1138, 608, 'Item', 2, 'product/produ381ct1-p1r', 'product/product/produ381ct1-p1r', 1, 'title', 'description', 'слова'),
(1139, 609, 'Item', 2, 'product/produ382ct1-p1r', 'product/product/produ382ct1-p1r', 1, 'title', 'description', 'слова'),
(1140, 610, 'Item', 2, 'product/produ383ct1-p1r', 'product/product/produ383ct1-p1r', 1, 'title', 'description', 'слова'),
(1141, 611, 'Item', 2, 'product/produ384ct1-p1r', 'product/product/produ384ct1-p1r', 1, 'title', 'description', 'слова'),
(1142, 612, 'Item', 2, 'product/produ385ct1-p1r', 'product/product/produ385ct1-p1r', 1, 'title', 'description', 'слова'),
(1143, 613, 'Item', 2, 'product/produ386ct1-p1r', 'product/product/produ386ct1-p1r', 1, 'title', 'description', 'слова'),
(1144, 614, 'Item', 2, 'product/produ387ct1-p1r', 'product/product/produ387ct1-p1r', 1, 'title', 'description', 'слова'),
(1145, 615, 'Item', 2, 'product/produ388ct1-p1r', 'product/product/produ388ct1-p1r', 1, 'title', 'description', 'слова'),
(1146, 616, 'Item', 2, 'product/produ389ct1-p1r', 'product/product/produ389ct1-p1r', 1, 'title', 'description', 'слова'),
(1147, 617, 'Item', 2, 'product/produ390ct1-p1r', 'product/product/produ390ct1-p1r', 1, 'title', 'description', 'слова'),
(1148, 618, 'Item', 2, 'product/produ391ct1-p1r', 'product/product/produ391ct1-p1r', 1, 'title', 'description', 'слова'),
(1149, 619, 'Item', 2, 'product/produ392ct1-p1r', 'product/product/produ392ct1-p1r', 1, 'title', 'description', 'слова'),
(1150, 620, 'Item', 2, 'product/produ393ct1-p1r', 'product/product/produ393ct1-p1r', 1, 'title', 'description', 'слова'),
(1151, 621, 'Item', 2, 'product/produ394ct1-p1r', 'product/product/produ394ct1-p1r', 1, 'title', 'description', 'слова'),
(1152, 622, 'Item', 2, 'product/produ395ct1-p1r', 'product/product/produ395ct1-p1r', 1, 'title', 'description', 'слова'),
(1153, 623, 'Item', 2, 'product/produ396ct1-p1r', 'product/product/produ396ct1-p1r', 1, 'title', 'description', 'слова'),
(1154, 624, 'Item', 2, 'product/produ397ct1-p1r', 'product/product/produ397ct1-p1r', 1, 'title', 'description', 'слова'),
(1155, 625, 'Item', 2, 'product/produ398ct1-p1r', 'product/product/produ398ct1-p1r', 1, 'title', 'description', 'слова'),
(1156, 626, 'Item', 2, 'product/produ399ct1-p1r', 'product/product/produ399ct1-p1r', 1, 'title', 'description', 'слова'),
(1157, 627, 'Item', 2, 'product/produ400ct1-p1r', 'product/product/produ400ct1-p1r', 1, 'title', 'description', 'слова'),
(1158, 628, 'Item', 2, 'product/produ401ct1-p1r', 'product/product/produ401ct1-p1r', 1, 'title', 'description', 'слова'),
(1159, 629, 'Item', 2, 'product/produ402ct1-p1r', 'product/product/produ402ct1-p1r', 1, 'title', 'description', 'слова'),
(1160, 630, 'Item', 2, 'product/produ403ct1-p1r', 'product/product/produ403ct1-p1r', 1, 'title', 'description', 'слова'),
(1161, 631, 'Item', 2, 'product/produ404ct1-p1r', 'product/product/produ404ct1-p1r', 1, 'title', 'description', 'слова'),
(1162, 632, 'Item', 2, 'product/produ405ct1-p1r', 'product/product/produ405ct1-p1r', 1, 'title', 'description', 'слова'),
(1163, 633, 'Item', 2, 'product/produ406ct1-p1r', 'product/product/produ406ct1-p1r', 1, 'title', 'description', 'слова'),
(1164, 634, 'Item', 2, 'product/produ407ct1-p1r', 'product/product/produ407ct1-p1r', 1, 'title', 'description', 'слова'),
(1165, 635, 'Item', 2, 'product/produ408ct1-p1r', 'product/product/produ408ct1-p1r', 1, 'title', 'description', 'слова'),
(1166, 636, 'Item', 2, 'product/produ409ct1-p1r', 'product/product/produ409ct1-p1r', 1, 'title', 'description', 'слова'),
(1167, 637, 'Item', 2, 'product/produ410ct1-p1r', 'product/product/produ410ct1-p1r', 1, 'title', 'description', 'слова'),
(1168, 638, 'Item', 2, 'product/produ411ct1-p1r', 'product/product/produ411ct1-p1r', 1, 'title', 'description', 'слова'),
(1169, 639, 'Item', 2, 'product/produ412ct1-p1r', 'product/product/produ412ct1-p1r', 1, 'title', 'description', 'слова'),
(1170, 640, 'Item', 2, 'product/produ413ct1-p1r', 'product/product/produ413ct1-p1r', 1, 'title', 'description', 'слова'),
(1171, 641, 'Item', 2, 'product/produ414ct1-p1r', 'product/product/produ414ct1-p1r', 1, 'title', 'description', 'слова'),
(1172, 642, 'Item', 2, 'product/produ415ct1-p1r', 'product/product/produ415ct1-p1r', 1, 'title', 'description', 'слова'),
(1173, 643, 'Item', 2, 'product/produ416ct1-p1r', 'product/product/produ416ct1-p1r', 1, 'title', 'description', 'слова'),
(1174, 644, 'Item', 2, 'product/produ417ct1-p1r', 'product/product/produ417ct1-p1r', 1, 'title', 'description', 'слова'),
(1175, 645, 'Item', 2, 'product/produ418ct1-p1r', 'product/product/produ418ct1-p1r', 1, 'title', 'description', 'слова'),
(1176, 646, 'Item', 2, 'product/produ419ct1-p1r', 'product/product/produ419ct1-p1r', 1, 'title', 'description', 'слова'),
(1177, 647, 'Item', 2, 'product/produ420ct1-p1r', 'product/product/produ420ct1-p1r', 1, 'title', 'description', 'слова'),
(1178, 648, 'Item', 2, 'product/produ421ct1-p1r', 'product/product/produ421ct1-p1r', 1, 'title', 'description', 'слова'),
(1179, 649, 'Item', 2, 'product/produ422ct1-p1r', 'product/product/produ422ct1-p1r', 1, 'title', 'description', 'слова'),
(1180, 650, 'Item', 2, 'product/produ423ct1-p1r', 'product/product/produ423ct1-p1r', 1, 'title', 'description', 'слова'),
(1181, 651, 'Item', 2, 'product/produ424ct1-p1r', 'product/product/produ424ct1-p1r', 1, 'title', 'description', 'слова'),
(1182, 652, 'Item', 2, 'product/produ425ct1-p1r', 'product/product/produ425ct1-p1r', 1, 'title', 'description', 'слова'),
(1183, 653, 'Item', 2, 'product/produ426ct1-p1r', 'product/product/produ426ct1-p1r', 1, 'title', 'description', 'слова'),
(1184, 654, 'Item', 2, 'product/produ427ct1-p1r', 'product/product/produ427ct1-p1r', 1, 'title', 'description', 'слова'),
(1185, 655, 'Item', 2, 'product/produ428ct1-p1r', 'product/product/produ428ct1-p1r', 1, 'title', 'description', 'слова'),
(1186, 656, 'Item', 2, 'product/produ429ct1-p1r', 'product/product/produ429ct1-p1r', 1, 'title', 'description', 'слова'),
(1187, 657, 'Item', 2, 'product/produ430ct1-p1r', 'product/product/produ430ct1-p1r', 1, 'title', 'description', 'слова'),
(1188, 658, 'Item', 2, 'product/produ431ct1-p1r', 'product/product/produ431ct1-p1r', 1, 'title', 'description', 'слова'),
(1189, 659, 'Item', 2, 'product/produ432ct1-p1r', 'product/product/produ432ct1-p1r', 1, 'title', 'description', 'слова'),
(1190, 660, 'Item', 2, 'product/produ433ct1-p1r', 'product/product/produ433ct1-p1r', 1, 'title', 'description', 'слова'),
(1191, 661, 'Item', 2, 'product/produ434ct1-p1r', 'product/product/produ434ct1-p1r', 1, 'title', 'description', 'слова'),
(1192, 662, 'Item', 2, 'product/produ435ct1-p1r', 'product/product/produ435ct1-p1r', 1, 'title', 'description', 'слова'),
(1193, 663, 'Item', 2, 'product/produ436ct1-p1r', 'product/product/produ436ct1-p1r', 1, 'title', 'description', 'слова'),
(1194, 664, 'Item', 2, 'product/produ437ct1-p1r', 'product/product/produ437ct1-p1r', 1, 'title', 'description', 'слова'),
(1195, 665, 'Item', 2, 'product/produ438ct1-p1r', 'product/product/produ438ct1-p1r', 1, 'title', 'description', 'слова'),
(1196, 666, 'Item', 2, 'product/produ439ct1-p1r', 'product/product/produ439ct1-p1r', 1, 'title', 'description', 'слова'),
(1197, 667, 'Item', 2, 'product/produ440ct1-p1r', 'product/product/produ440ct1-p1r', 1, 'title', 'description', 'слова'),
(1198, 668, 'Item', 2, 'product/produ441ct1-p1r', 'product/product/produ441ct1-p1r', 1, 'title', 'description', 'слова'),
(1199, 669, 'Item', 2, 'product/produ442ct1-p1r', 'product/product/produ442ct1-p1r', 1, 'title', 'description', 'слова'),
(1200, 670, 'Item', 2, 'product/produ443ct1-p1r', 'product/product/produ443ct1-p1r', 1, 'title', 'description', 'слова'),
(1201, 671, 'Item', 2, 'product/produ444ct1-p1r', 'product/product/produ444ct1-p1r', 1, 'title', 'description', 'слова'),
(1202, 672, 'Item', 2, 'product/produ445ct1-p1r', 'product/product/produ445ct1-p1r', 1, 'title', 'description', 'слова'),
(1203, 673, 'Item', 2, 'product/produ446ct1-p1r', 'product/product/produ446ct1-p1r', 1, 'title', 'description', 'слова'),
(1204, 674, 'Item', 2, 'product/produ447ct1-p1r', 'product/product/produ447ct1-p1r', 1, 'title', 'description', 'слова'),
(1205, 675, 'Item', 2, 'product/produ448ct1-p1r', 'product/product/produ448ct1-p1r', 1, 'title', 'description', 'слова'),
(1206, 676, 'Item', 2, 'product/produ449ct1-p1r', 'product/product/produ449ct1-p1r', 1, 'title', 'description', 'слова'),
(1207, 677, 'Item', 2, 'product/produ450ct1-p1r', 'product/product/produ450ct1-p1r', 1, 'title', 'description', 'слова'),
(1208, 678, 'Item', 2, 'product/produ451ct1-p1r', 'product/product/produ451ct1-p1r', 1, 'title', 'description', 'слова'),
(1209, 679, 'Item', 2, 'product/produ452ct1-p1r', 'product/product/produ452ct1-p1r', 1, 'title', 'description', 'слова'),
(1210, 680, 'Item', 2, 'product/produ453ct1-p1r', 'product/product/produ453ct1-p1r', 1, 'title', 'description', 'слова'),
(1211, 681, 'Item', 2, 'product/produ454ct1-p1r', 'product/product/produ454ct1-p1r', 1, 'title', 'description', 'слова'),
(1212, 682, 'Item', 2, 'product/produ455ct1-p1r', 'product/product/produ455ct1-p1r', 1, 'title', 'description', 'слова'),
(1213, 683, 'Item', 2, 'product/produ456ct1-p1r', 'product/product/produ456ct1-p1r', 1, 'title', 'description', 'слова'),
(1214, 684, 'Item', 2, 'product/produ457ct1-p1r', 'product/product/produ457ct1-p1r', 1, 'title', 'description', 'слова'),
(1215, 685, 'Item', 2, 'product/produ458ct1-p1r', 'product/product/produ458ct1-p1r', 1, 'title', 'description', 'слова'),
(1216, 686, 'Item', 2, 'product/produ459ct1-p1r', 'product/product/produ459ct1-p1r', 1, 'title', 'description', 'слова'),
(1217, 687, 'Item', 2, 'product/produ460ct1-p1r', 'product/product/produ460ct1-p1r', 1, 'title', 'description', 'слова'),
(1218, 688, 'Item', 2, 'product/produ461ct1-p1r', 'product/product/produ461ct1-p1r', 1, 'title', 'description', 'слова'),
(1219, 689, 'Item', 2, 'product/produ462ct1-p1r', 'product/product/produ462ct1-p1r', 1, 'title', 'description', 'слова'),
(1220, 690, 'Item', 2, 'product/produ463ct1-p1r', 'product/product/produ463ct1-p1r', 1, 'title', 'description', 'слова'),
(1221, 691, 'Item', 2, 'product/produ464ct1-p1r', 'product/product/produ464ct1-p1r', 1, 'title', 'description', 'слова'),
(1222, 692, 'Item', 2, 'product/produ465ct1-p1r', 'product/product/produ465ct1-p1r', 1, 'title', 'description', 'слова'),
(1223, 693, 'Item', 2, 'product/produ466ct1-p1r', 'product/product/produ466ct1-p1r', 1, 'title', 'description', 'слова'),
(1224, 694, 'Item', 2, 'product/produ467ct1-p1r', 'product/product/produ467ct1-p1r', 1, 'title', 'description', 'слова'),
(1225, 695, 'Item', 2, 'product/produ468ct1-p1r', 'product/product/produ468ct1-p1r', 1, 'title', 'description', 'слова'),
(1226, 696, 'Item', 2, 'product/produ469ct1-p1r', 'product/product/produ469ct1-p1r', 1, 'title', 'description', 'слова'),
(1227, 697, 'Item', 2, 'product/produ470ct1-p1r', 'product/product/produ470ct1-p1r', 1, 'title', 'description', 'слова'),
(1228, 698, 'Item', 2, 'product/produ471ct1-p1r', 'product/product/produ471ct1-p1r', 1, 'title', 'description', 'слова'),
(1229, 699, 'Item', 2, 'product/produ472ct1-p1r', 'product/product/produ472ct1-p1r', 1, 'title', 'description', 'слова'),
(1230, 700, 'Item', 2, 'product/produ473ct1-p1r', 'product/product/produ473ct1-p1r', 1, 'title', 'description', 'слова'),
(1231, 701, 'Item', 2, 'product/produ474ct1-p1r', 'product/product/produ474ct1-p1r', 1, 'title', 'description', 'слова'),
(1232, 702, 'Item', 2, 'product/produ475ct1-p1r', 'product/product/produ475ct1-p1r', 1, 'title', 'description', 'слова'),
(1233, 703, 'Item', 2, 'product/produ476ct1-p1r', 'product/product/produ476ct1-p1r', 1, 'title', 'description', 'слова'),
(1234, 704, 'Item', 2, 'product/produ477ct1-p1r', 'product/product/produ477ct1-p1r', 1, 'title', 'description', 'слова'),
(1235, 705, 'Item', 2, 'product/produ478ct1-p1r', 'product/product/produ478ct1-p1r', 1, 'title', 'description', 'слова'),
(1236, 706, 'Item', 2, 'product/produ479ct1-p1r', 'product/product/produ479ct1-p1r', 1, 'title', 'description', 'слова'),
(1237, 707, 'Item', 2, 'product/produ480ct1-p1r', 'product/product/produ480ct1-p1r', 1, 'title', 'description', 'слова'),
(1238, 708, 'Item', 2, 'product/produ481ct1-p1r', 'product/product/produ481ct1-p1r', 1, 'title', 'description', 'слова'),
(1239, 709, 'Item', 2, 'product/produ482ct1-p1r', 'product/product/produ482ct1-p1r', 1, 'title', 'description', 'слова'),
(1240, 710, 'Item', 2, 'product/produ483ct1-p1r', 'product/product/produ483ct1-p1r', 1, 'title', 'description', 'слова'),
(1241, 711, 'Item', 2, 'product/produ484ct1-p1r', 'product/product/produ484ct1-p1r', 1, 'title', 'description', 'слова'),
(1242, 712, 'Item', 2, 'product/produ485ct1-p1r', 'product/product/produ485ct1-p1r', 1, 'title', 'description', 'слова'),
(1243, 713, 'Item', 2, 'product/produ486ct1-p1r', 'product/product/produ486ct1-p1r', 1, 'title', 'description', 'слова'),
(1244, 714, 'Item', 2, 'product/produ487ct1-p1r', 'product/product/produ487ct1-p1r', 1, 'title', 'description', 'слова'),
(1245, 715, 'Item', 2, 'product/produ488ct1-p1r', 'product/product/produ488ct1-p1r', 1, 'title', 'description', 'слова'),
(1246, 716, 'Item', 2, 'product/produ489ct1-p1r', 'product/product/produ489ct1-p1r', 1, 'title', 'description', 'слова'),
(1247, 717, 'Item', 2, 'product/produ490ct1-p1r', 'product/product/produ490ct1-p1r', 1, 'title', 'description', 'слова'),
(1248, 718, 'Item', 2, 'product/produ491ct1-p1r', 'product/product/produ491ct1-p1r', 1, 'title', 'description', 'слова'),
(1249, 719, 'Item', 2, 'product/produ492ct1-p1r', 'product/product/produ492ct1-p1r', 1, 'title', 'description', 'слова'),
(1250, 720, 'Item', 2, 'product/produ493ct1-p1r', 'product/product/produ493ct1-p1r', 1, 'title', 'description', 'слова'),
(1251, 721, 'Item', 2, 'product/produ494ct1-p1r', 'product/product/produ494ct1-p1r', 1, 'title', 'description', 'слова'),
(1252, 722, 'Item', 2, 'product/produ495ct1-p1r', 'product/product/produ495ct1-p1r', 1, 'title', 'description', 'слова'),
(1253, 723, 'Item', 2, 'product/produ496ct1-p1r', 'product/product/produ496ct1-p1r', 1, 'title', 'description', 'слова'),
(1254, 724, 'Item', 2, 'product/produ497ct1-p1r', 'product/product/produ497ct1-p1r', 1, 'title', 'description', 'слова'),
(1255, 725, 'Item', 2, 'product/produ498ct1-p1r', 'product/product/produ498ct1-p1r', 1, 'title', 'description', 'слова'),
(1256, 726, 'Item', 2, 'product/produ499ct1-p1r', 'product/product/produ499ct1-p1r', 1, 'title', 'description', 'слова'),
(1257, 727, 'Item', 2, 'product/produ500ct1-p1r', 'product/product/produ500ct1-p1r', 1, 'title', 'description', 'слова'),
(1258, 728, 'Item', 2, 'product/produ501ct1-p1r', 'product/product/produ501ct1-p1r', 1, 'title', 'description', 'слова'),
(1259, 729, 'Item', 2, 'product/produ502ct1-p1r', 'product/product/produ502ct1-p1r', 1, 'title', 'description', 'слова'),
(1260, 730, 'Item', 2, 'product/produ503ct1-p1r', 'product/product/produ503ct1-p1r', 1, 'title', 'description', 'слова'),
(1261, 731, 'Item', 2, 'product/produ504ct1-p1r', 'product/product/produ504ct1-p1r', 1, 'title', 'description', 'слова'),
(1262, 732, 'Item', 2, 'product/produ505ct1-p1r', 'product/product/produ505ct1-p1r', 1, 'title', 'description', 'слова'),
(1263, 733, 'Item', 2, 'product/produ506ct1-p1r', 'product/product/produ506ct1-p1r', 1, 'title', 'description', 'слова'),
(1264, 734, 'Item', 2, 'product/produ507ct1-p1r', 'product/product/produ507ct1-p1r', 1, 'title', 'description', 'слова'),
(1265, 735, 'Item', 2, 'product/produ508ct1-p1r', 'product/product/produ508ct1-p1r', 1, 'title', 'description', 'слова'),
(1266, 736, 'Item', 2, 'product/produ509ct1-p1r', 'product/product/produ509ct1-p1r', 1, 'title', 'description', 'слова'),
(1267, 737, 'Item', 2, 'product/produ510ct1-p1r', 'product/product/produ510ct1-p1r', 1, 'title', 'description', 'слова'),
(1268, 738, 'Item', 2, 'product/produ511ct1-p1r', 'product/product/produ511ct1-p1r', 1, 'title', 'description', 'слова'),
(1269, 739, 'Item', 2, 'product/produ512ct1-p1r', 'product/product/produ512ct1-p1r', 1, 'title', 'description', 'слова'),
(1270, 740, 'Item', 2, 'product/produ513ct1-p1r', 'product/product/produ513ct1-p1r', 1, 'title', 'description', 'слова'),
(1271, 741, 'Item', 2, 'product/produ514ct1-p1r', 'product/product/produ514ct1-p1r', 1, 'title', 'description', 'слова'),
(1272, 742, 'Item', 2, 'product/produ515ct1-p1r', 'product/product/produ515ct1-p1r', 1, 'title', 'description', 'слова'),
(1273, 743, 'Item', 2, 'product/produ516ct1-p1r', 'product/product/produ516ct1-p1r', 1, 'title', 'description', 'слова'),
(1274, 744, 'Item', 2, 'product/produ517ct1-p1r', 'product/product/produ517ct1-p1r', 1, 'title', 'description', 'слова'),
(1275, 745, 'Item', 2, 'product/produ518ct1-p1r', 'product/product/produ518ct1-p1r', 1, 'title', 'description', 'слова'),
(1276, 746, 'Item', 2, 'product/produ519ct1-p1r', 'product/product/produ519ct1-p1r', 1, 'title', 'description', 'слова'),
(1277, 747, 'Item', 2, 'product/produ520ct1-p1r', 'product/product/produ520ct1-p1r', 1, 'title', 'description', 'слова'),
(1278, 748, 'Item', 2, 'product/produ521ct1-p1r', 'product/product/produ521ct1-p1r', 1, 'title', 'description', 'слова'),
(1279, 749, 'Item', 2, 'product/produ522ct1-p1r', 'product/product/produ522ct1-p1r', 1, 'title', 'description', 'слова'),
(1280, 750, 'Item', 2, 'product/produ523ct1-p1r', 'product/product/produ523ct1-p1r', 1, 'title', 'description', 'слова'),
(1281, 751, 'Item', 2, 'product/produ524ct1-p1r', 'product/product/produ524ct1-p1r', 1, 'title', 'description', 'слова'),
(1282, 752, 'Item', 2, 'product/produ525ct1-p1r', 'product/product/produ525ct1-p1r', 1, 'title', 'description', 'слова'),
(1283, 753, 'Item', 2, 'product/produ526ct1-p1r', 'product/product/produ526ct1-p1r', 1, 'title', 'description', 'слова'),
(1284, 754, 'Item', 2, 'product/produ527ct1-p1r', 'product/product/produ527ct1-p1r', 1, 'title', 'description', 'слова'),
(1285, 755, 'Item', 2, 'product/produ528ct1-p1r', 'product/product/produ528ct1-p1r', 1, 'title', 'description', 'слова'),
(1286, 756, 'Item', 2, 'product/produ529ct1-p1r', 'product/product/produ529ct1-p1r', 1, 'title', 'description', 'слова'),
(1287, 757, 'Item', 2, 'product/produ530ct1-p1r', 'product/product/produ530ct1-p1r', 1, 'title', 'description', 'слова'),
(1288, 758, 'Item', 2, 'product/produ531ct1-p1r', 'product/product/produ531ct1-p1r', 1, 'title', 'description', 'слова'),
(1289, 759, 'Item', 2, 'product/produ532ct1-p1r', 'product/product/produ532ct1-p1r', 1, 'title', 'description', 'слова'),
(1290, 760, 'Item', 2, 'product/produ533ct1-p1r', 'product/product/produ533ct1-p1r', 1, 'title', 'description', 'слова'),
(1291, 761, 'Item', 2, 'product/produ534ct1-p1r', 'product/product/produ534ct1-p1r', 1, 'title', 'description', 'слова'),
(1292, 762, 'Item', 2, 'product/produ535ct1-p1r', 'product/product/produ535ct1-p1r', 1, 'title', 'description', 'слова'),
(1293, 763, 'Item', 2, 'product/produ536ct1-p1r', 'product/product/produ536ct1-p1r', 1, 'title', 'description', 'слова'),
(1294, 764, 'Item', 2, 'product/produ537ct1-p1r', 'product/product/produ537ct1-p1r', 1, 'title', 'description', 'слова'),
(1295, 765, 'Item', 2, 'product/produ538ct1-p1r', 'product/product/produ538ct1-p1r', 1, 'title', 'description', 'слова'),
(1296, 766, 'Item', 2, 'product/produ539ct1-p1r', 'product/product/produ539ct1-p1r', 1, 'title', 'description', 'слова'),
(1297, 767, 'Item', 2, 'product/produ540ct1-p1r', 'product/product/produ540ct1-p1r', 1, 'title', 'description', 'слова'),
(1298, 768, 'Item', 2, 'product/produ541ct1-p1r', 'product/product/produ541ct1-p1r', 1, 'title', 'description', 'слова'),
(1299, 769, 'Item', 2, 'product/produ542ct1-p1r', 'product/product/produ542ct1-p1r', 1, 'title', 'description', 'слова'),
(1300, 770, 'Item', 2, 'product/produ543ct1-p1r', 'product/product/produ543ct1-p1r', 1, 'title', 'description', 'слова'),
(1301, 771, 'Item', 2, 'product/produ544ct1-p1r', 'product/product/produ544ct1-p1r', 1, 'title', 'description', 'слова'),
(1302, 772, 'Item', 2, 'product/produ545ct1-p1r', 'product/product/produ545ct1-p1r', 1, 'title', 'description', 'слова'),
(1303, 773, 'Item', 2, 'product/produ546ct1-p1r', 'product/product/produ546ct1-p1r', 1, 'title', 'description', 'слова'),
(1304, 774, 'Item', 2, 'product/produ547ct1-p1r', 'product/product/produ547ct1-p1r', 1, 'title', 'description', 'слова'),
(1305, 775, 'Item', 2, 'product/produ548ct1-p1r', 'product/product/produ548ct1-p1r', 1, 'title', 'description', 'слова'),
(1306, 776, 'Item', 2, 'product/produ549ct1-p1r', 'product/product/produ549ct1-p1r', 1, 'title', 'description', 'слова'),
(1307, 777, 'Item', 2, 'product/produ550ct1-p1r', 'product/product/produ550ct1-p1r', 1, 'title', 'description', 'слова'),
(1308, 778, 'Item', 2, 'product/produ551ct1-p1r', 'product/product/produ551ct1-p1r', 1, 'title', 'description', 'слова'),
(1309, 779, 'Item', 2, 'product/produ552ct1-p1r', 'product/product/produ552ct1-p1r', 1, 'title', 'description', 'слова'),
(1310, 780, 'Item', 2, 'product/produ553ct1-p1r', 'product/product/produ553ct1-p1r', 1, 'title', 'description', 'слова'),
(1311, 781, 'Item', 2, 'product/produ554ct1-p1r', 'product/product/produ554ct1-p1r', 1, 'title', 'description', 'слова'),
(1312, 782, 'Item', 2, 'product/produ555ct1-p1r', 'product/product/produ555ct1-p1r', 1, 'title', 'description', 'слова'),
(1313, 783, 'Item', 2, 'product/produ556ct1-p1r', 'product/product/produ556ct1-p1r', 1, 'title', 'description', 'слова'),
(1314, 784, 'Item', 2, 'product/produ557ct1-p1r', 'product/product/produ557ct1-p1r', 1, 'title', 'description', 'слова'),
(1315, 785, 'Item', 2, 'product/produ558ct1-p1r', 'product/product/produ558ct1-p1r', 1, 'title', 'description', 'слова'),
(1316, 786, 'Item', 2, 'product/produ559ct1-p1r', 'product/product/produ559ct1-p1r', 1, 'title', 'description', 'слова'),
(1317, 787, 'Item', 2, 'product/produ560ct1-p1r', 'product/product/produ560ct1-p1r', 1, 'title', 'description', 'слова'),
(1318, 788, 'Item', 2, 'product/produ561ct1-p1r', 'product/product/produ561ct1-p1r', 1, 'title', 'description', 'слова'),
(1319, 789, 'Item', 2, 'product/produ562ct1-p1r', 'product/product/produ562ct1-p1r', 1, 'title', 'description', 'слова'),
(1320, 790, 'Item', 2, 'product/produ563ct1-p1r', 'product/product/produ563ct1-p1r', 1, 'title', 'description', 'слова'),
(1321, 791, 'Item', 2, 'product/produ564ct1-p1r', 'product/product/produ564ct1-p1r', 1, 'title', 'description', 'слова'),
(1322, 792, 'Item', 2, 'product/produ565ct1-p1r', 'product/product/produ565ct1-p1r', 1, 'title', 'description', 'слова'),
(1323, 793, 'Item', 2, 'product/produ566ct1-p1r', 'product/product/produ566ct1-p1r', 1, 'title', 'description', 'слова'),
(1324, 794, 'Item', 2, 'product/produ567ct1-p1r', 'product/product/produ567ct1-p1r', 1, 'title', 'description', 'слова'),
(1325, 795, 'Item', 2, 'product/produ568ct1-p1r', 'product/product/produ568ct1-p1r', 1, 'title', 'description', 'слова'),
(1326, 796, 'Item', 2, 'product/produ569ct1-p1r', 'product/product/produ569ct1-p1r', 1, 'title', 'description', 'слова'),
(1327, 797, 'Item', 2, 'product/produ570ct1-p1r', 'product/product/produ570ct1-p1r', 1, 'title', 'description', 'слова'),
(1328, 798, 'Item', 2, 'product/produ571ct1-p1r', 'product/product/produ571ct1-p1r', 1, 'title', 'description', 'слова'),
(1329, 799, 'Item', 2, 'product/produ572ct1-p1r', 'product/product/produ572ct1-p1r', 1, 'title', 'description', 'слова'),
(1330, 800, 'Item', 2, 'product/produ573ct1-p1r', 'product/product/produ573ct1-p1r', 1, 'title', 'description', 'слова'),
(1331, 801, 'Item', 2, 'product/produ574ct1-p1r', 'product/product/produ574ct1-p1r', 1, 'title', 'description', 'слова'),
(1332, 802, 'Item', 2, 'product/produ575ct1-p1r', 'product/product/produ575ct1-p1r', 1, 'title', 'description', 'слова'),
(1333, 803, 'Item', 2, 'product/produ576ct1-p1r', 'product/product/produ576ct1-p1r', 1, 'title', 'description', 'слова'),
(1334, 804, 'Item', 2, 'product/produ577ct1-p1r', 'product/product/produ577ct1-p1r', 1, 'title', 'description', 'слова'),
(1335, 805, 'Item', 2, 'product/produ578ct1-p1r', 'product/product/produ578ct1-p1r', 1, 'title', 'description', 'слова'),
(1336, 806, 'Item', 2, 'product/produ579ct1-p1r', 'product/product/produ579ct1-p1r', 1, 'title', 'description', 'слова'),
(1337, 807, 'Item', 2, 'product/produ580ct1-p1r', 'product/product/produ580ct1-p1r', 1, 'title', 'description', 'слова'),
(1338, 808, 'Item', 2, 'product/produ581ct1-p1r', 'product/product/produ581ct1-p1r', 1, 'title', 'description', 'слова'),
(1339, 809, 'Item', 2, 'product/produ582ct1-p1r', 'product/product/produ582ct1-p1r', 1, 'title', 'description', 'слова'),
(1340, 810, 'Item', 2, 'product/produ583ct1-p1r', 'product/product/produ583ct1-p1r', 1, 'title', 'description', 'слова'),
(1341, 811, 'Item', 2, 'product/produ584ct1-p1r', 'product/product/produ584ct1-p1r', 1, 'title', 'description', 'слова'),
(1342, 812, 'Item', 2, 'product/produ585ct1-p1r', 'product/product/produ585ct1-p1r', 1, 'title', 'description', 'слова'),
(1343, 813, 'Item', 2, 'product/produ586ct1-p1r', 'product/product/produ586ct1-p1r', 1, 'title', 'description', 'слова'),
(1344, 814, 'Item', 2, 'product/produ587ct1-p1r', 'product/product/produ587ct1-p1r', 1, 'title', 'description', 'слова'),
(1345, 815, 'Item', 2, 'product/produ588ct1-p1r', 'product/product/produ588ct1-p1r', 1, 'title', 'description', 'слова'),
(1346, 816, 'Item', 2, 'product/produ589ct1-p1r', 'product/product/produ589ct1-p1r', 1, 'title', 'description', 'слова'),
(1347, 817, 'Item', 2, 'product/produ590ct1-p1r', 'product/product/produ590ct1-p1r', 1, 'title', 'description', 'слова'),
(1348, 818, 'Item', 2, 'product/produ591ct1-p1r', 'product/product/produ591ct1-p1r', 1, 'title', 'description', 'слова'),
(1349, 819, 'Item', 2, 'product/produ592ct1-p1r', 'product/product/produ592ct1-p1r', 1, 'title', 'description', 'слова'),
(1350, 820, 'Item', 2, 'product/produ593ct1-p1r', 'product/product/produ593ct1-p1r', 1, 'title', 'description', 'слова'),
(1351, 821, 'Item', 2, 'product/produ594ct1-p1r', 'product/product/produ594ct1-p1r', 1, 'title', 'description', 'слова'),
(1352, 822, 'Item', 2, 'product/produ595ct1-p1r', 'product/product/produ595ct1-p1r', 1, 'title', 'description', 'слова'),
(1353, 823, 'Item', 2, 'product/produ596ct1-p1r', 'product/product/produ596ct1-p1r', 1, 'title', 'description', 'слова'),
(1354, 824, 'Item', 2, 'product/produ597ct1-p1r', 'product/product/produ597ct1-p1r', 1, 'title', 'description', 'слова'),
(1355, 825, 'Item', 2, 'product/produ598ct1-p1r', 'product/product/produ598ct1-p1r', 1, 'title', 'description', 'слова'),
(1356, 826, 'Item', 2, 'product/produ599ct1-p1r', 'product/product/produ599ct1-p1r', 1, 'title', 'description', 'слова'),
(1357, 827, 'Item', 2, 'product/produ600ct1-p1r', 'product/product/produ600ct1-p1r', 1, 'title', 'description', 'слова'),
(1358, 828, 'Item', 2, 'product/produ601ct1-p1r', 'product/product/produ601ct1-p1r', 1, 'title', 'description', 'слова'),
(1359, 829, 'Item', 2, 'product/produ602ct1-p1r', 'product/product/produ602ct1-p1r', 1, 'title', 'description', 'слова'),
(1360, 830, 'Item', 2, 'product/produ603ct1-p1r', 'product/product/produ603ct1-p1r', 1, 'title', 'description', 'слова'),
(1361, 831, 'Item', 2, 'product/produ604ct1-p1r', 'product/product/produ604ct1-p1r', 1, 'title', 'description', 'слова'),
(1362, 832, 'Item', 2, 'product/produ605ct1-p1r', 'product/product/produ605ct1-p1r', 1, 'title', 'description', 'слова'),
(1363, 833, 'Item', 2, 'product/produ606ct1-p1r', 'product/product/produ606ct1-p1r', 1, 'title', 'description', 'слова'),
(1364, 834, 'Item', 2, 'product/produ607ct1-p1r', 'product/product/produ607ct1-p1r', 1, 'title', 'description', 'слова'),
(1365, 835, 'Item', 2, 'product/produ608ct1-p1r', 'product/product/produ608ct1-p1r', 1, 'title', 'description', 'слова'),
(1366, 836, 'Item', 2, 'product/produ609ct1-p1r', 'product/product/produ609ct1-p1r', 1, 'title', 'description', 'слова'),
(1367, 837, 'Item', 2, 'product/produ610ct1-p1r', 'product/product/produ610ct1-p1r', 1, 'title', 'description', 'слова'),
(1368, 838, 'Item', 2, 'product/produ611ct1-p1r', 'product/product/produ611ct1-p1r', 1, 'title', 'description', 'слова'),
(1369, 839, 'Item', 2, 'product/produ612ct1-p1r', 'product/product/produ612ct1-p1r', 1, 'title', 'description', 'слова'),
(1370, 840, 'Item', 2, 'product/produ613ct1-p1r', 'product/product/produ613ct1-p1r', 1, 'title', 'description', 'слова'),
(1371, 841, 'Item', 2, 'product/produ614ct1-p1r', 'product/product/produ614ct1-p1r', 1, 'title', 'description', 'слова'),
(1372, 842, 'Item', 2, 'product/produ615ct1-p1r', 'product/product/produ615ct1-p1r', 1, 'title', 'description', 'слова'),
(1373, 843, 'Item', 2, 'product/produ616ct1-p1r', 'product/product/produ616ct1-p1r', 1, 'title', 'description', 'слова'),
(1374, 844, 'Item', 2, 'product/produ617ct1-p1r', 'product/product/produ617ct1-p1r', 1, 'title', 'description', 'слова'),
(1375, 845, 'Item', 2, 'product/produ618ct1-p1r', 'product/product/produ618ct1-p1r', 1, 'title', 'description', 'слова'),
(1376, 846, 'Item', 2, 'product/produ619ct1-p1r', 'product/product/produ619ct1-p1r', 1, 'title', 'description', 'слова'),
(1377, 847, 'Item', 2, 'product/produ620ct1-p1r', 'product/product/produ620ct1-p1r', 1, 'title', 'description', 'слова'),
(1378, 848, 'Item', 2, 'product/produ621ct1-p1r', 'product/product/produ621ct1-p1r', 1, 'title', 'description', 'слова'),
(1379, 849, 'Item', 2, 'product/produ622ct1-p1r', 'product/product/produ622ct1-p1r', 1, 'title', 'description', 'слова'),
(1380, 850, 'Item', 2, 'product/produ623ct1-p1r', 'product/product/produ623ct1-p1r', 1, 'title', 'description', 'слова'),
(1381, 851, 'Item', 2, 'product/produ624ct1-p1r', 'product/product/produ624ct1-p1r', 1, 'title', 'description', 'слова'),
(1382, 852, 'Item', 2, 'product/produ625ct1-p1r', 'product/product/produ625ct1-p1r', 1, 'title', 'description', 'слова'),
(1383, 853, 'Item', 2, 'product/produ626ct1-p1r', 'product/product/produ626ct1-p1r', 1, 'title', 'description', 'слова'),
(1384, 854, 'Item', 2, 'product/produ627ct1-p1r', 'product/product/produ627ct1-p1r', 1, 'title', 'description', 'слова'),
(1385, 855, 'Item', 2, 'product/produ628ct1-p1r', 'product/product/produ628ct1-p1r', 1, 'title', 'description', 'слова'),
(1386, 856, 'Item', 2, 'product/produ629ct1-p1r', 'product/product/produ629ct1-p1r', 1, 'title', 'description', 'слова'),
(1387, 857, 'Item', 2, 'product/produ630ct1-p1r', 'product/product/produ630ct1-p1r', 1, 'title', 'description', 'слова'),
(1388, 858, 'Item', 2, 'product/produ631ct1-p1r', 'product/product/produ631ct1-p1r', 1, 'title', 'description', 'слова'),
(1389, 859, 'Item', 2, 'product/produ632ct1-p1r', 'product/product/produ632ct1-p1r', 1, 'title', 'description', 'слова'),
(1390, 860, 'Item', 2, 'product/produ633ct1-p1r', 'product/product/produ633ct1-p1r', 1, 'title', 'description', 'слова'),
(1391, 861, 'Item', 2, 'product/produ634ct1-p1r', 'product/product/produ634ct1-p1r', 1, 'title', 'description', 'слова'),
(1392, 862, 'Item', 2, 'product/produ635ct1-p1r', 'product/product/produ635ct1-p1r', 1, 'title', 'description', 'слова'),
(1393, 863, 'Item', 2, 'product/produ636ct1-p1r', 'product/product/produ636ct1-p1r', 1, 'title', 'description', 'слова'),
(1394, 864, 'Item', 2, 'product/produ637ct1-p1r', 'product/product/produ637ct1-p1r', 1, 'title', 'description', 'слова'),
(1395, 865, 'Item', 2, 'product/produ638ct1-p1r', 'product/product/produ638ct1-p1r', 1, 'title', 'description', 'слова'),
(1396, 866, 'Item', 2, 'product/produ639ct1-p1r', 'product/product/produ639ct1-p1r', 1, 'title', 'description', 'слова'),
(1397, 867, 'Item', 2, 'product/produ640ct1-p1r', 'product/product/produ640ct1-p1r', 1, 'title', 'description', 'слова'),
(1398, 868, 'Item', 2, 'product/produ641ct1-p1r', 'product/product/produ641ct1-p1r', 1, 'title', 'description', 'слова'),
(1399, 869, 'Item', 2, 'product/produ642ct1-p1r', 'product/product/produ642ct1-p1r', 1, 'title', 'description', 'слова'),
(1400, 870, 'Item', 2, 'product/produ643ct1-p1r', 'product/product/produ643ct1-p1r', 1, 'title', 'description', 'слова'),
(1401, 871, 'Item', 2, 'product/produ644ct1-p1r', 'product/product/produ644ct1-p1r', 1, 'title', 'description', 'слова'),
(1402, 872, 'Item', 2, 'product/produ645ct1-p1r', 'product/product/produ645ct1-p1r', 1, 'title', 'description', 'слова'),
(1403, 873, 'Item', 2, 'product/produ646ct1-p1r', 'product/product/produ646ct1-p1r', 1, 'title', 'description', 'слова'),
(1404, 874, 'Item', 2, 'product/produ647ct1-p1r', 'product/product/produ647ct1-p1r', 1, 'title', 'description', 'слова'),
(1405, 875, 'Item', 2, 'product/produ648ct1-p1r', 'product/product/produ648ct1-p1r', 1, 'title', 'description', 'слова'),
(1406, 876, 'Item', 2, 'product/produ649ct1-p1r', 'product/product/produ649ct1-p1r', 1, 'title', 'description', 'слова'),
(1407, 877, 'Item', 2, 'product/produ650ct1-p1r', 'product/product/produ650ct1-p1r', 1, 'title', 'description', 'слова'),
(1408, 878, 'Item', 2, 'product/produ651ct1-p1r', 'product/product/produ651ct1-p1r', 1, 'title', 'description', 'слова'),
(1409, 879, 'Item', 2, 'product/produ652ct1-p1r', 'product/product/produ652ct1-p1r', 1, 'title', 'description', 'слова'),
(1410, 880, 'Item', 2, 'product/produ653ct1-p1r', 'product/product/produ653ct1-p1r', 1, 'title', 'description', 'слова'),
(1411, 881, 'Item', 2, 'product/produ654ct1-p1r', 'product/product/produ654ct1-p1r', 1, 'title', 'description', 'слова'),
(1412, 882, 'Item', 2, 'product/produ655ct1-p1r', 'product/product/produ655ct1-p1r', 1, 'title', 'description', 'слова'),
(1413, 883, 'Item', 2, 'product/produ656ct1-p1r', 'product/product/produ656ct1-p1r', 1, 'title', 'description', 'слова'),
(1414, 884, 'Item', 2, 'product/produ657ct1-p1r', 'product/product/produ657ct1-p1r', 1, 'title', 'description', 'слова'),
(1415, 885, 'Item', 2, 'product/produ658ct1-p1r', 'product/product/produ658ct1-p1r', 1, 'title', 'description', 'слова'),
(1416, 886, 'Item', 2, 'product/produ659ct1-p1r', 'product/product/produ659ct1-p1r', 1, 'title', 'description', 'слова'),
(1417, 887, 'Item', 2, 'product/produ660ct1-p1r', 'product/product/produ660ct1-p1r', 1, 'title', 'description', 'слова'),
(1418, 888, 'Item', 2, 'product/produ661ct1-p1r', 'product/product/produ661ct1-p1r', 1, 'title', 'description', 'слова'),
(1419, 889, 'Item', 2, 'product/produ662ct1-p1r', 'product/product/produ662ct1-p1r', 1, 'title', 'description', 'слова'),
(1420, 890, 'Item', 2, 'product/produ663ct1-p1r', 'product/product/produ663ct1-p1r', 1, 'title', 'description', 'слова'),
(1421, 891, 'Item', 2, 'product/produ664ct1-p1r', 'product/product/produ664ct1-p1r', 1, 'title', 'description', 'слова'),
(1422, 892, 'Item', 2, 'product/produ665ct1-p1r', 'product/product/produ665ct1-p1r', 1, 'title', 'description', 'слова'),
(1423, 893, 'Item', 2, 'product/produ666ct1-p1r', 'product/product/produ666ct1-p1r', 1, 'title', 'description', 'слова'),
(1424, 894, 'Item', 2, 'product/produ667ct1-p1r', 'product/product/produ667ct1-p1r', 1, 'title', 'description', 'слова'),
(1425, 895, 'Item', 2, 'product/produ668ct1-p1r', 'product/product/produ668ct1-p1r', 1, 'title', 'description', 'слова'),
(1426, 896, 'Item', 2, 'product/produ669ct1-p1r', 'product/product/produ669ct1-p1r', 1, 'title', 'description', 'слова'),
(1427, 897, 'Item', 2, 'product/produ670ct1-p1r', 'product/product/produ670ct1-p1r', 1, 'title', 'description', 'слова'),
(1428, 898, 'Item', 2, 'product/produ671ct1-p1r', 'product/product/produ671ct1-p1r', 1, 'title', 'description', 'слова'),
(1429, 899, 'Item', 2, 'product/produ672ct1-p1r', 'product/product/produ672ct1-p1r', 1, 'title', 'description', 'слова'),
(1430, 900, 'Item', 2, 'product/produ673ct1-p1r', 'product/product/produ673ct1-p1r', 1, 'title', 'description', 'слова'),
(1431, 901, 'Item', 2, 'product/produ674ct1-p1r', 'product/product/produ674ct1-p1r', 1, 'title', 'description', 'слова'),
(1432, 902, 'Item', 2, 'product/produ675ct1-p1r', 'product/product/produ675ct1-p1r', 1, 'title', 'description', 'слова'),
(1433, 903, 'Item', 2, 'product/produ676ct1-p1r', 'product/product/produ676ct1-p1r', 1, 'title', 'description', 'слова'),
(1434, 904, 'Item', 2, 'product/produ677ct1-p1r', 'product/product/produ677ct1-p1r', 1, 'title', 'description', 'слова'),
(1435, 905, 'Item', 2, 'product/produ678ct1-p1r', 'product/product/produ678ct1-p1r', 1, 'title', 'description', 'слова'),
(1436, 906, 'Item', 2, 'product/produ679ct1-p1r', 'product/product/produ679ct1-p1r', 1, 'title', 'description', 'слова'),
(1437, 907, 'Item', 2, 'product/produ680ct1-p1r', 'product/product/produ680ct1-p1r', 1, 'title', 'description', 'слова'),
(1438, 908, 'Item', 2, 'product/produ681ct1-p1r', 'product/product/produ681ct1-p1r', 1, 'title', 'description', 'слова'),
(1439, 909, 'Item', 2, 'product/produ682ct1-p1r', 'product/product/produ682ct1-p1r', 1, 'title', 'description', 'слова'),
(1440, 910, 'Item', 2, 'product/produ683ct1-p1r', 'product/product/produ683ct1-p1r', 1, 'title', 'description', 'слова'),
(1441, 911, 'Item', 2, 'product/produ684ct1-p1r', 'product/product/produ684ct1-p1r', 1, 'title', 'description', 'слова'),
(1442, 912, 'Item', 2, 'product/produ685ct1-p1r', 'product/product/produ685ct1-p1r', 1, 'title', 'description', 'слова'),
(1443, 913, 'Item', 2, 'product/produ686ct1-p1r', 'product/product/produ686ct1-p1r', 1, 'title', 'description', 'слова'),
(1444, 914, 'Item', 2, 'product/produ687ct1-p1r', 'product/product/produ687ct1-p1r', 1, 'title', 'description', 'слова'),
(1445, 915, 'Item', 2, 'product/produ688ct1-p1r', 'product/product/produ688ct1-p1r', 1, 'title', 'description', 'слова'),
(1446, 916, 'Item', 2, 'product/produ689ct1-p1r', 'product/product/produ689ct1-p1r', 1, 'title', 'description', 'слова'),
(1447, 917, 'Item', 2, 'product/produ690ct1-p1r', 'product/product/produ690ct1-p1r', 1, 'title', 'description', 'слова'),
(1448, 918, 'Item', 2, 'product/produ691ct1-p1r', 'product/product/produ691ct1-p1r', 1, 'title', 'description', 'слова'),
(1449, 919, 'Item', 2, 'product/produ692ct1-p1r', 'product/product/produ692ct1-p1r', 1, 'title', 'description', 'слова'),
(1450, 920, 'Item', 2, 'product/produ693ct1-p1r', 'product/product/produ693ct1-p1r', 1, 'title', 'description', 'слова'),
(1451, 921, 'Item', 2, 'product/produ694ct1-p1r', 'product/product/produ694ct1-p1r', 1, 'title', 'description', 'слова'),
(1452, 922, 'Item', 2, 'product/produ695ct1-p1r', 'product/product/produ695ct1-p1r', 1, 'title', 'description', 'слова'),
(1453, 923, 'Item', 2, 'product/produ696ct1-p1r', 'product/product/produ696ct1-p1r', 1, 'title', 'description', 'слова'),
(1454, 924, 'Item', 2, 'product/produ697ct1-p1r', 'product/product/produ697ct1-p1r', 1, 'title', 'description', 'слова'),
(1455, 925, 'Item', 2, 'product/produ698ct1-p1r', 'product/product/produ698ct1-p1r', 1, 'title', 'description', 'слова'),
(1456, 926, 'Item', 2, 'product/produ699ct1-p1r', 'product/product/produ699ct1-p1r', 1, 'title', 'description', 'слова'),
(1457, 927, 'Item', 2, 'product/produ700ct1-p1r', 'product/product/produ700ct1-p1r', 1, 'title', 'description', 'слова'),
(1458, 928, 'Item', 2, 'product/produ701ct1-p1r', 'product/product/produ701ct1-p1r', 1, 'title', 'description', 'слова'),
(1459, 929, 'Item', 2, 'product/produ702ct1-p1r', 'product/product/produ702ct1-p1r', 1, 'title', 'description', 'слова'),
(1460, 930, 'Item', 2, 'product/produ703ct1-p1r', 'product/product/produ703ct1-p1r', 1, 'title', 'description', 'слова'),
(1461, 931, 'Item', 2, 'product/produ704ct1-p1r', 'product/product/produ704ct1-p1r', 1, 'title', 'description', 'слова'),
(1462, 932, 'Item', 2, 'product/produ705ct1-p1r', 'product/product/produ705ct1-p1r', 1, 'title', 'description', 'слова'),
(1463, 933, 'Item', 2, 'product/produ706ct1-p1r', 'product/product/produ706ct1-p1r', 1, 'title', 'description', 'слова'),
(1464, 934, 'Item', 2, 'product/produ707ct1-p1r', 'product/product/produ707ct1-p1r', 1, 'title', 'description', 'слова'),
(1465, 935, 'Item', 2, 'product/produ708ct1-p1r', 'product/product/produ708ct1-p1r', 1, 'title', 'description', 'слова'),
(1466, 936, 'Item', 2, 'product/produ709ct1-p1r', 'product/product/produ709ct1-p1r', 1, 'title', 'description', 'слова'),
(1467, 937, 'Item', 2, 'product/produ710ct1-p1r', 'product/product/produ710ct1-p1r', 1, 'title', 'description', 'слова'),
(1468, 938, 'Item', 2, 'product/produ711ct1-p1r', 'product/product/produ711ct1-p1r', 1, 'title', 'description', 'слова'),
(1469, 939, 'Item', 2, 'product/produ712ct1-p1r', 'product/product/produ712ct1-p1r', 1, 'title', 'description', 'слова'),
(1470, 940, 'Item', 2, 'product/produ713ct1-p1r', 'product/product/produ713ct1-p1r', 1, 'title', 'description', 'слова'),
(1471, 941, 'Item', 2, 'product/produ714ct1-p1r', 'product/product/produ714ct1-p1r', 1, 'title', 'description', 'слова'),
(1472, 942, 'Item', 2, 'product/produ715ct1-p1r', 'product/product/produ715ct1-p1r', 1, 'title', 'description', 'слова'),
(1473, 943, 'Item', 2, 'product/produ716ct1-p1r', 'product/product/produ716ct1-p1r', 1, 'title', 'description', 'слова'),
(1474, 944, 'Item', 2, 'product/produ717ct1-p1r', 'product/product/produ717ct1-p1r', 1, 'title', 'description', 'слова'),
(1475, 945, 'Item', 2, 'product/produ718ct1-p1r', 'product/product/produ718ct1-p1r', 1, 'title', 'description', 'слова'),
(1476, 946, 'Item', 2, 'product/produ719ct1-p1r', 'product/product/produ719ct1-p1r', 1, 'title', 'description', 'слова'),
(1477, 947, 'Item', 2, 'product/produ720ct1-p1r', 'product/product/produ720ct1-p1r', 1, 'title', 'description', 'слова'),
(1478, 948, 'Item', 2, 'product/produ721ct1-p1r', 'product/product/produ721ct1-p1r', 1, 'title', 'description', 'слова');
INSERT INTO `core_pages_meta` (`id`, `owner_id`, `model`, `page_id`, `request_path`, `target_path`, `active`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1479, 949, 'Item', 2, 'product/produ722ct1-p1r', 'product/product/produ722ct1-p1r', 1, 'title', 'description', 'слова'),
(1480, 950, 'Item', 2, 'product/produ723ct1-p1r', 'product/product/produ723ct1-p1r', 1, 'title', 'description', 'слова'),
(1481, 951, 'Item', 2, 'product/produ724ct1-p1r', 'product/product/produ724ct1-p1r', 1, 'title', 'description', 'слова'),
(1482, 952, 'Item', 2, 'product/produ725ct1-p1r', 'product/product/produ725ct1-p1r', 1, 'title', 'description', 'слова'),
(1483, 953, 'Item', 2, 'product/produ726ct1-p1r', 'product/product/produ726ct1-p1r', 1, 'title', 'description', 'слова'),
(1484, 954, 'Item', 2, 'product/produ727ct1-p1r', 'product/product/produ727ct1-p1r', 1, 'title', 'description', 'слова'),
(1485, 955, 'Item', 2, 'product/produ728ct1-p1r', 'product/product/produ728ct1-p1r', 1, 'title', 'description', 'слова'),
(1486, 956, 'Item', 2, 'product/produ729ct1-p1r', 'product/product/produ729ct1-p1r', 1, 'title', 'description', 'слова'),
(1487, 957, 'Item', 2, 'product/produ730ct1-p1r', 'product/product/produ730ct1-p1r', 1, 'title', 'description', 'слова'),
(1488, 958, 'Item', 2, 'product/produ731ct1-p1r', 'product/product/produ731ct1-p1r', 1, 'title', 'description', 'слова'),
(1489, 959, 'Item', 2, 'product/produ732ct1-p1r', 'product/product/produ732ct1-p1r', 1, 'title', 'description', 'слова'),
(1490, 960, 'Item', 2, 'product/produ733ct1-p1r', 'product/product/produ733ct1-p1r', 1, 'title', 'description', 'слова'),
(1491, 961, 'Item', 2, 'product/produ734ct1-p1r', 'product/product/produ734ct1-p1r', 1, 'title', 'description', 'слова'),
(1492, 962, 'Item', 2, 'product/produ735ct1-p1r', 'product/product/produ735ct1-p1r', 1, 'title', 'description', 'слова'),
(1493, 963, 'Item', 2, 'product/produ736ct1-p1r', 'product/product/produ736ct1-p1r', 1, 'title', 'description', 'слова'),
(1494, 964, 'Item', 2, 'product/produ737ct1-p1r', 'product/product/produ737ct1-p1r', 1, 'title', 'description', 'слова'),
(1495, 965, 'Item', 2, 'product/produ738ct1-p1r', 'product/product/produ738ct1-p1r', 1, 'title', 'description', 'слова'),
(1496, 966, 'Item', 2, 'product/produ739ct1-p1r', 'product/product/produ739ct1-p1r', 1, 'title', 'description', 'слова'),
(1497, 967, 'Item', 2, 'product/produ740ct1-p1r', 'product/product/produ740ct1-p1r', 1, 'title', 'description', 'слова'),
(1498, 968, 'Item', 2, 'product/produ741ct1-p1r', 'product/product/produ741ct1-p1r', 1, 'title', 'description', 'слова'),
(1499, 969, 'Item', 2, 'product/produ742ct1-p1r', 'product/product/produ742ct1-p1r', 1, 'title', 'description', 'слова'),
(1500, 970, 'Item', 2, 'product/produ743ct1-p1r', 'product/product/produ743ct1-p1r', 1, 'title', 'description', 'слова'),
(1501, 971, 'Item', 2, 'product/produ744ct1-p1r', 'product/product/produ744ct1-p1r', 1, 'title', 'description', 'слова'),
(1502, 972, 'Item', 2, 'product/produ745ct1-p1r', 'product/product/produ745ct1-p1r', 1, 'title', 'description', 'слова'),
(1503, 973, 'Item', 2, 'product/produ746ct1-p1r', 'product/product/produ746ct1-p1r', 1, 'title', 'description', 'слова'),
(1504, 974, 'Item', 2, 'product/produ747ct1-p1r', 'product/product/produ747ct1-p1r', 1, 'title', 'description', 'слова'),
(1505, 975, 'Item', 2, 'product/produ748ct1-p1r', 'product/product/produ748ct1-p1r', 1, 'title', 'description', 'слова'),
(1506, 976, 'Item', 2, 'product/produ749ct1-p1r', 'product/product/produ749ct1-p1r', 1, 'title', 'description', 'слова'),
(1507, 977, 'Item', 2, 'product/produ750ct1-p1r', 'product/product/produ750ct1-p1r', 1, 'title', 'description', 'слова'),
(1508, 978, 'Item', 2, 'product/produ751ct1-p1r', 'product/product/produ751ct1-p1r', 1, 'title', 'description', 'слова'),
(1509, 979, 'Item', 2, 'product/produ752ct1-p1r', 'product/product/produ752ct1-p1r', 1, 'title', 'description', 'слова'),
(1510, 980, 'Item', 2, 'product/produ753ct1-p1r', 'product/product/produ753ct1-p1r', 1, 'title', 'description', 'слова'),
(1511, 981, 'Item', 2, 'product/produ754ct1-p1r', 'product/product/produ754ct1-p1r', 1, 'title', 'description', 'слова'),
(1512, 982, 'Item', 2, 'product/produ755ct1-p1r', 'product/product/produ755ct1-p1r', 1, 'title', 'description', 'слова'),
(1513, 983, 'Item', 2, 'product/produ756ct1-p1r', 'product/product/produ756ct1-p1r', 1, 'title', 'description', 'слова'),
(1514, 984, 'Item', 2, 'product/produ757ct1-p1r', 'product/product/produ757ct1-p1r', 1, 'title', 'description', 'слова'),
(1515, 985, 'Item', 2, 'product/produ758ct1-p1r', 'product/product/produ758ct1-p1r', 1, 'title', 'description', 'слова'),
(1516, 986, 'Item', 2, 'product/produ759ct1-p1r', 'product/product/produ759ct1-p1r', 1, 'title', 'description', 'слова'),
(1517, 987, 'Item', 2, 'product/produ760ct1-p1r', 'product/product/produ760ct1-p1r', 1, 'title', 'description', 'слова'),
(1518, 988, 'Item', 2, 'product/produ761ct1-p1r', 'product/product/produ761ct1-p1r', 1, 'title', 'description', 'слова'),
(1519, 989, 'Item', 2, 'product/produ762ct1-p1r', 'product/product/produ762ct1-p1r', 1, 'title', 'description', 'слова'),
(1520, 990, 'Item', 2, 'product/produ763ct1-p1r', 'product/product/produ763ct1-p1r', 1, 'title', 'description', 'слова'),
(1521, 991, 'Item', 2, 'product/produ764ct1-p1r', 'product/product/produ764ct1-p1r', 1, 'title', 'description', 'слова'),
(1522, 992, 'Item', 2, 'product/produ765ct1-p1r', 'product/product/produ765ct1-p1r', 1, 'title', 'description', 'слова'),
(1523, 993, 'Item', 2, 'product/produ766ct1-p1r', 'product/product/produ766ct1-p1r', 1, 'title', 'description', 'слова'),
(1524, 994, 'Item', 2, 'product/produ767ct1-p1r', 'product/product/produ767ct1-p1r', 1, 'title', 'description', 'слова'),
(1525, 995, 'Item', 2, 'product/produ768ct1-p1r', 'product/product/produ768ct1-p1r', 1, 'title', 'description', 'слова'),
(1526, 996, 'Item', 2, 'product/produ769ct1-p1r', 'product/product/produ769ct1-p1r', 1, 'title', 'description', 'слова'),
(1527, 997, 'Item', 2, 'product/produ770ct1-p1r', 'product/product/produ770ct1-p1r', 1, 'title', 'description', 'слова'),
(1528, 998, 'Item', 2, 'product/produ771ct1-p1r', 'product/product/produ771ct1-p1r', 1, 'title', 'description', 'слова'),
(1529, 999, 'Item', 2, 'product/produ772ct1-p1r', 'product/product/produ772ct1-p1r', 1, 'title', 'description', 'слова'),
(1530, 1000, 'Item', 2, 'product/produ773ct1-p1r', 'product/product/produ773ct1-p1r', 1, 'title', 'description', 'слова'),
(1531, 1001, 'Item', 2, 'product/produ774ct1-p1r', 'product/product/produ774ct1-p1r', 1, 'title', 'description', 'слова'),
(1532, 1002, 'Item', 2, 'product/produ775ct1-p1r', 'product/product/produ775ct1-p1r', 1, 'title', 'description', 'слова'),
(1533, 1003, 'Item', 2, 'product/produ776ct1-p1r', 'product/product/produ776ct1-p1r', 1, 'title', 'description', 'слова'),
(1534, 1004, 'Item', 2, 'product/produ777ct1-p1r', 'product/product/produ777ct1-p1r', 1, 'title', 'description', 'слова'),
(1535, 1005, 'Item', 2, 'product/produ778ct1-p1r', 'product/product/produ778ct1-p1r', 1, 'title', 'description', 'слова'),
(1536, 1006, 'Item', 2, 'product/produ779ct1-p1r', 'product/product/produ779ct1-p1r', 1, 'title', 'description', 'слова'),
(1537, 1007, 'Item', 2, 'product/produ780ct1-p1r', 'product/product/produ780ct1-p1r', 1, 'title', 'description', 'слова'),
(1538, 1008, 'Item', 2, 'product/produ781ct1-p1r', 'product/product/produ781ct1-p1r', 1, 'title', 'description', 'слова'),
(1539, 1009, 'Item', 2, 'product/produ782ct1-p1r', 'product/product/produ782ct1-p1r', 1, 'title', 'description', 'слова'),
(1540, 1010, 'Item', 2, 'product/produ783ct1-p1r', 'product/product/produ783ct1-p1r', 1, 'title', 'description', 'слова'),
(1541, 1011, 'Item', 2, 'product/produ784ct1-p1r', 'product/product/produ784ct1-p1r', 1, 'title', 'description', 'слова'),
(1542, 1012, 'Item', 2, 'product/produ785ct1-p1r', 'product/product/produ785ct1-p1r', 1, 'title', 'description', 'слова'),
(1543, 1013, 'Item', 2, 'product/produ786ct1-p1r', 'product/product/produ786ct1-p1r', 1, 'title', 'description', 'слова'),
(1544, 1014, 'Item', 2, 'product/produ787ct1-p1r', 'product/product/produ787ct1-p1r', 1, 'title', 'description', 'слова'),
(1545, 1015, 'Item', 2, 'product/produ788ct1-p1r', 'product/product/produ788ct1-p1r', 1, 'title', 'description', 'слова'),
(1546, 1016, 'Item', 2, 'product/produ789ct1-p1r', 'product/product/produ789ct1-p1r', 1, 'title', 'description', 'слова'),
(1547, 1017, 'Item', 2, 'product/produ790ct1-p1r', 'product/product/produ790ct1-p1r', 1, 'title', 'description', 'слова'),
(1548, 1018, 'Item', 2, 'product/produ791ct1-p1r', 'product/product/produ791ct1-p1r', 1, 'title', 'description', 'слова'),
(1549, 1019, 'Item', 2, 'product/produ792ct1-p1r', 'product/product/produ792ct1-p1r', 1, 'title', 'description', 'слова'),
(1550, 1020, 'Item', 2, 'product/produ793ct1-p1r', 'product/product/produ793ct1-p1r', 1, 'title', 'description', 'слова'),
(1551, 1021, 'Item', 2, 'product/produ794ct1-p1r', 'product/product/produ794ct1-p1r', 1, 'title', 'description', 'слова'),
(1552, 1022, 'Item', 2, 'product/produ795ct1-p1r', 'product/product/produ795ct1-p1r', 1, 'title', 'description', 'слова'),
(1553, 1023, 'Item', 2, 'product/produ796ct1-p1r', 'product/product/produ796ct1-p1r', 1, 'title', 'description', 'слова'),
(1554, 1024, 'Item', 2, 'product/produ797ct1-p1r', 'product/product/produ797ct1-p1r', 1, 'title', 'description', 'слова'),
(1555, 1025, 'Item', 2, 'product/produ798ct1-p1r', 'product/product/produ798ct1-p1r', 1, 'title', 'description', 'слова'),
(1556, 1026, 'Item', 2, 'product/produ799ct1-p1r', 'product/product/produ799ct1-p1r', 1, 'title', 'description', 'слова'),
(1557, 1027, 'Item', 2, 'product/produ800ct1-p1r', 'product/product/produ800ct1-p1r', 1, 'title', 'description', 'слова'),
(1558, 1028, 'Item', 2, 'product/produ801ct1-p1r', 'product/product/produ801ct1-p1r', 1, 'title', 'description', 'слова'),
(1559, 1029, 'Item', 2, 'product/produ802ct1-p1r', 'product/product/produ802ct1-p1r', 1, 'title', 'description', 'слова'),
(1560, 1030, 'Item', 2, 'product/produ803ct1-p1r', 'product/product/produ803ct1-p1r', 1, 'title', 'description', 'слова'),
(1561, 1031, 'Item', 2, 'product/produ804ct1-p1r', 'product/product/produ804ct1-p1r', 1, 'title', 'description', 'слова'),
(1562, 1032, 'Item', 2, 'product/produ805ct1-p1r', 'product/product/produ805ct1-p1r', 1, 'title', 'description', 'слова'),
(1563, 1033, 'Item', 2, 'product/produ806ct1-p1r', 'product/product/produ806ct1-p1r', 1, 'title', 'description', 'слова'),
(1564, 1034, 'Item', 2, 'product/produ807ct1-p1r', 'product/product/produ807ct1-p1r', 1, 'title', 'description', 'слова'),
(1565, 1035, 'Item', 2, 'product/produ808ct1-p1r', 'product/product/produ808ct1-p1r', 1, 'title', 'description', 'слова'),
(1566, 1036, 'Item', 2, 'product/produ809ct1-p1r', 'product/product/produ809ct1-p1r', 1, 'title', 'description', 'слова'),
(1567, 1037, 'Item', 2, 'product/produ810ct1-p1r', 'product/product/produ810ct1-p1r', 1, 'title', 'description', 'слова'),
(1568, 1038, 'Item', 2, 'product/produ811ct1-p1r', 'product/product/produ811ct1-p1r', 1, 'title', 'description', 'слова'),
(1569, 1039, 'Item', 2, 'product/produ812ct1-p1r', 'product/product/produ812ct1-p1r', 1, 'title', 'description', 'слова'),
(1570, 1040, 'Item', 2, 'product/produ813ct1-p1r', 'product/product/produ813ct1-p1r', 1, 'title', 'description', 'слова'),
(1571, 1041, 'Item', 2, 'product/produ814ct1-p1r', 'product/product/produ814ct1-p1r', 1, 'title', 'description', 'слова'),
(1572, 1042, 'Item', 2, 'product/produ815ct1-p1r', 'product/product/produ815ct1-p1r', 1, 'title', 'description', 'слова'),
(1573, 1043, 'Item', 2, 'product/produ816ct1-p1r', 'product/product/produ816ct1-p1r', 1, 'title', 'description', 'слова'),
(1574, 1044, 'Item', 2, 'product/produ817ct1-p1r', 'product/product/produ817ct1-p1r', 1, 'title', 'description', 'слова'),
(1575, 1045, 'Item', 2, 'product/produ818ct1-p1r', 'product/product/produ818ct1-p1r', 1, 'title', 'description', 'слова'),
(1576, 1046, 'Item', 2, 'product/produ819ct1-p1r', 'product/product/produ819ct1-p1r', 1, 'title', 'description', 'слова'),
(1577, 1047, 'Item', 2, 'product/produ820ct1-p1r', 'product/product/produ820ct1-p1r', 1, 'title', 'description', 'слова'),
(1578, 1048, 'Item', 2, 'product/produ821ct1-p1r', 'product/product/produ821ct1-p1r', 1, 'title', 'description', 'слова'),
(1579, 1049, 'Item', 2, 'product/produ822ct1-p1r', 'product/product/produ822ct1-p1r', 1, 'title', 'description', 'слова'),
(1580, 1050, 'Item', 2, 'product/produ823ct1-p1r', 'product/product/produ823ct1-p1r', 1, 'title', 'description', 'слова'),
(1581, 1051, 'Item', 2, 'product/produ824ct1-p1r', 'product/product/produ824ct1-p1r', 1, 'title', 'description', 'слова'),
(1582, 1052, 'Item', 2, 'product/produ825ct1-p1r', 'product/product/produ825ct1-p1r', 1, 'title', 'description', 'слова'),
(1583, 1053, 'Item', 2, 'product/produ826ct1-p1r', 'product/product/produ826ct1-p1r', 1, 'title', 'description', 'слова'),
(1584, 1054, 'Item', 2, 'product/produ827ct1-p1r', 'product/product/produ827ct1-p1r', 1, 'title', 'description', 'слова'),
(1585, 1055, 'Item', 2, 'product/produ828ct1-p1r', 'product/product/produ828ct1-p1r', 1, 'title', 'description', 'слова'),
(1586, 1056, 'Item', 2, 'product/produ829ct1-p1r', 'product/product/produ829ct1-p1r', 1, 'title', 'description', 'слова'),
(1587, 1057, 'Item', 2, 'product/produ830ct1-p1r', 'product/product/produ830ct1-p1r', 1, 'title', 'description', 'слова'),
(1588, 1058, 'Item', 2, 'product/produ831ct1-p1r', 'product/product/produ831ct1-p1r', 1, 'title', 'description', 'слова'),
(1589, 1059, 'Item', 2, 'product/produ832ct1-p1r', 'product/product/produ832ct1-p1r', 1, 'title', 'description', 'слова'),
(1590, 1060, 'Item', 2, 'product/produ833ct1-p1r', 'product/product/produ833ct1-p1r', 1, 'title', 'description', 'слова'),
(1591, 1061, 'Item', 2, 'product/produ834ct1-p1r', 'product/product/produ834ct1-p1r', 1, 'title', 'description', 'слова'),
(1592, 1062, 'Item', 2, 'product/produ835ct1-p1r', 'product/product/produ835ct1-p1r', 1, 'title', 'description', 'слова'),
(1593, 1063, 'Item', 2, 'product/produ836ct1-p1r', 'product/product/produ836ct1-p1r', 1, 'title', 'description', 'слова'),
(1594, 1064, 'Item', 2, 'product/produ837ct1-p1r', 'product/product/produ837ct1-p1r', 1, 'title', 'description', 'слова'),
(1595, 1065, 'Item', 2, 'product/produ838ct1-p1r', 'product/product/produ838ct1-p1r', 1, 'title', 'description', 'слова'),
(1596, 1066, 'Item', 2, 'product/produ839ct1-p1r', 'product/product/produ839ct1-p1r', 1, 'title', 'description', 'слова'),
(1597, 1067, 'Item', 2, 'product/produ840ct1-p1r', 'product/product/produ840ct1-p1r', 1, 'title', 'description', 'слова'),
(1598, 1068, 'Item', 2, 'product/produ841ct1-p1r', 'product/product/produ841ct1-p1r', 1, 'title', 'description', 'слова'),
(1599, 1069, 'Item', 2, 'product/produ842ct1-p1r', 'product/product/produ842ct1-p1r', 1, 'title', 'description', 'слова'),
(1600, 1070, 'Item', 2, 'product/produ843ct1-p1r', 'product/product/produ843ct1-p1r', 1, 'title', 'description', 'слова'),
(1601, 1071, 'Item', 2, 'product/produ844ct1-p1r', 'product/product/produ844ct1-p1r', 1, 'title', 'description', 'слова'),
(1602, 1072, 'Item', 2, 'product/produ845ct1-p1r', 'product/product/produ845ct1-p1r', 1, 'title', 'description', 'слова'),
(1603, 1073, 'Item', 2, 'product/produ846ct1-p1r', 'product/product/produ846ct1-p1r', 1, 'title', 'description', 'слова'),
(1604, 1074, 'Item', 2, 'product/produ847ct1-p1r', 'product/product/produ847ct1-p1r', 1, 'title', 'description', 'слова'),
(1605, 1075, 'Item', 2, 'product/produ848ct1-p1r', 'product/product/produ848ct1-p1r', 1, 'title', 'description', 'слова'),
(1606, 1076, 'Item', 2, 'product/produ849ct1-p1r', 'product/product/produ849ct1-p1r', 1, 'title', 'description', 'слова'),
(1607, 1077, 'Item', 2, 'product/produ850ct1-p1r', 'product/product/produ850ct1-p1r', 1, 'title', 'description', 'слова'),
(1608, 1078, 'Item', 2, 'product/produ851ct1-p1r', 'product/product/produ851ct1-p1r', 1, 'title', 'description', 'слова'),
(1609, 1079, 'Item', 2, 'product/produ852ct1-p1r', 'product/product/produ852ct1-p1r', 1, 'title', 'description', 'слова'),
(1610, 1080, 'Item', 2, 'product/produ853ct1-p1r', 'product/product/produ853ct1-p1r', 1, 'title', 'description', 'слова'),
(1611, 1081, 'Item', 2, 'product/produ854ct1-p1r', 'product/product/produ854ct1-p1r', 1, 'title', 'description', 'слова'),
(1612, 1082, 'Item', 2, 'product/produ855ct1-p1r', 'product/product/produ855ct1-p1r', 1, 'title', 'description', 'слова'),
(1613, 1083, 'Item', 2, 'product/produ856ct1-p1r', 'product/product/produ856ct1-p1r', 1, 'title', 'description', 'слова'),
(1614, 1084, 'Item', 2, 'product/produ857ct1-p1r', 'product/product/produ857ct1-p1r', 1, 'title', 'description', 'слова'),
(1615, 1085, 'Item', 2, 'product/produ858ct1-p1r', 'product/product/produ858ct1-p1r', 1, 'title', 'description', 'слова'),
(1616, 1086, 'Item', 2, 'product/produ859ct1-p1r', 'product/product/produ859ct1-p1r', 1, 'title', 'description', 'слова'),
(1617, 1087, 'Item', 2, 'product/produ860ct1-p1r', 'product/product/produ860ct1-p1r', 1, 'title', 'description', 'слова'),
(1618, 1088, 'Item', 2, 'product/produ861ct1-p1r', 'product/product/produ861ct1-p1r', 1, 'title', 'description', 'слова'),
(1619, 1089, 'Item', 2, 'product/produ862ct1-p1r', 'product/product/produ862ct1-p1r', 1, 'title', 'description', 'слова'),
(1620, 1090, 'Item', 2, 'product/produ863ct1-p1r', 'product/product/produ863ct1-p1r', 1, 'title', 'description', 'слова'),
(1621, 1091, 'Item', 2, 'product/produ864ct1-p1r', 'product/product/produ864ct1-p1r', 1, 'title', 'description', 'слова'),
(1622, 1092, 'Item', 2, 'product/produ865ct1-p1r', 'product/product/produ865ct1-p1r', 1, 'title', 'description', 'слова'),
(1623, 1093, 'Item', 2, 'product/produ866ct1-p1r', 'product/product/produ866ct1-p1r', 1, 'title', 'description', 'слова'),
(1624, 1094, 'Item', 2, 'product/produ867ct1-p1r', 'product/product/produ867ct1-p1r', 1, 'title', 'description', 'слова'),
(1625, 1095, 'Item', 2, 'product/produ868ct1-p1r', 'product/product/produ868ct1-p1r', 1, 'title', 'description', 'слова'),
(1626, 1096, 'Item', 2, 'product/produ869ct1-p1r', 'product/product/produ869ct1-p1r', 1, 'title', 'description', 'слова'),
(1627, 1097, 'Item', 2, 'product/produ870ct1-p1r', 'product/product/produ870ct1-p1r', 1, 'title', 'description', 'слова'),
(1628, 1098, 'Item', 2, 'product/produ871ct1-p1r', 'product/product/produ871ct1-p1r', 1, 'title', 'description', 'слова'),
(1629, 1099, 'Item', 2, 'product/produ872ct1-p1r', 'product/product/produ872ct1-p1r', 1, 'title', 'description', 'слова'),
(1630, 1100, 'Item', 2, 'product/produ873ct1-p1r', 'product/product/produ873ct1-p1r', 1, 'title', 'description', 'слова'),
(1631, 1101, 'Item', 2, 'product/produ874ct1-p1r', 'product/product/produ874ct1-p1r', 1, 'title', 'description', 'слова'),
(1632, 1102, 'Item', 2, 'product/produ875ct1-p1r', 'product/product/produ875ct1-p1r', 1, 'title', 'description', 'слова'),
(1633, 1103, 'Item', 2, 'product/produ876ct1-p1r', 'product/product/produ876ct1-p1r', 1, 'title', 'description', 'слова'),
(1634, 1104, 'Item', 2, 'product/produ877ct1-p1r', 'product/product/produ877ct1-p1r', 1, 'title', 'description', 'слова'),
(1635, 1105, 'Item', 2, 'product/produ878ct1-p1r', 'product/product/produ878ct1-p1r', 1, 'title', 'description', 'слова'),
(1636, 1106, 'Item', 2, 'product/produ879ct1-p1r', 'product/product/produ879ct1-p1r', 1, 'title', 'description', 'слова'),
(1637, 1107, 'Item', 2, 'product/produ880ct1-p1r', 'product/product/produ880ct1-p1r', 1, 'title', 'description', 'слова'),
(1638, 1108, 'Item', 2, 'product/produ881ct1-p1r', 'product/product/produ881ct1-p1r', 1, 'title', 'description', 'слова'),
(1639, 1109, 'Item', 2, 'product/produ882ct1-p1r', 'product/product/produ882ct1-p1r', 1, 'title', 'description', 'слова'),
(1640, 1110, 'Item', 2, 'product/produ883ct1-p1r', 'product/product/produ883ct1-p1r', 1, 'title', 'description', 'слова'),
(1641, 1111, 'Item', 2, 'product/produ884ct1-p1r', 'product/product/produ884ct1-p1r', 1, 'title', 'description', 'слова'),
(1642, 1112, 'Item', 2, 'product/produ885ct1-p1r', 'product/product/produ885ct1-p1r', 1, 'title', 'description', 'слова'),
(1643, 1113, 'Item', 2, 'product/produ886ct1-p1r', 'product/product/produ886ct1-p1r', 1, 'title', 'description', 'слова'),
(1644, 1114, 'Item', 2, 'product/produ887ct1-p1r', 'product/product/produ887ct1-p1r', 1, 'title', 'description', 'слова'),
(1645, 1115, 'Item', 2, 'product/produ888ct1-p1r', 'product/product/produ888ct1-p1r', 1, 'title', 'description', 'слова'),
(1646, 1116, 'Item', 2, 'product/produ889ct1-p1r', 'product/product/produ889ct1-p1r', 1, 'title', 'description', 'слова'),
(1647, 1117, 'Item', 2, 'product/produ890ct1-p1r', 'product/product/produ890ct1-p1r', 1, 'title', 'description', 'слова'),
(1648, 1118, 'Item', 2, 'product/produ891ct1-p1r', 'product/product/produ891ct1-p1r', 1, 'title', 'description', 'слова'),
(1649, 1119, 'Item', 2, 'product/produ892ct1-p1r', 'product/product/produ892ct1-p1r', 1, 'title', 'description', 'слова'),
(1650, 1120, 'Item', 2, 'product/produ893ct1-p1r', 'product/product/produ893ct1-p1r', 1, 'title', 'description', 'слова'),
(1651, 1121, 'Item', 2, 'product/produ894ct1-p1r', 'product/product/produ894ct1-p1r', 1, 'title', 'description', 'слова'),
(1652, 1122, 'Item', 2, 'product/produ895ct1-p1r', 'product/product/produ895ct1-p1r', 1, 'title', 'description', 'слова'),
(1653, 1123, 'Item', 2, 'product/produ896ct1-p1r', 'product/product/produ896ct1-p1r', 1, 'title', 'description', 'слова'),
(1654, 1124, 'Item', 2, 'product/produ897ct1-p1r', 'product/product/produ897ct1-p1r', 1, 'title', 'description', 'слова'),
(1655, 1125, 'Item', 2, 'product/produ898ct1-p1r', 'product/product/produ898ct1-p1r', 1, 'title', 'description', 'слова'),
(1656, 1126, 'Item', 2, 'product/produ899ct1-p1r', 'product/product/produ899ct1-p1r', 1, 'title', 'description', 'слова'),
(1657, 1127, 'Item', 2, 'product/produ900ct1-p1r', 'product/product/produ900ct1-p1r', 1, 'title', 'description', 'слова'),
(1658, 1128, 'Item', 2, 'product/produ901ct1-p1r', 'product/product/produ901ct1-p1r', 1, 'title', 'description', 'слова'),
(1659, 1129, 'Item', 2, 'product/produ902ct1-p1r', 'product/product/produ902ct1-p1r', 1, 'title', 'description', 'слова'),
(1660, 1130, 'Item', 2, 'product/produ903ct1-p1r', 'product/product/produ903ct1-p1r', 1, 'title', 'description', 'слова'),
(1661, 1131, 'Item', 2, 'product/produ904ct1-p1r', 'product/product/produ904ct1-p1r', 1, 'title', 'description', 'слова'),
(1662, 1132, 'Item', 2, 'product/produ905ct1-p1r', 'product/product/produ905ct1-p1r', 1, 'title', 'description', 'слова'),
(1663, 1133, 'Item', 2, 'product/produ906ct1-p1r', 'product/product/produ906ct1-p1r', 1, 'title', 'description', 'слова'),
(1664, 1134, 'Item', 2, 'product/produ907ct1-p1r', 'product/product/produ907ct1-p1r', 1, 'title', 'description', 'слова'),
(1665, 1135, 'Item', 2, 'product/produ908ct1-p1r', 'product/product/produ908ct1-p1r', 1, 'title', 'description', 'слова'),
(1666, 1136, 'Item', 2, 'product/produ909ct1-p1r', 'product/product/produ909ct1-p1r', 1, 'title', 'description', 'слова'),
(1667, 1137, 'Item', 2, 'product/produ910ct1-p1r', 'product/product/produ910ct1-p1r', 1, 'title', 'description', 'слова'),
(1668, 1138, 'Item', 2, 'product/produ911ct1-p1r', 'product/product/produ911ct1-p1r', 1, 'title', 'description', 'слова'),
(1669, 1139, 'Item', 2, 'product/produ912ct1-p1r', 'product/product/produ912ct1-p1r', 1, 'title', 'description', 'слова'),
(1670, 1140, 'Item', 2, 'product/produ913ct1-p1r', 'product/product/produ913ct1-p1r', 1, 'title', 'description', 'слова'),
(1671, 1141, 'Item', 2, 'product/produ914ct1-p1r', 'product/product/produ914ct1-p1r', 1, 'title', 'description', 'слова'),
(1672, 1142, 'Item', 2, 'product/produ915ct1-p1r', 'product/product/produ915ct1-p1r', 1, 'title', 'description', 'слова'),
(1673, 1143, 'Item', 2, 'product/produ916ct1-p1r', 'product/product/produ916ct1-p1r', 1, 'title', 'description', 'слова'),
(1674, 1144, 'Item', 2, 'product/produ917ct1-p1r', 'product/product/produ917ct1-p1r', 1, 'title', 'description', 'слова'),
(1675, 1145, 'Item', 2, 'product/produ918ct1-p1r', 'product/product/produ918ct1-p1r', 1, 'title', 'description', 'слова'),
(1676, 1146, 'Item', 2, 'product/produ919ct1-p1r', 'product/product/produ919ct1-p1r', 1, 'title', 'description', 'слова'),
(1677, 1147, 'Item', 2, 'product/produ920ct1-p1r', 'product/product/produ920ct1-p1r', 1, 'title', 'description', 'слова'),
(1678, 1148, 'Item', 2, 'product/produ921ct1-p1r', 'product/product/produ921ct1-p1r', 1, 'title', 'description', 'слова'),
(1679, 1149, 'Item', 2, 'product/produ922ct1-p1r', 'product/product/produ922ct1-p1r', 1, 'title', 'description', 'слова'),
(1680, 1150, 'Item', 2, 'product/produ923ct1-p1r', 'product/product/produ923ct1-p1r', 1, 'title', 'description', 'слова'),
(1681, 1151, 'Item', 2, 'product/produ924ct1-p1r', 'product/product/produ924ct1-p1r', 1, 'title', 'description', 'слова'),
(1682, 1152, 'Item', 2, 'product/produ925ct1-p1r', 'product/product/produ925ct1-p1r', 1, 'title', 'description', 'слова'),
(1683, 1153, 'Item', 2, 'product/produ926ct1-p1r', 'product/product/produ926ct1-p1r', 1, 'title', 'description', 'слова'),
(1684, 1154, 'Item', 2, 'product/produ927ct1-p1r', 'product/product/produ927ct1-p1r', 1, 'title', 'description', 'слова'),
(1685, 1155, 'Item', 2, 'product/produ928ct1-p1r', 'product/product/produ928ct1-p1r', 1, 'title', 'description', 'слова'),
(1686, 1156, 'Item', 2, 'product/produ929ct1-p1r', 'product/product/produ929ct1-p1r', 1, 'title', 'description', 'слова'),
(1687, 1157, 'Item', 2, 'product/produ930ct1-p1r', 'product/product/produ930ct1-p1r', 1, 'title', 'description', 'слова'),
(1688, 1158, 'Item', 2, 'product/produ931ct1-p1r', 'product/product/produ931ct1-p1r', 1, 'title', 'description', 'слова'),
(1689, 1159, 'Item', 2, 'product/produ932ct1-p1r', 'product/product/produ932ct1-p1r', 1, 'title', 'description', 'слова'),
(1690, 1160, 'Item', 2, 'product/produ933ct1-p1r', 'product/product/produ933ct1-p1r', 1, 'title', 'description', 'слова'),
(1691, 1161, 'Item', 2, 'product/produ934ct1-p1r', 'product/product/produ934ct1-p1r', 1, 'title', 'description', 'слова'),
(1692, 1162, 'Item', 2, 'product/produ935ct1-p1r', 'product/product/produ935ct1-p1r', 1, 'title', 'description', 'слова'),
(1693, 1163, 'Item', 2, 'product/produ936ct1-p1r', 'product/product/produ936ct1-p1r', 1, 'title', 'description', 'слова'),
(1694, 1164, 'Item', 2, 'product/produ937ct1-p1r', 'product/product/produ937ct1-p1r', 1, 'title', 'description', 'слова'),
(1695, 1165, 'Item', 2, 'product/produ938ct1-p1r', 'product/product/produ938ct1-p1r', 1, 'title', 'description', 'слова'),
(1696, 1166, 'Item', 2, 'product/produ939ct1-p1r', 'product/product/produ939ct1-p1r', 1, 'title', 'description', 'слова'),
(1697, 1167, 'Item', 2, 'product/produ940ct1-p1r', 'product/product/produ940ct1-p1r', 1, 'title', 'description', 'слова'),
(1698, 1168, 'Item', 2, 'product/produ941ct1-p1r', 'product/product/produ941ct1-p1r', 1, 'title', 'description', 'слова'),
(1699, 1169, 'Item', 2, 'product/produ942ct1-p1r', 'product/product/produ942ct1-p1r', 1, 'title', 'description', 'слова'),
(1700, 1170, 'Item', 2, 'product/produ943ct1-p1r', 'product/product/produ943ct1-p1r', 1, 'title', 'description', 'слова'),
(1701, 1171, 'Item', 2, 'product/produ944ct1-p1r', 'product/product/produ944ct1-p1r', 1, 'title', 'description', 'слова'),
(1702, 1172, 'Item', 2, 'product/produ945ct1-p1r', 'product/product/produ945ct1-p1r', 1, 'title', 'description', 'слова'),
(1703, 1173, 'Item', 2, 'product/produ946ct1-p1r', 'product/product/produ946ct1-p1r', 1, 'title', 'description', 'слова'),
(1704, 1174, 'Item', 2, 'product/produ947ct1-p1r', 'product/product/produ947ct1-p1r', 1, 'title', 'description', 'слова'),
(1705, 1175, 'Item', 2, 'product/produ948ct1-p1r', 'product/product/produ948ct1-p1r', 1, 'title', 'description', 'слова'),
(1706, 1176, 'Item', 2, 'product/produ949ct1-p1r', 'product/product/produ949ct1-p1r', 1, 'title', 'description', 'слова'),
(1707, 1177, 'Item', 2, 'product/produ950ct1-p1r', 'product/product/produ950ct1-p1r', 1, 'title', 'description', 'слова'),
(1708, 1178, 'Item', 2, 'product/produ951ct1-p1r', 'product/product/produ951ct1-p1r', 1, 'title', 'description', 'слова'),
(1709, 1179, 'Item', 2, 'product/produ952ct1-p1r', 'product/product/produ952ct1-p1r', 1, 'title', 'description', 'слова'),
(1710, 1180, 'Item', 2, 'product/produ953ct1-p1r', 'product/product/produ953ct1-p1r', 1, 'title', 'description', 'слова'),
(1711, 1181, 'Item', 2, 'product/produ954ct1-p1r', 'product/product/produ954ct1-p1r', 1, 'title', 'description', 'слова'),
(1712, 1182, 'Item', 2, 'product/produ955ct1-p1r', 'product/product/produ955ct1-p1r', 1, 'title', 'description', 'слова'),
(1713, 1183, 'Item', 2, 'product/produ956ct1-p1r', 'product/product/produ956ct1-p1r', 1, 'title', 'description', 'слова'),
(1714, 1184, 'Item', 2, 'product/produ957ct1-p1r', 'product/product/produ957ct1-p1r', 1, 'title', 'description', 'слова'),
(1715, 1185, 'Item', 2, 'product/produ958ct1-p1r', 'product/product/produ958ct1-p1r', 1, 'title', 'description', 'слова'),
(1716, 1186, 'Item', 2, 'product/produ959ct1-p1r', 'product/product/produ959ct1-p1r', 1, 'title', 'description', 'слова'),
(1717, 1187, 'Item', 2, 'product/produ960ct1-p1r', 'product/product/produ960ct1-p1r', 1, 'title', 'description', 'слова'),
(1718, 1188, 'Item', 2, 'product/produ961ct1-p1r', 'product/product/produ961ct1-p1r', 1, 'title', 'description', 'слова'),
(1719, 1189, 'Item', 2, 'product/produ962ct1-p1r', 'product/product/produ962ct1-p1r', 1, 'title', 'description', 'слова'),
(1720, 1190, 'Item', 2, 'product/produ963ct1-p1r', 'product/product/produ963ct1-p1r', 1, 'title', 'description', 'слова'),
(1721, 1191, 'Item', 2, 'product/produ964ct1-p1r', 'product/product/produ964ct1-p1r', 1, 'title', 'description', 'слова'),
(1722, 1192, 'Item', 2, 'product/produ965ct1-p1r', 'product/product/produ965ct1-p1r', 1, 'title', 'description', 'слова'),
(1723, 1193, 'Item', 2, 'product/produ966ct1-p1r', 'product/product/produ966ct1-p1r', 1, 'title', 'description', 'слова'),
(1724, 1194, 'Item', 2, 'product/produ967ct1-p1r', 'product/product/produ967ct1-p1r', 1, 'title', 'description', 'слова'),
(1725, 1195, 'Item', 2, 'product/produ968ct1-p1r', 'product/product/produ968ct1-p1r', 1, 'title', 'description', 'слова'),
(1726, 1196, 'Item', 2, 'product/produ969ct1-p1r', 'product/product/produ969ct1-p1r', 1, 'title', 'description', 'слова'),
(1727, 1197, 'Item', 2, 'product/produ970ct1-p1r', 'product/product/produ970ct1-p1r', 1, 'title', 'description', 'слова'),
(1728, 1198, 'Item', 2, 'product/produ971ct1-p1r', 'product/product/produ971ct1-p1r', 1, 'title', 'description', 'слова'),
(1729, 1199, 'Item', 2, 'product/produ972ct1-p1r', 'product/product/produ972ct1-p1r', 1, 'title', 'description', 'слова'),
(1730, 1200, 'Item', 2, 'product/produ973ct1-p1r', 'product/product/produ973ct1-p1r', 1, 'title', 'description', 'слова'),
(1731, 1201, 'Item', 2, 'product/produ974ct1-p1r', 'product/product/produ974ct1-p1r', 1, 'title', 'description', 'слова'),
(1732, 1202, 'Item', 2, 'product/produ975ct1-p1r', 'product/product/produ975ct1-p1r', 1, 'title', 'description', 'слова'),
(1733, 1203, 'Item', 2, 'product/produ976ct1-p1r', 'product/product/produ976ct1-p1r', 1, 'title', 'description', 'слова'),
(1734, 1204, 'Item', 2, 'product/produ977ct1-p1r', 'product/product/produ977ct1-p1r', 1, 'title', 'description', 'слова'),
(1735, 1205, 'Item', 2, 'product/produ978ct1-p1r', 'product/product/produ978ct1-p1r', 1, 'title', 'description', 'слова'),
(1736, 1206, 'Item', 2, 'product/produ979ct1-p1r', 'product/product/produ979ct1-p1r', 1, 'title', 'description', 'слова'),
(1737, 1207, 'Item', 2, 'product/produ980ct1-p1r', 'product/product/produ980ct1-p1r', 1, 'title', 'description', 'слова'),
(1738, 1208, 'Item', 2, 'product/produ981ct1-p1r', 'product/product/produ981ct1-p1r', 1, 'title', 'description', 'слова'),
(1739, 1209, 'Item', 2, 'product/produ982ct1-p1r', 'product/product/produ982ct1-p1r', 1, 'title', 'description', 'слова'),
(1740, 1210, 'Item', 2, 'product/produ983ct1-p1r', 'product/product/produ983ct1-p1r', 1, 'title', 'description', 'слова'),
(1741, 1211, 'Item', 2, 'product/produ984ct1-p1r', 'product/product/produ984ct1-p1r', 1, 'title', 'description', 'слова'),
(1742, 1212, 'Item', 2, 'product/produ985ct1-p1r', 'product/product/produ985ct1-p1r', 1, 'title', 'description', 'слова'),
(1743, 1213, 'Item', 2, 'product/produ986ct1-p1r', 'product/product/produ986ct1-p1r', 1, 'title', 'description', 'слова'),
(1744, 1214, 'Item', 2, 'product/produ987ct1-p1r', 'product/product/produ987ct1-p1r', 1, 'title', 'description', 'слова'),
(1745, 1215, 'Item', 2, 'product/produ988ct1-p1r', 'product/product/produ988ct1-p1r', 1, 'title', 'description', 'слова'),
(1746, 1216, 'Item', 2, 'product/produ989ct1-p1r', 'product/product/produ989ct1-p1r', 1, 'title', 'description', 'слова'),
(1747, 1217, 'Item', 2, 'product/produ990ct1-p1r', 'product/product/produ990ct1-p1r', 1, 'title', 'description', 'слова'),
(1748, 1218, 'Item', 2, 'product/produ991ct1-p1r', 'product/product/produ991ct1-p1r', 1, 'title', 'description', 'слова'),
(1749, 1219, 'Item', 2, 'product/produ992ct1-p1r', 'product/product/produ992ct1-p1r', 1, 'title', 'description', 'слова'),
(1750, 1220, 'Item', 2, 'product/produ993ct1-p1r', 'product/product/produ993ct1-p1r', 1, 'title', 'description', 'слова'),
(1751, 1221, 'Item', 2, 'product/produ994ct1-p1r', 'product/product/produ994ct1-p1r', 1, 'title', 'description', 'слова'),
(1752, 1222, 'Item', 2, 'product/produ995ct1-p1r', 'product/product/produ995ct1-p1r', 1, 'title', 'description', 'слова'),
(1753, 1223, 'Item', 2, 'product/produ996ct1-p1r', 'product/product/produ996ct1-p1r', 1, 'title', 'description', 'слова'),
(1754, 1224, 'Item', 2, 'product/produ997ct1-p1r', 'product/product/produ997ct1-p1r', 1, 'title', 'description', 'слова'),
(1755, 1225, 'Item', 2, 'product/produ998ct1-p1r', 'product/product/produ998ct1-p1r', 1, 'title', 'description', 'слова'),
(1756, 1226, 'Item', 2, 'product/produ999ct1-p1r', 'product/product/produ999ct1-p1r', 1, 'title', 'description', 'слова'),
(1757, 1227, 'Item', 2, 'product/product1-p1rqwerty', 'product/product/product1-p1rqwerty', 0, '', 'description', 'слова'),
(1758, 1228, 'Item', 2, 'product/test-1qw', 'product/product/test-1qw', 0, 'title 6', 'description 6', 'keywords 6'),
(1759, 1229, 'Item', 2, 'product/product1-p1ras', 'product/product/product1-p1ras', 0, '', 'description', 'слова'),
(1760, 1230, 'Item', 2, 'product/product1-p1rwe', 'product/product/product1-p1rwe', 0, '', 'description', 'слова'),
(1761, 1231, 'Item', 2, 'product/product1-p1raa', 'product/product/product1-p1raa', 0, '', 'description', 'слова'),
(1762, 1232, 'Item', 2, 'product/product1-p1raas', 'product/product/product1-p1raas', 0, '', 'description', 'слова'),
(1763, 1233, 'Item', 2, 'product/product1-p1rqqq', 'product/product/product1-p1rqqq', 0, '', 'description', 'слова'),
(1764, 1234, 'Item', 2, 'product/product1-p1rwerrew', 'product/product/product1-p1rwerrew', 0, '', 'description', 'слова'),
(1765, 1235, 'Item', 2, 'product/product1-p1rzxc', 'product/product/product1-p1rzxc', 0, '', 'description', 'слова'),
(1766, 1236, 'Item', 2, 'product/product1-p1rsdfsdf', 'product/product/product1-p1rsdfsdf', 0, '', 'description', 'слова'),
(1767, 1237, 'Item', 2, 'product/product1-p1rdxhfghdfdf', 'product/product/product1-p1rdxhfghdfdf', 0, '', 'description', 'слова'),
(1768, 3, 'CategoryProduct', 2, 'product/cat', 'product/category/cat', 0, '', '', ''),
(1769, 1238, 'Item', 2, 'product/product1-p1rs', 'product/product/product1-p1rs', 0, '', 'description', 'слова');

-- --------------------------------------------------------

--
-- Структура таблицы `core_social_links`
--

CREATE TABLE IF NOT EXISTS `core_social_links` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_templates`
--

CREATE TABLE IF NOT EXISTS `core_templates` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `path` varchar(20) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `core_templates`
--

INSERT INTO `core_templates` (`id`, `title`, `path`, `display_order`, `active`) VALUES
(1, 'Одноколоночный шаблон', 'main', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `core_text_block`
--

CREATE TABLE IF NOT EXISTS `core_text_block` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `uri` varchar(255) NOT NULL,
  `content` text,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `core_type_menu`
--

CREATE TABLE IF NOT EXISTS `core_type_menu` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  `type` enum('fix','percent') NOT NULL,
  `value` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `count`, `type`, `value`) VALUES
(4, '57fb33df59e64', 10, 'fix', 10),
(5, '57fb35088ee0c', 100, 'percent', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `group_name`
--

CREATE TABLE IF NOT EXISTS `group_name` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `group_name`
--

INSERT INTO `group_name` (`id`, `name`, `type`, `active`) VALUES
(1, 'Север', 1, 0),
(2, 'Центр', 1, 1),
(3, 'Восток', 1, 1),
(4, 'Запад', 1, 1),
(5, 'new_user', 2, 1),
(6, 'old_user', 2, 1),
(7, 'qwerty', 1, 1),
(8, '123', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `group_type`
--

CREATE TABLE IF NOT EXISTS `group_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `group_type`
--

INSERT INTO `group_type` (`id`, `name`) VALUES
(1, 'region'),
(2, 'user_id');

-- --------------------------------------------------------

--
-- Структура таблицы `links_news_category`
--

CREATE TABLE IF NOT EXISTS `links_news_category` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1469003857),
('m160331_140545_users', 1469004415),
('m160610_104353_products', 1469008390),
('m160610_124342_photo', 1469008345),
('m160610_133100_core', 1469004401),
('m160610_135250_news', 1469008257),
('m160614_124839_advertising', 1469008017),
('m160624_083822_order', 1469008391),
('m160906_115650_price_book', 1473751974);

-- --------------------------------------------------------

--
-- Структура таблицы `news_category`
--

CREATE TABLE IF NOT EXISTS `news_category` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `description` text,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news_news`
--

CREATE TABLE IF NOT EXISTS `news_news` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `small_description` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `photo_album`
--

CREATE TABLE IF NOT EXISTS `photo_album` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `photo_album`
--

INSERT INTO `photo_album` (`id`, `name`, `description`, `display_order`, `active`) VALUES
(1, 'альбом', '1', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `photo_image`
--

CREATE TABLE IF NOT EXISTS `photo_image` (
  `id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alt` varchar(50) DEFAULT NULL,
  `description` text,
  `display_order` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `price_book`
--

CREATE TABLE IF NOT EXISTS `price_book` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `price_book`
--

INSERT INTO `price_book` (`id`, `product_id`, `price`, `group_id`) VALUES
(1, 1, 15, 1),
(3, 5, 111, 1),
(4, 5, 111, 2),
(5, 4, 343454, 3),
(6, 5, 123, 5),
(7, 5, 555, 6),
(8, 183, 55, 5),
(9, 1, 20, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `product_brand`
--

CREATE TABLE IF NOT EXISTS `product_brand` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_brand`
--

INSERT INTO `product_brand` (`id`, `title`, `display_order`, `active`) VALUES
(1, 'бренд', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_category`
--

CREATE TABLE IF NOT EXISTS `product_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `page_id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_category`
--

INSERT INTO `product_category` (`id`, `parent_id`, `page_id`, `uri`, `preview`, `title`, `description`, `display_order`, `active`) VALUES
(1, 1, 1, '2', '3', 'Категория 1', 'описание', 1, 1),
(2, 1, 1, '3', '4', 'Категория 2', 'описание2', 1, 1),
(3, 0, 2, 'cat', '', 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `product_delivery`
--

CREATE TABLE IF NOT EXISTS `product_delivery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_delivery`
--

INSERT INTO `product_delivery` (`id`, `title`, `display_order`, `active`) VALUES
(1, 'доставка', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_item`
--

CREATE TABLE IF NOT EXISTS `product_item` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `articul` varchar(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `small_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `discount` smallint(2) DEFAULT '0',
  `count` int(11) DEFAULT '0',
  `preview` varchar(255) DEFAULT NULL,
  `active` smallint(1) DEFAULT '0',
  `photo_album_id` int(11) DEFAULT NULL,
  `create_at` int(11) NOT NULL,
  `update_at` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1239 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_item`
--

INSERT INTO `product_item` (`id`, `category_id`, `delivery_id`, `page_id`, `brand_id`, `articul`, `title`, `uri`, `small_description`, `description`, `price`, `discount`, `count`, `preview`, `active`, `photo_album_id`, `create_at`, `update_at`, `master_id`) VALUES
(1, 1, 1, 2, 1, 'abc_r', 'Обновленный', 'product1-p1r', 'маленькое описание11', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470140361, 1474351488, 1),
(2, 1, 1, 2, 1, 'abc_1r', 'продукт 2', 'product1-p2r', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470140361, 1470318316, 1),
(3, 2, 1, 2, 1, 'abb_r', 'продукт 3', 'product2-p1r', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470140361, 1470318316, NULL),
(4, 2, 1, 2, 1, 'abb_1r', 'продукт 5', 'product2-p3r', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470140361, 1470318316, 3),
(5, 1, 1, 2, 1, 'abc_cccccr', 'продукт 6', 'product1-p111r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470140362, 1470318316, 1),
(6, 1, 1, 2, 1, 'abc_1232r', 'продукт 7', 'product1-p123r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470140362, 1470318316, 1),
(7, 1, 1, 2, 1, 'zzz_', 'test 1', 'test-1', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470140362, 1470318316, NULL),
(129, 1, 1, 2, 1, 'zzz_123', 'test 1-1', 'test-1-1', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470205216, 1470318316, 7),
(130, 1, 1, 2, 1, 'abc_a1', 'продукт 1', 'product1-p11', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470222283, 1470318316, NULL),
(131, 1, 1, 2, 1, 'abc_1a2', 'продукт 2', 'product1-p21', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470222283, 1470318316, 1),
(132, 2, 1, 2, 1, 'abb_a3', 'продукт 3', 'product2-p121', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470222283, 1470318316, NULL),
(133, 2, 1, 2, 1, 'abb_1a4', 'продукт 5', 'product2-p31', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470222283, 1470318316, 3),
(134, 1, 1, 2, 1, 'abc_ccccca5', 'продукт 6', 'product1-p1112', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470222283, 1470318316, 1),
(135, 1, 1, 2, 1, 'abc_1232a6', 'продукт 7', 'product1-p1231', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470222283, 1470318316, 1),
(136, 1, 1, 2, 1, 'zzz_a7', 'test 1', 'test-11', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470222283, 1470318316, NULL),
(137, 1, 1, 2, 1, 'zzz_123a8', 'test 1-1', 'test-1-11', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470222283, 1470318316, 7),
(138, 1, 1, 2, 1, 'abc_a12', 'продукт 1', 'product1-p112', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470222398, 1470318316, NULL),
(139, 1, 1, 2, 1, 'abc_1a22', 'продукт 2', 'product1-p212', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470222398, 1470318316, 1),
(140, 2, 1, 2, 1, 'abb_a32', 'продукт 3', 'product2-p1212', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470222398, 1470318316, NULL),
(141, 2, 1, 2, 1, 'abb_1a42', 'продукт 5', 'product2-p312', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470222398, 1470318316, 3),
(142, 1, 1, 2, 1, 'abc_ccccca52', 'продукт 6', 'product1-p11122', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470222398, 1470318316, 1),
(143, 1, 1, 2, 1, 'abc_1232a62', 'продукт 7', 'product1-p12312', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470222398, 1470318316, 1),
(144, 1, 1, 2, 1, 'zzz_a72', 'test 1', 'test-112', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470222398, 1470318316, NULL),
(145, 1, 1, 2, 1, 'zzz_123a82', 'test 1-1', 'test-1-112', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470222398, 1470318316, 7),
(146, 1, 1, 2, 1, 'abc_a123', 'продукт 1', 'product1-p1123', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470222479, 1470318316, NULL),
(147, 1, 1, 2, 1, 'abc_1a223', 'продукт 2', 'product1-p2123', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470222479, 1470318316, 1),
(148, 2, 1, 2, 1, 'abb_a323', 'продукт 3', 'product2-p12123', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470222479, 1470318316, NULL),
(149, 2, 1, 2, 1, 'abb_1a423', 'продукт 5', 'product2-p3123', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470222479, 1470318316, 3),
(150, 1, 1, 2, 1, 'abc_ccccca523', 'продукт 6', 'product1-p111223', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470222479, 1470318316, 1),
(151, 1, 1, 2, 1, 'abc_1232a623', 'продукт 7', 'product1-p123123', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470222479, 1470318316, 1),
(152, 1, 1, 2, 1, 'zzz_a723', 'test 1', 'test-1123', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470222479, 1470318316, NULL),
(153, 1, 1, 2, 1, 'zzz_123a823', 'test 1-1', 'test-1-1123', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470222479, 1470318316, 7),
(154, 1, 1, 2, 1, 'zzz_r', 'test 1', 'test-1r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223166, 1470318316, NULL),
(155, 2, 1, 2, 1, 'abb_1a42r', 'продукт 5', 'product2-p312r', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223166, 1470318316, 3),
(156, 1, 1, 2, 1, 'abc_ccccca52r', 'продукт 6', 'product1-p11122r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223166, 1470318316, 1),
(157, 1, 1, 2, 1, 'abc_1232a62r', 'продукт 7', 'product1-p12312r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223166, 1470318316, 1),
(158, 1, 1, 2, 1, 'zzz_a72r', 'test 1', 'test-112r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223166, 1470318316, NULL),
(159, 1, 1, 2, 1, 'zzz_123a82r', 'test 1-1', 'test-1-112r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223167, 1470318316, 7),
(160, 1, 1, 2, 1, 'abc_a123r', 'продукт 1', 'product1-p1123r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223167, 1470318316, NULL),
(161, 1, 1, 2, 1, 'abc_1a223r', 'продукт 2', 'product1-p2123r', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223167, 1470318316, 1),
(162, 2, 1, 2, 1, 'abb_a323r', 'продукт 3', 'product2-p12123r', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223167, 1470318316, NULL),
(163, 2, 1, 2, 1, 'abb_1a423r', 'продукт 5', 'product2-p3123r', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223167, 1470318316, 3),
(164, 1, 1, 2, 1, 'abc_ccccca523r', 'продукт 6', 'product1-p111223r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223167, 1470318316, 1),
(165, 1, 1, 2, 1, 'abc_1232a623r', 'продукт 7', 'product1-p123123r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223167, 1470318316, 1),
(166, 1, 1, 2, 1, 'zzz_a723r', 'test 1', 'test-1123r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223167, 1470318316, NULL),
(167, 1, 1, 2, 1, 'zzz_123a823r', 'test 1-1', 'test-1-1123r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223167, 1470318316, 7),
(168, 1, 1, 2, 1, 'zzz_123r', 'test 1-1', 'test-1-1r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223335, 1470318316, 7),
(169, 1, 1, 2, 1, 'abc_a1r', 'продукт 1', 'product1-p11r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223335, 1470318316, NULL),
(170, 1, 1, 2, 1, 'abc_1a2r', 'продукт 2', 'product1-p21r', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223335, 1470318316, 1),
(171, 2, 1, 2, 1, 'abb_a3r', 'продукт 3', 'product2-p121r', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223335, 1470318316, NULL),
(172, 2, 1, 2, 1, 'abb_1a4r', 'продукт 5', 'product2-p31r', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223335, 1470318316, 3),
(173, 1, 1, 2, 1, 'abc_ccccca5r', 'продукт 6', 'product1-p1112r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223335, 1470318316, 1),
(174, 1, 1, 2, 1, 'abc_1232a6r', 'продукт 7', 'product1-p1231r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223335, 1470318316, 1),
(175, 1, 1, 2, 1, 'zzz_a7r', 'test 1', 'test-11r', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223335, 1470318316, NULL),
(176, 1, 1, 2, 1, 'abc_a12r', 'продукт 1', 'product1-p112r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223335, 1470318316, NULL),
(177, 1, 1, 2, 1, 'abc_1a22r', 'продукт 2', 'product1-p212r', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223336, 1470318316, 1),
(178, 2, 1, 2, 1, 'abb_a32r', 'продукт 3', 'product2-p1212r', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223336, 1470318316, NULL),
(179, 1, 1, 2, 1, 'abc_rq', 'продукт 1', 'product1-p1rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223703, 1470318316, NULL),
(180, 1, 1, 2, 1, 'abc_1rq', 'продукт 2', 'product1-p2rq', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223703, 1470318316, 1),
(181, 2, 1, 2, 1, 'abb_rq', 'продукт 3', 'product2-p1rq', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223703, 1470318316, NULL),
(182, 2, 1, 2, 1, 'abb_1rq', 'продукт 5', 'product2-p3rq', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223703, 1470318316, 3),
(183, 1, 1, 2, 1, 'abc_cccccrq', 'продукт 6', 'product1-p111rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223703, 1470318316, 1),
(184, 1, 1, 2, 1, 'abc_1232rq', 'продукт 7', 'product1-p123rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223703, 1470318316, 1),
(185, 1, 1, 2, 1, 'zzz_rq', 'test 1', 'test-1rq', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223703, 1470318316, NULL),
(186, 1, 1, 2, 1, 'zzz_123rq', 'test 1-1', 'test-1-1rq', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223703, 1470318316, 7),
(187, 1, 1, 2, 1, 'abc_a1rq', 'продукт 1', 'product1-p11rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223703, 1470318316, NULL),
(188, 1, 1, 2, 1, 'abc_1a2rd', 'продукт 2', 'product1-p21rd', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223703, 1470318316, 1),
(189, 2, 1, 2, 1, 'abb_a3rd', 'продукт 3', 'product2-p121rd', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223703, 1470318316, NULL),
(190, 2, 1, 2, 1, 'abb_1a4rd', 'продукт 5', 'product2-p31rd', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223703, 1470318316, 3),
(191, 1, 1, 2, 1, 'abc_ccccca5rd', 'продукт 6', 'product1-p1112rd', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223703, 1470318316, 1),
(192, 1, 1, 2, 1, 'abc_1232a6rd', 'продукт 7', 'product1-p1231rd', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223704, 1470318316, 1),
(193, 1, 1, 2, 1, 'zzz_a7rd', 'test 1', 'test-11rd', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223704, 1470318316, NULL),
(194, 1, 1, 2, 1, 'zzz_123a8rd', 'test 1-1', 'test-1-11rd', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223704, 1470318316, 7),
(195, 1, 1, 2, 1, 'abc_a12rd', 'продукт 1', 'product1-p112dr', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223704, 1470318316, NULL),
(196, 1, 1, 2, 1, 'abc_1a22rd', 'продукт 2', 'product1-p212dr', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223704, 1470318316, 1),
(197, 2, 1, 2, 1, 'abb_a32dr', 'продукт 3', 'product2-p121d2r', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223704, 1470318316, NULL),
(198, 2, 1, 2, 1, 'abb_1a42rd', 'продукт 5', 'product2-p312rd', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223704, 1470318316, 3),
(199, 1, 1, 2, 1, 'abc_ccccca5d2r', 'продукт 6', 'product1-p11122rd', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223704, 1470318316, 1),
(200, 1, 1, 2, 1, 'abc_1232a62rd', 'продукт 7', 'product1-p12312rd', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223704, 1470318316, 1),
(201, 1, 1, 2, 1, 'zzz_a72rf', 'test 1', 'test-112fr', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223704, 1470318316, NULL),
(202, 1, 1, 2, 1, 'zzz_1f23a82r', 'test 1-1', 'test-1-112fr', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223704, 1470318316, 7),
(203, 1, 1, 2, 1, 'abc_a12f3r', 'продукт 1', 'product1-p11f23r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223704, 1470318316, NULL),
(204, 1, 1, 2, 1, 'abc_1a223fr', 'продукт 2', 'product1-p2123fr', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223704, 1470318316, 1),
(205, 2, 1, 2, 1, 'abb_a323fr', 'продукт 3', 'product2-p121f23r', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223704, 1470318316, NULL),
(206, 2, 1, 2, 1, 'abb_1a423fr', 'продукт 5', 'product2-p3123fr', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223704, 1470318316, 3),
(207, 1, 1, 2, 1, 'abc_ccccca5f23r', 'продукт 6', 'product1-p111223fr', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223704, 1470318316, 1),
(208, 1, 1, 2, 1, 'abc_1232a623rf', 'продукт 7', 'product1-p123123rf', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223704, 1470318316, 1),
(209, 1, 1, 2, 1, 'zzz_a723fr', 'test 1', 'test-1123rf', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223705, 1470318316, NULL),
(210, 1, 1, 2, 1, 'zzz_123fa823r', 'test 1-1', 'test-1-1123fr', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223705, 1470318316, 7),
(211, 1, 1, 2, 1, 'abc_req', 'продукт 1', 'product1-pe1rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223854, 1470318316, NULL),
(212, 1, 1, 2, 1, 'abc_1req', 'продукт 2', 'product1-p2erq', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223854, 1470318316, 1),
(213, 2, 1, 2, 1, 'abb_rtq', 'продукт 3', 'product2-p1trq', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223854, 1470318316, NULL),
(214, 2, 1, 2, 1, 'abb_1yrq', 'продукт 5', 'product2y-p3rq', 'маленькое описание', 'полное описание', 14, 1, 7, 'box.jpg', 1, 1, 1470223854, 1470318316, 3),
(215, 1, 1, 2, 1, 'abc_ccycccrq', 'продукт 6', 'product1-p1y11rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, NULL, 1470223854, 1470318316, 1),
(216, 1, 1, 2, 1, 'abc_1232yrq', 'продукт 7', 'product1-p12y3rq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, NULL, 1470223854, 1470318316, 1),
(217, 1, 1, 2, 1, 'zzz_ryq', 'test 1', 'test-1ryq', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223854, 1470318316, NULL),
(218, 1, 1, 2, 1, 'zzz_12y3rq', 'test 1-1', 'test-1-1yrq', 'test', 'test', 3, 0, 3, 'box.jpg', 1, 1, 1470223854, 1470318316, 7),
(219, 1, 1, 2, 1, 'abc_a1yrq', 'продукт 1', 'product1-p11yrq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470223854, 1470318316, NULL),
(220, 1, 1, 2, 1, 'abc_1a2yrd', 'продукт 2', 'product1-p2y1rd', 'маленькое описание', 'полное описание', 12, 2, 10, 'box.jpg', 1, 1, 1470223854, 1470318316, 1),
(221, 2, 1, 2, 1, 'abb_a3yrd', 'продукт 3', 'product2-p121yrd', 'маленькое описание', 'полное описание', 12, 4, 1, 'box.jpg', 0, 1, 1470223854, 1470318316, NULL),
(224, 1, 1, 2, 1, 'abc_r1', 'продукт 1 update', 'product1-p1r1', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470312734, 1470313050, 1),
(225, 1, 1, 2, 1, 'tabc_a123', 'продукт 1', 'tproduct1-p1123', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470316437, 1470316437, NULL),
(226, 1, 1, 2, 1, 'ttabc_a123', 'продукт 1', 'ttproduct1-p1123', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470316521, 1470316521, NULL),
(227, 1, 1, 2, 1, '0abc_r', 'Обновленный', 'produ0ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(228, 1, 1, 2, 1, '1abc_r', 'Обновленный', 'produ1ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(229, 1, 1, 2, 1, '2abc_r', 'Обновленный', 'produ2ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(230, 1, 1, 2, 1, '3abc_r', 'Обновленный', 'produ3ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(231, 1, 1, 2, 1, '4abc_r', 'Обновленный', 'produ4ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(232, 1, 1, 2, 1, '5abc_r', 'Обновленный', 'produ5ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(233, 1, 1, 2, 1, '6abc_r', 'Обновленный', 'produ6ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(234, 1, 1, 2, 1, '7abc_r', 'Обновленный', 'produ7ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(235, 1, 1, 2, 1, '8abc_r', 'Обновленный', 'produ8ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(236, 1, 1, 2, 1, '9abc_r', 'Обновленный', 'produ9ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(237, 1, 1, 2, 1, '10abc_r', 'Обновленный', 'produ10ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(238, 1, 1, 2, 1, '11abc_r', 'Обновленный', 'produ11ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(239, 1, 1, 2, 1, '12abc_r', 'Обновленный', 'produ12ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(240, 1, 1, 2, 1, '13abc_r', 'Обновленный', 'produ13ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(241, 1, 1, 2, 1, '14abc_r', 'Обновленный', 'produ14ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(242, 1, 1, 2, 1, '15abc_r', 'Обновленный', 'produ15ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(243, 1, 1, 2, 1, '16abc_r', 'Обновленный', 'produ16ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(244, 1, 1, 2, 1, '17abc_r', 'Обновленный', 'produ17ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(245, 1, 1, 2, 1, '18abc_r', 'Обновленный', 'produ18ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(246, 1, 1, 2, 1, '19abc_r', 'Обновленный', 'produ19ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(247, 1, 1, 2, 1, '20abc_r', 'Обновленный', 'produ20ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(248, 1, 1, 2, 1, '21abc_r', 'Обновленный', 'produ21ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(249, 1, 1, 2, 1, '22abc_r', 'Обновленный', 'produ22ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(250, 1, 1, 2, 1, '23abc_r', 'Обновленный', 'produ23ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(251, 1, 1, 2, 1, '24abc_r', 'Обновленный', 'produ24ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(252, 1, 1, 2, 1, '25abc_r', 'Обновленный', 'produ25ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(253, 1, 1, 2, 1, '26abc_r', 'Обновленный', 'produ26ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(254, 1, 1, 2, 1, '27abc_r', 'Обновленный', 'produ27ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(255, 1, 1, 2, 1, '28abc_r', 'Обновленный', 'produ28ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(256, 1, 1, 2, 1, '29abc_r', 'Обновленный', 'produ29ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(257, 1, 1, 2, 1, '30abc_r', 'Обновленный', 'produ30ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(258, 1, 1, 2, 1, '31abc_r', 'Обновленный', 'produ31ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(259, 1, 1, 2, 1, '32abc_r', 'Обновленный', 'produ32ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(260, 1, 1, 2, 1, '33abc_r', 'Обновленный', 'produ33ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(261, 1, 1, 2, 1, '34abc_r', 'Обновленный', 'produ34ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(262, 1, 1, 2, 1, '35abc_r', 'Обновленный', 'produ35ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(263, 1, 1, 2, 1, '36abc_r', 'Обновленный', 'produ36ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(264, 1, 1, 2, 1, '37abc_r', 'Обновленный', 'produ37ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(265, 1, 1, 2, 1, '38abc_r', 'Обновленный', 'produ38ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(266, 1, 1, 2, 1, '39abc_r', 'Обновленный', 'produ39ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(267, 1, 1, 2, 1, '40abc_r', 'Обновленный', 'produ40ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(268, 1, 1, 2, 1, '41abc_r', 'Обновленный', 'produ41ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(269, 1, 1, 2, 1, '42abc_r', 'Обновленный', 'produ42ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(270, 1, 1, 2, 1, '43abc_r', 'Обновленный', 'produ43ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(271, 1, 1, 2, 1, '44abc_r', 'Обновленный', 'produ44ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(272, 1, 1, 2, 1, '45abc_r', 'Обновленный', 'produ45ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(273, 1, 1, 2, 1, '46abc_r', 'Обновленный', 'produ46ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(274, 1, 1, 2, 1, '47abc_r', 'Обновленный', 'produ47ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(275, 1, 1, 2, 1, '48abc_r', 'Обновленный', 'produ48ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(276, 1, 1, 2, 1, '49abc_r', 'Обновленный', 'produ49ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(277, 1, 1, 2, 1, '50abc_r', 'Обновленный', 'produ50ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(278, 1, 1, 2, 1, '51abc_r', 'Обновленный', 'produ51ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(279, 1, 1, 2, 1, '52abc_r', 'Обновленный', 'produ52ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(280, 1, 1, 2, 1, '53abc_r', 'Обновленный', 'produ53ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(281, 1, 1, 2, 1, '54abc_r', 'Обновленный', 'produ54ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(282, 1, 1, 2, 1, '55abc_r', 'Обновленный', 'produ55ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(283, 1, 1, 2, 1, '56abc_r', 'Обновленный', 'produ56ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(284, 1, 1, 2, 1, '57abc_r', 'Обновленный', 'produ57ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(285, 1, 1, 2, 1, '58abc_r', 'Обновленный', 'produ58ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(286, 1, 1, 2, 1, '59abc_r', 'Обновленный', 'produ59ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(287, 1, 1, 2, 1, '60abc_r', 'Обновленный', 'produ60ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(288, 1, 1, 2, 1, '61abc_r', 'Обновленный', 'produ61ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(289, 1, 1, 2, 1, '62abc_r', 'Обновленный', 'produ62ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(290, 1, 1, 2, 1, '63abc_r', 'Обновленный', 'produ63ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(291, 1, 1, 2, 1, '64abc_r', 'Обновленный', 'produ64ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(292, 1, 1, 2, 1, '65abc_r', 'Обновленный', 'produ65ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(293, 1, 1, 2, 1, '66abc_r', 'Обновленный', 'produ66ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(294, 1, 1, 2, 1, '67abc_r', 'Обновленный', 'produ67ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(295, 1, 1, 2, 1, '68abc_r', 'Обновленный', 'produ68ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(296, 1, 1, 2, 1, '69abc_r', 'Обновленный', 'produ69ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(297, 1, 1, 2, 1, '70abc_r', 'Обновленный', 'produ70ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(298, 1, 1, 2, 1, '71abc_r', 'Обновленный', 'produ71ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(299, 1, 1, 2, 1, '72abc_r', 'Обновленный', 'produ72ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(300, 1, 1, 2, 1, '73abc_r', 'Обновленный', 'produ73ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(301, 1, 1, 2, 1, '74abc_r', 'Обновленный', 'produ74ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(302, 1, 1, 2, 1, '75abc_r', 'Обновленный', 'produ75ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(303, 1, 1, 2, 1, '76abc_r', 'Обновленный', 'produ76ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(304, 1, 1, 2, 1, '77abc_r', 'Обновленный', 'produ77ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(305, 1, 1, 2, 1, '78abc_r', 'Обновленный', 'produ78ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(306, 1, 1, 2, 1, '79abc_r', 'Обновленный', 'produ79ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(307, 1, 1, 2, 1, '80abc_r', 'Обновленный', 'produ80ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(308, 1, 1, 2, 1, '81abc_r', 'Обновленный', 'produ81ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(309, 1, 1, 2, 1, '82abc_r', 'Обновленный', 'produ82ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(310, 1, 1, 2, 1, '83abc_r', 'Обновленный', 'produ83ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(311, 1, 1, 2, 1, '84abc_r', 'Обновленный', 'produ84ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(312, 1, 1, 2, 1, '85abc_r', 'Обновленный', 'produ85ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(313, 1, 1, 2, 1, '86abc_r', 'Обновленный', 'produ86ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(314, 1, 1, 2, 1, '87abc_r', 'Обновленный', 'produ87ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(315, 1, 1, 2, 1, '88abc_r', 'Обновленный', 'produ88ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(316, 1, 1, 2, 1, '89abc_r', 'Обновленный', 'produ89ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(317, 1, 1, 2, 1, '90abc_r', 'Обновленный', 'produ90ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(318, 1, 1, 2, 1, '91abc_r', 'Обновленный', 'produ91ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(319, 1, 1, 2, 1, '92abc_r', 'Обновленный', 'produ92ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(320, 1, 1, 2, 1, '93abc_r', 'Обновленный', 'produ93ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(321, 1, 1, 2, 1, '94abc_r', 'Обновленный', 'produ94ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(322, 1, 1, 2, 1, '95abc_r', 'Обновленный', 'produ95ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(323, 1, 1, 2, 1, '96abc_r', 'Обновленный', 'produ96ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(324, 1, 1, 2, 1, '97abc_r', 'Обновленный', 'produ97ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(325, 1, 1, 2, 1, '98abc_r', 'Обновленный', 'produ98ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(326, 1, 1, 2, 1, '99abc_r', 'Обновленный', 'produ99ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(327, 1, 1, 2, 1, '100abc_r', 'Обновленный', 'produ100ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(328, 1, 1, 2, 1, '101abc_r', 'Обновленный', 'produ101ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(329, 1, 1, 2, 1, '102abc_r', 'Обновленный', 'produ102ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(330, 1, 1, 2, 1, '103abc_r', 'Обновленный', 'produ103ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(331, 1, 1, 2, 1, '104abc_r', 'Обновленный', 'produ104ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(332, 1, 1, 2, 1, '105abc_r', 'Обновленный', 'produ105ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(333, 1, 1, 2, 1, '106abc_r', 'Обновленный', 'produ106ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(334, 1, 1, 2, 1, '107abc_r', 'Обновленный', 'produ107ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(335, 1, 1, 2, 1, '108abc_r', 'Обновленный', 'produ108ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(336, 1, 1, 2, 1, '109abc_r', 'Обновленный', 'produ109ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(337, 1, 1, 2, 1, '110abc_r', 'Обновленный', 'produ110ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(338, 1, 1, 2, 1, '111abc_r', 'Обновленный', 'produ111ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(339, 1, 1, 2, 1, '112abc_r', 'Обновленный', 'produ112ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(340, 1, 1, 2, 1, '113abc_r', 'Обновленный', 'produ113ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(341, 1, 1, 2, 1, '114abc_r', 'Обновленный', 'produ114ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(342, 1, 1, 2, 1, '115abc_r', 'Обновленный', 'produ115ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(343, 1, 1, 2, 1, '116abc_r', 'Обновленный', 'produ116ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(344, 1, 1, 2, 1, '117abc_r', 'Обновленный', 'produ117ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(345, 1, 1, 2, 1, '118abc_r', 'Обновленный', 'produ118ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(346, 1, 1, 2, 1, '119abc_r', 'Обновленный', 'produ119ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(347, 1, 1, 2, 1, '120abc_r', 'Обновленный', 'produ120ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(348, 1, 1, 2, 1, '121abc_r', 'Обновленный', 'produ121ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(349, 1, 1, 2, 1, '122abc_r', 'Обновленный', 'produ122ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(350, 1, 1, 2, 1, '123abc_r', 'Обновленный', 'produ123ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(351, 1, 1, 2, 1, '124abc_r', 'Обновленный', 'produ124ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(352, 1, 1, 2, 1, '125abc_r', 'Обновленный', 'produ125ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(353, 1, 1, 2, 1, '126abc_r', 'Обновленный', 'produ126ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(354, 1, 1, 2, 1, '127abc_r', 'Обновленный', 'produ127ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(355, 1, 1, 2, 1, '128abc_r', 'Обновленный', 'produ128ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(356, 1, 1, 2, 1, '129abc_r', 'Обновленный', 'produ129ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(357, 1, 1, 2, 1, '130abc_r', 'Обновленный', 'produ130ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(358, 1, 1, 2, 1, '131abc_r', 'Обновленный', 'produ131ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(359, 1, 1, 2, 1, '132abc_r', 'Обновленный', 'produ132ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(360, 1, 1, 2, 1, '133abc_r', 'Обновленный', 'produ133ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(361, 1, 1, 2, 1, '134abc_r', 'Обновленный', 'produ134ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(362, 1, 1, 2, 1, '135abc_r', 'Обновленный', 'produ135ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(363, 1, 1, 2, 1, '136abc_r', 'Обновленный', 'produ136ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(364, 1, 1, 2, 1, '137abc_r', 'Обновленный', 'produ137ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(365, 1, 1, 2, 1, '138abc_r', 'Обновленный', 'produ138ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(366, 1, 1, 2, 1, '139abc_r', 'Обновленный', 'produ139ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(367, 1, 1, 2, 1, '140abc_r', 'Обновленный', 'produ140ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(368, 1, 1, 2, 1, '141abc_r', 'Обновленный', 'produ141ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(369, 1, 1, 2, 1, '142abc_r', 'Обновленный', 'produ142ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(370, 1, 1, 2, 1, '143abc_r', 'Обновленный', 'produ143ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(371, 1, 1, 2, 1, '144abc_r', 'Обновленный', 'produ144ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(372, 1, 1, 2, 1, '145abc_r', 'Обновленный', 'produ145ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(373, 1, 1, 2, 1, '146abc_r', 'Обновленный', 'produ146ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(374, 1, 1, 2, 1, '147abc_r', 'Обновленный', 'produ147ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(375, 1, 1, 2, 1, '148abc_r', 'Обновленный', 'produ148ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(376, 1, 1, 2, 1, '149abc_r', 'Обновленный', 'produ149ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(377, 1, 1, 2, 1, '150abc_r', 'Обновленный', 'produ150ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(378, 1, 1, 2, 1, '151abc_r', 'Обновленный', 'produ151ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(379, 1, 1, 2, 1, '152abc_r', 'Обновленный', 'produ152ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(380, 1, 1, 2, 1, '153abc_r', 'Обновленный', 'produ153ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(381, 1, 1, 2, 1, '154abc_r', 'Обновленный', 'produ154ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(382, 1, 1, 2, 1, '155abc_r', 'Обновленный', 'produ155ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(383, 1, 1, 2, 1, '156abc_r', 'Обновленный', 'produ156ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(384, 1, 1, 2, 1, '157abc_r', 'Обновленный', 'produ157ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(385, 1, 1, 2, 1, '158abc_r', 'Обновленный', 'produ158ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(386, 1, 1, 2, 1, '159abc_r', 'Обновленный', 'produ159ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(387, 1, 1, 2, 1, '160abc_r', 'Обновленный', 'produ160ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(388, 1, 1, 2, 1, '161abc_r', 'Обновленный', 'produ161ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(389, 1, 1, 2, 1, '162abc_r', 'Обновленный', 'produ162ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(390, 1, 1, 2, 1, '163abc_r', 'Обновленный', 'produ163ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(391, 1, 1, 2, 1, '164abc_r', 'Обновленный', 'produ164ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(392, 1, 1, 2, 1, '165abc_r', 'Обновленный', 'produ165ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(393, 1, 1, 2, 1, '166abc_r', 'Обновленный', 'produ166ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(394, 1, 1, 2, 1, '167abc_r', 'Обновленный', 'produ167ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(395, 1, 1, 2, 1, '168abc_r', 'Обновленный', 'produ168ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(396, 1, 1, 2, 1, '169abc_r', 'Обновленный', 'produ169ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(397, 1, 1, 2, 1, '170abc_r', 'Обновленный', 'produ170ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(398, 1, 1, 2, 1, '171abc_r', 'Обновленный', 'produ171ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(399, 1, 1, 2, 1, '172abc_r', 'Обновленный', 'produ172ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(400, 1, 1, 2, 1, '173abc_r', 'Обновленный', 'produ173ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(401, 1, 1, 2, 1, '174abc_r', 'Обновленный', 'produ174ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(402, 1, 1, 2, 1, '175abc_r', 'Обновленный', 'produ175ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(403, 1, 1, 2, 1, '176abc_r', 'Обновленный', 'produ176ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(404, 1, 1, 2, 1, '177abc_r', 'Обновленный', 'produ177ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(405, 1, 1, 2, 1, '178abc_r', 'Обновленный', 'produ178ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(406, 1, 1, 2, 1, '179abc_r', 'Обновленный', 'produ179ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(407, 1, 1, 2, 1, '180abc_r', 'Обновленный', 'produ180ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(408, 1, 1, 2, 1, '181abc_r', 'Обновленный', 'produ181ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(409, 1, 1, 2, 1, '182abc_r', 'Обновленный', 'produ182ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(410, 1, 1, 2, 1, '183abc_r', 'Обновленный', 'produ183ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(411, 1, 1, 2, 1, '184abc_r', 'Обновленный', 'produ184ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(412, 1, 1, 2, 1, '185abc_r', 'Обновленный', 'produ185ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(413, 1, 1, 2, 1, '186abc_r', 'Обновленный', 'produ186ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(414, 1, 1, 2, 1, '187abc_r', 'Обновленный', 'produ187ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(415, 1, 1, 2, 1, '188abc_r', 'Обновленный', 'produ188ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(416, 1, 1, 2, 1, '189abc_r', 'Обновленный', 'produ189ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(417, 1, 1, 2, 1, '190abc_r', 'Обновленный', 'produ190ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(418, 1, 1, 2, 1, '191abc_r', 'Обновленный', 'produ191ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(419, 1, 1, 2, 1, '192abc_r', 'Обновленный', 'produ192ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(420, 1, 1, 2, 1, '193abc_r', 'Обновленный', 'produ193ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(421, 1, 1, 2, 1, '194abc_r', 'Обновленный', 'produ194ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(422, 1, 1, 2, 1, '195abc_r', 'Обновленный', 'produ195ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(423, 1, 1, 2, 1, '196abc_r', 'Обновленный', 'produ196ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(424, 1, 1, 2, 1, '197abc_r', 'Обновленный', 'produ197ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(425, 1, 1, 2, 1, '198abc_r', 'Обновленный', 'produ198ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(426, 1, 1, 2, 1, '199abc_r', 'Обновленный', 'produ199ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(427, 1, 1, 2, 1, '200abc_r', 'Обновленный', 'produ200ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(428, 1, 1, 2, 1, '201abc_r', 'Обновленный', 'produ201ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(429, 1, 1, 2, 1, '202abc_r', 'Обновленный', 'produ202ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(430, 1, 1, 2, 1, '203abc_r', 'Обновленный', 'produ203ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(431, 1, 1, 2, 1, '204abc_r', 'Обновленный', 'produ204ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(432, 1, 1, 2, 1, '205abc_r', 'Обновленный', 'produ205ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(433, 1, 1, 2, 1, '206abc_r', 'Обновленный', 'produ206ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(434, 1, 1, 2, 1, '207abc_r', 'Обновленный', 'produ207ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(435, 1, 1, 2, 1, '208abc_r', 'Обновленный', 'produ208ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(436, 1, 1, 2, 1, '209abc_r', 'Обновленный', 'produ209ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(437, 1, 1, 2, 1, '210abc_r', 'Обновленный', 'produ210ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(438, 1, 1, 2, 1, '211abc_r', 'Обновленный', 'produ211ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(439, 1, 1, 2, 1, '212abc_r', 'Обновленный', 'produ212ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(440, 1, 1, 2, 1, '213abc_r', 'Обновленный', 'produ213ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(441, 1, 1, 2, 1, '214abc_r', 'Обновленный', 'produ214ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(442, 1, 1, 2, 1, '215abc_r', 'Обновленный', 'produ215ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1);
INSERT INTO `product_item` (`id`, `category_id`, `delivery_id`, `page_id`, `brand_id`, `articul`, `title`, `uri`, `small_description`, `description`, `price`, `discount`, `count`, `preview`, `active`, `photo_album_id`, `create_at`, `update_at`, `master_id`) VALUES
(443, 1, 1, 2, 1, '216abc_r', 'Обновленный', 'produ216ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(444, 1, 1, 2, 1, '217abc_r', 'Обновленный', 'produ217ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(445, 1, 1, 2, 1, '218abc_r', 'Обновленный', 'produ218ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(446, 1, 1, 2, 1, '219abc_r', 'Обновленный', 'produ219ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(447, 1, 1, 2, 1, '220abc_r', 'Обновленный', 'produ220ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(448, 1, 1, 2, 1, '221abc_r', 'Обновленный', 'produ221ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(449, 1, 1, 2, 1, '222abc_r', 'Обновленный', 'produ222ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(450, 1, 1, 2, 1, '223abc_r', 'Обновленный', 'produ223ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(451, 1, 1, 2, 1, '224abc_r', 'Обновленный', 'produ224ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(452, 1, 1, 2, 1, '225abc_r', 'Обновленный', 'produ225ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(453, 1, 1, 2, 1, '226abc_r', 'Обновленный', 'produ226ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(454, 1, 1, 2, 1, '227abc_r', 'Обновленный', 'produ227ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(455, 1, 1, 2, 1, '228abc_r', 'Обновленный', 'produ228ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(456, 1, 1, 2, 1, '229abc_r', 'Обновленный', 'produ229ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(457, 1, 1, 2, 1, '230abc_r', 'Обновленный', 'produ230ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(458, 1, 1, 2, 1, '231abc_r', 'Обновленный', 'produ231ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(459, 1, 1, 2, 1, '232abc_r', 'Обновленный', 'produ232ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(460, 1, 1, 2, 1, '233abc_r', 'Обновленный', 'produ233ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(461, 1, 1, 2, 1, '234abc_r', 'Обновленный', 'produ234ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(462, 1, 1, 2, 1, '235abc_r', 'Обновленный', 'produ235ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(463, 1, 1, 2, 1, '236abc_r', 'Обновленный', 'produ236ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(464, 1, 1, 2, 1, '237abc_r', 'Обновленный', 'produ237ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(465, 1, 1, 2, 1, '238abc_r', 'Обновленный', 'produ238ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(466, 1, 1, 2, 1, '239abc_r', 'Обновленный', 'produ239ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(467, 1, 1, 2, 1, '240abc_r', 'Обновленный', 'produ240ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(468, 1, 1, 2, 1, '241abc_r', 'Обновленный', 'produ241ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(469, 1, 1, 2, 1, '242abc_r', 'Обновленный', 'produ242ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(470, 1, 1, 2, 1, '243abc_r', 'Обновленный', 'produ243ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(471, 1, 1, 2, 1, '244abc_r', 'Обновленный', 'produ244ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(472, 1, 1, 2, 1, '245abc_r', 'Обновленный', 'produ245ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(473, 1, 1, 2, 1, '246abc_r', 'Обновленный', 'produ246ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(474, 1, 1, 2, 1, '247abc_r', 'Обновленный', 'produ247ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(475, 1, 1, 2, 1, '248abc_r', 'Обновленный', 'produ248ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(476, 1, 1, 2, 1, '249abc_r', 'Обновленный', 'produ249ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(477, 1, 1, 2, 1, '250abc_r', 'Обновленный', 'produ250ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(478, 1, 1, 2, 1, '251abc_r', 'Обновленный', 'produ251ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(479, 1, 1, 2, 1, '252abc_r', 'Обновленный', 'produ252ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(480, 1, 1, 2, 1, '253abc_r', 'Обновленный', 'produ253ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(481, 1, 1, 2, 1, '254abc_r', 'Обновленный', 'produ254ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(482, 1, 1, 2, 1, '255abc_r', 'Обновленный', 'produ255ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(483, 1, 1, 2, 1, '256abc_r', 'Обновленный', 'produ256ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(484, 1, 1, 2, 1, '257abc_r', 'Обновленный', 'produ257ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(485, 1, 1, 2, 1, '258abc_r', 'Обновленный', 'produ258ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(486, 1, 1, 2, 1, '259abc_r', 'Обновленный', 'produ259ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(487, 1, 1, 2, 1, '260abc_r', 'Обновленный', 'produ260ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(488, 1, 1, 2, 1, '261abc_r', 'Обновленный', 'produ261ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(489, 1, 1, 2, 1, '262abc_r', 'Обновленный', 'produ262ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(490, 1, 1, 2, 1, '263abc_r', 'Обновленный', 'produ263ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(491, 1, 1, 2, 1, '264abc_r', 'Обновленный', 'produ264ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(492, 1, 1, 2, 1, '265abc_r', 'Обновленный', 'produ265ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(493, 1, 1, 2, 1, '266abc_r', 'Обновленный', 'produ266ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(494, 1, 1, 2, 1, '267abc_r', 'Обновленный', 'produ267ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(495, 1, 1, 2, 1, '268abc_r', 'Обновленный', 'produ268ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(496, 1, 1, 2, 1, '269abc_r', 'Обновленный', 'produ269ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(497, 1, 1, 2, 1, '270abc_r', 'Обновленный', 'produ270ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(498, 1, 1, 2, 1, '271abc_r', 'Обновленный', 'produ271ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(499, 1, 1, 2, 1, '272abc_r', 'Обновленный', 'produ272ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(500, 1, 1, 2, 1, '273abc_r', 'Обновленный', 'produ273ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(501, 1, 1, 2, 1, '274abc_r', 'Обновленный', 'produ274ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(502, 1, 1, 2, 1, '275abc_r', 'Обновленный', 'produ275ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(503, 1, 1, 2, 1, '276abc_r', 'Обновленный', 'produ276ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(504, 1, 1, 2, 1, '277abc_r', 'Обновленный', 'produ277ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(505, 1, 1, 2, 1, '278abc_r', 'Обновленный', 'produ278ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(506, 1, 1, 2, 1, '279abc_r', 'Обновленный', 'produ279ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(507, 1, 1, 2, 1, '280abc_r', 'Обновленный', 'produ280ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(508, 1, 1, 2, 1, '281abc_r', 'Обновленный', 'produ281ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(509, 1, 1, 2, 1, '282abc_r', 'Обновленный', 'produ282ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(510, 1, 1, 2, 1, '283abc_r', 'Обновленный', 'produ283ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(511, 1, 1, 2, 1, '284abc_r', 'Обновленный', 'produ284ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(512, 1, 1, 2, 1, '285abc_r', 'Обновленный', 'produ285ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(513, 1, 1, 2, 1, '286abc_r', 'Обновленный', 'produ286ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(514, 1, 1, 2, 1, '287abc_r', 'Обновленный', 'produ287ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(515, 1, 1, 2, 1, '288abc_r', 'Обновленный', 'produ288ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(516, 1, 1, 2, 1, '289abc_r', 'Обновленный', 'produ289ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(517, 1, 1, 2, 1, '290abc_r', 'Обновленный', 'produ290ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(518, 1, 1, 2, 1, '291abc_r', 'Обновленный', 'produ291ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(519, 1, 1, 2, 1, '292abc_r', 'Обновленный', 'produ292ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(520, 1, 1, 2, 1, '293abc_r', 'Обновленный', 'produ293ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(521, 1, 1, 2, 1, '294abc_r', 'Обновленный', 'produ294ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(522, 1, 1, 2, 1, '295abc_r', 'Обновленный', 'produ295ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(523, 1, 1, 2, 1, '296abc_r', 'Обновленный', 'produ296ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(524, 1, 1, 2, 1, '297abc_r', 'Обновленный', 'produ297ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(525, 1, 1, 2, 1, '298abc_r', 'Обновленный', 'produ298ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(526, 1, 1, 2, 1, '299abc_r', 'Обновленный', 'produ299ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(527, 1, 1, 2, 1, '300abc_r', 'Обновленный', 'produ300ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(528, 1, 1, 2, 1, '301abc_r', 'Обновленный', 'produ301ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(529, 1, 1, 2, 1, '302abc_r', 'Обновленный', 'produ302ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(530, 1, 1, 2, 1, '303abc_r', 'Обновленный', 'produ303ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(531, 1, 1, 2, 1, '304abc_r', 'Обновленный', 'produ304ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(532, 1, 1, 2, 1, '305abc_r', 'Обновленный', 'produ305ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(533, 1, 1, 2, 1, '306abc_r', 'Обновленный', 'produ306ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(534, 1, 1, 2, 1, '307abc_r', 'Обновленный', 'produ307ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(535, 1, 1, 2, 1, '308abc_r', 'Обновленный', 'produ308ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(536, 1, 1, 2, 1, '309abc_r', 'Обновленный', 'produ309ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(537, 1, 1, 2, 1, '310abc_r', 'Обновленный', 'produ310ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(538, 1, 1, 2, 1, '311abc_r', 'Обновленный', 'produ311ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(539, 1, 1, 2, 1, '312abc_r', 'Обновленный', 'produ312ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(540, 1, 1, 2, 1, '313abc_r', 'Обновленный', 'produ313ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(541, 1, 1, 2, 1, '314abc_r', 'Обновленный', 'produ314ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(542, 1, 1, 2, 1, '315abc_r', 'Обновленный', 'produ315ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(543, 1, 1, 2, 1, '316abc_r', 'Обновленный', 'produ316ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(544, 1, 1, 2, 1, '317abc_r', 'Обновленный', 'produ317ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(545, 1, 1, 2, 1, '318abc_r', 'Обновленный', 'produ318ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(546, 1, 1, 2, 1, '319abc_r', 'Обновленный', 'produ319ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(547, 1, 1, 2, 1, '320abc_r', 'Обновленный', 'produ320ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(548, 1, 1, 2, 1, '321abc_r', 'Обновленный', 'produ321ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(549, 1, 1, 2, 1, '322abc_r', 'Обновленный', 'produ322ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(550, 1, 1, 2, 1, '323abc_r', 'Обновленный', 'produ323ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(551, 1, 1, 2, 1, '324abc_r', 'Обновленный', 'produ324ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(552, 1, 1, 2, 1, '325abc_r', 'Обновленный', 'produ325ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(553, 1, 1, 2, 1, '326abc_r', 'Обновленный', 'produ326ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(554, 1, 1, 2, 1, '327abc_r', 'Обновленный', 'produ327ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(555, 1, 1, 2, 1, '328abc_r', 'Обновленный', 'produ328ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(556, 1, 1, 2, 1, '329abc_r', 'Обновленный', 'produ329ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(557, 1, 1, 2, 1, '330abc_r', 'Обновленный', 'produ330ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(558, 1, 1, 2, 1, '331abc_r', 'Обновленный', 'produ331ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(559, 1, 1, 2, 1, '332abc_r', 'Обновленный', 'produ332ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(560, 1, 1, 2, 1, '333abc_r', 'Обновленный', 'produ333ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(561, 1, 1, 2, 1, '334abc_r', 'Обновленный', 'produ334ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(562, 1, 1, 2, 1, '335abc_r', 'Обновленный', 'produ335ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(563, 1, 1, 2, 1, '336abc_r', 'Обновленный', 'produ336ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(564, 1, 1, 2, 1, '337abc_r', 'Обновленный', 'produ337ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(565, 1, 1, 2, 1, '338abc_r', 'Обновленный', 'produ338ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(566, 1, 1, 2, 1, '339abc_r', 'Обновленный', 'produ339ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(567, 1, 1, 2, 1, '340abc_r', 'Обновленный', 'produ340ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(568, 1, 1, 2, 1, '341abc_r', 'Обновленный', 'produ341ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(569, 1, 1, 2, 1, '342abc_r', 'Обновленный', 'produ342ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(570, 1, 1, 2, 1, '343abc_r', 'Обновленный', 'produ343ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(571, 1, 1, 2, 1, '344abc_r', 'Обновленный', 'produ344ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(572, 1, 1, 2, 1, '345abc_r', 'Обновленный', 'produ345ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(573, 1, 1, 2, 1, '346abc_r', 'Обновленный', 'produ346ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(574, 1, 1, 2, 1, '347abc_r', 'Обновленный', 'produ347ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(575, 1, 1, 2, 1, '348abc_r', 'Обновленный', 'produ348ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(576, 1, 1, 2, 1, '349abc_r', 'Обновленный', 'produ349ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(577, 1, 1, 2, 1, '350abc_r', 'Обновленный', 'produ350ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(578, 1, 1, 2, 1, '351abc_r', 'Обновленный', 'produ351ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(579, 1, 1, 2, 1, '352abc_r', 'Обновленный', 'produ352ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(580, 1, 1, 2, 1, '353abc_r', 'Обновленный', 'produ353ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(581, 1, 1, 2, 1, '354abc_r', 'Обновленный', 'produ354ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(582, 1, 1, 2, 1, '355abc_r', 'Обновленный', 'produ355ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(583, 1, 1, 2, 1, '356abc_r', 'Обновленный', 'produ356ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(584, 1, 1, 2, 1, '357abc_r', 'Обновленный', 'produ357ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(585, 1, 1, 2, 1, '358abc_r', 'Обновленный', 'produ358ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(586, 1, 1, 2, 1, '359abc_r', 'Обновленный', 'produ359ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(587, 1, 1, 2, 1, '360abc_r', 'Обновленный', 'produ360ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(588, 1, 1, 2, 1, '361abc_r', 'Обновленный', 'produ361ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(589, 1, 1, 2, 1, '362abc_r', 'Обновленный', 'produ362ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(590, 1, 1, 2, 1, '363abc_r', 'Обновленный', 'produ363ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(591, 1, 1, 2, 1, '364abc_r', 'Обновленный', 'produ364ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(592, 1, 1, 2, 1, '365abc_r', 'Обновленный', 'produ365ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(593, 1, 1, 2, 1, '366abc_r', 'Обновленный', 'produ366ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(594, 1, 1, 2, 1, '367abc_r', 'Обновленный', 'produ367ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(595, 1, 1, 2, 1, '368abc_r', 'Обновленный', 'produ368ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(596, 1, 1, 2, 1, '369abc_r', 'Обновленный', 'produ369ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(597, 1, 1, 2, 1, '370abc_r', 'Обновленный', 'produ370ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(598, 1, 1, 2, 1, '371abc_r', 'Обновленный', 'produ371ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(599, 1, 1, 2, 1, '372abc_r', 'Обновленный', 'produ372ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(600, 1, 1, 2, 1, '373abc_r', 'Обновленный', 'produ373ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(601, 1, 1, 2, 1, '374abc_r', 'Обновленный', 'produ374ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(602, 1, 1, 2, 1, '375abc_r', 'Обновленный', 'produ375ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(603, 1, 1, 2, 1, '376abc_r', 'Обновленный', 'produ376ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(604, 1, 1, 2, 1, '377abc_r', 'Обновленный', 'produ377ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(605, 1, 1, 2, 1, '378abc_r', 'Обновленный', 'produ378ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(606, 1, 1, 2, 1, '379abc_r', 'Обновленный', 'produ379ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(607, 1, 1, 2, 1, '380abc_r', 'Обновленный', 'produ380ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(608, 1, 1, 2, 1, '381abc_r', 'Обновленный', 'produ381ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(609, 1, 1, 2, 1, '382abc_r', 'Обновленный', 'produ382ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(610, 1, 1, 2, 1, '383abc_r', 'Обновленный', 'produ383ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(611, 1, 1, 2, 1, '384abc_r', 'Обновленный', 'produ384ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(612, 1, 1, 2, 1, '385abc_r', 'Обновленный', 'produ385ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(613, 1, 1, 2, 1, '386abc_r', 'Обновленный', 'produ386ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(614, 1, 1, 2, 1, '387abc_r', 'Обновленный', 'produ387ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(615, 1, 1, 2, 1, '388abc_r', 'Обновленный', 'produ388ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(616, 1, 1, 2, 1, '389abc_r', 'Обновленный', 'produ389ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(617, 1, 1, 2, 1, '390abc_r', 'Обновленный', 'produ390ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(618, 1, 1, 2, 1, '391abc_r', 'Обновленный', 'produ391ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(619, 1, 1, 2, 1, '392abc_r', 'Обновленный', 'produ392ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(620, 1, 1, 2, 1, '393abc_r', 'Обновленный', 'produ393ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(621, 1, 1, 2, 1, '394abc_r', 'Обновленный', 'produ394ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(622, 1, 1, 2, 1, '395abc_r', 'Обновленный', 'produ395ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(623, 1, 1, 2, 1, '396abc_r', 'Обновленный', 'produ396ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(624, 1, 1, 2, 1, '397abc_r', 'Обновленный', 'produ397ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(625, 1, 1, 2, 1, '398abc_r', 'Обновленный', 'produ398ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(626, 1, 1, 2, 1, '399abc_r', 'Обновленный', 'produ399ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(627, 1, 1, 2, 1, '400abc_r', 'Обновленный', 'produ400ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(628, 1, 1, 2, 1, '401abc_r', 'Обновленный', 'produ401ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(629, 1, 1, 2, 1, '402abc_r', 'Обновленный', 'produ402ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(630, 1, 1, 2, 1, '403abc_r', 'Обновленный', 'produ403ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(631, 1, 1, 2, 1, '404abc_r', 'Обновленный', 'produ404ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(632, 1, 1, 2, 1, '405abc_r', 'Обновленный', 'produ405ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(633, 1, 1, 2, 1, '406abc_r', 'Обновленный', 'produ406ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(634, 1, 1, 2, 1, '407abc_r', 'Обновленный', 'produ407ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(635, 1, 1, 2, 1, '408abc_r', 'Обновленный', 'produ408ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(636, 1, 1, 2, 1, '409abc_r', 'Обновленный', 'produ409ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(637, 1, 1, 2, 1, '410abc_r', 'Обновленный', 'produ410ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(638, 1, 1, 2, 1, '411abc_r', 'Обновленный', 'produ411ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(639, 1, 1, 2, 1, '412abc_r', 'Обновленный', 'produ412ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(640, 1, 1, 2, 1, '413abc_r', 'Обновленный', 'produ413ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(641, 1, 1, 2, 1, '414abc_r', 'Обновленный', 'produ414ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(642, 1, 1, 2, 1, '415abc_r', 'Обновленный', 'produ415ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(643, 1, 1, 2, 1, '416abc_r', 'Обновленный', 'produ416ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(644, 1, 1, 2, 1, '417abc_r', 'Обновленный', 'produ417ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(645, 1, 1, 2, 1, '418abc_r', 'Обновленный', 'produ418ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(646, 1, 1, 2, 1, '419abc_r', 'Обновленный', 'produ419ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(647, 1, 1, 2, 1, '420abc_r', 'Обновленный', 'produ420ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(648, 1, 1, 2, 1, '421abc_r', 'Обновленный', 'produ421ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(649, 1, 1, 2, 1, '422abc_r', 'Обновленный', 'produ422ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(650, 1, 1, 2, 1, '423abc_r', 'Обновленный', 'produ423ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(651, 1, 1, 2, 1, '424abc_r', 'Обновленный', 'produ424ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(652, 1, 1, 2, 1, '425abc_r', 'Обновленный', 'produ425ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(653, 1, 1, 2, 1, '426abc_r', 'Обновленный', 'produ426ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(654, 1, 1, 2, 1, '427abc_r', 'Обновленный', 'produ427ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(655, 1, 1, 2, 1, '428abc_r', 'Обновленный', 'produ428ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(656, 1, 1, 2, 1, '429abc_r', 'Обновленный', 'produ429ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(657, 1, 1, 2, 1, '430abc_r', 'Обновленный', 'produ430ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(658, 1, 1, 2, 1, '431abc_r', 'Обновленный', 'produ431ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(659, 1, 1, 2, 1, '432abc_r', 'Обновленный', 'produ432ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(660, 1, 1, 2, 1, '433abc_r', 'Обновленный', 'produ433ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(661, 1, 1, 2, 1, '434abc_r', 'Обновленный', 'produ434ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(662, 1, 1, 2, 1, '435abc_r', 'Обновленный', 'produ435ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(663, 1, 1, 2, 1, '436abc_r', 'Обновленный', 'produ436ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(664, 1, 1, 2, 1, '437abc_r', 'Обновленный', 'produ437ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(665, 1, 1, 2, 1, '438abc_r', 'Обновленный', 'produ438ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(666, 1, 1, 2, 1, '439abc_r', 'Обновленный', 'produ439ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(667, 1, 1, 2, 1, '440abc_r', 'Обновленный', 'produ440ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(668, 1, 1, 2, 1, '441abc_r', 'Обновленный', 'produ441ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(669, 1, 1, 2, 1, '442abc_r', 'Обновленный', 'produ442ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(670, 1, 1, 2, 1, '443abc_r', 'Обновленный', 'produ443ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(671, 1, 1, 2, 1, '444abc_r', 'Обновленный', 'produ444ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(672, 1, 1, 2, 1, '445abc_r', 'Обновленный', 'produ445ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(673, 1, 1, 2, 1, '446abc_r', 'Обновленный', 'produ446ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(674, 1, 1, 2, 1, '447abc_r', 'Обновленный', 'produ447ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(675, 1, 1, 2, 1, '448abc_r', 'Обновленный', 'produ448ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(676, 1, 1, 2, 1, '449abc_r', 'Обновленный', 'produ449ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(677, 1, 1, 2, 1, '450abc_r', 'Обновленный', 'produ450ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(678, 1, 1, 2, 1, '451abc_r', 'Обновленный', 'produ451ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(679, 1, 1, 2, 1, '452abc_r', 'Обновленный', 'produ452ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(680, 1, 1, 2, 1, '453abc_r', 'Обновленный', 'produ453ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(681, 1, 1, 2, 1, '454abc_r', 'Обновленный', 'produ454ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(682, 1, 1, 2, 1, '455abc_r', 'Обновленный', 'produ455ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(683, 1, 1, 2, 1, '456abc_r', 'Обновленный', 'produ456ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(684, 1, 1, 2, 1, '457abc_r', 'Обновленный', 'produ457ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(685, 1, 1, 2, 1, '458abc_r', 'Обновленный', 'produ458ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(686, 1, 1, 2, 1, '459abc_r', 'Обновленный', 'produ459ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(687, 1, 1, 2, 1, '460abc_r', 'Обновленный', 'produ460ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(688, 1, 1, 2, 1, '461abc_r', 'Обновленный', 'produ461ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(689, 1, 1, 2, 1, '462abc_r', 'Обновленный', 'produ462ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(690, 1, 1, 2, 1, '463abc_r', 'Обновленный', 'produ463ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(691, 1, 1, 2, 1, '464abc_r', 'Обновленный', 'produ464ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(692, 1, 1, 2, 1, '465abc_r', 'Обновленный', 'produ465ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(693, 1, 1, 2, 1, '466abc_r', 'Обновленный', 'produ466ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(694, 1, 1, 2, 1, '467abc_r', 'Обновленный', 'produ467ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(695, 1, 1, 2, 1, '468abc_r', 'Обновленный', 'produ468ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(696, 1, 1, 2, 1, '469abc_r', 'Обновленный', 'produ469ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(697, 1, 1, 2, 1, '470abc_r', 'Обновленный', 'produ470ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(698, 1, 1, 2, 1, '471abc_r', 'Обновленный', 'produ471ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(699, 1, 1, 2, 1, '472abc_r', 'Обновленный', 'produ472ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(700, 1, 1, 2, 1, '473abc_r', 'Обновленный', 'produ473ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(701, 1, 1, 2, 1, '474abc_r', 'Обновленный', 'produ474ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(702, 1, 1, 2, 1, '475abc_r', 'Обновленный', 'produ475ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(703, 1, 1, 2, 1, '476abc_r', 'Обновленный', 'produ476ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(704, 1, 1, 2, 1, '477abc_r', 'Обновленный', 'produ477ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(705, 1, 1, 2, 1, '478abc_r', 'Обновленный', 'produ478ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(706, 1, 1, 2, 1, '479abc_r', 'Обновленный', 'produ479ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(707, 1, 1, 2, 1, '480abc_r', 'Обновленный', 'produ480ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(708, 1, 1, 2, 1, '481abc_r', 'Обновленный', 'produ481ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(709, 1, 1, 2, 1, '482abc_r', 'Обновленный', 'produ482ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(710, 1, 1, 2, 1, '483abc_r', 'Обновленный', 'produ483ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(711, 1, 1, 2, 1, '484abc_r', 'Обновленный', 'produ484ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(712, 1, 1, 2, 1, '485abc_r', 'Обновленный', 'produ485ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(713, 1, 1, 2, 1, '486abc_r', 'Обновленный', 'produ486ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(714, 1, 1, 2, 1, '487abc_r', 'Обновленный', 'produ487ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(715, 1, 1, 2, 1, '488abc_r', 'Обновленный', 'produ488ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(716, 1, 1, 2, 1, '489abc_r', 'Обновленный', 'produ489ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(717, 1, 1, 2, 1, '490abc_r', 'Обновленный', 'produ490ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(718, 1, 1, 2, 1, '491abc_r', 'Обновленный', 'produ491ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(719, 1, 1, 2, 1, '492abc_r', 'Обновленный', 'produ492ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(720, 1, 1, 2, 1, '493abc_r', 'Обновленный', 'produ493ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(721, 1, 1, 2, 1, '494abc_r', 'Обновленный', 'produ494ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(722, 1, 1, 2, 1, '495abc_r', 'Обновленный', 'produ495ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(723, 1, 1, 2, 1, '496abc_r', 'Обновленный', 'produ496ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(724, 1, 1, 2, 1, '497abc_r', 'Обновленный', 'produ497ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(725, 1, 1, 2, 1, '498abc_r', 'Обновленный', 'produ498ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(726, 1, 1, 2, 1, '499abc_r', 'Обновленный', 'produ499ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(727, 1, 1, 2, 1, '500abc_r', 'Обновленный', 'produ500ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(728, 1, 1, 2, 1, '501abc_r', 'Обновленный', 'produ501ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(729, 1, 1, 2, 1, '502abc_r', 'Обновленный', 'produ502ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(730, 1, 1, 2, 1, '503abc_r', 'Обновленный', 'produ503ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(731, 1, 1, 2, 1, '504abc_r', 'Обновленный', 'produ504ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(732, 1, 1, 2, 1, '505abc_r', 'Обновленный', 'produ505ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(733, 1, 1, 2, 1, '506abc_r', 'Обновленный', 'produ506ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(734, 1, 1, 2, 1, '507abc_r', 'Обновленный', 'produ507ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(735, 1, 1, 2, 1, '508abc_r', 'Обновленный', 'produ508ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(736, 1, 1, 2, 1, '509abc_r', 'Обновленный', 'produ509ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(737, 1, 1, 2, 1, '510abc_r', 'Обновленный', 'produ510ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(738, 1, 1, 2, 1, '511abc_r', 'Обновленный', 'produ511ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(739, 1, 1, 2, 1, '512abc_r', 'Обновленный', 'produ512ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(740, 1, 1, 2, 1, '513abc_r', 'Обновленный', 'produ513ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(741, 1, 1, 2, 1, '514abc_r', 'Обновленный', 'produ514ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(742, 1, 1, 2, 1, '515abc_r', 'Обновленный', 'produ515ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(743, 1, 1, 2, 1, '516abc_r', 'Обновленный', 'produ516ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(744, 1, 1, 2, 1, '517abc_r', 'Обновленный', 'produ517ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(745, 1, 1, 2, 1, '518abc_r', 'Обновленный', 'produ518ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(746, 1, 1, 2, 1, '519abc_r', 'Обновленный', 'produ519ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(747, 1, 1, 2, 1, '520abc_r', 'Обновленный', 'produ520ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(748, 1, 1, 2, 1, '521abc_r', 'Обновленный', 'produ521ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(749, 1, 1, 2, 1, '522abc_r', 'Обновленный', 'produ522ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(750, 1, 1, 2, 1, '523abc_r', 'Обновленный', 'produ523ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(751, 1, 1, 2, 1, '524abc_r', 'Обновленный', 'produ524ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(752, 1, 1, 2, 1, '525abc_r', 'Обновленный', 'produ525ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(753, 1, 1, 2, 1, '526abc_r', 'Обновленный', 'produ526ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(754, 1, 1, 2, 1, '527abc_r', 'Обновленный', 'produ527ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1);
INSERT INTO `product_item` (`id`, `category_id`, `delivery_id`, `page_id`, `brand_id`, `articul`, `title`, `uri`, `small_description`, `description`, `price`, `discount`, `count`, `preview`, `active`, `photo_album_id`, `create_at`, `update_at`, `master_id`) VALUES
(755, 1, 1, 2, 1, '528abc_r', 'Обновленный', 'produ528ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(756, 1, 1, 2, 1, '529abc_r', 'Обновленный', 'produ529ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(757, 1, 1, 2, 1, '530abc_r', 'Обновленный', 'produ530ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(758, 1, 1, 2, 1, '531abc_r', 'Обновленный', 'produ531ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(759, 1, 1, 2, 1, '532abc_r', 'Обновленный', 'produ532ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(760, 1, 1, 2, 1, '533abc_r', 'Обновленный', 'produ533ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(761, 1, 1, 2, 1, '534abc_r', 'Обновленный', 'produ534ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(762, 1, 1, 2, 1, '535abc_r', 'Обновленный', 'produ535ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(763, 1, 1, 2, 1, '536abc_r', 'Обновленный', 'produ536ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(764, 1, 1, 2, 1, '537abc_r', 'Обновленный', 'produ537ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(765, 1, 1, 2, 1, '538abc_r', 'Обновленный', 'produ538ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(766, 1, 1, 2, 1, '539abc_r', 'Обновленный', 'produ539ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(767, 1, 1, 2, 1, '540abc_r', 'Обновленный', 'produ540ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(768, 1, 1, 2, 1, '541abc_r', 'Обновленный', 'produ541ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(769, 1, 1, 2, 1, '542abc_r', 'Обновленный', 'produ542ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(770, 1, 1, 2, 1, '543abc_r', 'Обновленный', 'produ543ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(771, 1, 1, 2, 1, '544abc_r', 'Обновленный', 'produ544ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(772, 1, 1, 2, 1, '545abc_r', 'Обновленный', 'produ545ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(773, 1, 1, 2, 1, '546abc_r', 'Обновленный', 'produ546ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(774, 1, 1, 2, 1, '547abc_r', 'Обновленный', 'produ547ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(775, 1, 1, 2, 1, '548abc_r', 'Обновленный', 'produ548ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(776, 1, 1, 2, 1, '549abc_r', 'Обновленный', 'produ549ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(777, 1, 1, 2, 1, '550abc_r', 'Обновленный', 'produ550ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(778, 1, 1, 2, 1, '551abc_r', 'Обновленный', 'produ551ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(779, 1, 1, 2, 1, '552abc_r', 'Обновленный', 'produ552ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(780, 1, 1, 2, 1, '553abc_r', 'Обновленный', 'produ553ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(781, 1, 1, 2, 1, '554abc_r', 'Обновленный', 'produ554ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(782, 1, 1, 2, 1, '555abc_r', 'Обновленный', 'produ555ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(783, 1, 1, 2, 1, '556abc_r', 'Обновленный', 'produ556ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(784, 1, 1, 2, 1, '557abc_r', 'Обновленный', 'produ557ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(785, 1, 1, 2, 1, '558abc_r', 'Обновленный', 'produ558ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(786, 1, 1, 2, 1, '559abc_r', 'Обновленный', 'produ559ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(787, 1, 1, 2, 1, '560abc_r', 'Обновленный', 'produ560ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(788, 1, 1, 2, 1, '561abc_r', 'Обновленный', 'produ561ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(789, 1, 1, 2, 1, '562abc_r', 'Обновленный', 'produ562ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(790, 1, 1, 2, 1, '563abc_r', 'Обновленный', 'produ563ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(791, 1, 1, 2, 1, '564abc_r', 'Обновленный', 'produ564ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(792, 1, 1, 2, 1, '565abc_r', 'Обновленный', 'produ565ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(793, 1, 1, 2, 1, '566abc_r', 'Обновленный', 'produ566ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(794, 1, 1, 2, 1, '567abc_r', 'Обновленный', 'produ567ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(795, 1, 1, 2, 1, '568abc_r', 'Обновленный', 'produ568ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(796, 1, 1, 2, 1, '569abc_r', 'Обновленный', 'produ569ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(797, 1, 1, 2, 1, '570abc_r', 'Обновленный', 'produ570ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(798, 1, 1, 2, 1, '571abc_r', 'Обновленный', 'produ571ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(799, 1, 1, 2, 1, '572abc_r', 'Обновленный', 'produ572ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(800, 1, 1, 2, 1, '573abc_r', 'Обновленный', 'produ573ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(801, 1, 1, 2, 1, '574abc_r', 'Обновленный', 'produ574ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(802, 1, 1, 2, 1, '575abc_r', 'Обновленный', 'produ575ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(803, 1, 1, 2, 1, '576abc_r', 'Обновленный', 'produ576ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(804, 1, 1, 2, 1, '577abc_r', 'Обновленный', 'produ577ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(805, 1, 1, 2, 1, '578abc_r', 'Обновленный', 'produ578ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(806, 1, 1, 2, 1, '579abc_r', 'Обновленный', 'produ579ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(807, 1, 1, 2, 1, '580abc_r', 'Обновленный', 'produ580ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(808, 1, 1, 2, 1, '581abc_r', 'Обновленный', 'produ581ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(809, 1, 1, 2, 1, '582abc_r', 'Обновленный', 'produ582ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(810, 1, 1, 2, 1, '583abc_r', 'Обновленный', 'produ583ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(811, 1, 1, 2, 1, '584abc_r', 'Обновленный', 'produ584ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(812, 1, 1, 2, 1, '585abc_r', 'Обновленный', 'produ585ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(813, 1, 1, 2, 1, '586abc_r', 'Обновленный', 'produ586ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(814, 1, 1, 2, 1, '587abc_r', 'Обновленный', 'produ587ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(815, 1, 1, 2, 1, '588abc_r', 'Обновленный', 'produ588ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(816, 1, 1, 2, 1, '589abc_r', 'Обновленный', 'produ589ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(817, 1, 1, 2, 1, '590abc_r', 'Обновленный', 'produ590ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(818, 1, 1, 2, 1, '591abc_r', 'Обновленный', 'produ591ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(819, 1, 1, 2, 1, '592abc_r', 'Обновленный', 'produ592ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(820, 1, 1, 2, 1, '593abc_r', 'Обновленный', 'produ593ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(821, 1, 1, 2, 1, '594abc_r', 'Обновленный', 'produ594ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(822, 1, 1, 2, 1, '595abc_r', 'Обновленный', 'produ595ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(823, 1, 1, 2, 1, '596abc_r', 'Обновленный', 'produ596ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(824, 1, 1, 2, 1, '597abc_r', 'Обновленный', 'produ597ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(825, 1, 1, 2, 1, '598abc_r', 'Обновленный', 'produ598ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(826, 1, 1, 2, 1, '599abc_r', 'Обновленный', 'produ599ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(827, 1, 1, 2, 1, '600abc_r', 'Обновленный', 'produ600ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(828, 1, 1, 2, 1, '601abc_r', 'Обновленный', 'produ601ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(829, 1, 1, 2, 1, '602abc_r', 'Обновленный', 'produ602ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(830, 1, 1, 2, 1, '603abc_r', 'Обновленный', 'produ603ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(831, 1, 1, 2, 1, '604abc_r', 'Обновленный', 'produ604ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(832, 1, 1, 2, 1, '605abc_r', 'Обновленный', 'produ605ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(833, 1, 1, 2, 1, '606abc_r', 'Обновленный', 'produ606ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(834, 1, 1, 2, 1, '607abc_r', 'Обновленный', 'produ607ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(835, 1, 1, 2, 1, '608abc_r', 'Обновленный', 'produ608ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(836, 1, 1, 2, 1, '609abc_r', 'Обновленный', 'produ609ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(837, 1, 1, 2, 1, '610abc_r', 'Обновленный', 'produ610ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(838, 1, 1, 2, 1, '611abc_r', 'Обновленный', 'produ611ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(839, 1, 1, 2, 1, '612abc_r', 'Обновленный', 'produ612ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(840, 1, 1, 2, 1, '613abc_r', 'Обновленный', 'produ613ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(841, 1, 1, 2, 1, '614abc_r', 'Обновленный', 'produ614ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(842, 1, 1, 2, 1, '615abc_r', 'Обновленный', 'produ615ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(843, 1, 1, 2, 1, '616abc_r', 'Обновленный', 'produ616ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(844, 1, 1, 2, 1, '617abc_r', 'Обновленный', 'produ617ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(845, 1, 1, 2, 1, '618abc_r', 'Обновленный', 'produ618ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(846, 1, 1, 2, 1, '619abc_r', 'Обновленный', 'produ619ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(847, 1, 1, 2, 1, '620abc_r', 'Обновленный', 'produ620ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(848, 1, 1, 2, 1, '621abc_r', 'Обновленный', 'produ621ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(849, 1, 1, 2, 1, '622abc_r', 'Обновленный', 'produ622ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(850, 1, 1, 2, 1, '623abc_r', 'Обновленный', 'produ623ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(851, 1, 1, 2, 1, '624abc_r', 'Обновленный', 'produ624ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(852, 1, 1, 2, 1, '625abc_r', 'Обновленный', 'produ625ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(853, 1, 1, 2, 1, '626abc_r', 'Обновленный', 'produ626ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(854, 1, 1, 2, 1, '627abc_r', 'Обновленный', 'produ627ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(855, 1, 1, 2, 1, '628abc_r', 'Обновленный', 'produ628ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(856, 1, 1, 2, 1, '629abc_r', 'Обновленный', 'produ629ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(857, 1, 1, 2, 1, '630abc_r', 'Обновленный', 'produ630ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(858, 1, 1, 2, 1, '631abc_r', 'Обновленный', 'produ631ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(859, 1, 1, 2, 1, '632abc_r', 'Обновленный', 'produ632ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(860, 1, 1, 2, 1, '633abc_r', 'Обновленный', 'produ633ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(861, 1, 1, 2, 1, '634abc_r', 'Обновленный', 'produ634ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(862, 1, 1, 2, 1, '635abc_r', 'Обновленный', 'produ635ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(863, 1, 1, 2, 1, '636abc_r', 'Обновленный', 'produ636ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(864, 1, 1, 2, 1, '637abc_r', 'Обновленный', 'produ637ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(865, 1, 1, 2, 1, '638abc_r', 'Обновленный', 'produ638ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(866, 1, 1, 2, 1, '639abc_r', 'Обновленный', 'produ639ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(867, 1, 1, 2, 1, '640abc_r', 'Обновленный', 'produ640ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(868, 1, 1, 2, 1, '641abc_r', 'Обновленный', 'produ641ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(869, 1, 1, 2, 1, '642abc_r', 'Обновленный', 'produ642ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(870, 1, 1, 2, 1, '643abc_r', 'Обновленный', 'produ643ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(871, 1, 1, 2, 1, '644abc_r', 'Обновленный', 'produ644ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(872, 1, 1, 2, 1, '645abc_r', 'Обновленный', 'produ645ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(873, 1, 1, 2, 1, '646abc_r', 'Обновленный', 'produ646ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(874, 1, 1, 2, 1, '647abc_r', 'Обновленный', 'produ647ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(875, 1, 1, 2, 1, '648abc_r', 'Обновленный', 'produ648ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(876, 1, 1, 2, 1, '649abc_r', 'Обновленный', 'produ649ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(877, 1, 1, 2, 1, '650abc_r', 'Обновленный', 'produ650ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(878, 1, 1, 2, 1, '651abc_r', 'Обновленный', 'produ651ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(879, 1, 1, 2, 1, '652abc_r', 'Обновленный', 'produ652ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(880, 1, 1, 2, 1, '653abc_r', 'Обновленный', 'produ653ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(881, 1, 1, 2, 1, '654abc_r', 'Обновленный', 'produ654ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(882, 1, 1, 2, 1, '655abc_r', 'Обновленный', 'produ655ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(883, 1, 1, 2, 1, '656abc_r', 'Обновленный', 'produ656ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(884, 1, 1, 2, 1, '657abc_r', 'Обновленный', 'produ657ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(885, 1, 1, 2, 1, '658abc_r', 'Обновленный', 'produ658ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(886, 1, 1, 2, 1, '659abc_r', 'Обновленный', 'produ659ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(887, 1, 1, 2, 1, '660abc_r', 'Обновленный', 'produ660ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(888, 1, 1, 2, 1, '661abc_r', 'Обновленный', 'produ661ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(889, 1, 1, 2, 1, '662abc_r', 'Обновленный', 'produ662ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(890, 1, 1, 2, 1, '663abc_r', 'Обновленный', 'produ663ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(891, 1, 1, 2, 1, '664abc_r', 'Обновленный', 'produ664ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(892, 1, 1, 2, 1, '665abc_r', 'Обновленный', 'produ665ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(893, 1, 1, 2, 1, '666abc_r', 'Обновленный', 'produ666ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(894, 1, 1, 2, 1, '667abc_r', 'Обновленный', 'produ667ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(895, 1, 1, 2, 1, '668abc_r', 'Обновленный', 'produ668ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(896, 1, 1, 2, 1, '669abc_r', 'Обновленный', 'produ669ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(897, 1, 1, 2, 1, '670abc_r', 'Обновленный', 'produ670ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(898, 1, 1, 2, 1, '671abc_r', 'Обновленный', 'produ671ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(899, 1, 1, 2, 1, '672abc_r', 'Обновленный', 'produ672ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(900, 1, 1, 2, 1, '673abc_r', 'Обновленный', 'produ673ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(901, 1, 1, 2, 1, '674abc_r', 'Обновленный', 'produ674ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(902, 1, 1, 2, 1, '675abc_r', 'Обновленный', 'produ675ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(903, 1, 1, 2, 1, '676abc_r', 'Обновленный', 'produ676ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(904, 1, 1, 2, 1, '677abc_r', 'Обновленный', 'produ677ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(905, 1, 1, 2, 1, '678abc_r', 'Обновленный', 'produ678ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(906, 1, 1, 2, 1, '679abc_r', 'Обновленный', 'produ679ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(907, 1, 1, 2, 1, '680abc_r', 'Обновленный', 'produ680ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(908, 1, 1, 2, 1, '681abc_r', 'Обновленный', 'produ681ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(909, 1, 1, 2, 1, '682abc_r', 'Обновленный', 'produ682ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(910, 1, 1, 2, 1, '683abc_r', 'Обновленный', 'produ683ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(911, 1, 1, 2, 1, '684abc_r', 'Обновленный', 'produ684ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(912, 1, 1, 2, 1, '685abc_r', 'Обновленный', 'produ685ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(913, 1, 1, 2, 1, '686abc_r', 'Обновленный', 'produ686ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(914, 1, 1, 2, 1, '687abc_r', 'Обновленный', 'produ687ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(915, 1, 1, 2, 1, '688abc_r', 'Обновленный', 'produ688ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(916, 1, 1, 2, 1, '689abc_r', 'Обновленный', 'produ689ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(917, 1, 1, 2, 1, '690abc_r', 'Обновленный', 'produ690ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(918, 1, 1, 2, 1, '691abc_r', 'Обновленный', 'produ691ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(919, 1, 1, 2, 1, '692abc_r', 'Обновленный', 'produ692ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(920, 1, 1, 2, 1, '693abc_r', 'Обновленный', 'produ693ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(921, 1, 1, 2, 1, '694abc_r', 'Обновленный', 'produ694ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(922, 1, 1, 2, 1, '695abc_r', 'Обновленный', 'produ695ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(923, 1, 1, 2, 1, '696abc_r', 'Обновленный', 'produ696ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(924, 1, 1, 2, 1, '697abc_r', 'Обновленный', 'produ697ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(925, 1, 1, 2, 1, '698abc_r', 'Обновленный', 'produ698ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(926, 1, 1, 2, 1, '699abc_r', 'Обновленный', 'produ699ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(927, 1, 1, 2, 1, '700abc_r', 'Обновленный', 'produ700ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(928, 1, 1, 2, 1, '701abc_r', 'Обновленный', 'produ701ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(929, 1, 1, 2, 1, '702abc_r', 'Обновленный', 'produ702ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(930, 1, 1, 2, 1, '703abc_r', 'Обновленный', 'produ703ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(931, 1, 1, 2, 1, '704abc_r', 'Обновленный', 'produ704ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(932, 1, 1, 2, 1, '705abc_r', 'Обновленный', 'produ705ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(933, 1, 1, 2, 1, '706abc_r', 'Обновленный', 'produ706ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(934, 1, 1, 2, 1, '707abc_r', 'Обновленный', 'produ707ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(935, 1, 1, 2, 1, '708abc_r', 'Обновленный', 'produ708ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(936, 1, 1, 2, 1, '709abc_r', 'Обновленный', 'produ709ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(937, 1, 1, 2, 1, '710abc_r', 'Обновленный', 'produ710ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(938, 1, 1, 2, 1, '711abc_r', 'Обновленный', 'produ711ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(939, 1, 1, 2, 1, '712abc_r', 'Обновленный', 'produ712ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(940, 1, 1, 2, 1, '713abc_r', 'Обновленный', 'produ713ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(941, 1, 1, 2, 1, '714abc_r', 'Обновленный', 'produ714ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(942, 1, 1, 2, 1, '715abc_r', 'Обновленный', 'produ715ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(943, 1, 1, 2, 1, '716abc_r', 'Обновленный', 'produ716ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(944, 1, 1, 2, 1, '717abc_r', 'Обновленный', 'produ717ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(945, 1, 1, 2, 1, '718abc_r', 'Обновленный', 'produ718ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(946, 1, 1, 2, 1, '719abc_r', 'Обновленный', 'produ719ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(947, 1, 1, 2, 1, '720abc_r', 'Обновленный', 'produ720ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(948, 1, 1, 2, 1, '721abc_r', 'Обновленный', 'produ721ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(949, 1, 1, 2, 1, '722abc_r', 'Обновленный', 'produ722ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(950, 1, 1, 2, 1, '723abc_r', 'Обновленный', 'produ723ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(951, 1, 1, 2, 1, '724abc_r', 'Обновленный', 'produ724ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(952, 1, 1, 2, 1, '725abc_r', 'Обновленный', 'produ725ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(953, 1, 1, 2, 1, '726abc_r', 'Обновленный', 'produ726ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(954, 1, 1, 2, 1, '727abc_r', 'Обновленный', 'produ727ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(955, 1, 1, 2, 1, '728abc_r', 'Обновленный', 'produ728ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(956, 1, 1, 2, 1, '729abc_r', 'Обновленный', 'produ729ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(957, 1, 1, 2, 1, '730abc_r', 'Обновленный', 'produ730ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(958, 1, 1, 2, 1, '731abc_r', 'Обновленный', 'produ731ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(959, 1, 1, 2, 1, '732abc_r', 'Обновленный', 'produ732ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(960, 1, 1, 2, 1, '733abc_r', 'Обновленный', 'produ733ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(961, 1, 1, 2, 1, '734abc_r', 'Обновленный', 'produ734ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(962, 1, 1, 2, 1, '735abc_r', 'Обновленный', 'produ735ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(963, 1, 1, 2, 1, '736abc_r', 'Обновленный', 'produ736ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(964, 1, 1, 2, 1, '737abc_r', 'Обновленный', 'produ737ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(965, 1, 1, 2, 1, '738abc_r', 'Обновленный', 'produ738ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(966, 1, 1, 2, 1, '739abc_r', 'Обновленный', 'produ739ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(967, 1, 1, 2, 1, '740abc_r', 'Обновленный', 'produ740ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(968, 1, 1, 2, 1, '741abc_r', 'Обновленный', 'produ741ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(969, 1, 1, 2, 1, '742abc_r', 'Обновленный', 'produ742ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(970, 1, 1, 2, 1, '743abc_r', 'Обновленный', 'produ743ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(971, 1, 1, 2, 1, '744abc_r', 'Обновленный', 'produ744ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(972, 1, 1, 2, 1, '745abc_r', 'Обновленный', 'produ745ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(973, 1, 1, 2, 1, '746abc_r', 'Обновленный', 'produ746ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(974, 1, 1, 2, 1, '747abc_r', 'Обновленный', 'produ747ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(975, 1, 1, 2, 1, '748abc_r', 'Обновленный', 'produ748ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(976, 1, 1, 2, 1, '749abc_r', 'Обновленный', 'produ749ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(977, 1, 1, 2, 1, '750abc_r', 'Обновленный', 'produ750ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(978, 1, 1, 2, 1, '751abc_r', 'Обновленный', 'produ751ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(979, 1, 1, 2, 1, '752abc_r', 'Обновленный', 'produ752ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(980, 1, 1, 2, 1, '753abc_r', 'Обновленный', 'produ753ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(981, 1, 1, 2, 1, '754abc_r', 'Обновленный', 'produ754ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(982, 1, 1, 2, 1, '755abc_r', 'Обновленный', 'produ755ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(983, 1, 1, 2, 1, '756abc_r', 'Обновленный', 'produ756ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(984, 1, 1, 2, 1, '757abc_r', 'Обновленный', 'produ757ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(985, 1, 1, 2, 1, '758abc_r', 'Обновленный', 'produ758ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(986, 1, 1, 2, 1, '759abc_r', 'Обновленный', 'produ759ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(987, 1, 1, 2, 1, '760abc_r', 'Обновленный', 'produ760ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(988, 1, 1, 2, 1, '761abc_r', 'Обновленный', 'produ761ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(989, 1, 1, 2, 1, '762abc_r', 'Обновленный', 'produ762ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(990, 1, 1, 2, 1, '763abc_r', 'Обновленный', 'produ763ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(991, 1, 1, 2, 1, '764abc_r', 'Обновленный', 'produ764ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(992, 1, 1, 2, 1, '765abc_r', 'Обновленный', 'produ765ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(993, 1, 1, 2, 1, '766abc_r', 'Обновленный', 'produ766ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(994, 1, 1, 2, 1, '767abc_r', 'Обновленный', 'produ767ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(995, 1, 1, 2, 1, '768abc_r', 'Обновленный', 'produ768ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(996, 1, 1, 2, 1, '769abc_r', 'Обновленный', 'produ769ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(997, 1, 1, 2, 1, '770abc_r', 'Обновленный', 'produ770ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(998, 1, 1, 2, 1, '771abc_r', 'Обновленный', 'produ771ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(999, 1, 1, 2, 1, '772abc_r', 'Обновленный', 'produ772ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1000, 1, 1, 2, 1, '773abc_r', 'Обновленный', 'produ773ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1001, 1, 1, 2, 1, '774abc_r', 'Обновленный', 'produ774ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1002, 1, 1, 2, 1, '775abc_r', 'Обновленный', 'produ775ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1003, 1, 1, 2, 1, '776abc_r', 'Обновленный', 'produ776ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1004, 1, 1, 2, 1, '777abc_r', 'Обновленный', 'produ777ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1005, 1, 1, 2, 1, '778abc_r', 'Обновленный', 'produ778ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1006, 1, 1, 2, 1, '779abc_r', 'Обновленный', 'produ779ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1007, 1, 1, 2, 1, '780abc_r', 'Обновленный', 'produ780ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1008, 1, 1, 2, 1, '781abc_r', 'Обновленный', 'produ781ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1009, 1, 1, 2, 1, '782abc_r', 'Обновленный', 'produ782ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1010, 1, 1, 2, 1, '783abc_r', 'Обновленный', 'produ783ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1011, 1, 1, 2, 1, '784abc_r', 'Обновленный', 'produ784ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1012, 1, 1, 2, 1, '785abc_r', 'Обновленный', 'produ785ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1013, 1, 1, 2, 1, '786abc_r', 'Обновленный', 'produ786ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1014, 1, 1, 2, 1, '787abc_r', 'Обновленный', 'produ787ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1015, 1, 1, 2, 1, '788abc_r', 'Обновленный', 'produ788ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1016, 1, 1, 2, 1, '789abc_r', 'Обновленный', 'produ789ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1017, 1, 1, 2, 1, '790abc_r', 'Обновленный', 'produ790ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1018, 1, 1, 2, 1, '791abc_r', 'Обновленный', 'produ791ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1019, 1, 1, 2, 1, '792abc_r', 'Обновленный', 'produ792ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1020, 1, 1, 2, 1, '793abc_r', 'Обновленный', 'produ793ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1021, 1, 1, 2, 1, '794abc_r', 'Обновленный', 'produ794ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1022, 1, 1, 2, 1, '795abc_r', 'Обновленный', 'produ795ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1023, 1, 1, 2, 1, '796abc_r', 'Обновленный', 'produ796ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1024, 1, 1, 2, 1, '797abc_r', 'Обновленный', 'produ797ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1025, 1, 1, 2, 1, '798abc_r', 'Обновленный', 'produ798ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1026, 1, 1, 2, 1, '799abc_r', 'Обновленный', 'produ799ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1027, 1, 1, 2, 1, '800abc_r', 'Обновленный', 'produ800ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1028, 1, 1, 2, 1, '801abc_r', 'Обновленный', 'produ801ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1029, 1, 1, 2, 1, '802abc_r', 'Обновленный', 'produ802ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1030, 1, 1, 2, 1, '803abc_r', 'Обновленный', 'produ803ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1031, 1, 1, 2, 1, '804abc_r', 'Обновленный', 'produ804ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1032, 1, 1, 2, 1, '805abc_r', 'Обновленный', 'produ805ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1033, 1, 1, 2, 1, '806abc_r', 'Обновленный', 'produ806ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1034, 1, 1, 2, 1, '807abc_r', 'Обновленный', 'produ807ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1035, 1, 1, 2, 1, '808abc_r', 'Обновленный', 'produ808ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1036, 1, 1, 2, 1, '809abc_r', 'Обновленный', 'produ809ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1037, 1, 1, 2, 1, '810abc_r', 'Обновленный', 'produ810ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1038, 1, 1, 2, 1, '811abc_r', 'Обновленный', 'produ811ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1039, 1, 1, 2, 1, '812abc_r', 'Обновленный', 'produ812ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1040, 1, 1, 2, 1, '813abc_r', 'Обновленный', 'produ813ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1041, 1, 1, 2, 1, '814abc_r', 'Обновленный', 'produ814ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1042, 1, 1, 2, 1, '815abc_r', 'Обновленный', 'produ815ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1043, 1, 1, 2, 1, '816abc_r', 'Обновленный', 'produ816ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1044, 1, 1, 2, 1, '817abc_r', 'Обновленный', 'produ817ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1045, 1, 1, 2, 1, '818abc_r', 'Обновленный', 'produ818ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1046, 1, 1, 2, 1, '819abc_r', 'Обновленный', 'produ819ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1047, 1, 1, 2, 1, '820abc_r', 'Обновленный', 'produ820ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1048, 1, 1, 2, 1, '821abc_r', 'Обновленный', 'produ821ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1049, 1, 1, 2, 1, '822abc_r', 'Обновленный', 'produ822ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1050, 1, 1, 2, 1, '823abc_r', 'Обновленный', 'produ823ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1051, 1, 1, 2, 1, '824abc_r', 'Обновленный', 'produ824ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1052, 1, 1, 2, 1, '825abc_r', 'Обновленный', 'produ825ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1053, 1, 1, 2, 1, '826abc_r', 'Обновленный', 'produ826ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1054, 1, 1, 2, 1, '827abc_r', 'Обновленный', 'produ827ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1055, 1, 1, 2, 1, '828abc_r', 'Обновленный', 'produ828ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1056, 1, 1, 2, 1, '829abc_r', 'Обновленный', 'produ829ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1057, 1, 1, 2, 1, '830abc_r', 'Обновленный', 'produ830ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1058, 1, 1, 2, 1, '831abc_r', 'Обновленный', 'produ831ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1059, 1, 1, 2, 1, '832abc_r', 'Обновленный', 'produ832ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1060, 1, 1, 2, 1, '833abc_r', 'Обновленный', 'produ833ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1061, 1, 1, 2, 1, '834abc_r', 'Обновленный', 'produ834ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1062, 1, 1, 2, 1, '835abc_r', 'Обновленный', 'produ835ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1063, 1, 1, 2, 1, '836abc_r', 'Обновленный', 'produ836ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1064, 1, 1, 2, 1, '837abc_r', 'Обновленный', 'produ837ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1065, 1, 1, 2, 1, '838abc_r', 'Обновленный', 'produ838ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1066, 1, 1, 2, 1, '839abc_r', 'Обновленный', 'produ839ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1);
INSERT INTO `product_item` (`id`, `category_id`, `delivery_id`, `page_id`, `brand_id`, `articul`, `title`, `uri`, `small_description`, `description`, `price`, `discount`, `count`, `preview`, `active`, `photo_album_id`, `create_at`, `update_at`, `master_id`) VALUES
(1067, 1, 1, 2, 1, '840abc_r', 'Обновленный', 'produ840ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1068, 1, 1, 2, 1, '841abc_r', 'Обновленный', 'produ841ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1069, 1, 1, 2, 1, '842abc_r', 'Обновленный', 'produ842ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1070, 1, 1, 2, 1, '843abc_r', 'Обновленный', 'produ843ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1071, 1, 1, 2, 1, '844abc_r', 'Обновленный', 'produ844ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1072, 1, 1, 2, 1, '845abc_r', 'Обновленный', 'produ845ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1073, 1, 1, 2, 1, '846abc_r', 'Обновленный', 'produ846ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1074, 1, 1, 2, 1, '847abc_r', 'Обновленный', 'produ847ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1075, 1, 1, 2, 1, '848abc_r', 'Обновленный', 'produ848ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1076, 1, 1, 2, 1, '849abc_r', 'Обновленный', 'produ849ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1077, 1, 1, 2, 1, '850abc_r', 'Обновленный', 'produ850ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1078, 1, 1, 2, 1, '851abc_r', 'Обновленный', 'produ851ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1079, 1, 1, 2, 1, '852abc_r', 'Обновленный', 'produ852ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1080, 1, 1, 2, 1, '853abc_r', 'Обновленный', 'produ853ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1081, 1, 1, 2, 1, '854abc_r', 'Обновленный', 'produ854ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1082, 1, 1, 2, 1, '855abc_r', 'Обновленный', 'produ855ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1083, 1, 1, 2, 1, '856abc_r', 'Обновленный', 'produ856ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1084, 1, 1, 2, 1, '857abc_r', 'Обновленный', 'produ857ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1085, 1, 1, 2, 1, '858abc_r', 'Обновленный', 'produ858ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1086, 1, 1, 2, 1, '859abc_r', 'Обновленный', 'produ859ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1087, 1, 1, 2, 1, '860abc_r', 'Обновленный', 'produ860ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1088, 1, 1, 2, 1, '861abc_r', 'Обновленный', 'produ861ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1089, 1, 1, 2, 1, '862abc_r', 'Обновленный', 'produ862ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1090, 1, 1, 2, 1, '863abc_r', 'Обновленный', 'produ863ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1091, 1, 1, 2, 1, '864abc_r', 'Обновленный', 'produ864ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1092, 1, 1, 2, 1, '865abc_r', 'Обновленный', 'produ865ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1093, 1, 1, 2, 1, '866abc_r', 'Обновленный', 'produ866ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1094, 1, 1, 2, 1, '867abc_r', 'Обновленный', 'produ867ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1095, 1, 1, 2, 1, '868abc_r', 'Обновленный', 'produ868ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1096, 1, 1, 2, 1, '869abc_r', 'Обновленный', 'produ869ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1097, 1, 1, 2, 1, '870abc_r', 'Обновленный', 'produ870ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1098, 1, 1, 2, 1, '871abc_r', 'Обновленный', 'produ871ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1099, 1, 1, 2, 1, '872abc_r', 'Обновленный', 'produ872ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1100, 1, 1, 2, 1, '873abc_r', 'Обновленный', 'produ873ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1101, 1, 1, 2, 1, '874abc_r', 'Обновленный', 'produ874ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1102, 1, 1, 2, 1, '875abc_r', 'Обновленный', 'produ875ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1103, 1, 1, 2, 1, '876abc_r', 'Обновленный', 'produ876ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1104, 1, 1, 2, 1, '877abc_r', 'Обновленный', 'produ877ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1105, 1, 1, 2, 1, '878abc_r', 'Обновленный', 'produ878ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1106, 1, 1, 2, 1, '879abc_r', 'Обновленный', 'produ879ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1107, 1, 1, 2, 1, '880abc_r', 'Обновленный', 'produ880ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1108, 1, 1, 2, 1, '881abc_r', 'Обновленный', 'produ881ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1109, 1, 1, 2, 1, '882abc_r', 'Обновленный', 'produ882ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1110, 1, 1, 2, 1, '883abc_r', 'Обновленный', 'produ883ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1111, 1, 1, 2, 1, '884abc_r', 'Обновленный', 'produ884ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1112, 1, 1, 2, 1, '885abc_r', 'Обновленный', 'produ885ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1113, 1, 1, 2, 1, '886abc_r', 'Обновленный', 'produ886ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1114, 1, 1, 2, 1, '887abc_r', 'Обновленный', 'produ887ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1115, 1, 1, 2, 1, '888abc_r', 'Обновленный', 'produ888ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1116, 1, 1, 2, 1, '889abc_r', 'Обновленный', 'produ889ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1117, 1, 1, 2, 1, '890abc_r', 'Обновленный', 'produ890ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1118, 1, 1, 2, 1, '891abc_r', 'Обновленный', 'produ891ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1119, 1, 1, 2, 1, '892abc_r', 'Обновленный', 'produ892ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1120, 1, 1, 2, 1, '893abc_r', 'Обновленный', 'produ893ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1121, 1, 1, 2, 1, '894abc_r', 'Обновленный', 'produ894ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1122, 1, 1, 2, 1, '895abc_r', 'Обновленный', 'produ895ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1123, 1, 1, 2, 1, '896abc_r', 'Обновленный', 'produ896ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1124, 1, 1, 2, 1, '897abc_r', 'Обновленный', 'produ897ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1125, 1, 1, 2, 1, '898abc_r', 'Обновленный', 'produ898ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1126, 1, 1, 2, 1, '899abc_r', 'Обновленный', 'produ899ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1127, 1, 1, 2, 1, '900abc_r', 'Обновленный', 'produ900ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1128, 1, 1, 2, 1, '901abc_r', 'Обновленный', 'produ901ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1129, 1, 1, 2, 1, '902abc_r', 'Обновленный', 'produ902ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1130, 1, 1, 2, 1, '903abc_r', 'Обновленный', 'produ903ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1131, 1, 1, 2, 1, '904abc_r', 'Обновленный', 'produ904ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1132, 1, 1, 2, 1, '905abc_r', 'Обновленный', 'produ905ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1133, 1, 1, 2, 1, '906abc_r', 'Обновленный', 'produ906ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1134, 1, 1, 2, 1, '907abc_r', 'Обновленный', 'produ907ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1135, 1, 1, 2, 1, '908abc_r', 'Обновленный', 'produ908ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1136, 1, 1, 2, 1, '909abc_r', 'Обновленный', 'produ909ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1137, 1, 1, 2, 1, '910abc_r', 'Обновленный', 'produ910ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1138, 1, 1, 2, 1, '911abc_r', 'Обновленный', 'produ911ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1139, 1, 1, 2, 1, '912abc_r', 'Обновленный', 'produ912ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1140, 1, 1, 2, 1, '913abc_r', 'Обновленный', 'produ913ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1141, 1, 1, 2, 1, '914abc_r', 'Обновленный', 'produ914ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1142, 1, 1, 2, 1, '915abc_r', 'Обновленный', 'produ915ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1143, 1, 1, 2, 1, '916abc_r', 'Обновленный', 'produ916ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1144, 1, 1, 2, 1, '917abc_r', 'Обновленный', 'produ917ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1145, 1, 1, 2, 1, '918abc_r', 'Обновленный', 'produ918ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1146, 1, 1, 2, 1, '919abc_r', 'Обновленный', 'produ919ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1147, 1, 1, 2, 1, '920abc_r', 'Обновленный', 'produ920ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1148, 1, 1, 2, 1, '921abc_r', 'Обновленный', 'produ921ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1149, 1, 1, 2, 1, '922abc_r', 'Обновленный', 'produ922ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1150, 1, 1, 2, 1, '923abc_r', 'Обновленный', 'produ923ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1151, 1, 1, 2, 1, '924abc_r', 'Обновленный', 'produ924ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1152, 1, 1, 2, 1, '925abc_r', 'Обновленный', 'produ925ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1153, 1, 1, 2, 1, '926abc_r', 'Обновленный', 'produ926ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1154, 1, 1, 2, 1, '927abc_r', 'Обновленный', 'produ927ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1155, 1, 1, 2, 1, '928abc_r', 'Обновленный', 'produ928ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1156, 1, 1, 2, 1, '929abc_r', 'Обновленный', 'produ929ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1157, 1, 1, 2, 1, '930abc_r', 'Обновленный', 'produ930ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1158, 1, 1, 2, 1, '931abc_r', 'Обновленный', 'produ931ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1159, 1, 1, 2, 1, '932abc_r', 'Обновленный', 'produ932ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1160, 1, 1, 2, 1, '933abc_r', 'Обновленный', 'produ933ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1161, 1, 1, 2, 1, '934abc_r', 'Обновленный', 'produ934ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1162, 1, 1, 2, 1, '935abc_r', 'Обновленный', 'produ935ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1163, 1, 1, 2, 1, '936abc_r', 'Обновленный', 'produ936ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1164, 1, 1, 2, 1, '937abc_r', 'Обновленный', 'produ937ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1165, 1, 1, 2, 1, '938abc_r', 'Обновленный', 'produ938ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1166, 1, 1, 2, 1, '939abc_r', 'Обновленный', 'produ939ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1167, 1, 1, 2, 1, '940abc_r', 'Обновленный', 'produ940ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1168, 1, 1, 2, 1, '941abc_r', 'Обновленный', 'produ941ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1169, 1, 1, 2, 1, '942abc_r', 'Обновленный', 'produ942ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1170, 1, 1, 2, 1, '943abc_r', 'Обновленный', 'produ943ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1171, 1, 1, 2, 1, '944abc_r', 'Обновленный', 'produ944ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1172, 1, 1, 2, 1, '945abc_r', 'Обновленный', 'produ945ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1173, 1, 1, 2, 1, '946abc_r', 'Обновленный', 'produ946ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1174, 1, 1, 2, 1, '947abc_r', 'Обновленный', 'produ947ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1175, 1, 1, 2, 1, '948abc_r', 'Обновленный', 'produ948ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1176, 1, 1, 2, 1, '949abc_r', 'Обновленный', 'produ949ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1177, 1, 1, 2, 1, '950abc_r', 'Обновленный', 'produ950ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1178, 1, 1, 2, 1, '951abc_r', 'Обновленный', 'produ951ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1179, 1, 1, 2, 1, '952abc_r', 'Обновленный', 'produ952ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1180, 1, 1, 2, 1, '953abc_r', 'Обновленный', 'produ953ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1181, 1, 1, 2, 1, '954abc_r', 'Обновленный', 'produ954ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1182, 1, 1, 2, 1, '955abc_r', 'Обновленный', 'produ955ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1183, 1, 1, 2, 1, '956abc_r', 'Обновленный', 'produ956ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1184, 1, 1, 2, 1, '957abc_r', 'Обновленный', 'produ957ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1185, 1, 1, 2, 1, '958abc_r', 'Обновленный', 'produ958ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1186, 1, 1, 2, 1, '959abc_r', 'Обновленный', 'produ959ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1187, 1, 1, 2, 1, '960abc_r', 'Обновленный', 'produ960ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1188, 1, 1, 2, 1, '961abc_r', 'Обновленный', 'produ961ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1189, 1, 1, 2, 1, '962abc_r', 'Обновленный', 'produ962ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1190, 1, 1, 2, 1, '963abc_r', 'Обновленный', 'produ963ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1191, 1, 1, 2, 1, '964abc_r', 'Обновленный', 'produ964ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1192, 1, 1, 2, 1, '965abc_r', 'Обновленный', 'produ965ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1193, 1, 1, 2, 1, '966abc_r', 'Обновленный', 'produ966ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1194, 1, 1, 2, 1, '967abc_r', 'Обновленный', 'produ967ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1195, 1, 1, 2, 1, '968abc_r', 'Обновленный', 'produ968ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1196, 1, 1, 2, 1, '969abc_r', 'Обновленный', 'produ969ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1197, 1, 1, 2, 1, '970abc_r', 'Обновленный', 'produ970ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1198, 1, 1, 2, 1, '971abc_r', 'Обновленный', 'produ971ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1199, 1, 1, 2, 1, '972abc_r', 'Обновленный', 'produ972ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1200, 1, 1, 2, 1, '973abc_r', 'Обновленный', 'produ973ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1201, 1, 1, 2, 1, '974abc_r', 'Обновленный', 'produ974ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1202, 1, 1, 2, 1, '975abc_r', 'Обновленный', 'produ975ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1203, 1, 1, 2, 1, '976abc_r', 'Обновленный', 'produ976ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1204, 1, 1, 2, 1, '977abc_r', 'Обновленный', 'produ977ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1205, 1, 1, 2, 1, '978abc_r', 'Обновленный', 'produ978ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1206, 1, 1, 2, 1, '979abc_r', 'Обновленный', 'produ979ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1207, 1, 1, 2, 1, '980abc_r', 'Обновленный', 'produ980ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1208, 1, 1, 2, 1, '981abc_r', 'Обновленный', 'produ981ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1209, 1, 1, 2, 1, '982abc_r', 'Обновленный', 'produ982ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1210, 1, 1, 2, 1, '983abc_r', 'Обновленный', 'produ983ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1211, 1, 1, 2, 1, '984abc_r', 'Обновленный', 'produ984ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1212, 1, 1, 2, 1, '985abc_r', 'Обновленный', 'produ985ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1213, 1, 1, 2, 1, '986abc_r', 'Обновленный', 'produ986ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1214, 1, 1, 2, 1, '987abc_r', 'Обновленный', 'produ987ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1215, 1, 1, 2, 1, '988abc_r', 'Обновленный', 'produ988ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1216, 1, 1, 2, 1, '989abc_r', 'Обновленный', 'produ989ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1217, 1, 1, 2, 1, '990abc_r', 'Обновленный', 'produ990ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1218, 1, 1, 2, 1, '991abc_r', 'Обновленный', 'produ991ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1219, 1, 1, 2, 1, '992abc_r', 'Обновленный', 'produ992ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1220, 1, 1, 2, 1, '993abc_r', 'Обновленный', 'produ993ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1221, 1, 1, 2, 1, '994abc_r', 'Обновленный', 'produ994ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1222, 1, 1, 2, 1, '995abc_r', 'Обновленный', 'produ995ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1223, 1, 1, 2, 1, '996abc_r', 'Обновленный', 'produ996ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1224, 1, 1, 2, 1, '997abc_r', 'Обновленный', 'produ997ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1225, 1, 1, 2, 1, '998abc_r', 'Обновленный', 'produ998ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1226, 1, 1, 2, 1, '999abc_r', 'Обновленный', 'produ999ct1-p1r', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 1, 1, 1470319687, 1470319687, 1),
(1227, 1, 1, 2, 1, 'abc_rqwerty', 'Обновленный', 'product1-p1rqwerty', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470382548, 1470382548, 1),
(1228, 1, 1, 2, 1, 'zzz_qw', 'test 1', 'test-1qw', 'test', 'test', 3, 0, 3, 'box.jpg', 0, 1, 1470382634, 1470382634, 7),
(1229, 1, 1, 2, 1, 'abc_ras', 'Обновленный', 'product1-p1ras', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470382733, 1470382733, 1),
(1230, 1, 1, 2, 1, 'abc_rйц', 'Обновленный', 'product1-p1rwe', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470383155, 1470383155, 1),
(1231, 1, 1, 2, 1, 'abc_raa', 'Обновленный', 'product1-p1raa', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470383305, 1470383305, 1),
(1232, 1, 1, 2, 1, 'abc_raas', 'Обновленный', 'product1-p1raas', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470383629, 1470383629, 1),
(1233, 1, 1, 2, 1, 'abc_rqqq', 'Обновленный', 'product1-p1rqqq', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470383740, 1470383740, 1),
(1234, 1, 1, 2, 1, 'abc_rewrwe', 'Обновленный', 'product1-p1rwerrew', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470383995, 1470383995, 1),
(1235, 1, 1, 2, 1, 'abc_rzxc', 'Обновленный', 'product1-p1rzxc', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470384072, 1470384072, 1),
(1236, 1, 1, 2, 1, 'abc_rsdfsdf', 'Обновленный', 'product1-p1rsdfsdf', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470384148, 1470384148, 1),
(1237, 1, 1, 2, 1, 'abc_rdsgfdfg', 'Обновленный', 'product1-p1rdxhfghdfdf', 'маленькое описание', 'полное описание', 10, 1, 21, 'box.jpg', 0, 1, 1470386663, 1470386663, 1),
(1238, 1, 1, 2, 1, 'abc_rs', 'Обновленный', 'product1-p1rs', 'маленькое описание', 'полное описание', 10, 1, 21, '', 0, 1, 1472738565, 1472738565, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_links_order_item`
--

CREATE TABLE IF NOT EXISTS `product_links_order_item` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
  `id` int(11) NOT NULL,
  `order_number` int(11) NOT NULL,
  `to` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `type_pay_id` int(11) NOT NULL,
  `type_delivery_id` int(11) NOT NULL,
  `sum` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `product_order_type_delivery`
--

CREATE TABLE IF NOT EXISTS `product_order_type_delivery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `product_order_type_pay`
--

CREATE TABLE IF NOT EXISTS `product_order_type_pay` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `active` smallint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `region_group`
--

CREATE TABLE IF NOT EXISTS `region_group` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `region` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `region_group`
--

INSERT INTO `region_group` (`id`, `group_id`, `region`) VALUES
(1, 1, 'Chernihiv'),
(2, 1, 'Sums''ka Oblast'),
(3, 2, 'Kiev'),
(5, 3, 'Harkiv'),
(6, 2, 'Vinnica'),
(11, 1, 'Connecticut');

-- --------------------------------------------------------

--
-- Структура таблицы `userid_group`
--

CREATE TABLE IF NOT EXISTS `userid_group` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `userid_group`
--

INSERT INTO `userid_group` (`id`, `user_id`, `group_id`) VALUES
(1, 20, 5),
(2, 2, 5),
(3, 3, 5),
(5, 1, 6),
(6, 17, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL,
  `surname` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `patronymic` varchar(30) DEFAULT NULL,
  `date_birth` int(11) DEFAULT NULL,
  `sex` enum('w','m','n') DEFAULT 'n',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `surname`, `name`, `patronymic`, `date_birth`, `sex`, `user_id`) VALUES
(1, 'Бублик', 'Евгений', 'Владимирович', 705848400, 'm', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `active` smallint(1) DEFAULT '0',
  `display_order` int(11) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `update_at` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`, `active`, `display_order`, `created_at`, `update_at`) VALUES
(1, 'administrator', 1, 10, 1469004414, 1469004414),
(2, 'user', 1, 20, 1469004414, 1469004414);

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles_access_admin`
--

CREATE TABLE IF NOT EXISTS `user_roles_access_admin` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_roles_access_admin`
--

INSERT INTO `user_roles_access_admin` (`id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_socials`
--

CREATE TABLE IF NOT EXISTS `user_socials` (
  `id` int(11) NOT NULL,
  `provider` varchar(100) NOT NULL,
  `client_id` varchar(100) NOT NULL,
  `created_at` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_users`
--

CREATE TABLE IF NOT EXISTS `user_users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password_hash` varchar(100) NOT NULL,
  `reset_password_token` int(11) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `confirm_email_at` int(11) DEFAULT NULL,
  `register_ip` varchar(15) NOT NULL,
  `created_at` int(11) NOT NULL,
  `update_at` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `login_with_social` smallint(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_users`
--

INSERT INTO `user_users` (`id`, `email`, `username`, `password_hash`, `reset_password_token`, `auth_key`, `blocked_at`, `confirm_email_at`, `register_ip`, `created_at`, `update_at`, `role_id`, `login_with_social`) VALUES
(1, 'jeka.deadline@gmail.com', 'admin', '$2y$13$0v4I4dJUR2punXhJB5Ie.OLRbja5hW10tb3miHTnJ5n3oTIvWrxf6', NULL, '3a762344e2fcf4054a93f5708f3a8bdb', NULL, 1469004415, '127.0.0.1', 1469004415, 1469004415, 1, 0),
(2, 'user@user.com', 'user', '$2y$13$BaFYbnU0cCG.Dcgx8o1GoeE5QREtoUXy86Pk/FXDShLBTggygud..', NULL, '1470667858_7eYthoPyYDiRPaK0y4g7D', NULL, NULL, '127.0.0.1', 1470667859, 1470667859, 2, 0),
(3, 'user2@user.com', 'user2', '$2y$13$vnpPp9gnTxQYfYbN1NhvNOj3PdqdNiqAwPcfuvho/841a4OsMyxQS', NULL, '1474959462_ecvi5yB1pu-SOd473SzaT', NULL, NULL, '127.0.0.1', 1474959462, 1474959462, 2, 0),
(4, 'user3@user.com', 'user3', '$2y$13$63phvI.wYoSzwUAiZzChqOloSzN9zzzescqdkmXFEoxT1PUUS9iZu', NULL, '1474959668_OSR4J78_jHgauYi41gJHZ', NULL, NULL, '127.0.0.1', 1474959668, 1474959668, 2, 0),
(5, 'qw@qw.df', 'qwerty', '$2y$13$Z6JeXYlNWC8l54FV9H37d.OhSoflk4LJad2O7zxKVCxKjCK/wZbrO', NULL, '1474959813_l0721Z3H8gQis4hiJgcJL', NULL, NULL, '127.0.0.1', 1474959813, 1474959813, 2, 0),
(6, 'asd@sadf.df', 'asd', '$2y$13$hKNfYzRMJEf0WtQyAoqSOusm4nZrq8PnTyFv1GfUYZFY/SbtkUlpa', NULL, '1474959964_eAizc8JV64yIBHvU6gGNw', NULL, NULL, '127.0.0.1', 1474959964, 1474959964, 2, 0),
(7, 'dfgh@dr.rt', 'fgh', '$2y$13$UofnhcpTZUtUjR9PukeSR.9tPwKQsHK9WIP6QNqrS4DAu5nD7xAnS', NULL, '1474960303_JUxdt9pbJ1xMvTnbyHLi0', NULL, NULL, '127.0.0.1', 1474960320, 1474960320, 2, 0),
(8, 'rty@ert.ert', 'rtyrty', '$2y$13$UOAa/f.OCFEWYvbAPtbWyORrQPojk2Nfw.DU.mzP6jHm2nfZSMfb.', NULL, '1474960812_rM9afKi9NttRHkOmnLmSf', NULL, NULL, '127.0.0.1', 1474960812, 1474960812, 2, 0),
(9, 'xgfh@we.we', 'fhg', '$2y$13$qdCVlPViBWdcoIJd4BEpJuvXABH1QcG2EAO2DPlHuVCKSDbLsmbwa', NULL, '1474960948_AvAZA97x1UKJ4Ya2ErakR', NULL, NULL, '127.0.0.1', 1474960948, 1474960948, 2, 0),
(10, 'sfd@df.df', 'xdfg', '$2y$13$7Wnw9cG3fgoobVPyaQ5GCeRH67Tafoi/cN98RxEM4QplEclRe2mGe', NULL, '1474961025_27-AJiMGozyY9t5bVHAM4', NULL, NULL, '127.0.0.1', 1474961025, 1474961025, 2, 0),
(11, 'sdfg@df.df', 'sdgf', '$2y$13$stxaBVs2slgbsUTk5YrUSeozjH5EDq29haHFrHrgZqtnViOcO/BvC', NULL, '1474961149_XgppaAGfkSE6E8CGho1e7', NULL, NULL, '127.0.0.1', 1474961149, 1474961149, 2, 0),
(12, 'sdf@d.df', 'ыкап', '$2y$13$7OPeNVigH1/suCYThzZ9negs6gogRvGAcMlOciJx2CE2c/s1gJ0RO', NULL, '1474961351_xnd0Aah2fcLY1e6N0-xms', NULL, NULL, '127.0.0.1', 1474961351, 1474961351, 2, 0),
(13, 'sdf@df.fg', 'sf', '$2y$13$YOIROdn.uFvczv0Thubt1uOxquVuuXrEwUw1o21x5PhgRifU7nbqa', NULL, '1474961447_wHTZN388P32HtJsw-13-4', NULL, NULL, '127.0.0.1', 1474961447, 1474961447, 2, 0),
(14, 'hgf@er.rt', 'fghgh', '$2y$13$l.S7Op53mrcIK2TSdLgZOO/LKkJPkVHV4CH3zsfeOjx0dfpnb022i', NULL, '1474961651_W9OG4PMdPHWo3n44n3w39', NULL, NULL, '127.0.0.1', 1474961651, 1474961651, 2, 0),
(15, 'fgh@rt.tr', 'fghfg', '$2y$13$RhmMgAiAqHUw4ZAK36YZ/e5zmiBTg4MQ0nOHbvNdTm0mbeAOdTzce', NULL, '1474961713_SWilqRhaS_-RTA-CGpXjp', NULL, NULL, '127.0.0.1', 1474961713, 1474961713, 2, 0),
(16, 'dfg@er.ty', 'dfg', '$2y$13$n4k7.zRZeX9QekyFreVEW.tTSh.Hzo09AaWEAHuVot7unGzkQ0NRi', NULL, '1474961769_JTADWMvfbFaNRXFq2u73q', NULL, NULL, '127.0.0.1', 1474961769, 1474961769, 2, 0),
(17, 'fg@fg.gf', ';kl', '$2y$13$BE1xyGNFomaWwYYATKPuG.600T.zc2GWLeOBnjfoQalwrt.6KAi0S', NULL, '1474961859_ANU1YKjbwBfwkkQwK6Sn2', NULL, NULL, '127.0.0.1', 1474961859, 1474961859, 2, 0),
(18, 'gfh@fd.fgh', 'fhgf', '$2y$13$Niim.r72T7GcYzRihWiEheJaJT.EA2KYdu4NKDYf77do6Y1XChZ0e', NULL, '1474961897_nYHhlxjPxqAL6dBTevmHt', NULL, NULL, '127.0.0.1', 1474961897, 1474961897, 2, 0),
(19, 'fgh@er.er', 'fghfgg', '$2y$13$xfeER9Ex2pp5Q0d77rJE7.5wBwPcUE.w6VaDop7N5fBVsA4MBp0NS', NULL, '1474962000_pPPv6onMZI-zRf7Q0fiUV', NULL, NULL, '127.0.0.1', 1474962000, 1474962000, 2, 0),
(20, 'super@user.com', 'superuser', '$2y$13$GFSO7CeTJ3njNOScS0mmRum07fWWHTlj/vl3wuf4e0TypRMomeTou', NULL, '1474962283_EfLPqyxZ8qPwbVsoDj94A', NULL, NULL, '127.0.0.1', 1474962283, 1474962283, 2, 0),
(21, 'as@sd.df', 'ываыв', '$2y$13$RQg4oDwi/D9LXaGDt0ghtOVqNuFCDa1Ys/TRuff4QJIDiVBy5.cFq', NULL, '1474962566_fkZitWbMW0z9acO8dJfJd', NULL, NULL, '127.0.0.1', 1474962566, 1474962566, 2, 0),
(22, 'dfg@fg.gh', 'dfghdfg', '$2y$13$PRAruymKjpdBlr5h9frJCOaZa9iLoUC1GMETPvTJEO7DzQhOFIe9C', NULL, '1474962628_BDBppZ2lLpdeRvtCrhT2c', NULL, NULL, '127.0.0.1', 1474962628, 1474962628, 2, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `advertising_bannerplaces`
--
ALTER TABLE `advertising_bannerplaces`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_advertising_bannerplaces_code` (`code`);

--
-- Индексы таблицы `advertising_banners`
--
ALTER TABLE `advertising_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_advertising_banners_bannerplace_id` (`bannerplace_id`);

--
-- Индексы таблицы `core_attachments`
--
ALTER TABLE `core_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_contacts`
--
ALTER TABLE `core_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_menu_pages`
--
ALTER TABLE `core_menu_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_core_menu_pages_page_id` (`page_id`),
  ADD KEY `idx_core_menu_pages_type_id` (`type_id`);

--
-- Индексы таблицы `core_pages`
--
ALTER TABLE `core_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_core_pages_uri` (`uri`),
  ADD KEY `idx_core_pages_template_id` (`template_id`);

--
-- Индексы таблицы `core_pages_meta`
--
ALTER TABLE `core_pages_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_core_pages_meta_page_id` (`page_id`),
  ADD KEY `IDX_core_pages_meta_owner_id` (`owner_id`),
  ADD KEY `IDX_core_pages_meta_model` (`model`);

--
-- Индексы таблицы `core_social_links`
--
ALTER TABLE `core_social_links`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_templates`
--
ALTER TABLE `core_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `core_text_block`
--
ALTER TABLE `core_text_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_core_text_block_uri` (`uri`);

--
-- Индексы таблицы `core_type_menu`
--
ALTER TABLE `core_type_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_core_type_menu_code` (`code`);

--
-- Индексы таблицы `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_coupons_code` (`code`);

--
-- Индексы таблицы `group_name`
--
ALTER TABLE `group_name`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_group_name_name` (`name`),
  ADD KEY `FK_group_name_group_type_id` (`type`);

--
-- Индексы таблицы `group_type`
--
ALTER TABLE `group_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_group_type_name` (`name`);

--
-- Индексы таблицы `links_news_category`
--
ALTER TABLE `links_news_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_links_news_category_news_id` (`news_id`),
  ADD KEY `idx_links_news_category_category_id` (`category_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_news_category_uri` (`uri`),
  ADD KEY `idx_news_category_page_id` (`page_id`);

--
-- Индексы таблицы `news_news`
--
ALTER TABLE `news_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_news_news_uri` (`uri`),
  ADD KEY `idx_news_news_user_id` (`user_id`),
  ADD KEY `idx_news_news_page_id` (`page_id`);

--
-- Индексы таблицы `photo_album`
--
ALTER TABLE `photo_album`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `photo_image`
--
ALTER TABLE `photo_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_photo_image_album_id` (`album_id`);

--
-- Индексы таблицы `price_book`
--
ALTER TABLE `price_book`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_price_book` (`product_id`,`group_id`),
  ADD KEY `FK_price_book_group_name_id` (`group_id`);

--
-- Индексы таблицы `product_brand`
--
ALTER TABLE `product_brand`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_product_category_uri` (`uri`),
  ADD KEY `idx_product_category_page_id` (`page_id`);

--
-- Индексы таблицы `product_delivery`
--
ALTER TABLE `product_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_item`
--
ALTER TABLE `product_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_product_itemy_uri` (`uri`),
  ADD UNIQUE KEY `idx_product_item_articul` (`articul`),
  ADD KEY `idx_product_item_page_id` (`page_id`),
  ADD KEY `idx_product_item_photo_album_id` (`photo_album_id`),
  ADD KEY `idx_product_item_category_id` (`category_id`),
  ADD KEY `idx_product_item_delivery_id` (`delivery_id`),
  ADD KEY `idx_product_item_brand_id` (`brand_id`),
  ADD KEY `FK_product_item_master_id` (`master_id`);

--
-- Индексы таблицы `product_links_order_item`
--
ALTER TABLE `product_links_order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_links_order_item_item_id` (`item_id`),
  ADD KEY `idx_product_links_order_item_order_id` (`order_id`);

--
-- Индексы таблицы `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_product_order_type_pay_id` (`type_pay_id`),
  ADD KEY `idx_product_order_type_delivery_id` (`type_delivery_id`);

--
-- Индексы таблицы `product_order_type_delivery`
--
ALTER TABLE `product_order_type_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_order_type_pay`
--
ALTER TABLE `product_order_type_pay`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `region_group`
--
ALTER TABLE `region_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_region_group_group_name_id` (`group_id`);

--
-- Индексы таблицы `userid_group`
--
ALTER TABLE `userid_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_userid_group_group_name_id` (`group_id`),
  ADD KEY `FK_userid_group_user_users_id` (`user_id`);

--
-- Индексы таблицы `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_profiles_user_id` (`user_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_user_roles_name` (`name`);

--
-- Индексы таблицы `user_roles_access_admin`
--
ALTER TABLE `user_roles_access_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_roles_access_admin_role_id` (`role_id`);

--
-- Индексы таблицы `user_socials`
--
ALTER TABLE `user_socials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_socials_user_id` (`user_id`);

--
-- Индексы таблицы `user_users`
--
ALTER TABLE `user_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_user_users_username` (`username`),
  ADD UNIQUE KEY `uix_user_users_email` (`email`),
  ADD KEY `idx_user_users_role_id` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `advertising_bannerplaces`
--
ALTER TABLE `advertising_bannerplaces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `advertising_banners`
--
ALTER TABLE `advertising_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_attachments`
--
ALTER TABLE `core_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_contacts`
--
ALTER TABLE `core_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_menu_pages`
--
ALTER TABLE `core_menu_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_pages`
--
ALTER TABLE `core_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `core_pages_meta`
--
ALTER TABLE `core_pages_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1770;
--
-- AUTO_INCREMENT для таблицы `core_social_links`
--
ALTER TABLE `core_social_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_templates`
--
ALTER TABLE `core_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `core_text_block`
--
ALTER TABLE `core_text_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `core_type_menu`
--
ALTER TABLE `core_type_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `group_name`
--
ALTER TABLE `group_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `group_type`
--
ALTER TABLE `group_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `links_news_category`
--
ALTER TABLE `links_news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `news_news`
--
ALTER TABLE `news_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `photo_album`
--
ALTER TABLE `photo_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `photo_image`
--
ALTER TABLE `photo_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `price_book`
--
ALTER TABLE `price_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `product_brand`
--
ALTER TABLE `product_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `product_delivery`
--
ALTER TABLE `product_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `product_item`
--
ALTER TABLE `product_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1239;
--
-- AUTO_INCREMENT для таблицы `product_links_order_item`
--
ALTER TABLE `product_links_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_order_type_delivery`
--
ALTER TABLE `product_order_type_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_order_type_pay`
--
ALTER TABLE `product_order_type_pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `region_group`
--
ALTER TABLE `region_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `userid_group`
--
ALTER TABLE `userid_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user_roles_access_admin`
--
ALTER TABLE `user_roles_access_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user_socials`
--
ALTER TABLE `user_socials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_users`
--
ALTER TABLE `user_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `advertising_banners`
--
ALTER TABLE `advertising_banners`
  ADD CONSTRAINT `fk_advertising_banners_bannerplace_id` FOREIGN KEY (`bannerplace_id`) REFERENCES `advertising_bannerplaces` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `core_menu_pages`
--
ALTER TABLE `core_menu_pages`
  ADD CONSTRAINT `fk_core_menu_pages_page_id` FOREIGN KEY (`page_id`) REFERENCES `core_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_core_menu_pages_type_id` FOREIGN KEY (`type_id`) REFERENCES `core_type_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `core_pages`
--
ALTER TABLE `core_pages`
  ADD CONSTRAINT `fk_core_pages_template_id` FOREIGN KEY (`template_id`) REFERENCES `core_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `core_pages_meta`
--
ALTER TABLE `core_pages_meta`
  ADD CONSTRAINT `fk_core_pages_meta_page_id` FOREIGN KEY (`page_id`) REFERENCES `core_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `group_name`
--
ALTER TABLE `group_name`
  ADD CONSTRAINT `FK_group_name_group_type_id` FOREIGN KEY (`type`) REFERENCES `group_type` (`id`);

--
-- Ограничения внешнего ключа таблицы `links_news_category`
--
ALTER TABLE `links_news_category`
  ADD CONSTRAINT `fk_links_news_category_category_id` FOREIGN KEY (`category_id`) REFERENCES `news_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_links_news_category_news_id` FOREIGN KEY (`news_id`) REFERENCES `news_news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `news_category`
--
ALTER TABLE `news_category`
  ADD CONSTRAINT `fk_news_category_page_id` FOREIGN KEY (`page_id`) REFERENCES `core_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `news_news`
--
ALTER TABLE `news_news`
  ADD CONSTRAINT `fk_news_news_page_id` FOREIGN KEY (`page_id`) REFERENCES `core_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `photo_image`
--
ALTER TABLE `photo_image`
  ADD CONSTRAINT `fk_photo_image_album_id` FOREIGN KEY (`album_id`) REFERENCES `photo_album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `price_book`
--
ALTER TABLE `price_book`
  ADD CONSTRAINT `FK_price_book_group_name_id` FOREIGN KEY (`group_id`) REFERENCES `group_name` (`id`),
  ADD CONSTRAINT `FK_price_book_product_item_id` FOREIGN KEY (`product_id`) REFERENCES `product_item` (`id`);

--
-- Ограничения внешнего ключа таблицы `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `fk_product_category_page_id` FOREIGN KEY (`page_id`) REFERENCES `core_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_item`
--
ALTER TABLE `product_item`
  ADD CONSTRAINT `fk_product_item_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `product_brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_item_category_id` FOREIGN KEY (`category_id`) REFERENCES `product_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_item_delivery_id` FOREIGN KEY (`delivery_id`) REFERENCES `product_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_product_item_master_id` FOREIGN KEY (`master_id`) REFERENCES `product_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_item_page_id` FOREIGN KEY (`page_id`) REFERENCES `core_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_item_photo_album_id` FOREIGN KEY (`photo_album_id`) REFERENCES `photo_album` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_links_order_item`
--
ALTER TABLE `product_links_order_item`
  ADD CONSTRAINT `fk_product_links_order_item_item_id` FOREIGN KEY (`item_id`) REFERENCES `product_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_links_order_item_order_id` FOREIGN KEY (`order_id`) REFERENCES `product_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `fk_product_order_type_delivery_id` FOREIGN KEY (`type_delivery_id`) REFERENCES `product_order_type_delivery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_product_order_type_pay_id` FOREIGN KEY (`type_pay_id`) REFERENCES `product_order_type_pay` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `region_group`
--
ALTER TABLE `region_group`
  ADD CONSTRAINT `FK_region_group_group_name_id` FOREIGN KEY (`group_id`) REFERENCES `group_name` (`id`);

--
-- Ограничения внешнего ключа таблицы `userid_group`
--
ALTER TABLE `userid_group`
  ADD CONSTRAINT `FK_userid_group_group_name_id` FOREIGN KEY (`group_id`) REFERENCES `group_name` (`id`),
  ADD CONSTRAINT `FK_userid_group_user_users_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD CONSTRAINT `fk_user_profiles_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_roles_access_admin`
--
ALTER TABLE `user_roles_access_admin`
  ADD CONSTRAINT `fk_user_roles_access_admin_role_id` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_socials`
--
ALTER TABLE `user_socials`
  ADD CONSTRAINT `fk_user_socials_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_users`
--
ALTER TABLE `user_users`
  ADD CONSTRAINT `fk_user_users_role_id` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
